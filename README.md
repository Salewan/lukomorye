# lukomorye

1. Postgresql **v9.4 (можно выше)** должен быть установлен.
2. Сделать роль `hayday` с паролем `hayday`; создать БД `hayday`, владелец `hayday`. 

Например:
```
> sudo su postgres
> psql
> create role hayday with LOGIN CREATEDB CREATEROLE PASSWORD 'hayday';
> create database hayday owner hayday;
```

3. Установить скрипт для запуска sbt, https://github.com/paulp/sbt-extras

4. Запуск дев-сервера: `sbt playRun`