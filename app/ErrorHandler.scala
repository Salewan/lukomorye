import javax.inject._

import hibernate.HibernateSession
import play.api._
import play.api.cache.CacheApi
import play.api.http.DefaultHttpErrorHandler
import play.api.mvc.Results._
import play.api.mvc._
import play.api.routing.Router

import scala.concurrent._

@Singleton
class ErrorHandler @Inject() (
                               env: Environment,
                               config: Configuration,
                               sourceMapper: OptionalSourceMapper,
                               router: Provider[Router],
                               cache: CacheApi
                             ) extends DefaultHttpErrorHandler(env, config, sourceMapper, router) {

  private val logger = Logger(classOf[ErrorHandler])

  private def closeHibernateSession(request: RequestHeader): Unit = {
    val requestId = request.id.toString
    logger.debug(s"Closing hibernate session #$requestId without commit due to some exception")
    cache.get[HibernateSession](requestId).foreach(session => session.close(commit = false, cache.remove(requestId)))
  }

  override protected def onNotFound(request: RequestHeader, message: String): Future[Result] = Future.successful {
    closeHibernateSession(request)
    Ok(views.html.common.notFoundPage()(request))
  }

  override protected def onProdServerError(request: RequestHeader, exception: UsefulException): Future[Result] =
    Future.successful {
      closeHibernateSession(request)
      Ok(views.html.common.internalErrorPage()(request))
    }

  override protected def onOtherClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] =
    Future.successful {
      closeHibernateSession(request)
      Logger.error(s"Other client error statusCode=$statusCode, message=$message")
      Ok(views.html.common.internalErrorPage()(request))
    }

  override protected def onDevServerError(request: RequestHeader, exception: UsefulException): Future[Result] = {
    closeHibernateSession(request)
    super.onDevServerError(request, exception)
  }
}
