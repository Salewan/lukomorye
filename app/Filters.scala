import javax.inject.Inject

import hibernate.HibernateFilter
import http.UserLockFilter
import play.api.http.HttpFilters

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
class Filters @Inject() (hib: HibernateFilter, lock: UserLockFilter) extends HttpFilters {

  val filters = Seq(hib, lock)
}
