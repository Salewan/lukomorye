package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 26.01.2017.
  */
class AbstractMessage extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Author: User = _

  @BeanProperty
  protected var Text: String = _

  @BeanProperty
  protected var Deleted: java.lang.Boolean = false

  @BeanProperty
  protected var Moderator: java.lang.Boolean = false

  // Getters
  def creationTime: Long = CreationTime.getTime
  def author: User = Author
  def text: String = Text
  def deleted: Boolean = Deleted
  def moderator: Boolean = Moderator

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def author(arg: User) = update {Author = arg}
  def text(arg: String) = update {Text = arg}
  def deleted(arg: Boolean) = update {Deleted = arg}
  def moderator(arg: Boolean) = update {Moderator = arg}
}
