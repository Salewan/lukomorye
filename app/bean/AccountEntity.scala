package bean

import components.{HibernateSessionAware, UserEvidence}
import model.price.{CoinPrice, DustPrice, Price, PriceContext, RubyPrice}
import model.{CoinDeficit, Deficit, DreamDustDeficit, EmptyDeficit, RubyDeficit, TransactionType}

import scala.language.implicitConversions

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
trait AccountEntity extends Bean with PriceContext {

  implicit def toHS(hib: HibernateSessionAware) = hib.hibernateSession
  implicit def toSession(hib: HibernateSessionAware) = hib.hibernateSession.unwrap

  def profile(implicit hs: HibernateSessionAware): Profile = hs.hibernateSession.get(classOf[Profile], id).get

  def user(implicit hs: HibernateSessionAware): User = hs.hibernateSession.get(classOf[User], id).get

  def gameUser(implicit hs: HibernateSessionAware): GameUser = hs.hibernateSession.get(classOf[GameUser], id).get

  def avatar(implicit hs: HibernateSessionAware): Avatar = hs.hibernateSession.get(classOf[Avatar], id).get

  def money(implicit hs: HibernateSessionAware): UserMoney = hs.hibernateSession.get(classOf[UserMoney], id).get

  def tutorial(implicit hs: HibernateSessionAware): Tutorial = {
    if (id == 0) new Tutorial
    else hs.hibernateSession.get(classOf[Tutorial], id).getOrElse {
      val t = new Tutorial
      t.dust(200)
      hs.hibernateSession.persist(t.id(id))
      t
    }
  }

  def settings(implicit hs: HibernateSessionAware): Settings = {
    if (id == 0) new Settings
    else hs.hibernateSession.get(classOf[Settings], id).getOrElse {
        val s = new Settings
        hs.hibernateSession.persist(s.id(id))
        s
      }
  }

  def transform(implicit hs: HibernateSessionAware): Transform = {
    if (id == 0) new Transform
    else hs.hibernateSession.get(classOf[Transform], id).getOrElse {
      val s = new Transform
      hs.hibernateSession.persist(s.id(id))
      s
    }
  }

  def canBuy(price: Price)(implicit ctx: Context): Boolean = {
    val userMoney = money
    val need = price.value
    price match {
      case _: CoinPrice => userMoney.coins >= need
      case _: RubyPrice => userMoney.rubies >= need
      case _: DustPrice => userMoney.dreamDust >= need
      case unknown => throw new RuntimeException(s"Unknown Price class ${unknown.toString}")
    }
  }

  def buy(price: Price, tr: TransactionType)(implicit ctx: Context): Boolean = {
    if (canBuy(price)) {
      val need = price.value
      val userMoney = money
      price match {
        case _: CoinPrice => userMoney.addCoins(-need, tr)
        case _: RubyPrice => userMoney.addRubies(-need, tr)
        case _: DustPrice => userMoney.addDreamDust(-need, tr)
        case unknown => throw new RuntimeException(s"Unknown Price class ${unknown.toString}")
      }
      true
    }
    else false
  }

  def deficit(price: Price)(implicit ctx: Context): Deficit = {
    val need = price.value
    if (canBuy(price)) EmptyDeficit
    else price match {
      case _: CoinPrice => CoinDeficit(need - money.coins)
      case _: RubyPrice => RubyDeficit(need - money.rubies)
      case _: DustPrice => DreamDustDeficit(need - money.dreamDust)
      case unknown => throw new RuntimeException(s"Unknown Price class ${unknown.toString}")
    }
  }

  def rubyRest(price: Price)(implicit ctx: Context): Long = {
    deficit(price) match {
      case CoinDeficit(x) => math.round(x / 100.0) max 1
      case RubyDeficit(x) => x
      case _ => 0L
    }
  }

  def buyWithBothCurrencies(price: Price, tr1: TransactionType, tr2: TransactionType)
                           (implicit ctx: Context): Boolean = {
    if (buy(price, tr1)) true
    else if (price.isInstanceOf[CoinPrice]) {
      val rest = rubyRest(price)
      val money = this.money
      if (money.rubies >= rest) {
        money.addRubies(-rest, tr2)
        money.addCoins(-money.coins, tr1)
        true
      } else false
    } else false
  }

  def restDeficit(price: Price)(implicit ctx: Context): Deficit = {
    val rest = rubyRest(price)
    if (rest == 0) EmptyDeficit
    else RubyDeficit(rest - money.rubies)
  }

  def isMe(implicit ctx: UserEvidence) = id == ctx.userId

}
