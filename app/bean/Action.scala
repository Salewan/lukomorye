package bean

import java.util.Date

import model.action.ActionType

import scala.beans.BeanProperty

/**
  * Created by Salewan on 29.06.2017.
  */
class Action extends Bean {

  @BeanProperty
  protected var Name: String = _

  @BeanProperty
  protected var TypeId: java.lang.Integer = _

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Author: User = _

  @BeanProperty
  protected var Queued: java.lang.Boolean = _

  // Getters
  def name: String = Name
  def typeId: Int = TypeId
  def creationTime: Long = CreationTime.getTime
  def author: User = Author
  def queued: Boolean = Queued

  // Setters
  def name(arg: String) = update {Name = arg}
  def typeId(arg: Int) = update {TypeId = arg}
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def author(arg: User) = update {Author = arg}
  def queued(arg: Boolean) = update {Queued = arg}

  def model: ActionType = ActionType.withType(typeId)
}

object Action {
  def apply(): Action = new Action
  def apply(action: Action): Action = new Action().name(action.name).typeId(action.typeId).
    creationTime(action.creationTime).author(action.author).queued(true)
}
