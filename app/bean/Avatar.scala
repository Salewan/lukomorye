package bean

import components.HibernateSessionAware
import util.WebFormsUtil

import scala.beans.BeanProperty

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
class Avatar extends AccountEntity {

  @BeanProperty
  protected var Nick: String = _
  def nick(arg: String) = { Nick = arg; this }
  def nick = Option(Nick)

  @BeanProperty
  protected var VisualNick: String = _
  def visualNick(arg: String) = { VisualNick = arg; this}
  def visualNick = Option(VisualNick).getOrElse("Гость")

  @BeanProperty
  protected var Sex: java.lang.Character = _
  def sex(arg: Char) = { Sex = arg; this }
  def sex = Sex

  @BeanProperty
  protected var ExileBan: java.lang.Boolean = false
  def exileBan: Boolean = ExileBan
  def exileBan(arg: Boolean) = update {ExileBan = arg}

  def isAnonymous = nick.isEmpty

  def image: String = if (sex == 'f') "female.png" else "male.png"
  def bigImage: String = if (sex == 'f') "woman.png" else "man.png"

  def isFemale = sex == 'f'

  def ifFemale(ftext: String, mtext: String): String = if (isFemale) ftext else mtext

  def icon: String = ""

  def isSpetialUser: Boolean = nick.exists(config.SPETIAL_USERS.contains)

  def updateNick(nick: String): Avatar = visualNick(WebFormsUtil.visualNickOf(nick)).nick(WebFormsUtil.nickOf(nick))

  def nickCssClass(implicit hib: HibernateSessionAware): String = {
    val user = this.user
    if (user.isAdminOrSupporter) " admin"
    else if (user.isModerator) " moderator"
    else if (user.isBanned) " banned"
    else " user"
  }

  def textCssClass(implicit hib: HibernateSessionAware): String = {
    val user = this.user
    if (user.isAdmin) " admin"
    else if (user.isSupporter) " support"
    else if (user.isModerator) " moderator"
    else if (user.isBanned) " banned"
    else " "
  }
}
