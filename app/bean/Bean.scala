package bean

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.11.2016.
  */
trait Bean extends BeanTemplate {

  @BeanProperty
  protected var Id: java.lang.Integer = _


  def id: Int = Option(Id) map (Int.unbox(_)) getOrElse 0

  def id(arg: Int) = update {Id = arg}
}
