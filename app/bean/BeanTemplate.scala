package bean

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
trait BeanTemplate {

  type T = this.type

  val now = System.currentTimeMillis()

  protected def update(expr: => Unit): T = {
    expr
    this
  }
}
