package bean

import model.buff.BuffType

import scala.beans.BeanProperty

/**
  * Created by Salewan on 28.06.2017.
  */
class Buff extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var BuffTypeId: java.lang.Integer = _

  @BeanProperty
  protected var Till: java.util.Date = _

  // getters
  def owner: GameUser = Owner
  def buffTypeId: Int = BuffTypeId
  def buffType: BuffType = BuffType.withType(buffTypeId)
  def till: Option[Long] = Option(Till).map(_.getTime)

  // setters
  def owner(arg: GameUser) = update {Owner = arg}
  def buffTypeId(arg: Int) = update {BuffTypeId = arg}
  def buffType(arg: BuffType) = update {BuffTypeId = arg.typeId}
  def till(arg: Option[Long]) = update {Till = arg.map(new java.util.Date(_)).orNull}

  def canBuy: Boolean = till.isEmpty || till.exists(till => till < now || till - now < buffType.maximumDuration)

  def isActive: Boolean = till.exists(_ > now)

  def millis: Long = till.filter(_ => isActive).map(_ - now).getOrElse(0L)

  def prolong(): Unit = till(Some(math.max(now, till.getOrElse(now)) + buffType.prolongStep))
}

object Buff {
  def apply(btype: BuffType, user: GameUser): Buff = apply(btype.typeId, user)

  def apply(btype: Int, user: GameUser): Buff = new Buff().buffTypeId(btype).owner(user).till(None)
}
