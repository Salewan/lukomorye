package bean

import model.price.CoinPrice

/**
  * @author Sergey Lebedev (salewan@gmail.com) 20.03.2017.
  */
class BuySlot extends ShopSlot {

  def price: CoinPrice = CoinPrice(value)
}
