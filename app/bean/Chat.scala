package bean

import java.util.Date

import bean.state.ClanRole
import com.google.common.collect.Sets
import components.HibernateSessionAware
import hibernate.HibernateSetAdapter
import util.Implicits._

import scala.beans.BeanProperty
import scala.language.postfixOps

class Chat extends Bean {

  @BeanProperty
  protected var Messages: java.util.Set[ChatMessage] = _

  @BeanProperty
  protected var LastCleanTime: Date = _

  @BeanProperty
  protected var LastMessageTime: Date = _

  lazy val messages = new HibernateSetAdapter[ChatMessage]({
    if (Messages == null) Messages = Sets.newLinkedHashSet()
    Messages
  })

  // Getters
  def lastCleanTime: Option[Long] = Option(LastCleanTime).map(_.getTime)
  def lastMessageTime: Option[Long] = Option(LastMessageTime).map(_.getTime)

  // Setters
  def lastCleanTime(arg: Option[Long]): T = update {LastCleanTime = arg.map(new Date(_)).orNull}
  def lastMessageTime(arg: Option[Long]): T = update {LastMessageTime = arg.map(new Date(_)).orNull}

  def canModerate(user: GameUser): Boolean = user.clanRole == ClanRole.LEADER || user.clanRole == ClanRole.preLeader
}

object Chat {
  val CHAT_CLEAN_INTERVAL: Long = 1 minute
  val CHAT_MESSAGE_LIVE_TIME: Long = 12 hours

  implicit class ChatOps(self: Chat)(implicit hib: HibernateSessionAware) {

    def addMessage(message: ChatMessage): Unit = {
      cleanMessages()
      self.messages += message
      self.lastMessageTime(Some(message.creationTime))
    }

    private def cleanMessages(): Unit = {
      if (self.lastCleanTime.isEmpty || self.lastCleanTime.exists(self.now - _ > CHAT_CLEAN_INTERVAL)) {
        self.lastCleanTime(Some(self.now))
        var count = 0
        val iterator = self.messages.underlyingCollection.iterator()
        while (iterator.hasNext) {
          val message = iterator.next()
          count = count + 1
          if (self.now - message.creationTime > CHAT_MESSAGE_LIVE_TIME || count > 120) {
            iterator.remove()
            hib.hibernateSession.delete(message)
          }

        }
        hib.hibernateSession.flush()
      }
    }
  }
}
