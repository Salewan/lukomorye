package bean

import bean.state.ClanRole
import components.HibernateSessionAware

class ChatMessage extends AbstractMessage {

  def canDelete(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = {
    author.id == user.id ||
      user.clanRole == ClanRole.LEADER ||
      (user.clanRole == ClanRole.preLeader && author.gameUser.clanRole != ClanRole.LEADER)
  }
}
