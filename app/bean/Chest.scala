package bean

import java.util.Date

import model.mission.MissionGenerator

import scala.beans.BeanProperty
import util.Implicits._

import scala.language.postfixOps

/**
  * Created by Salewan on 10.08.2017.
  */
class Chest extends Bean {

  @BeanProperty
  protected var ExpireTime: Date = _

  @BeanProperty
  protected var MissionGeneratorId: java.lang.Integer = _

  def expireTime: Long = ExpireTime.getTime

  def expireTime(arg: Long) = update {ExpireTime = new Date(arg)}

  def isExpired: Boolean = expireTime < now

  def missionGeneratorId(arg: Option[Int]) = update {MissionGeneratorId = arg.map(Int.box).orNull}
  def missionGeneratorId: Option[Int] = Option(MissionGeneratorId).map(Int.unbox)

  def missionGenerator: Option[MissionGenerator] = missionGeneratorId.flatMap(MissionGenerator.find)
}

object Chest {
  val DEFAULT_LIFETIME: Long = 1 day

  def apply(missionGeneratorId: Int): Chest =
    new Chest().missionGeneratorId(Some(missionGeneratorId)).expireTime(System.currentTimeMillis() + DEFAULT_LIFETIME)
}
