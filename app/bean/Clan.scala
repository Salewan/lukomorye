package bean

import java.util.Date

import bean.messages.forum.Folder
import bean.state.{ClanHistoryRecordType, ClanJoinResult, ClanRole}
import com.google.common.collect.{Maps, Sets}
import components.HibernateSessionAware
import dao.{BillingDAO, ClanDAO}
import hibernate.{HibernateMapAdapter, HibernateSession, HibernateSetAdapter}
import model.clan.{ClanBonus, ClanBonusRegistry}
import model.{ReputationModel, TransactionType}
import model.price.RubyPrice
import util.{ExpMoneyUnits, TimeUtil, WeekCounters}
import util.Implicits._

import scala.language.postfixOps
import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 09.11.2016.
  */
class Clan extends Bean {

  @BeanProperty
  protected var Name: String = _

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Creator: GameUser = _

  @BeanProperty
  protected var Members: java.util.Set[GameUser] = _

  @BeanProperty
  protected var History: java.util.Set[ClanHistoryRecord] = _

  @BeanProperty
  protected var Offers: java.util.Set[Offer] = _

  @BeanProperty
  protected var RequestsDisabled: java.lang.Boolean = _

  @BeanProperty
  protected var Folders: java.util.Set[Folder] = _

  @BeanProperty
  protected var Ignored: java.util.Set[Avatar] = _

  @BeanProperty
  protected var MembersCountUpgrade: java.lang.Integer = _

  @BeanProperty
  protected var AllianceType: java.lang.Integer = _

  @BeanProperty
  protected var AllianceChangeTime: Date = _

  @BeanProperty
  protected var Level: java.lang.Integer = 1

  @BeanProperty
  protected var Experiences: java.util.Map[Int, Long] = _

  @BeanProperty
  protected var MemberRubies: java.util.Map[Int, Long] = _

  @BeanProperty
  protected var ExpCurrent: java.lang.Long = _

  @BeanProperty
  protected var ReputationValues: java.util.Map[Int, Long] = _

  @BeanProperty
  protected var RubyMineCollectTime: java.lang.Long = _

  @BeanProperty
  protected var RubyMineLevel: java.lang.Integer = _

  @BeanProperty
  protected var LastOffersViewDate: Date = _

  @BeanProperty
  protected var Chat: bean.Chat = _

  @BeanProperty
  protected var Bonuses: java.util.Map[Int, Int] = _

  @BeanProperty
  protected var ExtraStamps: java.lang.Integer = _

  @BeanProperty
  protected var BonusRedistTime: Date = _

  @BeanProperty
  protected var NoticeLastTime: Date = _

  protected var MailingLastTime: Date = _

  lazy val bonuses = new HibernateMapAdapter[Int, Int]({
    if (Bonuses == null) Bonuses = Maps.newLinkedHashMap()
    Bonuses
  })

  lazy val reputationValues = new HibernateMapAdapter[Int, Long]({
    if (ReputationValues == null) ReputationValues = Maps.newLinkedHashMap()
    ReputationValues
  })

  lazy val experiences = new HibernateMapAdapter[Int, Long]({
    if (Experiences == null) Experiences = Maps.newLinkedHashMap()
    Experiences
  })

  lazy val memberRubies = new HibernateMapAdapter[Int, Long]({
    if (MemberRubies == null) MemberRubies = Maps.newLinkedHashMap()
    MemberRubies
  })

  lazy val members = new HibernateSetAdapter[GameUser]({
    if(Members == null) Members = Sets.newLinkedHashSet()
    Members
  })

  lazy val history = new HibernateSetAdapter[ClanHistoryRecord]({
    if(History == null) History = Sets.newLinkedHashSet()
    History
  })

  lazy val offers = new HibernateSetAdapter[Offer]({
    if(Offers == null) Offers = Sets.newLinkedHashSet()
    Offers
  })

  lazy val folders = new HibernateSetAdapter[Folder]({
    if (Folders == null) Folders = Sets.newLinkedHashSet()
    Folders
  })

  lazy val ignored = new HibernateSetAdapter[Avatar]({
    if (Ignored == null) Ignored = Sets.newLinkedHashSet()
    Ignored
  })

  // Getters

  def name: String = Name
  def creationTime: Date = CreationTime
  def creator: GameUser = Creator
  def requestsDisabled: Boolean = Option(RequestsDisabled) exists Boolean.unbox
  def level: Int = Level
  def membersCountUpgrade: Int = Option(MembersCountUpgrade).map(Int.unbox).getOrElse(0)
  def allianceType: Option[Int] = Option(AllianceType).map(Int.unbox)
  def allianceChangeTime: Option[Long] = Option(AllianceChangeTime).map(_.getTime)
  def expCurrent: Long = Option(ExpCurrent).map(Long.unbox).getOrElse(0L)
  def rubyMineCollectTime: Long = Option(RubyMineCollectTime).map(Long.unbox).getOrElse(0L)
  def rubyMineLevel: Int = Option(RubyMineLevel).map(Int.unbox).getOrElse(0)
  def lastOffersViewDate: Option[Long] = Option(LastOffersViewDate).map(_.getTime)
  def extraStamps: Int = Option(ExtraStamps).map(Int.unbox).getOrElse(0)
  def bonusRedistTime: Option[Long] = Option(BonusRedistTime).map(_.getTime)
  def noticeLastTime: Option[Long] = Option(NoticeLastTime).map(_.getTime)
  def mailingLastTime: Option[Long] = Option(MailingLastTime).map(_.getTime)

  // Setters

  def name(arg: String) = update {Name = arg}
  def creationTime(arg: Date) = update {CreationTime = arg}
  def creator(arg: GameUser) = update {Creator = arg}
  def requestsDisabled(arg: Boolean) = update {RequestsDisabled = arg}
  def level(arg: Int) = update {Level = arg}
  def membersCountUpgrade(arg: Int) = update {MembersCountUpgrade = arg}
  def allianceType(arg: Option[Int]) = update {AllianceType = arg.map(Int.box).orNull}
  def allianceChangeTime(arg: Option[Long]) = update {AllianceChangeTime = arg.map(new Date(_)).orNull}
  def expCurrent(arg: Long) = update {ExpCurrent = arg}
  def rubyMineCollectTime(arg: Long) = update {RubyMineCollectTime = arg}
  def rubyMineLevel(arg: Int) = update {RubyMineLevel = arg}
  def lastOffersViewDate(arg: Option[Long]) = update {LastOffersViewDate = arg.map(new Date(_)).orNull}
  def extraStamps(arg: Int) = update {ExtraStamps = arg}
  def bonusRedistTime(arg: Option[Long]) = update {BonusRedistTime = arg.map(new Date(_)).orNull }
  def noticeLastTime(arg: Long) = update {NoticeLastTime = new Date(arg)}
  def mailingLastTime(arg: Long) = update {MailingLastTime = new Date(arg)}

  def visualName: String = name.capitalize

  def isFull: Boolean = members.size >= maxMembers

  def getCashDeskLimit: Long = config.CLAN_BANK_LIMIT

  def maxMembers: Int = Clan.MAX_MEMBERS + (level - 1) + membersCountUpgrade

  def membersCountUpgradePrice: Option[RubyPrice] =
    if (membersCountUpgrade >= Clan.MEMBER_UPGRADES_LIMIT) None
    else Some(RubyPrice(1000 * (membersCountUpgrade + 1)))

  def alliance: Option[ReputationModel] = allianceType.flatMap(ReputationModel.withTypeOption)
  def alliance(arg: Option[ReputationModel]) = allianceType(arg.map(_.typeId))

  def allowedUpgradeMembersLimit(user: GameUser): Boolean = user.clan.exists(_.id == id) && user.clanRole == ClanRole.LEADER

  def allianceChangeCooldown: Option[Long] = allianceChangeTime.map(_ + Clan.ALLIANCE_COOLDOWN - now).filter(_ > 0)

  def changeAlliance(alliance: Option[ReputationModel]): Unit = {
    this.alliance(alliance)
    allianceChangeTime(Some(now))
  }

  def expRemainToNextLevel: Long = ExpMoneyUnits.experienceToNextClanLevel(level) - expCurrent

  def addExp(exp: Long, rep: ReputationModel, actor: Option[GameUser]): Unit = {
    if (exp < 0) throw new IllegalArgumentException("Exp should be positive")

    val remain = expRemainToNextLevel

    if (exp >= remain) {
      level(level + 1)
      expCurrent(exp - remain)
    } else {
      expCurrent(expCurrent + exp)
    }

    reputationValues.update(rep.typeId)(curr => curr.map(_ + exp).getOrElse(exp))

    for (actor <- actor) {
      experiences.update(actor.id)(op => op.map(_ + exp).getOrElse(exp))
    }
  }

  def expTotal: Long = reputationValues.values.sum

  def canUpgradeRubyMine: Boolean = (rubyMineLevel + 1) <= (level * 5)

  def getMaxAllowedRubiesInMine: Int = 10 + rubyMineLevel

  private def getRubyMineInterval: Long = (1 day) / getMaxAllowedRubiesInMine

  def getAllowedRubiesInMine: Int = {
    val waited = now - rubyMineCollectTime
    math.min(getMaxAllowedRubiesInMine, math.max(0, waited / getRubyMineInterval).asInstanceOf[Int])
  }

  def rubyMineUpgradePrice: Option[RubyPrice] =
    if (canUpgradeRubyMine) Some(RubyPrice(100 * (rubyMineLevel + 1))) else None

  def timePassedTobeTrueMember(joinTime: Option[Long]): Boolean = joinTime.exists(t => TimeUtil.passed(t, 3 days))

  def bankLimit: Long = 50000

  def hasNewRequests: Boolean = lastOffersViewDate.orElse(Some(0L)).exists {lastViewTime =>
    offers.exists(offer => offer.isRequest && offer.date.getTime > lastViewTime)
  }

  def heroBonus: Double = (1.0 * level / 2) / 100

  def bonusRegistry: ClanBonusRegistry = new ClanBonusRegistry(this)

  def maxStamps: Int = config.MIN_STAMPS + level * 10
  def stamps: Int = config.MIN_STAMPS + extraStamps
  def stampPrice: Option[RubyPrice] = if (stamps >= maxStamps) None else Some(RubyPrice(90 + 10 * (extraStamps + 1)))

  def valueOf(clanBonus: ClanBonus): Double = bonuses.get(clanBonus.typeId).map(clanBonus.value).getOrElse(0.0)

  def canRedistributeBonuses: Boolean = bonusRedistTime.isEmpty || bonusRedistTime.exists(now - _ > config.CLAN_BONUS_REDIST_PERIOD)

  def nextNoticement: Option[Long] = noticeLastTime.filter(now - _ < (1 hour)).map(_ + (1 hour) - now)
}

object Clan {

  val MAX_MEMBERS: Int = 10
  val MEMBER_UPGRADES_LIMIT = 30
  val ALLIANCE_COOLDOWN = 1 week

  implicit class ClanOps(self: Clan)(implicit hib: HibernateSessionAware) {
    implicit val session: HibernateSession = hib.hibernateSession

    def join(user: GameUser): ClanJoinResult = join(user, None)

    def join(user: GameUser, joiner: Option[GameUser]): ClanJoinResult = {
      if (user.clan.isDefined) ClanJoinResult.USER_IS_IN_CLAN
      else if (user.level < config.CLAN_REQUIRED_LEVEL) ClanJoinResult.LOW_LEVEL
      else if (self.isFull) ClanJoinResult.CLAN_IS_FULL
      else {
        self.members += user
        user.joinClan(self)

        if (joiner.isDefined) {
          val record = new ClanHistoryRecord
          record.clan(self)
          record.recType(ClanHistoryRecordType.USER_IN)
          record.actor(joiner.get)
          record.target(Some(user))
          record.date(System.currentTimeMillis())
          self.history += record
        }

        ClanJoinResult.OK
      }
    }

    def leave(user: GameUser, preventLog: Boolean = false): Unit = {
      self.members -= user
      user.leaveClan()
      if (!preventLog) self.history += new ClanHistoryRecord(self, ClanHistoryRecordType.USER_LEFT, user)
    }

    def checkRole(gu: GameUser, role: ClanRole): Boolean = gu.clan.exists(_.id == self.id) && role == gu.clanRole

    def checkRoles(gu: GameUser, roles: ClanRole*): Boolean = gu.clan.exists(_.id == self.id) && roles.contains(gu.clanRole)

    def checkRoleNot(gu: GameUser, roles: ClanRole*): Boolean = gu.clan.exists(_.id == self.id) && !roles.contains(gu.clanRole)

    def getNotDeletedFolders(gameUser: GameUser) = self.folders.filter(f => !f.deleted && gameUser.canViewClanFolder(f))

    def isIgnored(avatar: Avatar): Boolean = self.ignored.contains(avatar) && !avatar.user.isAdminOrSupporterOrModerator

    def clanMoney(): ClanMoney = ClanDAO(hib.hibernateSession).getClanMoney(self.id)

    def changeRuby(amount: Long, tx: TransactionType): Long = {
      clanMoney().changeRuby(amount, tx)
    }

    def addRubies(user: GameUser, amount: Long, tx: TransactionType): Unit = {
      changeRuby(amount, tx)

      WeekCounters.addWeekCounter(user.clanWeekRubies, amount)
      self.memberRubies.update(user.id)(curr => curr.map(x => x + amount).getOrElse(amount))
      user.lastClanFundTime(Some(self.now))
    }

    def canAddRubies(user: GameUser, amount: Int): Boolean = {
      val enoughPayments = BillingDAO().getPaymentSum(user.id) >= 500 || user.isLeader(self)
      val daysLimitPassed = self.timePassedTobeTrueMember(user.clanJoinTime)
      val todayRubies = WeekCounters.todayCounter(user.clanWeekRubies)
      val leftToLimit = self.bankLimit - self.clanMoney().rubies

      val canFund = (enoughPayments || (daysLimitPassed && todayRubies < 10)) && leftToLimit >= 1
      val canFund10 = (enoughPayments || (daysLimitPassed && todayRubies <= 0)) && leftToLimit >= 10
      val canFund100 = enoughPayments && leftToLimit >= 100
      val canFund1000 = enoughPayments && leftToLimit >= 1000
      val canFund10000 = enoughPayments && leftToLimit >= 10000

      (amount == 1 && canFund) ||
        (amount > 1 && amount <= 10 && canFund10) ||
        (amount > 10 && amount <= 100 && canFund100) ||
        (amount > 100 && amount <= 1000 && canFund1000) ||
        (amount > 1000 && amount <= 10000 && canFund10000)
    }

    def chat(): Chat = {
      if (self.Chat == null) {
        val ch = new bean.Chat
        session.persist(ch)
        self.setChat(ch)
      }
      self.Chat
    }
  }
}
