package bean

import java.util.Date

import bean.state.ClanHistoryRecordType

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.11.2016.
  */
class ClanHistoryRecord {

  def this(c: Clan, rec: ClanHistoryRecordType.Value, a: GameUser) {
    this()
    clan(c).recType(rec).actor(a).date(System.currentTimeMillis())
  }

  @BeanProperty
  protected var Id: java.lang.Integer = _
  def id(): Int = Id
  def id(arg: Int): ClanHistoryRecord = {Id = arg; this}

  @BeanProperty
  protected var TheClan: Clan = _
  def clan(): Clan = TheClan
  def clan(arg: Clan): ClanHistoryRecord = {TheClan = arg; this}

  @BeanProperty
  protected var RecType: java.lang.Integer = _
  def recType(): ClanHistoryRecordType.Value = ClanHistoryRecordType(RecType)
  def recType(arg: ClanHistoryRecordType.Value): ClanHistoryRecord = {RecType = arg.id; this}

  @BeanProperty
  protected var Actor: GameUser = _
  def actor(): GameUser = Actor
  def actor(arg: GameUser): ClanHistoryRecord = {Actor = arg; this}

  @BeanProperty
  protected var Target: GameUser = _
  def target(): Option[GameUser] = Option(Target)
  def target(arg: Option[GameUser]): ClanHistoryRecord = {Target = arg.orNull; this}

  @BeanProperty
  protected var Comment: String = _
  def comment(): Option[String] = Option(Comment)
  def comment(arg: String): ClanHistoryRecord = {Comment = arg; this}

  @BeanProperty
  protected var TheDate: Date = _
  def date(): Option[Long] = Option(TheDate) map (_.getTime)
  def date(arg: Long) = {TheDate = new Date(arg); this}

  @BeanProperty
  protected var Role: java.lang.Integer = _
  def role(): Option[Int] = Option(Role)
  def role(arg: Int): ClanHistoryRecord = {Role = arg; this}
}
