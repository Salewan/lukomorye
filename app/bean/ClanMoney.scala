package bean

import components.HibernateSessionAware
import model.TransactionType

import scala.beans.BeanProperty

/**
  * Created by Salewan on 27.08.2017.
  */
class ClanMoney extends Bean {

  @BeanProperty
  protected var version: java.lang.Long = _

  @BeanProperty
  protected var Rubies: java.lang.Long = _

  // Getters
  def rubies: Long = Option(Rubies) map Long.unbox getOrElse 0L

  // Setters
  def rubies(arg: Long) = update {Rubies = arg}
}

object ClanMoney {
  implicit class ClanMoneyOps(self: ClanMoney)(implicit hib: HibernateSessionAware) {

    def changeRuby(x: Long, tx:TransactionType): Long = {
      var amount = x
      val result = self.rubies + amount
      if (amount < 0 && result < 0) throw new IllegalArgumentException("Not enough rubies")

      hib.hibernateSession.get(classOf[Clan], self.id).map {clan =>
        if (amount > 0 && result > clan.getCashDeskLimit)
          amount = math.max(0, clan.getCashDeskLimit - self.rubies)

        self.rubies(self.rubies + amount)

        val transaction = new ClanMoneyTransaction
        transaction.clan(clan)
        transaction.amount(amount)
        transaction.creationTime(self.now)
        transaction.typeId(tx.typeId)
        transaction.result(self.rubies)
        transaction.level(clan.level)
        hib.hibernateSession.persist(transaction)
        amount
      } getOrElse (throw new RuntimeException("Clan was not found"))
    }
  }
}
