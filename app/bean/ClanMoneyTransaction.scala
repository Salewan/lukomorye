package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * Created by Salewan on 28.08.2017.
  */
class ClanMoneyTransaction extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Amount: java.lang.Long = _

  @BeanProperty
  protected var TypeId: java.lang.Integer = _

  @BeanProperty
  protected var Clan: Clan = _

  @BeanProperty
  protected var Result: java.lang.Long = _

  @BeanProperty
  protected var Level: java.lang.Integer = _

  // Getters
  def creationTime: Long = CreationTime.getTime
  def amount: Long = Amount
  def typeId: Int = TypeId
  def clan: Clan = Clan
  def result: Long = Result
  def level: Int = Level

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def amount(arg: Long) = update {Amount = Long.box(arg)}
  def typeId(arg: Int) = update {TypeId = Int.box(arg)}
  def clan(arg: Clan) = update {Clan = arg}
  def result(arg: Long) = update {Result = Long.box(arg)}
  def level(arg: Int) = update {Level = Int.box(arg)}

}
