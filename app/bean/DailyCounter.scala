package bean

import java.util.Date

import org.joda.time.DateTime

import scala.beans.BeanProperty

/**
  * Created by Salewan on 10.08.2017.
  */
class DailyCounter extends BeanTemplate {

  @BeanProperty
  protected var LastUpdate: Date = _

  @BeanProperty
  protected var Counter: java.lang.Long = _

  // Getters
  def lastUpdate: Option[Long] = Option(LastUpdate).map(_.getTime)
  def counter: Long = Option(Counter).map(Long.unbox).getOrElse(0L)

  // Setters
  def lastUpdate(arg: Long) = update {LastUpdate = new Date(arg)}
  def counter(arg: Long) = update {Counter = arg}


  def todayCounter(): Long = {
    if (lastUpdate.isEmpty) {
      lastUpdate(now)
      setupCounter(0, 1)
      0L
    } else {
      val previous = new DateTime(lastUpdate.get)
      val thisMoment = new DateTime()
      if (thisMoment.dayOfYear() != previous.dayOfYear()) setupCounter(0, 1)
      counter
    }
  }

  def available(max: Long): Long = math.max(max - todayCounter(), 0)

  def increment(max: Long): Unit = increment(1, max)

  def increment(amount: Long, max: Long): Unit = {
    if (lastUpdate.isEmpty) {
      lastUpdate(now)
      setupCounter(amount, max)
    } else {
      val previous = new DateTime(lastUpdate.get)
      val thisMoment = new DateTime()
      lastUpdate(now)
      if (thisMoment.dayOfYear().get() != previous.dayOfYear().get()) setupCounter(amount, max)
      else setupCounter(counter + amount, max)
    }
  }

  def setupCounter(counter: Long, max: Long): Unit = {
    this.Counter = math.min(counter, max)
  }
}

object DailyCounter {
  def apply(): DailyCounter = new DailyCounter().counter(0L)
  //def apply(counter: Long, lastUpdate: Long): DailyCounter = new DailyCounter
}
