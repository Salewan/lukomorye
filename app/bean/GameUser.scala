package bean

import java.util.Date

import bean.messages.forum.{Folder, FolderType, Topic}
import bean.state.{ClanHistoryRecordType, ClanRole, MissionState}
import com.google.common.collect.{Lists, Maps, Sets}
import components.HibernateSessionAware
import dao.{MessageDAO, MissionDAO, TaskDAO}
import hibernate.{DomainKeys, HibernateBagAdapter, HibernateMapAdapter, HibernateSetAdapter}
import model.Building.VOLSHEBNAYA_PECHKA
import model.TransactionGenerator.{LEVEL_UP_BONUS_TX, LEVEL_UP_TX}
import model._
import model.buff.{BuffFunction, BuffType}
import model.clan.ClanBonus
import model.hero.HeroTemplate
import model.hero.HeroTemplate.{Kot_uchenyj, Leshij, Rusalka}
import model.item.Seed.TravaMurova
import model.item.{Ingredient, Product, Seed}
import model.mission.MissionGenerator
import model.price.discount.impl.{BankUpgradeHalfPriceByAction, BuildingsUpgradeHalfPrice}
import model.price.{CoinPrice, Price, RubyPrice}
import play.api.Logger
import util.Implicits._
import util.{ExpMoneyUnits, LevelUtil, OnlineUtil, WeekCounters}

import scala.beans.BeanProperty
import scala.collection.mutable
import scala.language.postfixOps

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
class GameUser extends AccountEntity {

  @BeanProperty
  protected var TotalExperience: java.lang.Long = 0L

  @BeanProperty
  protected var CurrentLevelExperience: java.lang.Long = 0L

  @BeanProperty
  protected var LastActionTime: Date = _

  @BeanProperty
  protected var Level: java.lang.Integer = _

  @BeanProperty
  protected var TutorialStep: java.lang.Integer = 0

  @BeanProperty
  protected var Heroes: java.util.Map[Int, Hero] = Maps.newLinkedHashMap()

  @BeanProperty
  protected var Missions: java.util.Set[Mission] = _

  @BeanProperty
  protected var Barn: java.util.Map[Int, Int] = _

  @BeanProperty
  protected var TransformRecipes: java.util.Set[Int] = _

  @BeanProperty
  protected var ClanRoleId: java.lang.Integer = 0

  @BeanProperty
  protected var ClanJoinTime: Date = _

  @BeanProperty
  protected var MyClan: Clan = _

  @BeanProperty
  protected var Offers: java.util.Set[Offer] = _

  @BeanProperty
  protected var ClanRequests: java.util.Map[Int, Date] = _

  @BeanProperty
  protected var MissionsGenerationTime: Date = _

  @BeanProperty
  protected var TasksGenerationTime: Date = _

  @BeanProperty
  protected var Tasks: java.util.Set[Task] = _

  @BeanProperty
  protected var ObtainedEvents: java.util.Set[Int] = _

  @BeanProperty
  protected var TaskLevel: java.lang.Integer = _

  @BeanProperty
  protected var Reputation: java.util.Map[Int, Reputation] = _

  @BeanProperty
  protected var CurrentSeed: java.lang.Integer = _

  @BeanProperty
  protected var Productions: java.util.Set[Production] = _

  @BeanProperty
  protected var PetProductions: java.util.Set[PetProduction] = _

  @BeanProperty
  protected var Ignore: java.util.Set[GameUser] = _

  @BeanProperty
  protected var Friends: java.util.Set[GameUser] = _

  @BeanProperty
  protected var BuyList: java.util.Set[BuySlot] = _

  @BeanProperty
  protected var SellList: java.util.Set[SellSlot] = _

  @BeanProperty
  protected var BuyListGenTime: Date = _

  @BeanProperty
  protected var SellListGenTime: Date = _

  @BeanProperty
  protected var BarnLevel: java.lang.Integer = _

  @BeanProperty
  protected var MissionsLevel: java.lang.Integer = _

  @BeanProperty
  protected var TimeConversionX: java.lang.Long = _

  @BeanProperty
  protected var AdditionalActiveHeroes: java.lang.Integer = _

  @BeanProperty
  protected var Notifications: java.util.Set[Notification] = _

  @BeanProperty
  protected var Buffs: java.util.Map[Int, Buff] = _

  @BeanProperty
  protected var LastRubiesCollectTime: java.lang.Long = _

  @BeanProperty
  protected var LastFreeDustCollectTime: java.lang.Long = _

  @BeanProperty
  protected var BankLevel: java.lang.Integer = _

  @BeanProperty
  protected var StartingBuffEarned: java.lang.Boolean = _

  @BeanProperty
  protected var DailyKeys: DailyCounter = DailyCounter()

  @BeanProperty
  protected var DailyMissions: IntervalCounter = new IntervalCounter

  @BeanProperty
  protected var DailyTasks: IntervalCounter = new IntervalCounter

  @BeanProperty
  protected var Chests: java.util.Set[Chest] = _

  @BeanProperty
  protected var Keys: java.lang.Integer = _

  @BeanProperty
  protected var ShopRefreshTime: Date = _

  @BeanProperty
  protected var ClanWeekExp: java.util.Map[Int, Long] = _

  @BeanProperty
  protected var ClanWeekRubies: java.util.Map[Int, Long] = _

  @BeanProperty
  protected var LastClanFundTime: Date = _

  @BeanProperty
  protected var LastClanChatVisitTime: Date = _

  @BeanProperty
  protected var LastDiscountActionTime: Date = _

  @BeanProperty
  protected var LastAddressees: java.util.Map[Avatar, Long] = _

  lazy val lastAddresses = new HibernateMapAdapter[Avatar, Long]({
    if (LastAddressees == null) LastAddressees = Maps.newHashMap()
    LastAddressees
  })

  lazy val buffs = new HibernateMapAdapter[Int, Buff]({
    if (Buffs == null) Buffs = Maps.newLinkedHashMap()
    Buffs
  })

  lazy val heroes = new HibernateMapAdapter[Int, Hero]({
    if (Heroes == null) Heroes = Maps.newLinkedHashMap()
    Heroes
  })

  lazy val missions = new HibernateSetAdapter[Mission]({
    if (Missions == null) Missions = Sets.newLinkedHashSet()
    Missions
  })

  lazy val barn = new HibernateMapAdapter[Int, Int]({
    if (Barn == null) Barn = Maps.newLinkedHashMap()
    Barn
  }) with DomainKeys[Int, Int, Ingredient] {
    override def convert(key: Int): Ingredient = Ingredient.ingredientsMap(key)
  }

  lazy val transformRecipes = new HibernateSetAdapter[Int]({
    if (TransformRecipes == null) TransformRecipes = Sets.newLinkedHashSet()
    TransformRecipes
  })

  lazy val offers = new HibernateSetAdapter[Offer]({
    if (Offers == null) Offers = Sets.newLinkedHashSet()
    Offers
  })

  lazy val clanRequests = new HibernateMapAdapter[Int, Date]({
    if (ClanRequests == null) ClanRequests = Maps.newLinkedHashMap()
    ClanRequests
  })

  lazy val tasks = new HibernateSetAdapter[Task]({
    if (Tasks == null) Tasks = Sets.newLinkedHashSet()
    Tasks
  })

  lazy val obtainedEvents = new HibernateSetAdapter[Int]({
    if (ObtainedEvents == null) ObtainedEvents = Sets.newLinkedHashSet()
    ObtainedEvents
  })

  lazy val reputation = new HibernateMapAdapter[Int, Reputation]({
    if (Reputation == null) Reputation = Maps.newLinkedHashMap()
    Reputation
  })

  lazy val productions = new HibernateSetAdapter[Production]({
    if (Productions == null) Productions = Sets.newLinkedHashSet()
    Productions
  })

  lazy val petProductions = new HibernateSetAdapter[PetProduction]({
    if (PetProductions == null) PetProductions = Sets.newLinkedHashSet()
    PetProductions
  })

  lazy val ignore = new HibernateSetAdapter[GameUser]({
    if (Ignore == null) Ignore = Sets.newLinkedHashSet()
    Ignore
  })

  lazy val friends = new HibernateSetAdapter[GameUser]({
    if (Friends == null) Friends = Sets.newLinkedHashSet()
    Friends
  })

  lazy val buyList = new HibernateSetAdapter[BuySlot]({
    if (BuyList == null) BuyList = Sets.newLinkedHashSet()
    BuyList
  })

  lazy val sellList = new HibernateSetAdapter[SellSlot]({
    if (SellList == null) SellList = Sets.newLinkedHashSet()
    SellList
  })

  lazy val notifications = new HibernateSetAdapter[Notification]({
    if (Notifications == null) Notifications = Sets.newLinkedHashSet()
    Notifications
  })

  lazy val chests = new HibernateSetAdapter[Chest]({
    if (Chests == null) Chests = Sets.newLinkedHashSet()
    Chests
  })

  lazy val clanWeekExp = new HibernateMapAdapter[Int, Long]({
    if (ClanWeekExp == null) ClanWeekExp = Maps.newLinkedHashMap()
    ClanWeekExp
  })

  lazy val clanWeekRubies = new HibernateMapAdapter[Int, Long]({
    if (ClanWeekRubies == null) ClanWeekRubies = Maps.newLinkedHashMap()
    ClanWeekRubies
  })

  // Getters

  def keys: Int = Option(Keys) map Int.unbox getOrElse 0
  def dailyKeys: DailyCounter = DailyKeys
  def dailyMissions: IntervalCounter = DailyMissions
  def dailyTasks: IntervalCounter = DailyTasks
  def startingBuffEarned: Boolean = Option(StartingBuffEarned) exists Boolean.unbox
  def totalExperience: Long = TotalExperience
  def currentLevelExperience = CurrentLevelExperience
  def lastActionTime: Option[Long] = Option(LastActionTime).map(_.getTime)
  def level: Int = Level
  def tutorialStep = TutorialStep
  def isTutorialCompleted = TutorialStep > config.TUTORIAL_STEPS || level >= 4
  def clanRole: ClanRole = ClanRole(Option(ClanRoleId) map Int.unbox getOrElse 0)
  def clanJoinTime: Option[Long] = Option(ClanJoinTime) map (_.getTime)
  def clan: Option[Clan] = Option(MyClan)
  def missionsGenerationTime: Long = Option(MissionsGenerationTime) map (_.getTime) getOrElse 0L
  def tasksGenerationTime: Long = Option(TasksGenerationTime) map (_.getTime) getOrElse 0L
  def currentSeed: Option[Seed] = Option(CurrentSeed) map Int.unbox flatMap Seed.allSeedsMap.get
  def taskLevel: Int = Option(TaskLevel) map Int.unbox getOrElse 0
  def buyListGenTime: Option[Long] = Option(BuyListGenTime).map(_.getTime)
  def sellListGenTime: Option[Long] = Option(SellListGenTime).map(_.getTime)
  def barnLevel: Int = Option(BarnLevel) map Int.unbox getOrElse 0
  def missionsLevel: Int = Option(MissionsLevel) map Int.unbox getOrElse 0
  def timeConversionX: Long = Option(TimeConversionX) map Long.unbox getOrElse System.currentTimeMillis()
  def additionalActiveHeroes: Int = Option(AdditionalActiveHeroes) map Int.unbox getOrElse 0
  def lastRubiesCollectTime: Long = Option(LastRubiesCollectTime).map(Long.unbox).getOrElse(0L)
  def lastFreeDustCollectTime: Long = Option(LastFreeDustCollectTime).map(Long.unbox).getOrElse(0L)
  def bankLevel: Int = Option(BankLevel).map(Int.unbox).getOrElse(0)
  def shopRefreshTime: Option[Long] = Option(ShopRefreshTime).map(_.getTime)
  def lastClanFundTime: Option[Long] = Option(LastClanFundTime).map(_.getTime)
  def lastClanChatVisitTime: Option[Long] = Option(LastClanChatVisitTime).map(_.getTime)
  def lastDiscountActionTime: Option[Long] = Option(LastDiscountActionTime).map(_.getTime)

  // Setters

  def keys(arg: Int) = update {Keys = arg}
  def dailyKeys(arg: DailyCounter) = update {DailyKeys = arg}
  def startingBuffEarned(arg: Boolean) = update {StartingBuffEarned = arg}
  def totalExperience(arg: Long) = update {TotalExperience = arg}
  def currentLevelExperience(arg: Long) = update {CurrentLevelExperience = arg}
  def lastActionTime(arg: Long) = update {LastActionTime = new Date(arg)}
  def level(arg: Int): Reward = {Level = arg; Reward().setLevel(arg)}
  def tutorialStep(arg: Int) = update {TutorialStep = arg}
  def clanRole(arg: ClanRole) = update {ClanRoleId = arg.id}
  def clanJoinTime(arg: Option[Long]) = update {ClanJoinTime = arg.map(new Date(_)).orNull}
  def clan(arg: Option[Clan]) = update {MyClan = arg.orNull}
  def missionsGenerationTime(arg: Long) = update {MissionsGenerationTime = new Date(arg)}
  def tasksGenerationTime(arg: Long) = update {TasksGenerationTime = new Date(arg)}
  def taskLevel(arg: Int) = update {TaskLevel = arg}
  def currentSeed(arg: Option[Seed]) = update {CurrentSeed = arg.map(Int box _.typeId).orNull}
  def buyListGenTime(arg: Some[Long]) = update {BuyListGenTime = arg.map(new Date(_)).orNull}
  def sellListGenTime(arg: Some[Long]) = update {SellListGenTime = arg.map(new Date(_)).orNull}
  def barnLevel(arg: Int) = update {BarnLevel = arg}
  def missionsLevel(arg: Int) = update {MissionsLevel = arg}
  def timeConversionX(arg: Long) = update {TimeConversionX = arg}
  def additionalActiveHeroes(arg: Int) = update {AdditionalActiveHeroes = arg}
  def lastRubiesCollectTime(arg: Long) = update {LastRubiesCollectTime = arg}
  def lastFreeDustCollectTime(arg: Long) = update {LastFreeDustCollectTime = arg}
  def bankLevel(arg: Int) = update {BankLevel = arg}
  def shopRefreshTime(arg: Option[Long]) = update {ShopRefreshTime = arg.map(new Date(_)).orNull}
  def lastClanFundTime(arg: Option[Long]) = update {LastClanFundTime = arg.map(new Date(_)).orNull}
  def lastClanChatVisitTime(arg: Option[Long]) = update {LastClanChatVisitTime = arg.map(new Date(_)).orNull}
  def lastDiscountActionTime(arg: Option[Long]) = update {LastDiscountActionTime = arg.map(new Date(_)).orNull}

  def incBankLevel() = bankLevel(bankLevel + 1)
  def incAdditionalActiveHeroes() = additionalActiveHeroes(additionalActiveHeroes + 1)
  def incTimeConversionX(arg: Long) = timeConversionX(timeConversionX + arg)
  def incrementTutorial() = TutorialStep += 1
  def taskLevelInc(): Unit = taskLevel(taskLevel + 1)
  def isInClan: Boolean = clan.isDefined
  def incBarnLevel() = barnLevel(barnLevel + 1)
  def incMissionsLevel() = {
    missionsLevel(missionsLevel + 1)
  }


  def barnSize = {
    clan.map(_.valueOf(ClanBonus.Ambar).asInstanceOf[Int]).getOrElse(0) +
    reputation.foldLeft(0)(_ + _._2.addsExtraBarnSpace) + config.BARN_SIZE + barnLevel * 5
  }
  def barnSizeTaken = barn.values.sum
  def barnFreeSpace = 0 max (barnSize - barnSizeTaken)

  def barnUpgradePrice: Option[RubyPrice] = if (barnLevel >= 90) None else Some(RubyPrice(10 * (barnLevel + 1), BuildingsUpgradeHalfPrice))

  def getProduction(typeId: Int): Option[Production] = productions.find(_.buildingType == typeId)

  def getPetProduction(typeId: Int): Option[PetProduction] = petProductions.find(_.buildingType == typeId)

  def ingredientColor(ing: Ingredient, count: Int): String = if (barn.getOrElse(ing.typeId, 0) >= count) "green" else "red"

  def buildings = {
    val all = Building.all
    val available = all.filter(_.level <= level).sortBy(_.level)
    val notAvailable = all.filter(_.level > level).sortBy(_.level)
    notAvailable.headOption.map(available :+ _).getOrElse(available)
  }

  def createTutorialPetProduction: Unit = {
    val user = this
    val prod = new PetProduction().owner(user).buildingType(Building.VOLSHEBNYJ_NASEST.typeId)
    user.petProductions += prod
  }

  def hasCompletedMissions: Boolean = missions.exists(_.isCompleted)

  def maximumShopSlots: Int = {
    val add = reputation.foldLeft(0)(_ + _._2.addsExtraShopSlots) +
      clan.map(_.valueOf(ClanBonus.ShopSlots).asInstanceOf[Int]).getOrElse(0)
    config.SHOP_SLOTS + add
  }

  private def fillShopSlot[T <: ShopSlot](slot: T, xval: Double): T = {
    val item = util.RandomUtil random Ingredient.all.filter(i => !i.unlimited && i.level <= this.level)
    val amount = util.RandomUtil random(3, 9)
    val value = math.round(item.nominal * xval)
    slot.
      item(item).
      amount(amount).
      value(value).
      generationTime(now).
      sold(false).
      unique(util.RandomUtil.getRandom.nextLong())
    slot
  }

  def fillBuySlot(slot: BuySlot): BuySlot = fillShopSlot(slot, 1.5)
  def fillSellSlot(slot: SellSlot): SellSlot = fillShopSlot(slot, 0.5)

  def updateBuyList(interval: Long): Unit = {
    val max = maximumShopSlots

    if (max > 0 && buyList.size > 0 && buyList.size > max) {
      val col = buyList.take(max)
      buyList.clear()
      buyList.addAll(col)
    }

    val refreshCount = max min {
      buyListGenTime.
        map(gen => {(now - gen) / interval}.asInstanceOf[Int]).
        getOrElse(max)
    }
    if (refreshCount > 0) buyListGenTime(Some(now))

    for (i <- 1 to refreshCount) {
      val persist = buyList.size < i
      val slot =
        if (persist) Some(new BuySlot().owner(this))
        else util.RandomUtil randomOptional buyList.toSeq.filter(now - _.generationTime > interval)

      slot.foreach { slot =>
        fillBuySlot(slot)

        if (persist) buyList += slot
      }
    }
  }

  def updateSellList(interval: Long): Unit = {
    val max = maximumShopSlots

    if (max > 0 && sellList.size > 0 && sellList.size > max) {
      val col = sellList.take(max)
      sellList.clear()
      sellList.addAll(col)
    }

    val refreshCount = max min {
      sellListGenTime.
        map(gen => {(now - gen) / interval}.asInstanceOf[Int]).
        getOrElse(max)
    }
    if (refreshCount > 0) sellListGenTime(Some(now))

    for (i <- 1 to refreshCount) {
      val persist = sellList.size < i
      val slot =
        if (persist) Some(new SellSlot().owner(this))
        else util.RandomUtil randomOptional sellList.toSeq.filter(now - _.generationTime > interval)

      slot.foreach { slot =>
        fillSellSlot(slot)

        if (persist) sellList += slot
      }
    }
  }

  def getFreeHeroes: Set[Hero] = inactiveHeroes.values
    .filter(hero => hero.mission.isEmpty || hero.mission.exists(_.closed)).toSet

  def isEnoughIngredients(product: Product): Boolean = product.composition.forall {case (ing, cnt) =>
    barn.getOrElse(ing.typeId, 0) >= cnt
  }

  val SHOP_REFRESH_TICK = 1 minute // цена будет уменьшаться на 1 каждые SHOP_REFRESH_TICK
  val SHOP_REFRESH_RUBY_GROWTH = 1 // цена увеличивается с каждым рефрешем на SHOP_REFRESH_RUBY_GROWTH
  val SHOP_REFRESH_MAX_PRICE = 5 * SHOP_REFRESH_RUBY_GROWTH // цена за рефреш растёт максимум до SHOP_REFRESH_MAX_PRICE
  def shopRefreshPrice = RubyPrice(
    shopRefreshTime.map { t =>
      val time = math.max(t, now)

      math.min(
        SHOP_REFRESH_MAX_PRICE,

        SHOP_REFRESH_RUBY_GROWTH +
        math.ceil((1.0 * time - now) / SHOP_REFRESH_TICK).asInstanceOf[Int]
      )
    }.getOrElse(SHOP_REFRESH_RUBY_GROWTH).asInstanceOf[Long]
  )

  def shiftForwardShopRefreshTimer(): Unit = {
    val timeGrowth = SHOP_REFRESH_TICK * SHOP_REFRESH_RUBY_GROWTH
    shopRefreshTime(
      shopRefreshTime.map { time =>
        math.min(now + SHOP_REFRESH_MAX_PRICE * SHOP_REFRESH_TICK - timeGrowth, math.max(now, time) + timeGrowth)
      }.orElse(Some(now + timeGrowth))
    )
  }

  def dustYield(ingredient: Ingredient, amount: Int): Long = {
    val x = timeConversionX
    val z = math.min(1.0, math.max(0.01, 2 - math.max(0, x - System.currentTimeMillis) / 10000000.0))
    math.round(z * ingredient.nominal / 30.0 * amount)
  }

  // количество незапущенных поручений
  def newMissionListSize: Int = {
    missions.toSeq.count(_.state == MissionState.NEW)
  }

  // максимальное доступное количество активных героев
  def activeHeroesMax: Int = {
    config.ACTIVE_HEROES_COUNT +
      additionalActiveHeroes +
      reputation.values.foldLeft(0)(_ + _.addsExtraActiveHero)
  }

  // текущее количество активных героев
  def activeHeroesCount: Int = heroes.toSeq.count(_._2.active)

  // активные герои
  def activeHeroes: Map[Int, Hero] = heroes.filter(_._2.active)

  // неактивные герои
  def inactiveHeroes: Map[Int, Hero] = heroes.filter(_._2.nonActive)

  // цена за апгрейд количества активных героев
  def activeHeroesUpgradePrice: Option[RubyPrice] = {
    if (heroes.size >= 9 && additionalActiveHeroes < 5) Some(RubyPrice(100))
    else None
  }

  // опыт для следующего уровня
  def expToNextLevel: Long = ExpMoneyUnits.experienceToNextLevel(level)

  // осталось опыта до следующего уровня
  def leftExperience: Long = expToNextLevel - currentLevelExperience

  // осталось опыта в процентах
  def leftExperiencePercent: Double = math.floor(currentLevelExperience * 100.0 / expToNextLevel)

  // цена за апгрейд макс. количества поручений
  def priceForMissionsCountUpgrade: Option[Price] = {
    if (!canUpgradeMissions) None
    else Some(RubyPrice(10 * (missionsLevel + 1), BuildingsUpgradeHalfPrice))
  }

  // цена за апгрейд макс. количества заданий
  def priceForTasksCountUpgrade: Option[Price] = {
    if (!canUpgradeTasks) None
    else Some(RubyPrice(10 * (taskLevel + 1), BuildingsUpgradeHalfPrice))
  }

  def missionLimitExtend: Int = math.max(0, (level / 10) - 1) * 5

  // максимально возможное количество поручений
  def missionsCountMax: Int = {
    clan.map(_.valueOf(ClanBonus.MissionCount).asInstanceOf[Int]).getOrElse(0) +
    config.MISSIONS_BASIC_COUNT + missionsLevel + missionLimitExtend
  }

  // максимально возможное количество заданий
  def tasksCountMax: Int = {
    clan.map(_.valueOf(ClanBonus.TaskCount).asInstanceOf[Int]).getOrElse(0) +
    math.min(config.TASKS_MAX_VALUE, config.TASKS_BASIC_VALUE + taskLevel)
  }

  // максимальное количество сколько можно снимать рубинов в день
  def getMaxAllowedRubies: Int = 5 + bankLevel

  // длина тика, через который появляется новый доступный рубин
  private def getRubiesInterval: Long = (1 day) / getMaxAllowedRubies

  // сколько сейчас доступно рубинов
  def getAllowedRubies: Int = {
    val waited = now - lastRubiesCollectTime
    math.min(getMaxAllowedRubies, math.max(0, waited / getRubiesInterval).asInstanceOf[Int])
  }

  def canUpgradeBank: Boolean = getMaxAllowedRubies < 30

  def canUpgradeTasks: Boolean = config.TASKS_UPGRADE_LEVEL <= level && config.TASKS_MAX_VALUE > tasksCountMax
  def canUpgradeMissions: Boolean = config.MISSIONS_UPGRADE_LEVEL <= level && config.MISSIONS_MAX_UPGRADES > missionsLevel
  def canUpgradeBarn: Boolean = barnLevel < 90

  // цена за апгрейд банка
  def priceForBankUpgrade: Option[RubyPrice] = {
    if (canUpgradeBank) Some(RubyPrice(30 * (bankLevel + 1), BankUpgradeHalfPriceByAction.andThen(BuildingsUpgradeHalfPrice)))
    else None
  }

  def createProduction(building: Building with ProdFactory): Unit = {
    val prod = new Production()
    prod.owner(this)
    prod.buildingType(building.typeId)
    prod.buildTime(now + building.buildingTime)
    prod.parCount(1)
    if (building == Building.FEED_MILL) prod.basket += Product.IZBUSHKA_FOOD.typeId -> 1
    this.productions += prod
    for (_ <- 1 to config.INITIAL_SLOT_NUMBER) {
      prod.slots += new ProductionSlot().production(prod)
    }
  }

  def getMaxKeysDrop(): Int = 5

  def getKeysLimit(): Int = math.max(0, chests.size + 1 - keys)

  def addChests(n: Int, missionGeneratorId: Int): Reward = {
    val reward = Reward()
    for (_ <- 1 to n) {
      chests += Chest(missionGeneratorId)
      reward.addChests(1)
    }
    reward
  }

  def addKeys(n: Int): Int = {
    val add = if (keys + n > chests.size + 1) getKeysLimit() else n
    keys(keys + add)
    add
  }

  def complete(mission: Mission): Unit = {
    mission.slots.foreach(_.clear())
    mission.closed(true)
    missions -= mission
  }

  def activeHeroesColor: String = {
    val active = activeHeroesCount
    val max = activeHeroesMax
    if (active >= max) "red"
    else if (max - active == 1) "major"
    else "green"
  }

  def allowClaimRubyMine: Boolean = isInClan && clanRole == ClanRole.LEADER

  def allowUpgradeRubyMine: Boolean = isInClan && clanRole == ClanRole.LEADER

  def todayFunded: Long = WeekCounters.todayCounter(clanWeekRubies)

  def dustBankEnabled: Boolean = reputation.exists(_._2.dustBankEnabled)

  // максимально количество бесплатной пыли доступной через кощейский банк ежедневно
  def getMaxAllowedFreeDust: Int = reputation.foldLeft(0)(_ + _._2.dustBankCapacity)

  // длина тика, через который появляется новая единица пыли в день
  def getFreeDustInterval: Long = (1 day) / getMaxAllowedFreeDust

  // сколько сейчас доступно бесплатной пыли
  def getAllowedFreeDust: Int = {
    val waited = now - lastFreeDustCollectTime
    val maxAllowed = getMaxAllowedFreeDust
    if (maxAllowed > 0) math.min(getMaxAllowedFreeDust, math.max(0, waited / getFreeDustInterval).asInstanceOf[Int])
    else 0
  }

  def canSendMission: Boolean = level >= 15

  def canSendTask: Boolean = level >= 15

  def friendOrCoclaner(check: GameUser): Boolean = {
    friends.contains(check) || clan.exists(c => check.clan.exists(_.id == c.id))
  }

  def addAddressee(ava: Avatar): Unit = {
    val map = lastAddresses.asImmutableMap + (ava -> now)
    val seq = map.toSeq.sortBy(- _._2).take(5)
    lastAddresses.clear()
    lastAddresses.putAll(seq)
  }

  def isActive: Boolean = lastActionTime.exists(_ > now - util.javas.OnlineUtil.ACTIVE_TIME)

  def dailyTasksMax: Int = config.DAILY_TASKS_MAX +
    clan.map(_.valueOf(ClanBonus.TaskSendLimit).asInstanceOf[Int]).getOrElse(0)

  def dailyMissionsMax: Int = config.DAILY_MISSIONS_MAX +
    clan.map(_.valueOf(ClanBonus.MissionSendLimit).asInstanceOf[Int]).getOrElse(0)
}

object GameUser {
  implicit class GameUserOps(self: GameUser)(implicit hib: HibernateSessionAware) {

    implicit val session = hib.hibernateSession

    val now = System.currentTimeMillis()

    def addIngredient(arg: Ingredient): Reward = addIngredient(arg, 1)

    def addIngredient(ing: Ingredient, amount: Int): Reward = {
      self.barn.update(ing.typeId) {optionalValue =>
        val currentValue = optionalValue getOrElse 0
        val nextValue = currentValue + amount

        if (nextValue > self.barnSize || nextValue < 0)
          throw new RuntimeException(
            s"""Error user#${self.id} the next value is out of barn's capacity
               | prev=$currentValue next=$nextValue available=${self.barnSize}""".stripMargin)

        nextValue
      }
      Reward().addIngredient(ing, amount)
    }

    private def expWithBonus(baseExp: Long): Long = {
      var result = baseExp
      self.buffs.values.filter(_.isActive).foreach(_.buffType.buffFunction match {
        case f: BuffFunction.ExpFunction => result += f(baseExp)
        case _ =>
      })
      result
    }

    def addExp(e: Long): Reward = {
      val exp = expWithBonus(e)
      val remain = self.leftExperience
      val reward = Reward()
      if (self.level < config.MAX_USER_LEVEL) {
        if (exp >= remain) {
          reward += incLevel()
          if (self.level == config.MAX_USER_LEVEL) self.currentLevelExperience(0)
          else self.currentLevelExperience(exp - remain)
        } else {
          self.currentLevelExperience(self.currentLevelExperience + exp)
        }
      }

      self.totalExperience(self.totalExperience + exp)
      reward.addExp(exp)

      reward
    }

    def incLevel(): Reward = {
      val reward = Reward()
      if (self.level < config.MAX_USER_LEVEL) {
        val l = self.level
        reward += self.money.addDreamDust(l * 10, LEVEL_UP_TX.get(l + 1))
        reward += self.money.addCoins(l * 50, LEVEL_UP_TX.get(l + 1))
        reward += self.money.addRubies(25, LEVEL_UP_TX.get(l + 1))
        if ((l + 1) % 10 == 0) {
          self.money.addRubies(50, LEVEL_UP_BONUS_TX.get(l + 1))
          reward.addLevelUpBonus((l + 1) -> 50)
        }
        reward += self.level(l + 1)
        checkNewIngredients()
        checkNotifications()
        fillTasksAndMissions()
      }
      reward
    }

    private def fillTasksAndMissions(): Unit = {
      val session = hib.hibernateSession

      new TaskDAO(session).generateTasks(self)
      self.tasksGenerationTime(now)

      new MissionDAO(session).generateMissions(self)
      self.missionsGenerationTime(now)

      session.flush()
    }

    private def finishSomeMissions(): Unit = {
      (410 :: 418 :: 416 :: Nil zip Kot_uchenyj :: Leshij :: Rusalka :: Nil) foreach {case (missionType, heroTemplate) =>
          {
            for {
              mission <- self.missions.find(_.generator.typeId == missionType).orElse {
                MissionGenerator.find(missionType).map {gen =>
                  val mission = gen.generate(self)
                  self.missions += mission
                  mission
                }
              }.map { mission =>
                mission.generateEmptySlots()
                mission
              }
              slot <- mission.slots.headOption
              kot <- self.heroes.find(_._1 == heroTemplate.typeId).map(_._2)
            } yield (mission, slot, kot)

          } foreach { case (mission, slot, hero) =>
            slot.hero(hero)
            hero.mission(Some(mission))
            mission.start()
            mission.complete(now)
          }
      }
    }

    // проверить доступны ли какие нибудь новые ингредиенты после взятия уровня
    def checkNewIngredients(): Unit = {
      Ingredient.ingredients.filter(_.level == self.level).foreach(addIngredient(_, 0))
    }

    // проверка на подтверждаемые нотификации
    def checkNotifications(): Unit = {
      if (self.level == config.TAVERNA_LEVEL)
        self.notifications += Notification(self, NotificationType.IGotItAboutOneMoreHero, "")
    }

    def addReputation(rep: ReputationModel, value: Int): Reward = {
      val reward = Reward()
      val entityOpt = self.reputation.get(rep.typeId)

      if (entityOpt.isEmpty) {
        val entity = new Reputation().owner(self).reputationType(rep).value(0)
        reward += entity.changeValue(value)
        self.reputation += rep.typeId -> entity
      } else {
        reward += entityOpt.get.changeValue(value)
      }
      reward
    }

    // возвращает список "ингредиент -> количество", того что не хватает для производства `product`
    def deficit(product: Product): Seq[(Ingredient, Int)] = {
      val res: mutable.ArrayBuffer[(Ingredient, Int)] = mutable.ArrayBuffer()
      product.composition.foreach { case (ingredient, need) =>
        val have = self.barn.getOrElse(ingredient.typeId, 0)
        if (need > have) res.append(ingredient -> (need - have))
      }
      res
    }

    // цена за недостающие ингредиенты в рубинах
    def deficitPrice(deficit: Seq[(Ingredient, Int)]): RubyPrice = RubyPrice apply {
      math.ceil(deficit.map {case (ing, n) => n * ing.nominal / 100.0}.sum).asInstanceOf[Long]
    }

    // цена, которую нужно заплатить за недостающие ингредиенты при производстве `product`
    def deficitPrice(product: Product): RubyPrice = deficitPrice(deficit(product))


    def subtractIngredients(product: Product, forceMode: Boolean = false): Unit = {
      product.composition.foreach {case (ingredient, need) =>
        val have = self.barn.get(ingredient.typeId).getOrElse(0)
        val sub =
        if (need > have) {
          if (!forceMode) throw new RuntimeException("Cant subtractIngredietns") else -have
        } else -need

        addIngredient(ingredient, sub)
      }
    }

    def clearOffers(): Unit = {
      for (offer <- self.offers) {
        offer.clan.offers -= offer
        hib.hibernateSession.delete(offer)
      }
      self.offers.clear()
      hib.hibernateSession.flush()
    }

    def joinClan(clan: Clan): Unit = {
      if (!self.isInClan) {
        self.clan(Some(clan))
        self.clanRole(ClanRole.ORDINARY)
        self.clanJoinTime(Some(now))
      }
    }

    def areCoClanners(user: GameUser): Boolean = self.clan.exists(user.clan.contains)



    private def canInvite: Boolean = self.clanRole == ClanRole.LEADER || self.clanRole == ClanRole.OFFICER

    def canInviteToClan(user: User): Boolean = {
      val target = user.gameUser
      canInvite && !target.isInClan && target.level >= config.CLAN_REQUIRED_LEVEL &&
        !target.avatar.isAnonymous && !user.settings.doNotReceiveClanInvitations
    }

    def leaveClan(): Unit = {
      if (self.isInClan) {
        self.clan(None)
        self.clanRole(ClanRole.NONE)
        self.clanJoinTime(None)

        self.clanWeekExp.clear()
        self.clanWeekRubies.clear()
      }
    }

    def canSendClanRequest: Boolean = {
      self.level >= config.CLAN_REQUIRED_LEVEL && !self.avatar.isAnonymous && !self.isInClan
    }

    def canPromote(target: GameUser): Boolean = {
      if (self == target || !self.isInClan || !self.clan.exists(c => target.clan.contains(c))) false
      else if (self.clanRole == ClanRole.LEADER) target.clanRole.id < ClanRole.preLeader.id
      else self.clanRole == ClanRole.preLeader && target.clanRole.id < ClanRole.prePreLeader.id
    }

    def canDemote(target: GameUser): Boolean = {
      if (self == target || !self.isInClan || !self.clan.exists(c => target.clan.contains(c))) false
      else if (self.clanRole == ClanRole.LEADER) target.clanRole.id > ClanRole.ORDINARY.id
      else self.clanRole == ClanRole.preLeader && target.clanRole.id > ClanRole.ORDINARY.id &&
        target.clanRole.id < ClanRole.preLeader.id
    }

    def canKick(target: GameUser): Boolean = {
      self.clanRole.id >= ClanRole.preLeader.id && self.clanRole.id > target.clanRole.id &&
        self.isInClan && self.clan.exists(target.clan.contains(_))
    }

    def canMakeLeader(target: GameUser): Boolean = {
      self.clanRole == ClanRole.LEADER && target.clanRole == ClanRole.preLeader &&
        self.isInClan && self.clan.exists(target.clan.contains(_))
    }

    def makeLeader(target: GameUser): Unit = if (canMakeLeader(target)) {
      self.clanRole(ClanRole.preLeader)
      target.clanRole(ClanRole.LEADER)

      self.clan.foreach { c =>
        c.history += new ClanHistoryRecord(c, ClanHistoryRecordType.OWNER_CHANGED, self).
          target(Some(target)).role(target.clanRole.id)
      }
    }

    def kick(target: GameUser): Unit = if (canKick(target)) {
      self.clan.foreach { c =>
        c.leave(target, preventLog = true)
        c.history += new ClanHistoryRecord(c, ClanHistoryRecordType.USER_KICKED, self).target(Some(target))
      }
    }

    def promote(target: GameUser): Unit = if (canPromote(target)) {
      self.clan.foreach { c =>
        val nextRole = target.clanRole.getNext
        target.clanRole(nextRole)
        if (nextRole == ClanRole.OFFICER) {
          target.notifications += Notification(target, NotificationType.YouBecomeOfficer, c.visualName)
        }
        c.history += new ClanHistoryRecord(c, ClanHistoryRecordType.ROLE_UP, self).
          target(Some(target)).role(target.clanRole.id)
      }
    }

    def demote(target: GameUser): Unit = if (canDemote(target)) {
      self.clan.foreach { c =>
        target.clanRole(target.clanRole.getPrev)
        c.history += new ClanHistoryRecord(c, ClanHistoryRecordType.ROLE_DOWN, self).
          target(Some(target)).role(target.clanRole.id)
      }
    }

    def canManageClanSettings(clan: Clan): Boolean = self.clan.forall { ownClan =>
      ownClan == clan && self.clanRole == ClanRole.LEADER
    }

    def checkRole(clan: Clan, clanRole: ClanRole): Boolean = self.clanRole == clanRole && self.clan.contains(clan)

    def isLeader(clan: Clan): Boolean = checkRole(clan, ClanRole.LEADER)

    def isLeaderOrOfficer(clan: Clan): Boolean = checkRole(clan, ClanRole.LEADER) || checkRole(clan, ClanRole.OFFICER)

    def canWriteMessage(target: GameUser): Boolean = {
      val author = self
      if (author.avatar.isAnonymous ||
        author.level < config.MESSAGES_ALLOWED_LEVEL ||
        target.level < config.MESSAGES_ALLOWED_LEVEL) false
      else if (author != target) {
        if (target.settings.onlyFriendsMessages)
          author.user.isAdminOrSupporterOrModerator || target.friends.contains(author) || author.areCoClanners(target)
        else author.user.isAdminOrSupporterOrModerator || !target.ignore.contains(author)
      }
      else false
    }

    def allowMessagesFrom(whome: User): Boolean = {
      val whomeGameUser = whome.gameUser

      {whome.isAdminOrSupporter || config.SPETIAL_USERS.contains(whome.avatar.nick.getOrElse(""))} || {
        !whome.isUserBlocked && whomeGameUser.level >= config.MESSAGES_ALLOWED_LEVEL && !self.ignore.contains(whomeGameUser) &&
          {!self.settings.onlyFriendsMessages || self.friends.contains(whomeGameUser) || self.areCoClanners(whomeGameUser)}
      }
    }

    def removeFromFriends(user: GameUser): Unit = self.friends -= user

    def removeFromIgnore(user: GameUser): Unit = self.ignore -= user

    def addToIgnore(user: GameUser): Unit = {
      removeFromFriends(user)
      if (!user.user.isAdminOrSupporterOrModerator) self.ignore += user
    }

    def addToFriends(user: GameUser): Unit = {
      removeFromIgnore(user)
      self.friends += user
    }

    def canViewClanFolder(folder: Folder): Boolean =
      self.user.isAdmin || folder.clan.map((_, folder.folderType)).exists { case (folderClan, folderType) =>
        folderType == FolderType.GUESTS ||
          (folderType == FolderType.MEMBERS && self.clan.exists(_.id == folderClan.id)) ||
          (folderType == FolderType.CHIEFS && this.canManage(folderClan))
      }

    def canEditFolder(folder: Folder): Boolean = {
      self.user.isAdminOrSupporter || folder.clan.exists { folderClan =>
        self.clan.contains(folderClan) && {
          val clanRole = self.clanRole
          clanRole == ClanRole.LEADER ||
            (clanRole.id >= ClanRole.OFFICER.id && folder.folderType != FolderType.MEMBERS)
        }
      }
    }

    def canRemoveFolder(folder: Folder): Boolean = self.user.isAdmin || {
      folder.clan.exists(self.clan.contains) && self.clanRole == ClanRole.LEADER
    }

    def canCreateFolder(folder: Folder, clan: Option[Clan]): Boolean = {
      self.user.isAdminOrSupporter || {
        !folder.isPersistent || folder.clan.exists(clan.contains) && canCreateFolder(clan)
      }
    }

    def canCreateFolder(clan: Option[Clan]): Boolean = self.user.isAdminOrSupporter ||
      (self.clan.exists(clan.contains) && self.clanRole.id >= ClanRole.OFFICER.id)

    def canManage(clan: Clan): Boolean = self.clan.exists(userClan => userClan == clan && self.clanRole.id >= ClanRole.OFFICER.id)

    def hasNewForumMessages(clan: Clan): Boolean = {
      val dao = new MessageDAO(hib.hibernateSession)
      clan.folders.exists { folder =>
        val mark = dao.getFolderMark(self.user, folder)
        !folder.deleted && self.canViewClanFolder(folder) && folder.lastCommentTime.exists(_ > mark.lastViewTime)
      }
    }

    def isWriteAllowed: Boolean = !self.user.avatar.isAnonymous && self.level >= config.REQUIRED_FOR_WRITE_LEVEL

    def isTopicCreationAllowed(folder: Folder): Boolean = {
      val user = self.user
      if (!isWriteAllowed && !user.isAdmin) false
      else folder.folderType match {
        case FolderType.NORMAL => true
        case FolderType.NEWS => user.isAdmin
        case FolderType.ADMIN => user.isAdminOrSupporterOrModerator
        case FolderType.MEMBERS => self.clan.exists(folder.clan.contains)
        case FolderType.CHIEFS => folder.clan.exists(canManage)
        case FolderType.GUESTS => folder.clan.exists(canManage)
        case _ => false
      }
    }

    def canManage(topic: Topic): Boolean = {
      val user = self.user
      !user.isBanned && {
        user.isAdminOrSupporterOrModerator ||
          topic.folder.clan.exists(topicClan => {
            val myClan = self.clan.exists(_.id == topicClan.id)
            val topicAuthor = topic.author
            val isLeader = self.isLeader(topicClan)
            val isOfficer = self.checkRole(topicClan, ClanRole.OFFICER)
            val myTop = topicAuthor.id == self.id

            myClan && {
              isLeader || (isOfficer && myTop) || {
                (isLeader || isOfficer) && !topicAuthor.gameUser.isLeaderOrOfficer(topicClan)
              }
            }

          }) }
    }

    def canOpenClose(topic: Topic): Boolean = canManage(topic)

    def canPinUnpin(topic: Topic): Boolean = canManage(topic)

    def canDelete(topic: Topic): Boolean = canManage(topic)

    def canMove(topic: Topic): Boolean = canManage(topic)

    def canEdit(topic: Topic): Boolean = {
      val user = self.user
      lazy val folderClan = topic.folder.clan

      if (user.isAdmin) true
      else if (user.isBanned) false
      else if (folderClan.nonEmpty && topic.folder.folderType == FolderType.GUESTS && !topic.deleted) {
        val clan = folderClan.get
        self.isLeader(clan) || (topic.author == user && self.isLeaderOrOfficer(clan) )
      }
      else if (user.isSupporterOrModerator) !topic.deleted
      else {
        topic.author == user ||
          user.isAdminOrSupporterOrModerator ||
          folderClan.exists(self.isLeaderOrOfficer)
      } && !topic.deleted && !topic.closed
    }

    def ogorod(): Production = self.getProduction(Building.FARM.typeId).getOrElse {
      val session = hib.hibernateSession
      val production = new Production().
        owner(self).
        buildingType(Building.FARM.typeId).
        buildTime(now).
        parCount(Int.MaxValue)

      session.persist(production)
      self.productions += production

      val kolosok = Some(Seed.Solnechnik.typeId)
      for(_ <- 1 to config.START_DIRT_COUNT) {
        val dirt = new ProductionSlot().production(production)
        production.slots += dirt
      }

      session.flush()
      production
    }

    def groundPrice: Price = RubyPrice(8)

    def addGround(): Unit = {
      val og = this.ogorod()
      val slot = new ProductionSlot().production(og)
      slot.productType(Some(TravaMurova.typeId)).enqueueTime(Some(now)).started(Some(now)).interval(Some(0L))
      og.slots += slot
    }

    def canBuyGround: Boolean = ogorod().slots.size < util.LevelUtil.groundByLevel(self.level)

    def burnIngredient(ing: Ingredient, amount: Int): Reward = {
      val reward = Reward()
      if (self.barn.get(ing.typeId).exists(_ >= amount)) {
        val y = self.dustYield(ing, amount)
        self.incTimeConversionX(y * 10000)
        self.barn.update(ing.typeId)(_.get - amount)
        val trx = TransactionGenerator.BURN_INGREDIENT_TX.get(ing.typeId)
        reward += self.money.addDreamDust(y, trx)
      }
      reward
    }

    def finishTutorial(): Unit = {
      import Tutorial._
      if (self.level >= endLevel) {
        Logger.error(s"user#${self.id} level=${self.level} #finishTutorial")
      } else {
        self.tutorialStep(config.TUTORIAL_STEPS + 1)
        self.level(endLevel)
        self.money.coins(endMoneyValue).rubies(endRubieValue).dreamDust(endDustValue)
        self.totalExperience(endTotalExperience).currentLevelExperience(endCurrentExperience)

        self.createProduction(VOLSHEBNAYA_PECHKA)
        val nasest = new PetProduction().owner(self).buildingType(Building.VOLSHEBNYJ_NASEST.typeId)
        nasest.addPet()
        nasest.addPet()
        nasest.addPet()
        self.petProductions += nasest
        fillTasksAndMissions()
        finishSomeMissions()
      }
    }

    def getBuff(typeId: Int): Buff = {
      self.buffs.getOrElse(typeId, {
        val buff = Buff(typeId, self)
        self.buffs += typeId -> buff
        buff
      })
    }

    def findBuff(typeId: Int): Option[Buff] = BuffType.withTypeOption(typeId).map(_ => getBuff(typeId))

    def open(chest: Chest, extraKey: Boolean = false): Reward = {
      val reward = Reward()
      if (self.keys > 0 || extraKey) {
        self.keys(math.max(0, self.keys - 1))
        self.chests -= chest

        val level = chest.missionGenerator.map(_.level).getOrElse(1)
        val coins = math.round(math.pow(level, 1.2)*50)

        reward += self.money.addCoins(coins, TransactionGenerator.FROM_CHEST_TX.get)
      }
      reward
    }
  }
}
