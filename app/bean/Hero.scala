package bean

import com.google.common.collect.Sets
import components.HibernateSessionAware
import hibernate.HibernateSetAdapter
import model.Reward
import model.buff.BuffFunction
import model.clan.ClanBonus
import model.hero.{Ability, HeroTemplate}
import util.RandomUtil

import scala.beans.BeanProperty

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.10.2016.
 */
class Hero extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var HeroType: java.lang.Integer = _

  @BeanProperty
  protected var Level: java.lang.Integer = 1

  @BeanProperty
  protected var PendingLevel: java.lang.Integer = _

  @BeanProperty
  protected var Abilities: java.util.Set[Int] = _

  @BeanProperty
  protected var TotalExperience: java.lang.Long = 0L

  @BeanProperty
  protected var CurrentLevelExperience: java.lang.Long = 0L

  @BeanProperty
  protected var Mission: Mission = _

  @BeanProperty
  protected var Log: java.util.Set[HeroLog] = _

  lazy val log = new HibernateSetAdapter[HeroLog]({
    if (Log == null) Log = Sets.newLinkedHashSet()
    Log
  })

  lazy val abilities = new HibernateSetAdapter[Int]({
    if (Abilities == null) Abilities = Sets.newLinkedHashSet(); Abilities
  })

  // Getters
  def abilityObjs = abilities.map(Ability.withType)
  def owner = Owner
  def heroType: Int = HeroType
  def level: Int = Level
  def pendingLevel: Int = Option(PendingLevel) map Int.unbox getOrElse 0
  def totalExperience = TotalExperience
  def currentLevelExperience = CurrentLevelExperience
  def nonActive: Boolean = !active
  def mission: Option[Mission] = Option(Mission)

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def heroType(arg: Int) = update {HeroType = arg}
  def level(arg: Int): Reward = {Level = arg; Reward().setHeroLevel(heroType, arg) }
  def pendingLevel(arg: Int) = update {PendingLevel = arg}
  def totalExperience(arg: Long) = update {TotalExperience = arg}
  def currentLevelExperience(arg: Long) = update {CurrentLevelExperience = arg}
  def mission(arg: Option[Mission]) = update {Mission = arg.orNull}
  def active: Boolean = mission.exists(mission => mission.isInProgress || mission.isCompleted)

  // процент до уровня
  def currentExpPercent: Int = {
    val x = 1.0 * currentLevelExperience / Hero.experienceToNextLevel(level) * 100
    Math.min(100, Math.round(x)).asInstanceOf[Int]
  }

  def template = HeroTemplate.withType(heroType)

  def maxLevel = owner.level + 10

  def hasAbility(ability: Ability): Boolean = abilities.contains(ability.typeId)

  def addRandomAbility(): Reward = {
    val available = template.availableAbilities.filterNot(hasAbility)
    val reward = Reward()
    if (available.nonEmpty) {
      val randomAbility = RandomUtil.random(available)
      abilities += randomAbility.typeId
      reward.addHeroAbility(template, randomAbility)
    }
    reward
  }

  def incLevel(n: Int) = level(level + n)
  def incPendingLevel(n: Int) = pendingLevel(pendingLevel + n)
  def incCurrentLevelExperience(x: Long) = currentLevelExperience(currentLevelExperience + x)
  def incTotalExperience(x: Long) = totalExperience(totalExperience + x)

  def commitLevel(): Reward = {
    val reward = Reward()
    if (pendingLevel > 0) {
      incLevel(1)
      incPendingLevel(-1)
      reward.setHeroLevel(heroType, 1)
    }
    reward
  }

  def canBeInactivated(freeHeroes: Set[Int]): Boolean = freeHeroes.contains(id)

  def canUpgraded: Boolean = abilities.size < 2 && level >= 20

  def color: String = if (abilities.size > 1) "epic" else "enabled"
}

object Hero {

  def apply(user: GameUser, template: HeroTemplate): Hero = {
    val hero = new Hero
    hero.owner(user)
    hero.heroType(template.typeId)
    hero.abilities addAll template.initialAbilities.map(_.typeId)
    hero
  }

  implicit class HeroOps(self: Hero)(implicit hib: HibernateSessionAware) {
    def addExp(e: Long, reward: Reward = Reward()): Reward = {
      var bonus = self.owner.clan.map(clan => math.round(clan.heroBonus * e)).getOrElse(0L)
      self.owner.buffs.values.filter(_.isActive).foreach(_.buffType.buffFunction match {
        case f: BuffFunction.HeroExpFunction => bonus += f(e)
        case _ =>
      })
      bonus = bonus + math.round(e * self.owner.clan.map(_.valueOf(ClanBonus.HeroExp)).getOrElse(0.0))
      addExpInternal(e, bonus, reward)
    }

    val writeLog = false // TODO remove in production
    def addExpInternal(e: Long, bonus: Long, r: Reward): Reward = {
      val log = new HeroLog
      log.hero(self)
      log.creationTime(self.now)
      log.userLevel(self.owner.level)
      log.totalExpWas(self.totalExperience)
      log.currentExpWas(self.currentLevelExperience)

      val level = self.level + self.pendingLevel
      log.levelWas(level)

      val remain = Hero.experienceToNextLevel(level) - self.currentLevelExperience
      log.remainExp(remain)

      val exp = e + bonus
      log.baseExp(exp) // 8/12

      val reward = Reward()

      if (level == self.maxLevel) {
        log.levelBecome(level)
        log.currentExpBecome(self.currentLevelExperience)
        log.totalExpBecome(self.totalExperience)
        log.charged(0L)

        self.incTotalExperience(exp)
        reward += self.owner.addExp(exp)
        reward.addHeroExp(self, exp)

      } else if (exp < remain) {
        self.incCurrentLevelExperience(exp)
        self.incTotalExperience(exp)
        reward.addHeroExp(self, exp)

        log.levelBecome(level)
        log.currentExpBecome(self.currentLevelExperience)
        log.totalExpBecome(self.totalExperience)
        log.charged(exp)

      } else {
        val rest = exp - remain
        self.currentLevelExperience(0)
        self.incTotalExperience(remain)
        self.incPendingLevel(1)

        log.levelBecome(level + 1)
        log.currentExpBecome(self.currentLevelExperience)
        log.totalExpBecome(self.totalExperience)
        log.charged(remain)

        reward.addHeroExp(self, remain)
        reward.addHeroLevel(self)
        reward += addExpInternal(rest, 0, Reward())
      }

      if (writeLog) self.log += log
      reward += r
    }
  }

  // Опыт до следующего уровня для героя.
  def experienceToNextLevel(level: Int): Long = 5 * math.round(2 * math.pow (1.20, level))
}
