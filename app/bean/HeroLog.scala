package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * Created by Salewan on 21.08.2017.
  */
class HeroLog extends Bean {

  @BeanProperty
  protected var Hero: Hero = _

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var BaseExp: java.lang.Long = _

  @BeanProperty
  protected var Charged: java.lang.Long = _

  @BeanProperty
  protected var RemainExp: java.lang.Long = _

  @BeanProperty
  protected var UserLevel: java.lang.Integer = _

  @BeanProperty
  protected var LevelWas: java.lang.Integer = _
  @BeanProperty
  protected var LevelBecome: java.lang.Integer = _

  @BeanProperty
  protected var CurrentExpWas: java.lang.Long = _
  @BeanProperty
  protected var CurrentExpBecome: java.lang.Long = _

  @BeanProperty
  protected var TotalExpWas: java.lang.Long = _
  @BeanProperty
  protected var TotalExpBecome: java.lang.Long = _

  // getters
  def hero = Hero
  def creationTime: Long = CreationTime.getTime
  def baseExp: Long = BaseExp
  def charged: Long = Charged
  def userLevel: Int = UserLevel
  def levelWas: Int = LevelWas
  def levelBecome: Int = LevelBecome
  def currentExpWas: Long = CurrentExpWas
  def currentExpBecome: Long = CurrentExpBecome
  def totalExpWas: Long = TotalExpWas
  def totalExpBecome: Long = TotalExpBecome
  def remainExp: Long = RemainExp

  // setters
  def hero(arg: Hero) = update {Hero = arg}
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def baseExp(arg: Long) = update {BaseExp = arg}
  def charged(arg: Long) = update {Charged = arg}
  def userLevel(arg: Int) = update {UserLevel = arg}
  def levelWas(arg: Int) = update {LevelWas = arg}
  def levelBecome(arg: Int) = update {LevelBecome = arg}
  def currentExpWas(arg: Long) = update {CurrentExpWas = arg}
  def currentExpBecome(arg: Long) = update {CurrentExpBecome = arg}
  def totalExpWas(arg: Long) = update {TotalExpWas = arg}
  def totalExpBecome(arg: Long) = update {TotalExpBecome = arg}
  def remainExp(arg: Long) = update {RemainExp = arg}
}
