package bean

import util.Implicits._

import scala.beans.BeanProperty
import scala.language.postfixOps

class IntervalCounter extends BeanTemplate {

  @BeanProperty
  protected var TimeMark: java.lang.Long = 0L

  def timeMark: Option[Long] = Option(TimeMark).map(Long.unbox)
  def timeMark(arg: Long): Unit = {
    TimeMark = Long.box(arg)
  }

  def maxPeriod: Long = 1 day

  def interval(max: Int): Long = maxPeriod / max

  def available(max: Int): Int = math.min(max,
    timeMark.map(t => ((now - t) / interval(max)).asInstanceOf[Int]    ).getOrElse(max)
  )

  def increment(max: Int): Unit = {
    val inc = 1
    val shift = inc * interval(max)

    val safeMark = timeMark.filter(tm => tm > 0 && now - tm <= maxPeriod).orElse(Some(now - maxPeriod)).filter(now - _ >= shift)
    if (safeMark.isEmpty) throw new RuntimeException
    else {
      timeMark(safeMark.get + shift)
    }
  }
}
