package bean

import java.util.Date

import bean.state.MissionState
import bean.state.MissionState._
import com.google.common.collect.{Maps, Sets}
import components.HibernateSessionAware
import hibernate.{DomainKeys, HibernateMapAdapter, HibernateSetAdapter}
import model.Reward
import model.clan.ClanBonus
import model.hero.Ability
import model.item.Ingredient
import model.mission.{MissionGenerator, Questor}
import util.Implicits._

import scala.beans.BeanProperty
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.10.2016.
  *         Поручение
  */
class Mission extends Bean {

  @BeanProperty
  protected var Version: java.lang.Integer = _

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var Closed: java.lang.Boolean = false

  @BeanProperty
  protected var GenerationTime: Date = _

  @BeanProperty
  protected var Complete: Date = _

  @BeanProperty
  protected var Slots: java.util.Set[MissionSlot] = _

  @BeanProperty
  protected var ActiveSlot: java.lang.Integer = _

  @BeanProperty
  protected var GenType: java.lang.Integer = _

  @BeanProperty
  protected var Level: java.lang.Integer = _

  @BeanProperty
  protected var HeroesNum: java.lang.Integer = _

  @BeanProperty
  protected var Duration: java.lang.Long = _

  @BeanProperty
  protected var Difficulty: java.lang.Integer = _

  @BeanProperty
  protected var Abilities: java.util.Map[Int, Double] = _

  @BeanProperty
  protected var QuestorType: java.lang.Integer = _

  @BeanProperty
  protected var Artefact: java.lang.Integer = _

  @BeanProperty
  protected var UserExp: java.lang.Long = _

  @BeanProperty
  protected var HeroExp: java.lang.Long = _

  @BeanProperty
  protected var Money: java.lang.Long = _

  @BeanProperty
  protected var OriginalOwner: GameUser = _

  @BeanProperty
  protected var FutureOwner: GameUser = _

  def copy: Mission = {
    val mis = new Mission
    mis.GenerationTime = getGenerationTime
    mis.Complete = getComplete
    mis.GenType = getGenType
    mis.Level = getLevel
    mis.HeroesNum = getHeroesNum
    mis.Duration = getDuration
    mis.Difficulty = getDifficulty
    mis.abilities.putAll(abilities.asImmutableMap)
    mis.QuestorType = getQuestorType
    mis.Artefact = getArtefact
    mis.UserExp = getUserExp
    mis.HeroExp = getHeroExp
    mis.Money = getMoney
    mis
  }

  lazy val abilities = new HibernateMapAdapter({
    if (Abilities == null) Abilities = Maps.newLinkedHashMap()
    Abilities
  }) with DomainKeys[Int, Double, Ability] {
    override def convert(key: Int): Ability = Ability.withType(key)
  }

  // Getters
  var fakeOwner: Option[GameUser] = None
  def owner = Option(Owner).orElse(fakeOwner).orNull
  def closed: Boolean = Closed
  // TODO make not-null
  def generator: MissionGenerator = Option(GenType) map Int.unbox flatMap MissionGenerator.find getOrElse MissionGenerator.all(0)
  def complete: Option[Long] = Option(Complete).map(_.getTime)
  def slots = new HibernateSetAdapter[MissionSlot]({if(Slots == null) Slots = Sets.newLinkedHashSet(); Slots})
  def duration: Long = Duration
  def heroesNum: Int = HeroesNum
  def level: Int = Level
  def difficulty: Int = Difficulty
  def activeSlot: Int = Option(Int.unbox(ActiveSlot)) getOrElse 1
  def questor: Questor = Questor withType Option(QuestorType).map(Int.unbox).getOrElse(0)
  def artefact: Option[Ingredient] = Option(Artefact) map Int.unbox flatMap Ingredient.find
  def heroExp: Option[Long] = Option(HeroExp) map Long.unbox
  def userExp: Option[Long] = Option(UserExp) map Long.unbox
  def money: Option[Long] = Option(Money) map Long.unbox
  def generationTime: Long = GenerationTime.getTime
  def originalOwner: Option[GameUser] = Option(OriginalOwner)
  def futureOwner: Option[GameUser] = Option(FutureOwner)

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def closed(arg: Boolean) = update {Closed = arg}
  def generator(arg: Int) = update {GenType = arg}
  def complete(arg: Long) = update {Complete = new Date(arg)}
  def duration(arg: Long) = update {Duration = arg}
  def heroesNum(arg: Int) = update {HeroesNum = arg}
  def level(arg: Int) = update {Level = arg}
  def difficulty(arg: Int) = update {Difficulty = arg}
  def activeSlot(arg: Int) = update {ActiveSlot = arg}
  def questor(arg: Option[Questor]) = update {QuestorType = arg.map(Int box _.typeId).orNull}
  def artefact(arg: Ingredient) = update {Artefact = arg.typeId}
  def heroExp(arg: Long) = update {HeroExp = arg}
  def userExp(arg: Long) = update {UserExp = arg}
  def money(arg: Long) = update {Money = arg}
  def generationTime(arg: Long) = update {GenerationTime = new Date(arg)}
  def originalOwner(arg: Option[GameUser]) = update {OriginalOwner = arg.orNull}
  def futureOwner(arg: Option[GameUser]) = update {FutureOwner = arg.orNull}

  def isCompleted: Boolean = complete.exists(_ <= now)

  def isInProgress: Boolean = state == MissionState.IN_PROGRESS

  def isNew: Boolean = state == MissionState.NEW

  private def bestIfAdd(inside: Seq[Hero], choice: Set[Hero]): Option[(Hero, Double)] = {
    choice.toSeq.map(hero => hero -> formula(hero +: inside)).sortBy(-_._2).headOption
  }

  def generateEmptySlots(): Unit = {
    if (slots.isEmpty) slots.addAll {
      for (i <- 1 to heroesNum) yield new MissionSlot().index(i)
    }
  }

  def start(): Unit = {
    complete(now + time)
  }

  def activeCount: Int = participants.size + owner.activeHeroesCount

  def activeCountColor: String = {
    val active = activeCount
    val max = owner.activeHeroesMax
    if (active >= max) "red"
    else if (max - active == 1) "major"
    else "green"
  }

  def insert(hero: Hero, slot: MissionSlot): Boolean = {
    val can = slot.nonEmptySlot || activeCount < owner.activeHeroesMax
    if (can) {
      slot.clear()
      slot.hero(hero)
      hero.mission(Some(this))
    }
    can
  }

  /**
    * Вставить данного героя в пустой слот или подобрать слот наилучшим образом
    */
  def insert(hero: Hero): Boolean = {
    val slots = this.slots.toSeq
    val participants = this.participants.toSet

    val slot =
    slots.find(_.hero.isEmpty) getOrElse {
      {

        for {
          slot <- slots
          slotHero <- slot.hero
        } yield slot -> formula(hero +: (participants - slotHero).toSeq)

      }.sortBy(-_._2).headOption.map(_._1).getOrElse(slots.head)

    }
    insert(hero, slot)
  }

  /**
    * Выбрать лучшего героя и вставить в данный слот.
    */
  def setupBestHero(slot: MissionSlot): Boolean = {
    val user = owner

    val hero = {
      val participants = slots.toSeq.filterNot(_ == slot).flatMap(_.hero)
      bestIfAdd(participants, user.getFreeHeroes).map(_._1)
    }
    hero.exists(hero => insert(hero, slot))
  }

  def getSlot(i: Int): Option[MissionSlot] = slots.find(_.index == i)

  def state: MissionState =
    if (closed) CLOSED
    else if (complete.isEmpty) NEW
    else if (complete.exists(time => time > System.currentTimeMillis())) IN_PROGRESS
    else COMPLETE

  def isAllSlotsEmpty: Boolean = slots.forall(_.hero.isEmpty)

  def participants: Seq[Hero] = slots.toSeq.flatMap(_.hero)

  def participatedHeroesCount = participants.size

  private def required = difficulty

  private def sum(hseq: Seq[Hero]): Double = abilities.domainKeyMap.map({case (a,v) => maxHeroSkillValue(a, hseq) * v}).sum

  private def maxHeroSkillValue(ability: Ability, hseq: Seq[Hero]): Double = {
    val skill = (0 +: hseq.flatMap(hero => hero.abilities.toSeq.find(_ == ability.typeId).map(_ => hero.level)) ).max
    if (skill > 0) skill + 10.0
    else skill
  }

  private def getClanBonus: Double = owner.clan.map(_.valueOf(ClanBonus.MissionChance)).getOrElse(0.0)

  /**
    * Формула шанса.
    */
  private def formula(hseq: Seq[Hero]): Double = {
    val required = this.required.toDouble
    val sum = this.sum(hseq)
    var chance = {
      if (required < sum) 1.0 + (sum - required) * 0.02
      else 1.0 + (sum - required) * 0.05
    }
    chance = chance + getClanBonus
    val minChance = this.minChance

    if (minChance > 0.0) math.min(maxChance, math.max(minChance, chance))
    else math.min(maxChance, chance)
  }

  /**
    * Минимальный шанс.
    */
  def minChance: Double = {
    var d = (15 - owner.level) * 0.05
    d = d + getClanBonus
    d
  }

  def maxChance = 2.0

  /**
    * Текущий шанс
    */
  def chance: Double = formula(participants)

  /**
    * Проценты, которые пишутся под героем.
    */
  def contribution(hero: Hero): Double = {
    val slots = this.slots.toSeq
    val participants = this.participants

    if (slots.exists(_.hero.isEmpty)) formula(hero +: participants) - chance
    else {
      val parSet = participants.toSet
      participants.map(par => formula(hero +: (parSet - par).toSeq)).max - chance
    }
  }

  /**
    * Вернёт героя, который лучшим образом увеличивает шанс.
    * @param inside герои, которые (как-бы) уже установлены в поручение
    * @param choice тестируемые герои
    * @return
    */
  private def fittestHero(inside: Seq[Hero], choice: Set[Hero]): Option[(Hero, Double)] = {
    if (inside.size < heroesNum) bestIfAdd(inside, choice)
    else {
      val insideSet = inside.toSet
      inside.flatMap(in => bestIfAdd((insideSet - in).toSeq, choice))
    }.sortBy(-_._2).headOption
  }

  /**
    *
    */
  private def bestChanceRec(inside: List[Hero], choice: Set[Hero]): List[Hero] = {
    if (inside.size == heroesNum || choice.isEmpty) inside
    else fittestHero(inside, choice) match {
      case Some((hero, _)) => bestChanceRec(hero :: inside, choice - hero)
      case _ => inside
    }
  }

  /**
    * Подсчёт лучшего шанса в поручении.
    */
  def idealizedChance: Double = {
    if (state != NEW) chance
    else formula(bestChanceRec(List(), owner.getFreeHeroes))
  }

  def time: Long = {
    duration / math.max(1, slots.count(_.hero.nonEmpty))
  }

  def isExpired: Boolean = generationTime + generator.expires <= now

  def expiresAfter: Long = math.max(0, generationTime + generator.expires - now)

  def assuredExp(win: Boolean): Double = {
    val x = 1.0 * duration / (1 minute)
    if (win) x
    else x / 4.0
  }

  def inScare: Boolean = {
    val expired = now - (generationTime + generator.expires)
    isExpired && expired > 0 && expired < config.SCARE_INTERVAL
  }

  def canBeenSent: Boolean = {
    originalOwner.isEmpty
  }

  def isIncoming: Boolean = originalOwner.nonEmpty

  def delete(): Unit = {
    slots.toSeq.foreach(_.clear())
    owner.missions -= this
  }
}

object Mission {

  def apply(): Mission = {
    new Mission().generationTime(System.currentTimeMillis())
  }

  implicit class MissionOps(self: Mission)(implicit hib: HibernateSessionAware) {
    def giveExperienceToHeroes(win: Boolean, mult: Double): Reward = {
      val reward = Reward()
      val givenExp = math.round(self.assuredExp(win) / self.participants.size * mult)

      self.slots.foreach(_.hero.foreach(reward += _.addExp(givenExp)))
      reward
    }
  }
}
