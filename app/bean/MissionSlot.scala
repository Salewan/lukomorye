package bean

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.10.2016.
  */
class MissionSlot {

  @BeanProperty
  protected var Id: java.lang.Integer = _
  def id(arg: Int): MissionSlot = { Id = arg; this }
  def id = Option(Id).map(Int.unbox(_)).getOrElse(0)

  @BeanProperty
  protected var Version: java.lang.Integer = _

  @BeanProperty
  protected var Hero: Hero = _
  def hero(arg: Hero): MissionSlot = { Hero = arg; this}
  def hero: Option[Hero] = Option(Hero)

  @BeanProperty
  protected var Index: java.lang.Integer = _
  def index: Int = Index
  def index(arg: Int): MissionSlot = {Index = arg; this}

  def clear(): Unit = {
    hero.foreach {hero =>
      hero.mission(None)
      Hero = None.orNull
    }
  }

  def isEmptySlot: Boolean = hero.isEmpty

  def nonEmptySlot: Boolean = !isEmptySlot
}
