package bean

import java.util.Date

import model.{TG, TransactionGenerator, TransactionType}

import scala.beans.BeanProperty

/**
  * Created by Salewan on 08.06.2017.
  */
class MoneyTransaction extends Bean {

  @BeanProperty
  var CreationTime: Date = _

  @BeanProperty
  protected var Amount: java.lang.Long = _

  @BeanProperty
  protected var TypeId: java.lang.Integer = _

  @BeanProperty
  protected var Param: java.lang.Integer = _

  @BeanProperty
  protected var Actor: GameUser = _

  @BeanProperty
  protected var Result: java.lang.Long = _

  @BeanProperty
  protected var Comment: String = _

  @BeanProperty
  protected var Currency: java.lang.Integer = _

  // Getters
  def creationTime: Long = CreationTime.getTime
  def amount: Long = Amount
  def typeId: Int = TypeId
  def actor: GameUser = Actor
  def result: Long = Result
  def comment: Option[String] = Option(Comment)
  def currency: Int = Currency
  def param: Option[Int] = Option(Param).map(Int.unbox)

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def amount(arg: Long) = update {Amount = arg}
  def typeId(arg: Int) = update {TypeId = arg}
  def actor(arg: GameUser) = update {Actor = arg}
  def result(arg: Long) = update {Result = arg}
  def comment(arg: Option[String]) = update {Comment = arg.orNull}
  def currency(arg: Int) = update {Currency = arg}
  def currency(arg: model.price.Currency) = update {Currency = arg.typeId}
  def param(arg: Option[Int]) = update {Param = arg.map(Int.box).orNull}

  def transactionType(arg: TransactionType) = update {TypeId = arg.typeId}

  def typeInWords: String = TransactionGenerator.withType(typeId).asInstanceOf[TG].name
}

object MoneyTransaction {
  def apply(user: GameUser): MoneyTransaction = {
    new MoneyTransaction().actor(user).creationTime(System.currentTimeMillis())
  }
}
