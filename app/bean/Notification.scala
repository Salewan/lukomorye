package bean

import java.util.Date

import components.context.PlayContext
import model.NotificationType
import play.api.mvc.AnyContent

import scala.beans.BeanProperty

/**
  * Created by Salewan on 25.05.2017.
  */
class Notification extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var TypeId: java.lang.Integer = _

  @BeanProperty
  protected var SendTime: Date = _

  @BeanProperty
  protected var Content: String = _

  // Getters
  def owner: GameUser = Owner
  def typeId: Int = TypeId
  def sendTime: Long = SendTime.getTime
  def content: String = Content

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def typeId(arg:  Int) = update {TypeId = arg}
  def sendTime(arg: Long) = update {SendTime = new Date(arg)}
  def content(arg: String) = update {Content = arg}

  def render(implicit ctx: PlayContext[AnyContent]) = {
    NotificationType.withType(typeId).render(this)
  }
}

object Notification {
  def apply(owner: GameUser, notificationType: NotificationType, content: String): Notification = {
    new Notification().owner(owner).sendTime(System.currentTimeMillis()).typeId(notificationType.typeId).content(content)
  }
}
