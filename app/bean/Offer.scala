package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.11.2016.
  */
class Offer extends Bean {

  @BeanProperty
  protected var Master: GameUser = _

  @BeanProperty
  protected var Candidate: GameUser = _

  @BeanProperty
  protected var TheDate: Date = _

  @BeanProperty
  protected var TheClan: Clan = _

  // Getters

  def master: Option[GameUser] = Option(Master)
  def candidate: GameUser = Candidate
  def date: Date = TheDate
  def clan: Clan = TheClan

  // Setters

  def master(arg: GameUser) = update {Master = arg}
  def candidate(arg: GameUser) = update {Candidate = arg}
  def date(arg: Long) = update {TheDate = new Date(arg)}
  def clan(arg: Clan) = update {TheClan = arg}


  def isOffer = master.isDefined

  def isRequest = master.isEmpty
}
