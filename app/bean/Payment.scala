package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * Created by Salewan on 11.07.2017.
  */
class Payment extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Amount: java.lang.Integer = 0

  @BeanProperty
  protected var Level: java.lang.Integer = 0

  @BeanProperty
  protected var TypeStr: String = _

  @BeanProperty
  protected var ExternalId: String = _

  @BeanProperty
  protected var Phone: String = _

  @BeanProperty
  protected var Canceled: java.lang.Boolean = false

  @BeanProperty
  protected var RealAmount: java.lang.Double = 0.0

  @BeanProperty
  protected var PaymentRequestId: java.lang.Long = _

  // Getters
  def owner: GameUser = Owner
  def creationTime: Long = CreationTime.getTime
  def amount: Int = Amount
  def level: Int = Level
  def typeStr: String = TypeStr
  def externalId: Option[String] = Option(ExternalId)
  def phone: Option[String] = Option(Phone)
  def canceled: Boolean = Canceled
  def realAmount: Double = RealAmount
  def paymentRequestId: Option[Long] = Option(PaymentRequestId).map(Long.unbox)

  // Setters
  def owner(arg: GameUser): T = update {Owner = arg}
  def creationTime(arg: Long): T = update {CreationTime = new Date(arg)}
  def amount(arg: Int): T = update {Amount = arg}
  def level(arg: Int): T = update {Level = arg}
  def typeStr(arg: String): T = update {TypeStr = arg}
  def externalId(arg: Option[String]): T = update {ExternalId = arg.orNull}
  def phone(arg: Option[String]): T = update {Phone = arg.orNull}
  def canceled(arg: Boolean): T = update {Canceled = arg}
  def realAmount(arg: Double): T = update {RealAmount = arg}
  def paymentRequestId(arg: Option[Long]): T = update {PaymentRequestId = arg.map(Long.box).orNull}

}

object Payment {
  def apply(): Payment = new Payment
}
