package bean

import java.util.Date

import components.HibernateSessionAware
import model.Reward
import util.TimeUtil

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 13.01.2017.
  */
class Pet extends Bean {

  @BeanProperty
  protected var Fullness: java.util.Date = _

  @BeanProperty
  protected var PetProduction: PetProduction = _

  // Getters
  def fullness: Option[Long] = Option(Fullness) map (_.getTime)
  def petProduction = PetProduction

  // Setters
  def fullness(arg: Option[Long]) = update {Fullness = arg.map(new Date(_)).orNull}
  def petProduction(arg: PetProduction) = update {PetProduction = arg}

  def building = petProduction.building

  def image: String = {
    val building = this.building
    val now = System.currentTimeMillis()
    if (isHungry) building.petHungry
    else if (isFull) building.petWalk
    else building.petFull
  }

  def isFull = fullness.exists(now >= _)

  def isWalk = fullness.exists(now < _)

  def isHungry = fullness.isEmpty

  def remainTime: Option[Long] = fullness.map(_ - now)

  def timeView: String = remainTime.map(TimeUtil.formatTime).getOrElse("")

  def food = building.food

  def product = building.product

  def interval = building.interval
}

object Pet {
  implicit class PetOps(self: Pet)(implicit hib: HibernateSessionAware) {

    def collect(): Reward = {
      val user = self.petProduction.owner
      self.fullness(None)
      user.addIngredient(self.product) += user.addExp(self.product.farmingExp)
    }

    def doFeed(force: Boolean = false): Boolean = {
      val owner = self.petProduction.owner
      val food = self.food
      self.isHungry && (force || owner.barn.getOrElse(food.typeId, 0)> 0) && {
        if (!force) {
          owner.addIngredient(food, -1)
        }
        self.fullness(Some(System.currentTimeMillis() + self.interval))
        true
      }
    }
  }
}
