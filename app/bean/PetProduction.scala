package bean

import com.google.common.collect.Sets
import hibernate.HibernateSetAdapter
import model.Building
import model.price.{Price, RubyPrice}

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 13.01.2017.
  */
class PetProduction extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var BuildingType: java.lang.Integer = _

  @BeanProperty
  protected var Pets: java.util.Set[Pet] = _

  lazy val pets = new HibernateSetAdapter[Pet]({
    if (Pets == null) Pets = Sets.newLinkedHashSet()
    Pets
  })

  // Getters
  def owner = Owner
  def buildingType: Int = BuildingType

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def buildingType(arg: Int) = update {BuildingType = arg}


  def building = Building.petHouse(buildingType)

  def hasFull = pets.exists(_.isFull)

  def canBuyNewPet: Boolean = pets.size < util.LevelUtil.petsByLevel(building, owner.level)

  def newPetPrice: Option[Price] = {
    if (pets.size == 10) None
    else Some(RubyPrice(8))
  }

  def addPet(): Unit = pets += new Pet().petProduction(this)

  def addTutorialPet(): Unit = pets += new Pet().petProduction(this).fullness(Some(now))
}
