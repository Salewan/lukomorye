package bean

import java.util.Date

import com.google.common.collect.{Lists, Maps}
import components.HibernateSessionAware
import hibernate.{DomainKeys, HibernateBagAdapter, HibernateMapAdapter}
import model.item.{Gradual, Ingredient, Product}
import model.{Building, Reward}
import util.{NumberUtils, TimeUtil}

import scala.annotation.tailrec
import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.01.2017.
  */
class Production extends Bean {

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var BuildingType: java.lang.Integer = _

  @BeanProperty
  protected var Slots: java.util.List[ProductionSlot] = _

  @BeanProperty
  protected var Basket: java.util.Map[Int, Int] = _

  @BeanProperty
  protected var BuildTime: Date = _

  @BeanProperty
  protected var Visited: java.lang.Boolean = _

  @BeanProperty
  protected var ParCount: java.lang.Integer = _

  @BeanProperty
  protected var BasketFlag: java.lang.Boolean = _

  @BeanProperty
  protected var ReadyTime: Date = _

  @BeanProperty
  protected var Selection: java.lang.Integer = _

  @BeanProperty
  protected var BriefView: java.lang.Boolean = _

  lazy val slots = new HibernateBagAdapter[ProductionSlot]({
    if (Slots == null) Slots = Lists.newArrayList()
    Slots
  })

  lazy val basket = new HibernateMapAdapter[Int, Int]({
    if (Basket == null) Basket = Maps.newLinkedHashMap()
    Basket
  }) with DomainKeys[Int, Int, Product] {
    override def convert(key: Int): Product = Product(key)
  }

  // Getters
  def owner = Owner
  def buildingType: Int = BuildingType
  def readyTime: Option[Long] = Option(ReadyTime).map(_.getTime)
  def buildTime: Long = BuildTime.getTime
  def parCount: Int = ParCount
  def basketFlag: Boolean = Option(BasketFlag) exists Boolean.unbox
  def visited: Boolean = Option(Visited) exists Boolean.unbox
  def selection: Int = Option(Selection) map Int.unbox getOrElse building.products(0).typeId
  def briefView: Boolean = Option(BriefView) exists Boolean.unbox

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def buildingType(arg: Int) = update {BuildingType = arg}
  def readyTime(arg: Option[Long]) = update {ReadyTime = arg.map(new Date(_)).orNull}
  def buildTime(arg: Long) = update {BuildTime = new Date(arg)}
  def parCount(arg: Int) = update {ParCount = arg}
  def basketFlag(arg: Boolean) = update {BasketFlag = arg}
  def visited(arg: Boolean) = update {Visited = arg}
  def selection(arg: Int) = update {Selection = arg}
  def briefView(arg: Boolean) = update {BriefView = arg}

  def toggleBriefView() = briefView(!briefView)

  def getQueue: Seq[ProductionSlot] = slots.toSeq.sortWith { case (o1, o2) =>
    val t1 = o1.enqueueTime.getOrElse(Long.MaxValue)
    val t2 = o2.enqueueTime.getOrElse(Long.MaxValue)
    t1 < t2 || t1 == t2 && o1.id < o2.id
  }

  private def collect(productType: Int): Unit = {
    basket.update(productType)(_.getOrElse(0) + 1)
    basketFlag(true)
  }

  private def tryStartPending(since: Option[Long]): Unit = {
    val queue = getQueue
    val inProgress = queue.count(_.isInProgress)
    if (inProgress < parCount) queue.find(_.isPending).foreach(_.started(since))
  }

  @tailrec
  private def innerCheck(): Unit = {
    val ready = getQueue.find(_.isReady)
    if (ready.isEmpty) ()
    else {
      val slot = ready.get
      slot.productType.foreach(collect)
      val since = slot.readySince
      slot.dequeue()

      tryStartPending(since)
      innerCheck()
    }
  }

  def checkReady(): Unit = {
    innerCheck()
    readyTime(NumberUtils.min(slots.toSeq.flatMap(_.readySince)))
  }

  private def createPending(id: Int, time: Long): Unit = {
    getQueue.find(_.isEmpty).foreach { slot =>
      slot.productType(Some(id))
      slot.interval(Some(time))
      slot.enqueueTime(Some(now))
    }
  }

  def enqueue(ingredient: Ingredient with Gradual): Unit = enqueue(ingredient, 1)

  def enqueue(ingredient: Ingredient with Gradual, count: Int): Unit = {
    innerCheck()
    for(_ <- 1 to count) {
      createPending(ingredient.typeId, ingredient.duration)
      tryStartPending(Some(now))
    }
    readyTime(NumberUtils.min(slots.toSeq.flatMap(_.readySince)))
  }

  def timeLeft: Option[Long] = {
    val times = slots.iter().flatMap(_.timeLeft)
    if (times.isEmpty) None
    else Some(times.min)
  }

  def timeLeftStr: String = timeLeft map TimeUtil.formatTime getOrElse ""

  def getReadyBasket: Seq[Ingredient] = basket.toSeq.flatMap { case (typeId, count) =>
    val p = Ingredient(typeId)
    for (_ <- 1 to count) yield p
  }

  def basketSize: Int = basket.map(_._2).sum

  def howManyCanMake: Int = (slots.filter(_.isEmpty).size - basketSize) max 0

  def canMakeMore: Boolean = canMakeMore(1)

  def canMakeMore(count: Int): Boolean = howManyCanMake >= count

  def hasPlus: Boolean = basketFlag || readyTime.exists(_ <= now)

  def timeToBuild: Option[Long] = owner.getProduction(buildingType).map(_.buildTime).filter(_ > now).map(_ - now)

  def building = Building.prodFactory(buildingType)

  def visitWarn: Boolean = isBuilt && !visited

  def visit(): Unit = if (visitWarn) visited(true)

  def isBuilt: Boolean = buildTime < now

  def isFeedMill: Boolean = buildingType == Building.FEED_MILL.typeId

  def isQueueFull: Boolean = slots.filter(_.isEmpty).isEmpty
}

object Production {

  val dirtSorter: ((_, Seq[ProductionSlot]), (_, Seq[ProductionSlot])) => Boolean = (a,b) => {
    val seq1 = a._2
    val seq2 = b._2
    NumberUtils.min(seq1.flatMap(_.timeLeft)).getOrElse(0L) < NumberUtils.min(seq2.flatMap(_.timeLeft)).getOrElse(0L)
  }

  implicit class ProductionOps(self: Production)(implicit hib: HibernateSessionAware) {

    def takeProduct(product: Product): Reward = takeProduct(product, 1)

    def takeProduct(ingredient: Ingredient, claimCount: Int): Reward = {
      val reward = Reward()
      val id = ingredient.typeId
      self.basket.get(id).filter(_ > 0).foreach {inBasket =>
        val count = (1 max claimCount) min inBasket

        self.basket.update(id, inBasket - count)
        self.basketFlag(self.basket.exists(_._2 > 0))

        val owner = self.owner
        reward += owner.addIngredient(ingredient, if (self.isFeedMill) count * 3 else count)
        reward += owner.addExp(ingredient.farmingExp * count)
      }
      reward
    }
  }
}
