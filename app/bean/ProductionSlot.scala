package bean

import java.util.Date

import model.item.Ingredient
import util.TimeUtil

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 13.02.2017.
  */
class ProductionSlot extends Bean {

  @BeanProperty
  protected var Production: Production = _

  @BeanProperty
  protected var ProductType: java.lang.Integer = _

  @BeanProperty
  protected var Interval: java.lang.Long = _

  @BeanProperty
  protected var EnqueueTime: Date = _

  @BeanProperty
  protected var Started: Date = _

  // Getters
  def production: Production = Production
  def productType: Option[Int] = Option(ProductType).map(Int.unbox)
  def interval: Option[Long] = Option(Interval) map Long.unbox
  def enqueueTime: Option[Long] = Option(EnqueueTime).map(_.getTime)
  def started: Option[Long] = Option(Started).map(_.getTime)

  // Setters
  def production(arg: Production) = update {Production = arg}
  def productType(arg: Option[Int]) = update {ProductType = arg.map(Int.box).orNull}
  def interval(arg: Option[Long]) = update {Interval = arg.map(Long.box).orNull}
  def enqueueTime(arg: Option[Long]) = update {EnqueueTime = arg.map(new Date(_)).orNull}
  def started(arg: Option[Long]) = update {Started = arg.map(new Date(_)).orNull}

  def dequeue(): Unit = productType(None).interval(None).enqueueTime(None).started(None)

  def image: Option[String] = productType.flatMap(Ingredient.find).map(_.image)

  def readySince: Option[Long] = for (s <- started; i <- interval) yield s + i

  def timeLeft: Option[Long] = readySince.map(_ - now).filter(_ > 0)

  def timeLeftStr: String = timeLeft map TimeUtil.formatTime getOrElse ""

  def isReady: Boolean = readySince.exists(_ <= now)

  def isEmpty: Boolean = productType.isEmpty && interval.isEmpty && enqueueTime.isEmpty && started.isEmpty

  def isPending: Boolean = productType.nonEmpty && interval.nonEmpty && enqueueTime.nonEmpty && started.isEmpty

  def isInProgress: Boolean = started.nonEmpty

  def becomeReady(): Unit = for (s <- started; i <- interval) {started(Some(now - i))}
}
