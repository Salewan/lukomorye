package bean

import java.util.Date

import com.google.common.collect.Sets
import hibernate.HibernateSetAdapter
import org.apache.commons.lang3.StringUtils
import org.joda.time.{DateTime, Days}

import scala.beans.BeanProperty

/**
 * @author Sergey Lebedev (salewan@gmail.com) 09.03.2016.
 */
class Profile extends AccountEntity {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Password: String = _

  @BeanProperty
  protected var BanComment: String = _

  @BeanProperty
  protected var BannedBy: User = _

  @BeanProperty
  protected var BanLog: java.util.Set[UserBanRecord] = _

  @BeanProperty
  protected var Domain: String = _

  @BeanProperty
  protected var RegistrationIp: String = _

  @BeanProperty
  protected var LastIp: String = _

  @BeanProperty
  protected var UserAgent: String = _

  @BeanProperty
  protected var OperaUserAgent: String = _

  @BeanProperty
  protected var VirusId: java.lang.Integer = _

  @BeanProperty
  protected var About: String = _

  @BeanProperty
  protected var Email: String = _

  lazy val banLog = new HibernateSetAdapter[UserBanRecord]({
    if (BanLog == null) BanLog = Sets.newLinkedHashSet()
    BanLog
  })

  // Getters
  def creationTime: Long = CreationTime.getTime
  def password = Password
  def banComment: Option[String] = Option(BanComment)
  def bannedBy: Option[User] = Option(BannedBy)
  def domain: String = Option(Domain).getOrElse("")
  def registrationIp: String = Option(RegistrationIp).getOrElse("undefined")
  def lastIp: String = Option(LastIp).getOrElse("undefined")
  def userAgent: String = Option(UserAgent).getOrElse("undefined")
  def operaUserAgent: Option[String] = Option(OperaUserAgent)
  def virusId: Int = Option(VirusId) map Int.unbox getOrElse id
  def about: Option[String] = Option(About)
  def email: Option[String] = Option(Email)

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def password(arg: String) = update {Password = arg}
  def banComment(arg: String) = update {BanComment = arg}
  def bannedBy(arg: User) = update {BannedBy = arg}
  def domain(arg: String) = update {Domain = arg}
  def registrationIp(arg: String) = update {RegistrationIp = arg}
  def lastIp(arg: String) = update {LastIp = arg}
  def userAgent(arg: Option[String]) = update {UserAgent = arg.orNull}
  def operaUserAgent(arg: Option[String]) = update {OperaUserAgent = arg.orNull}
  def virusId(arg: Option[Int]) = update {VirusId = arg.map(Int.box).orNull}
  def about(arg: Option[String]) = update {About = arg.map(StringUtils.trimToNull).orNull}
  def email(arg: Option[String]) = update {Email = arg.orNull}

  def clearBanComment() = update {BanComment = None.orNull}
  def clearBannedBy() = update {BannedBy = None.orNull}

  def daysInGame: Int = Days.daysBetween(new DateTime(creationTime).toLocalDate, new DateTime(now).toLocalDate).getDays
}
