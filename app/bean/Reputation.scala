package bean

import model.{ReputationModel, Reward, TransactionGenerator}
import util.WeekCounters
import ReputationModel._
import components.HibernateSessionAware
import model.clan.ClanBonus

import scala.beans.BeanProperty
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 26.11.2016.
  */
class Reputation extends Bean {

  @BeanProperty
  protected var Version: java.lang.Integer = _

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var ReputationType: java.lang.Integer = _

  @BeanProperty
  protected var Value: java.lang.Integer = _ // legacy field, RepForRating instead

  @BeanProperty
  protected var RepForLvl: java.lang.Long = _

  @BeanProperty
  protected var RepForRating: java.lang.Long = _

  @BeanProperty
  protected var Lvl2Earned: java.lang.Boolean = _

  // Getters
  def owner: GameUser = Owner
  def reputationType: Int = ReputationType
  def value: Int = Value
  def repForLvl: Long = Option(RepForLvl).map(Long.unbox).getOrElse(0L)
  def repForRating: Long = Option(RepForRating).map(Long.unbox).getOrElse(0L)
  def lvl2Earned: Boolean = Option(Lvl2Earned).exists(Boolean.unbox)

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def reputationType(arg: ReputationModel) = update {ReputationType = arg.typeId}
  def value(arg: Int) = update {Value = arg}
  def repForLvl(arg: Long) = update {RepForLvl = arg}
  def repForRating(arg: Long) = update {RepForRating = arg}
  def lvl2Earned(arg: Boolean) = update {Lvl2Earned = arg}

  def reputation: ReputationModel = ReputationModel.withType(reputationType)

  def level: Int = {
    1 :: Nil ::: (for ((test, index) <- Reputation.EXPERIENCES.zipWithIndex if test <= repForLvl) yield index + 2) last
  }

  def getClanBonus(clan: Clan): Double = {
    Option(
      if (reputation == ReputationModel.SALTAN) ClanBonus.SaltanBonus
      else if (reputation == ReputationModel.PADISHAH) ClanBonus.PadishahBonus
      else if (reputation == ReputationModel.KOSHEY) ClanBonus.KosheyBonus
      else null
    ).map(clan.valueOf).getOrElse(0.0)
  }

  def changeValue(arg: Int)(implicit hibernateSessionAware: HibernateSessionAware): Reward = {
    val reward = Reward()

    if (arg > 0) {

      var x = arg
      for(clan <- owner.clan) {
        val N = ReputationModel.values.size
        var mul = clan.allianceType.map(al => if (al == reputationType) 1 + 0.5 * (N - 1) else 0.5).getOrElse(1.0)
        mul = mul + getClanBonus(clan)
        x = math.ceil(x * mul).asInstanceOf[Int]
        clan.addExp(x, reputation, Some(owner))
        WeekCounters.addWeekCounter(owner.clanWeekExp, x)
      }

      value(value + x)
      reward.addReputation(reputationType, x)

      internalChange(x, reward)
    }

    reward
  }

  private def internalChange(arg: Int, reward: Reward)(implicit hibernateSessionAware: HibernateSessionAware): Reward = {
    val oldLevel = level
    repForLvl(math.min(Reputation.MAX_FOR_LVL, repForLvl + arg))
    val newLevel = level

    checkForExtraShopSlots()
    checkForSingleReward(reward)

    repForRating(repForRating + arg)

    if (newLevel > oldLevel) {
      reward.addReputationLevel(model, newLevel)
    }

    reward
  }

  private def checkForSingleReward(reward: Reward)(implicit hibernateSessionAware: HibernateSessionAware): Reward = {
    if (!lvl2Earned && level >= 3) {
      lvl2Earned(true)
      if (reputation == SALTAN) reward += owner.addExp(25000)
      else if (reputation == PADISHAH) reward += owner.money.addCoins(30000, TransactionGenerator.REPUTATION_COINS_TX.get)
      else if (reputation == KOSHEY) reward += owner.money.addDreamDust(1250, TransactionGenerator.REPUTATION_DUST_TX.get)
    }
    reward
  }

  private def checkForExtraShopSlots(): Unit = {
    if (reputation == ReputationModel.PADISHAH) {
      val max = addsExtraShopSlots + config.SHOP_SLOTS
      val cur = owner.buyList.size
      val more = math.max(0, max - cur)
      for (i <- 1 to more) {
        owner.sellList += owner.fillSellSlot(new SellSlot().owner(owner).generationTime(0L))
        owner.buyList += owner.fillBuySlot(new BuySlot().owner(owner).generationTime(0L))
      }
    }
  }

  def addsExtraActiveHero: Int = {
    if (level >= 4) 1
    else 0
  }

  def addsExtraShopSlots: Int = {
    if (reputation == ReputationModel.PADISHAH) {
      if (level >= 2 && level < 5) 1
      else if (level >= 5) 4
      else 0
    } else 0
  }

  def addsExtraBarnSpace: Int = {
    if (reputation == ReputationModel.SALTAN) {
      if (level >= 2 && level < 5) 5
      else if (level >= 5) 15
      else 0
    } else 0
  }

  def dustBankEnabled: Boolean = level >= 2 && reputation == ReputationModel.KOSHEY

  def dustBankCapacity: Int = {
    if (reputation == ReputationModel.KOSHEY) {
      if (level >= 2 && level < 5) 50
      else if (level >= 5) 150
      else 0
    } else 0
  }

  def model: ReputationModel = ReputationModel.withType(reputationType)

  def currentLevelExp: Long = {
    def calc(list: List[Long], x: Long): Long = {
      if (list.isEmpty || repForLvl < list.head) x
      else calc(list.tail, list.head)
    }
    repForLvl - calc(Reputation.EXPERIENCES, 0)
  }

  def expToNextLevel: Long = {
    val list: List[Long] = 0 :: Reputation.EXPERIENCES
    if (level >= list.length) 0L
    else list(level) - list(level - 1)
  }

  def percentForNextLevel: Double = math.floor(currentLevelExp * 100.0 / expToNextLevel)
}

object Reputation {
  // 2ур - 30к, 3ур - 70к, 4ур - 150к, 5ур - 500к
  val EXPERIENCES: List[Long] = 30000L :: 100000L :: 250000L :: 750000L :: Nil
  val MAX_FOR_LVL: Long = EXPERIENCES.sum
}
