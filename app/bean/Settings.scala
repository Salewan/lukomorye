package bean

import components.{DefaultWidth, WindowWidth}
import controllers.SettingsData

import scala.beans.BeanProperty

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
class Settings extends AccountEntity {

  @BeanProperty
  protected var ConfirmSmallSpendings: java.lang.Boolean = _

  @BeanProperty
  protected var Width: java.lang.Integer = _

  @BeanProperty
  protected var ClanMembersShown: java.lang.Boolean = _

  @BeanProperty
  protected var DoNotReceiveClanInvitations: java.lang.Boolean = _

  @BeanProperty
  protected var OnlyFriendsMessages: java.lang.Boolean = _

  @BeanProperty
  protected var ManageBlockExpanded: java.lang.Boolean = _

  @BeanProperty
  protected var ShowNewMailNotifications: java.lang.Boolean = _

  @BeanProperty
  protected var ProductDetailsShown: java.lang.Boolean = _

  @BeanProperty
  protected var DetailedInventar: java.lang.Boolean = _

  @BeanProperty
  protected var TaskViewBriefed: java.lang.Boolean = _

  @BeanProperty
  protected var SeedSelectViewBriefed: java.lang.Boolean = _

  @BeanProperty
  protected var ShopViewDetailed: java.lang.Boolean = _

  @BeanProperty
  protected var ShowAdminPanel: java.lang.Boolean = _

  @BeanProperty
  protected var ShowSupportPanel: java.lang.Boolean = _

  @BeanProperty
  protected var ShowDeveloperPanel: java.lang.Boolean = _

  @BeanProperty
  protected var MissionSortMode: java.lang.Integer = _

  @BeanProperty
  protected var MissionDetails: java.lang.Boolean = _

  @BeanProperty
  protected var BarnOrdering: java.lang.Boolean = _ // false - asc

  @BeanProperty
  protected var LabOrdering: java.lang.Boolean = _ // false - asc

  @BeanProperty
  protected var ClanDetailsShown: java.lang.Boolean = _

  @BeanProperty
  protected var ChatModerationMode: java.lang.Boolean = _

  @BeanProperty
  protected var MissionRemoveConfirmation: java.lang.Boolean = _

  @BeanProperty
  protected var ClanBonusesDetailes: java.lang.Boolean = _
  // Getters
  def onlyFriendsMessages: Boolean = Option(OnlyFriendsMessages) exists Boolean.unbox
  def confirmSmallSpendings = Option(ConfirmSmallSpendings) forall Boolean.unbox
  def clanMembersShown(): Boolean = Option(ClanMembersShown) exists Boolean.unbox
  def doNotReceiveClanInvitations(): Boolean = Option(DoNotReceiveClanInvitations) exists Boolean.unbox
  def manageBlockExpanded: Boolean = Option(ManageBlockExpanded) exists Boolean.unbox
  def showNewMailNotifications: Boolean = Option(ShowNewMailNotifications) exists Boolean.unbox
  def productDetailsShown: Boolean = Option(ProductDetailsShown) exists Boolean.unbox
  def detailedInvetnar: Boolean = Option(DetailedInventar) exists Boolean.unbox
  def taskViewBriefed: Boolean = Option(TaskViewBriefed) exists Boolean.unbox
  def seedSelectViewBriefed: Boolean = Option(SeedSelectViewBriefed) exists Boolean.unbox
  def shopViewDetailed: Boolean = Option(ShopViewDetailed) exists Boolean.unbox
  def showAdminPanel: Boolean = Option(ShowAdminPanel) exists Boolean.unbox
  def showSupportPanel: Boolean = Option(ShowSupportPanel) exists Boolean.unbox
  def missionSortMode: Int = Option(MissionSortMode) map Int.unbox getOrElse 0
  def missionDetails: Boolean = Option(MissionDetails) forall Boolean.unbox
  def barnOrdering: Boolean = Option(BarnOrdering) exists Boolean.unbox
  def labOrdering: Boolean = Option(LabOrdering) exists Boolean.unbox
  def showDeveloperPanel: Boolean = Option(ShowDeveloperPanel) exists Boolean.unbox
  def clanDetailsShown: Boolean = Option(ClanDetailsShown) forall Boolean.unbox
  def chatModerationMode: Boolean = Option(ChatModerationMode) exists Boolean.unbox
  def missionRemoveConfirmation: Boolean = Option(MissionRemoveConfirmation) forall Boolean.unbox
  def clanBonusesDetails: Boolean = Option(ClanBonusesDetailes) forall Boolean.unbox

  // Setters
  def onlyFriendsMessages(arg: Boolean) = update {OnlyFriendsMessages = arg}
  def confirmSmallSpendings(arg: Boolean) = update {ConfirmSmallSpendings = arg}
  def windowWidth(arg: WindowWidth) = update {Width = arg.code}
  def clanMembersShown(arg: Boolean) = update {ClanMembersShown = arg}
  def doNotReceiveClanInvitations(arg: Boolean) = update {DoNotReceiveClanInvitations = arg}
  def manageBlockExpanded(arg: Boolean) = update {ManageBlockExpanded = arg}
  def showNewMailNotifications(arg: Boolean) = update {ShowNewMailNotifications = arg}
  def productDetailsShown(arg: Boolean) = update {ProductDetailsShown = arg}
  def detailedInventar(arg: Boolean) = update {DetailedInventar = arg}
  def taskViewBriefed(arg: Boolean) = update {TaskViewBriefed = arg}
  def seedSelectViewBriefed(arg: Boolean) = update {SeedSelectViewBriefed = arg}
  def shopViewDetailed(arg: Boolean) = update {ShopViewDetailed = arg}
  def showAdminPanel(arg: Boolean) = update {ShowAdminPanel = arg}
  def showSupportPanel(arg: Boolean) = update {ShowSupportPanel = arg}
  def missionSortMode(arg: Int) = update {MissionSortMode = arg}
  def missionDetails(arg: Boolean) = update {MissionDetails = arg}
  def barnOrdering(arg: Boolean) = update {BarnOrdering = arg}
  def labOrdering(arg: Boolean) = update {LabOrdering = arg}
  def showDeveloperPanel(arg: Boolean) = update {ShowDeveloperPanel = arg}
  def clanDetailsShown(arg: Boolean) = update {ClanDetailsShown = arg}
  def chatModerationMode(arg: Boolean) = update {ChatModerationMode = arg}
  def missionRemoveConfirmation(arg: Boolean) = update {MissionRemoveConfirmation = arg}
  def clanBonusesDetails(arg: Boolean) = update {ClanBonusesDetailes = arg}

  def windowWidth(): WindowWidth = Option(Int.unbox(Width)) match {
    case None => DefaultWidth
    case Some(x) => WindowWidth(x)
  }

  def toggleMissionRemoveConfirmation() = missionRemoveConfirmation(!missionRemoveConfirmation)

  def toggleChatModerationMode() = chatModerationMode(!chatModerationMode)

  def toggleClanDetailsShown() = clanDetailsShown(!clanDetailsShown)

  def toggleDeveloperPanel() = showDeveloperPanel(!showDeveloperPanel)

  def toggleLabOrdering() = labOrdering(!labOrdering)

  def toggleBarnOrdering() = barnOrdering(!barnOrdering)

  def toggleMissionDetails() = missionDetails(!missionDetails)

  def toggleShowAdminPanel() = showAdminPanel(!showAdminPanel)

  def toggleShowSupportPanel() = showSupportPanel(!showSupportPanel)

  def toggleShopView() = shopViewDetailed(!shopViewDetailed)

  def toggleSeedSelectView() = seedSelectViewBriefed(!seedSelectViewBriefed)

  def toggleTaskView() = taskViewBriefed(!taskViewBriefed)

  def toggleInventar() = detailedInventar(!detailedInvetnar)

  def productDetailsToggle() = productDetailsShown(!productDetailsShown)

  def clanMembersShownToggle(): Unit = clanMembersShown(!clanMembersShown())

  def toggleManageBlockExpanding(): Unit = update {ManageBlockExpanded = !manageBlockExpanded}

  def getSettingsData: SettingsData = SettingsData(
    onlyFriendsMessages, confirmSmallSpendings, doNotReceiveClanInvitations, showNewMailNotifications,
    missionRemoveConfirmation
  )

  def applySettings(arg: SettingsData): Unit = {
    onlyFriendsMessages(arg.onlyFriendsMessages)
    confirmSmallSpendings(arg.smallSpendConfirm)
    doNotReceiveClanInvitations(arg.noReceiveGuildInvites)
    showNewMailNotifications(arg.showNewMailNotifications)
    missionRemoveConfirmation(arg.missionRemoves)
  }
}
