package bean

import java.util.Date

import model.item.Ingredient

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 20.03.2017.
  */
class ShopSlot extends Bean {

  @BeanProperty
  var Owner: GameUser = _

  @BeanProperty
  var GenerationTime: Date = new Date()

  @BeanProperty
  var ItemType: java.lang.Integer = _

  @BeanProperty
  var Amount: java.lang.Integer = _

  @BeanProperty
  var Value: java.lang.Long = _

  @BeanProperty
  var Sold: java.lang.Boolean = _

  @BeanProperty
  var Uniqueness: Long = 0

  // Getters
  def owner: GameUser = Owner
  def generationTime: Long = GenerationTime.getTime
  def item: Ingredient = Ingredient(ItemType)
  def amount: Int = Amount
  def value: Long = Value
  def sold: Boolean = Option(Sold) exists Boolean.unbox
  def unique: Long = Uniqueness

  // Setters
  def owner(arg: GameUser) = update {Owner = arg}
  def generationTime(arg: Long) = update {GenerationTime = new Date(arg)}
  def item(arg: Ingredient) = update {ItemType = arg.typeId}
  def amount(arg: Int) = update {Amount = arg}
  def value(arg: Long) = update {Value = arg}
  def sold(arg: Boolean) = update {Sold = arg}
  def unique(arg: Long) = update {Uniqueness = arg}

  def buyOrSell(n: Int): Unit = {
    if (n <= amount) {
      amount(amount - n)
      if (amount == 0)
        sold(true)
    }
  }
}
