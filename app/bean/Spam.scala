package bean

import java.util.Date

import bean.messages.personal.PrivateMessage

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.01.2017.
  */
class Spam extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var OriginalCreationTime: Date = _

  @BeanProperty
  protected var Actor: User = _

  @BeanProperty
  protected var Sender: User = _

  @BeanProperty
  protected var Text: String = _

  def this(actor: User, message: PrivateMessage) {
    this()
    this.creationTime(now)
    this.actor(actor)
    this.sender(message.author)
    this.originalCreationTime(message.creationTime)
    this.text(message.text)
  }
  // Getters
  def creationTime: Long = CreationTime.getTime
  def originalCreationTime: Long = OriginalCreationTime.getTime
  def actor = Actor
  def sender = Sender
  def text = Text

  // Setter
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def originalCreationTime(arg: Long) = update {OriginalCreationTime = new Date(arg)}
  def actor(arg: User) = update {Actor = arg}
  def sender(arg: User) = update {Sender = arg}
  def text(arg: String) = update {Text = arg}
}
