package bean

import java.util.Date

import com.google.common.collect.Sets
import components.HibernateSessionAware
import hibernate.HibernateSetAdapter
import model._
import model.task.TaskGenerator
import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.11.2016.
  */
class Task extends Bean {

  @BeanProperty
  protected var Version: java.lang.Integer = _

  @BeanProperty
  protected var Owner: GameUser = _

  @BeanProperty
  protected var GenerationTime: Date = _

  @BeanProperty
  protected var CompletionTime: Date = _

  @BeanProperty
  protected var Slots: java.util.Set[TaskSlot] = _

  @BeanProperty
  protected var GenType: java.lang.Integer = _

  @BeanProperty
  protected var OriginalOwner: GameUser = _

  @BeanProperty
  protected var FutureOwner: GameUser = _

  def copy: Task = {
    val tas = new Task
    tas.GenerationTime = getGenerationTime
    tas.CompletionTime = getCompletionTime
    tas.Slots = slotsCopy(tas)
    tas.GenType = getGenType
    tas
  }

  private def slotsCopy(task: Task): java.util.Set[TaskSlot] = {
    val iter = getSlots.iterator()
    val bag = Sets.newLinkedHashSet[TaskSlot]()
    while (iter.hasNext) {
      val slot = iter.next()
      bag.add(slot.copy(task))
    }
    bag
  }

  lazy val slots = new HibernateSetAdapter[TaskSlot] ({
    if(Slots == null) Slots = Sets.newLinkedHashSetWithExpectedSize(3)
    Slots
  })

  // Getters
  var fakeOwner: Option[GameUser] = None
  def owner = Option(Owner).orElse(fakeOwner).orNull

  def generationTime: Long = GenerationTime.getTime

  def completionTime: Option[Long] = Option(CompletionTime) map (_.getTime)

  def reputationType: ReputationModel = generator.reputation

  // TODO make not null
  def generator: TaskGenerator = Option(GenType) map Int.unbox flatMap TaskGenerator.find getOrElse TaskGenerator.all(0)

  def originalOwner: Option[GameUser] = Option(OriginalOwner)

  def futureOwner: Option[GameUser] = Option(FutureOwner)

  // Setters

  def owner(arg: GameUser) = update {Owner = arg}

  def generationTime(arg: Long) = update {GenerationTime = new Date(arg)}

  def completionTime(arg: Long) = update {CompletionTime = new Date(arg)}

  def generator(arg: Int) = update {GenType = arg}

  def originalOwner(arg: Option[GameUser]) = update {OriginalOwner = arg.orNull}

  def futureOwner(arg: Option[GameUser]) = update {FutureOwner = arg.orNull}



  def canComplete: Boolean = slots.forall(_.isSatisfied)

  def complete(implicit hib: HibernateSessionAware): Reward = if (canComplete) {
    delete()

    slots.foreach {slot =>
      owner.addIngredient(slot.gameItem, -slot.count)
    }

    val points = given
    val trx = TransactionGenerator.TASK_COMPLETE_TX.get(generator.typeId)
    owner.money.addCoins(points, trx) += owner.addExp(points) += owner.addReputation(reputationType, points.asInstanceOf[Int])

  } else Reward()

  def given: Long = slots.foldLeft[Long](0L)((acc, slot) => acc + slot.count * slot.gameItem.taskExp)

  def isCompleted: Boolean = completionTime.nonEmpty

  def isExpired: Boolean = generationTime + generator.expires <= now

  def expiresAfter: Long = math.max(0, generationTime + generator.expires - now)

  def delete(): Unit = {
    owner.tasks -= this
  }

  def canBeenSent: Boolean = {
    originalOwner.isEmpty
  }
}

object Task {

  def apply(): Task = {
    new Task().generationTime(System.currentTimeMillis())
  }
}
