package bean

import model.item.Ingredient

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.11.2016.
  */
class TaskSlot extends Bean {

  @BeanProperty
  protected var GameItemType: java.lang.Integer = _

  @BeanProperty
  protected var Count: java.lang.Integer = _

  @BeanProperty
  protected var Index: java.lang.Integer = _

  @BeanProperty
  protected var TheTask: Task = _

  def copy(task: Task): TaskSlot = {
    val slot = new TaskSlot
    slot.GameItemType = getGameItemType
    slot.Count = getCount
    slot.Index = getIndex
    slot.TheTask = task
    slot
  }

  // Getters
  def gameItem: Ingredient = Ingredient(GameItemType)

  def count: Int = Count

  def index: Int = Index

  def task: Task = TheTask

  // Setters
  def gameItem(arg: Ingredient) = update {GameItemType = arg.typeId}

  def count(arg: Int) = update {Count = arg}

  def index(arg: Int) = update {Index = arg}

  def task(arg: Task) = update {TheTask = arg}


  def isSatisfied: Boolean = {
    val barn = task.owner.barn
    barn.get(gameItem.typeId).exists(_ >= count)
  }

  def have: Int = {
    task.owner.barn.get(gameItem.typeId).getOrElse(0)
  }

  def className: String = {
    if (have < count) "red"
    else "green"
  }
}
