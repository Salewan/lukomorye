package bean

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
case class TestEntity(id: Int)
