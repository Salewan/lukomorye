package bean

import java.util.Date

import components.context.GameContext
import model.TicketState
import play.api.mvc.AnyContent

import scala.beans.BeanProperty

/**
  * Created by Salewan on 12.04.2017.
  */
class Ticket extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var ResolveTime: Date = _

  @BeanProperty
  protected var Sender: User = _

  @BeanProperty
  protected var Resolver: User = _

  @BeanProperty
  protected var TypeId: String = _

  @BeanProperty
  protected var Text: String = _

  @BeanProperty
  protected var StateId: java.lang.Character = _

  // Getters
  def creationTime: Long = CreationTime.getTime
  def resolveTime: Option[Long] = Option(ResolveTime).map(_.getTime)
  def sender: User = Sender
  def resolver: Option[User] = Option(Resolver)
  def typeId: String = TypeId
  def text: String = Text
  def stateId: Char = Char unbox StateId

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def resolveTime(arg: Option[Long]) = update {ResolveTime = arg.map(new Date(_)).orNull}
  def sender(arg: User) = update {Sender = arg}
  def resolver(arg: Option[User]) = update {Resolver = arg.orNull}
  def typeId(arg: String) = update {TypeId = arg}
  def text(arg: String) = update {Text = arg}
  def stateId(arg: Char) = update {StateId = Char box arg}

  def state: TicketState = TicketState.withType(stateId)
}

object Ticket {
  implicit class TicketOps(self: Ticket)(implicit ctx: GameContext[AnyContent]) {
    def isEnabled(state: TicketState): Boolean = !(self.resolver.exists(_ != ctx.user) && !ctx.user.isAdmin) && self.state != state
  }

  def apply(): Ticket = new Ticket().creationTime(System.currentTimeMillis()).stateId(TicketState.NEW.typeId)
}
