package bean

import bean.state.TransformState
import components.HibernateSessionAware
import model.item.{Group, Ingredient}
import model.transform.recipe.TransformRecipe

import scala.beans.BeanProperty
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 07.11.2016.
  */
class Transform extends AccountEntity {

  @BeanProperty
  protected var Ing0: java.lang.Integer = _
  def ing0(arg: Int): Transform = {Ing0 = arg; this}
  def ing0(arg: Option[Int]): Transform = {Ing0 = arg map Int.box orNull; this}
  def ing0(): Option[Ingredient] = Option(Int.unbox(Ing0)) flatMap Ingredient.ingredientsMap.get


  @BeanProperty
  protected var Ing1: java.lang.Integer = _
  def ing1(arg: Int): Transform = { Ing1 = arg; this}
  def ing1(arg: Option[Int]): Transform = {Ing1 = arg map Int.box orNull; this}
  def ing1(): Option[Ingredient] = Option(Int.unbox(Ing1)) flatMap Ingredient.ingredientsMap.get

  @BeanProperty
  protected var ActiveSlot: java.lang.Integer = _
  def activeSlot(arg: Int): Transform = {ActiveSlot = arg; this}
  def activeSlot(): Int = Option(ActiveSlot) map Int.unbox getOrElse 0

  def clear(): Unit = ing0(None).ing1(None).activeSlot(0)

  def ing(slotNum: Int, v: Option[Int]): Unit = slotNum match {
    case 0 => ing0(v)
    case 1 => ing1(v)
    case _ => ()
  }
  def ing(slotNum: Int): Option[Ingredient] = slotNum match {
    case 0 => ing0()
    case 1 => ing1()
    case _ => None
  }

  def makeNextSlotActive(): Unit = {
    val next = (activeSlot() - 1).abs
    if (ing(next).isEmpty) activeSlot(next)
    ()
  }

  def getCountableIngredients(implicit hib: HibernateSessionAware): Map[Ingredient, Int] =
    gameUser.barn.domainKeyMap.filter(_._2 > 0)

  def getUncountableIngredients(implicit hib: HibernateSessionAware): Iterable[Ingredient] =
    gameUser.barn.keys.filter(Group.UnlimitedIngs.contains).flatMap(Ingredient.find)

  // ингредиент -> количество
  def getAvailableIngredients(implicit hs: HibernateSessionAware): Map[Int, Int] =
    gameUser.barn map { case (ingr, amount) =>
      val inSlots = {for {
        slot <- 0 to 1
        i <- ing(slot)
        if i.typeId == ingr
      } yield i}.size
      (ingr, amount - inSlots)
    } filter { case (ing, amount) =>
        Group.UnlimitedIngs.contains(ing) || amount > 0
    }

  def getFirstAvailableIngredient(implicit hs: HibernateSessionAware): Option[Ingredient] =
    getCountableIngredients.headOption.map(_._1) orElse getUncountableIngredients.headOption

  def futureRecipe: Option[TransformRecipe] = {
    for (a <- ing0(); b <- ing1()) yield Set(a, b)
  } flatMap TransformRecipe.findRecipe

  def state(implicit hs: HibernateSessionAware): TransformState = {
    if (ing0().isEmpty || ing1().isEmpty) TransformState.Partial
    else {
      val recipeOpt = futureRecipe
      if (recipeOpt.isEmpty) TransformState.NoRecipe
      else if (gameUser.transformRecipes.contains(recipeOpt.get.typeId)) TransformState.Available
      else TransformState.NotAvailable
    }
  }

}
