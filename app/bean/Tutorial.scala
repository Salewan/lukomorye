package bean

import java.util.Date

import controllers.TutorialController
import model.Reward

import scala.beans.BeanProperty

/**
  * Created by Salewan on 16.06.2017.
  */
class Tutorial extends AccountEntity {

  @BeanProperty
  protected var KolobokEnqueueTime: Date = _

  @BeanProperty
  protected var Hens: java.lang.Integer = _

  @BeanProperty
  protected var Eggs: java.lang.Integer = _

  @BeanProperty
  protected var Exp: java.lang.Integer = 0

  @BeanProperty
  protected var Ruby: java.lang.Long = 0

  @BeanProperty
  protected var Coin: java.lang.Long = 0

  @BeanProperty
  protected var DreamDust: java.lang.Long = 0

  @BeanProperty
  protected var Reputation: java.lang.Long = 0

  @BeanProperty
  protected var Level: java.lang.Integer = 1

  def kolobokEnqueueTime: Option[Long] = Option(KolobokEnqueueTime) map (_.getTime)

  def kolobokEnqueueTime(arg: Option[Long]) = update {KolobokEnqueueTime = arg.map(new Date(_)).orNull}

  def hens: Int = Option(Hens) map Int.unbox getOrElse 0

  def hens(arg: Int) = update {Hens = arg}

  def eggs: Int = Option(Eggs) map Int.unbox getOrElse 0

  def eggs(arg: Int) = update {Eggs = arg}

  def exp: Int = Int.unbox(Exp) max 3

  def exp(arg: Int) = update {Exp = arg}

  def ruby: Long = Ruby
  def coin: Long = Coin
  def dust: Long = DreamDust
  def reputation: Long = Reputation
  def level: Int = Level

  def ruby(arg: Long) = update {Ruby = arg}
  def coin(arg: Long) = update {Coin = arg}
  def dust(arg: Long) = update {DreamDust = arg}
  def reputation(arg: Long) = update {Reputation = arg}
  def level(arg: Int) = update {Level = arg}

  def addRuby(arg: Long): Reward = {ruby(ruby + arg); Reward().addRuby(arg) }
  def addCoin(arg: Long): Reward = {coin(coin + arg); Reward().addCoin(arg) }
  def addDust(arg: Long): Reward = {dust(dust + arg); Reward().addDreamDust(arg)}
  def addReputation(arg: Int): Reward = {
    reputation(reputation + arg)
    Reward().addReputation(TutorialController.firstTaskReputation.typeId, arg)
  }
  def incLevel(): Reward = {level(level + 1); Reward().setLevel(level)}
}

object Tutorial {

  val endRubieValue = 75
  val endMoneyValue = 140
  val endDustValue = 250
  val endCurrentExperience = 300
  val endTotalExperience = 300
  val endLevel = 4
}
