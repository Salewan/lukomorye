package bean

import java.util.Date

import bean.messages.forum.Folder
import bean.messages.personal.{PrivateDialog, PrivateMessage}
import com.google.common.collect.Maps
import components.HibernateSessionAware
import components.context.PlayContext
import controllers.OdnoklassnikiController
import dao.{MessageDAO, UserDAO}
import hibernate.HibernateMapAdapter
import org.apache.commons.lang3.RandomStringUtils
import play.api.mvc.AnyContent
import util.Implicits._
import util.WebFormsUtil

import scala.beans.BeanProperty
import scala.language.postfixOps

/**
 * @author Sergey Lebedev (salewan@gmail.com) 09.03.2016.
 */
class User extends AccountEntity {

  @BeanProperty
  protected var Role: String = _

  @BeanProperty
  protected var Dialogs: java.util.Map[Int, PrivateDialog] = _

  @BeanProperty
  protected var NewMessagesCount: java.lang.Integer = 0

  @BeanProperty
  protected var SpamPenaltyEndTime: Date = _

  @BeanProperty
  protected var LastMessageTime: Date = _

  @BeanProperty
  protected var LastMessagesViewTime: Date = _

  @BeanProperty
  protected var BanEndTime: Date = _

  @BeanProperty
  protected var Blocked: java.lang.Boolean = _

  @BeanProperty
  protected var NewsViewed: Date = _

  @BeanProperty
  protected var Secret: String = _

  @BeanProperty
  protected var ExternalId: String = _

  @BeanProperty
  protected var LastPaymentTime: Date = _

  @BeanProperty
  protected var PhoneNumber: String = _

  @BeanProperty
  protected var PromoChannel: String = _

  @BeanProperty
  protected var RestorePasswordKey: String = _

  @BeanProperty
  protected var FirstPayment: java.lang.Boolean = _

  @BeanProperty
  protected var AltPartnerParams: String = _

  @BeanProperty
  protected var VictoryGiftGot: Date = _

  lazy val dialogs = new HibernateMapAdapter[Int, PrivateDialog]({
    if (Dialogs == null) Dialogs = Maps.newLinkedHashMap()
    Dialogs
  })

  // Getters
  def role = Role.toCharArray.head
  def newMessagesCount: Int = NewMessagesCount
  def spamPenaltyEndTime: Option[Long] = Option(SpamPenaltyEndTime).map(_.getTime)
  def lastMessageTime: Option[Long] = Option(LastMessageTime).map(_.getTime)
  def lastMessagesViewTime: Long = Option(LastMessagesViewTime).map(_.getTime).getOrElse(0)
  def banEndTime: Option[Long] = Option(BanEndTime) map (_.getTime)
  def blocked: Boolean = Option(Blocked) exists Boolean.unbox
  def newsViewed: Option[Long] = Option(NewsViewed) map (_.getTime)
  def validSecret: String = Option(Secret).getOrElse {
    Secret = RandomStringUtils.randomAlphanumeric(32)
    Secret
  }
  def externalId: Option[String] = Option(ExternalId)
  def lastPaymentTime: Option[Long] = Option(LastPaymentTime).map(_.getTime)
  def phoneNumber: Option[String] = Option(PhoneNumber)
  def promoChannel: Option[String] = Option(PromoChannel)
  def restorePasswordKey: Option[String] = Option(RestorePasswordKey)
  def firstPayment: Boolean = false //Option(FirstPayment).forall(Boolean.unbox) //TODO
  def altPartnerParams: Option[String] = Option(AltPartnerParams)
  def victoryGiftGot: Option[Long] = Option(VictoryGiftGot).map(_.getTime)

  // Setters
  def role(arg: Char) = update { Role = "" + arg}
  def newMessagesCount(arg: Int) = update {NewMessagesCount = arg}
  def spamPenaltyEndTime(arg: Long) = update {SpamPenaltyEndTime = new Date(arg)}
  def lastMessageTime(arg: Long) = update {LastMessageTime = new Date(arg)}
  def lastMessagesViewTime(arg: Long) = update {LastMessagesViewTime = new Date(arg)}
  def banEndTime(arg: Long) = update {BanEndTime = new Date(arg)}
  def blocked(arg: Boolean) = update {Blocked = arg}
  def newsViewed(arg: Long) = update {NewsViewed = new Date(arg)}
  def secret(arg: Option[String]) = update {Secret = arg.orNull}
  def externalId(arg: Option[String]) = update {ExternalId = arg.orNull}
  def lastPaymentTime(arg: Option[Long]) = update {LastPaymentTime = arg.map(new Date(_)).orNull}
  def phoneNumber(arg: Option[String]) = update {PhoneNumber = arg.orNull}
  def promoChannel(arg: Option[String]) = update {PromoChannel = arg.orNull}
  def restorePasswordKey(arg: Option[String]) = update {RestorePasswordKey = arg.orNull}
  def victoryGiftGot(arg: Option[Long]) = update {VictoryGiftGot = arg.map(new Date(_)).orNull}

  def clearSpamPenaltyEndTime() = update {SpamPenaltyEndTime = None.orNull}
  def clearBlocked() = update {Blocked = None.orNull}
  def clearBanEndTime() = update {BanEndTime = None.orNull}
  def clearLastMessageTime() = update {LastMessageTime = None.orNull}
  def clearNewsViewed() = update {NewsViewed = None.orNull}
  def firstPayment(arg: Boolean) = update {FirstPayment = arg}
  def altPartnerParams(arg: Option[String]) = update {AltPartnerParams = arg.orNull}


  def isAdmin: Boolean = role == 'a'

  def isSupporter: Boolean = role == 's'

  def isModerator: Boolean = role == 'm'

  def isUser: Boolean = role == 'u'

  def isAdminOrSupporterOrModerator: Boolean = isAdmin || isSupporter || isModerator

  def isAdminOrSupporter: Boolean = isAdmin || isSupporter

  def isSupporterOrModerator: Boolean = isSupporter || isModerator

  def isBanned: Boolean = banEndTime exists (_ > now)

  def isUserBlocked: Boolean = isBanned && banEndTime.exists(_ > now + (1 year))

  def canBan: Boolean = isAdminOrSupporterOrModerator

  def banEndTimeFormatted: String = if (BanEndTime != null) config.DATE_FORMAT_DETAILED2.format(BanEndTime) else ""

  def dialogWith(user: User, create: Boolean): Option[PrivateDialog] =
    if (create || dialogs.containsKey(user.id)) Some(dialogWith(user)) else None

  def dialogWith(user: User): PrivateDialog = {
    dialogs.get(user.id).getOrElse {
      val dialog = new PrivateDialog().owner(this).target(user).lastMessageDate(now).lastViewDate(now)
      dialogs += user.id -> dialog
      dialog
    }
  }

  def isSocialUser: Boolean = externalId.nonEmpty

  def didPayment(): Unit = lastPaymentTime(Some(now))

  def isOdnoklassnikiUser: Boolean = externalId.exists(_.startsWith(OdnoklassnikiController.PARTNER_ODNOKLASSNIKI))
}

object User {

  implicit class UserRegOps(self: User)(implicit ctx: PlayContext[AnyContent]) {
    def provideContextInfo(): User = {
      val profile = self.profile
      profile.registrationIp(ctx.getIp)
      profile.domain(ctx.getDomain)
      self.promoChannel(ctx.promoChannel)
      self
    }
  }

  implicit class UserOps(self: User)(implicit hib: HibernateSessionAware) {

    def registerWith(nick: String, pass: String, email: Option[String]): Unit = {
      self.avatar.updateNick(nick)
      self.profile.password(pass).email(email)
      self.secret(None)
    }

    def canReportSpam(message: PrivateMessage): Boolean = {
      val userDAO = new UserDAO(hib.hibernateSession)
      message.author != self && !message.author.isAdminOrSupporterOrModerator &&
        !userDAO.findByNick(config.ADMINISTRATION_LOGIN).map(_.user).contains(message.author) &&
        !userDAO.findByNick(config.POST_LOGIN).map(_.user).contains(message.author)
    }

    def isMessagesAllowed: Boolean = self.gameUser.level >= config.MESSAGES_ALLOWED_LEVEL

    def isSpecialUser: Boolean = self.avatar.nick.exists(config.SPETIAL_USERS.contains)

    def canMakeFriend(target: User): Boolean = !self.gameUser.friends.contains(target.gameUser) && !target.isSpecialUser

    def canIgnore(target: User): Boolean = !self.gameUser.ignore.contains(target.gameUser) && ! target.isSpecialUser

    def canRemoveDialog(target: User): Boolean = !target.isAdminOrSupporter && !target.isSpecialUser

    def folderIcon(folder: Folder): String = {
      val messageDAO = new MessageDAO(hib.hibernateSession)
      val mark = messageDAO.getFolderMark(self, folder)
      val isNew = folder.lastCommentTime.exists(_ > mark.lastViewTime)
      if (isNew) "folder_new.png" else "folder.png"
    }

    def ban(time: Long, actor: User, comment: String, messageText: String): Unit = {
      val target = self
      if (target != actor && !target.isUserBlocked && !target.banEndTime.exists(_ >= System.currentTimeMillis() + time)) {
        val dao = new UserDAO(hib.hibernateSession).ban(target, actor, time, comment, messageText)
      }
    }

    def unban(actor: User): Unit = {
      if (self.isBanned || self.isUserBlocked) {
        val dao = new UserDAO(hib.hibernateSession)
        dao.unban(self, actor)
      }
    }
  }
}
