package bean

import java.util.Date

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.02.2017.
  */
class UserBanRecord extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var EndTime: Date = _

  @BeanProperty
  protected var Actor: User = _

  @BeanProperty
  protected var Target: User = _

  @BeanProperty
  protected var Comment: String = _

  @BeanProperty
  protected var MessageText: String = _

  // Getters
  def creationTime: Long = CreationTime.getTime
  def endTime: Option[Long] = Option(EndTime) map (_.getTime)
  def actor: User = Actor
  def target: User = Target
  def comment: Option[String] = Option(Comment)
  def messageText: Option[String] = Option(MessageText)

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def endTime(arg: Long) = update {EndTime = new Date(arg)}
  def actor(arg: User) = update {Actor = arg}
  def target(arg: User) = update {Target = arg}
  def comment(arg: String) = update {Comment = arg}
  def messageText(arg: String) = update {MessageText = arg}

  def clearEndTime() = update {EndTime = None.orNull}
  def clearComment() = update {Comment = None.orNull}
  def clearMessageText() = update {MessageText = None.orNull}
}
