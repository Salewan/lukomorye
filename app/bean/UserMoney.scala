package bean

import components.HibernateSessionAware
import model.price.Currency
import model.{Reward, TransactionType}

import scala.beans.BeanProperty

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
class UserMoney extends AccountEntity {

  @BeanProperty
  protected var Version: java.lang.Integer = _

  @BeanProperty
  protected var Rubies: java.lang.Long = _

  @BeanProperty
  protected var Coins: java.lang.Long = _

  @BeanProperty
  protected var DreamDust: java.lang.Long = _

  //Setters
  def rubies(arg: Long) = update {Rubies = arg}
  def coins(arg: Long) = update {Coins = arg}
  def dreamDust(arg: Long): UserMoney = update {DreamDust = arg}

  //Getters
  def rubies: Long = Option(Rubies) map Long.unbox getOrElse 0L
  def coins: Long = Option(Coins) map Long.unbox getOrElse 0L
  def dreamDust: Long = Option(DreamDust) map Long.unbox getOrElse 0L
}

object UserMoney {

  implicit class UserMoneyOps(self: UserMoney)(implicit hib: HibernateSessionAware) {
    def addRubies(arg: Long, tr: TransactionType, comment: Option[String] = None): Reward = {
      val result = math.max(0, self.rubies + arg)
      val realRubies = result - self.rubies
      self.rubies(result)

      val reward = Reward()
      if (realRubies != 0) {
        val moneyTransaction = MoneyTransaction(self.gameUser)
        moneyTransaction.amount(realRubies)
        moneyTransaction.transactionType(tr)
        moneyTransaction.param(tr.param)
        moneyTransaction.result(self.rubies)
        moneyTransaction.currency(Currency.Ruby)
        moneyTransaction.comment(comment)
        hib.hibernateSession.persist(moneyTransaction)
        reward.addRuby(realRubies)
      }
      reward
    }

    def addCoins(arg: Long, tr: TransactionType, comment: Option[String] = None): Reward = {
      val result = math.max(0, self.coins + arg)
      val realCoins = result - self.coins
      self.coins(result)

      val reward = Reward()
      if (realCoins != 0) {
        val moneyTransaction = MoneyTransaction(self.gameUser)
        moneyTransaction.amount(realCoins)
        moneyTransaction.transactionType(tr)
        moneyTransaction.param(tr.param)
        moneyTransaction.result(self.coins)
        moneyTransaction.currency(Currency.Coin)
        moneyTransaction.comment(comment)
        hib.hibernateSession.persist(moneyTransaction)
        reward.addCoin(realCoins)
      }
      reward
    }

    def addDreamDust(arg: Long, tr: TransactionType, comment: Option[String] = None): Reward = {
      val result = math.max(0, self.dreamDust + arg)
      val realDust = result - self.dreamDust
      self.dreamDust(result)

      val reward = Reward()
      if (realDust != 0) {
        val moneyTransaction = MoneyTransaction(self.gameUser)
        moneyTransaction.amount(realDust)
        moneyTransaction.transactionType(tr)
        moneyTransaction.param(tr.param)
        moneyTransaction.result(self.dreamDust)
        moneyTransaction.currency(Currency.Dust)
        moneyTransaction.comment(comment)
        hib.hibernateSession.persist(moneyTransaction)
        reward.addDreamDust(realDust)
      }
      reward
    }
  }
}
