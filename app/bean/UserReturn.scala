package bean

import java.util.Date

import util.TimeUtil

import scala.beans.BeanProperty
import util.Implicits._

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 29.03.2017.
  */
class UserReturn extends Bean {

  @BeanProperty
  protected var UserId: java.lang.Integer = _

  @BeanProperty
  protected var LastActionTime: Date = _

  @BeanProperty
  protected var ReturnTime: Date = _

  // Getters
  def uid: Int = Int unbox UserId
  def lastActionTime: Long = LastActionTime.getTime
  def returnTime: Long = ReturnTime.getTime

  // Setters
  def uid(arg: Int) = update {UserId = arg}
  def lastActionTime(arg: Option[Long]) = update {LastActionTime = arg.map(new Date(_)).orNull}
  def returnTime(arg: Long) = update {ReturnTime = new Date(arg)}
}

object UserReturn {

  def apply(user: GameUser): UserReturn =
    new UserReturn().uid(user.id).lastActionTime(user.lastActionTime).returnTime(System.currentTimeMillis())

  def isReturnedUser(user: GameUser): Boolean = user.lastActionTime.exists(TimeUtil.passed(_, 1 week))

}
