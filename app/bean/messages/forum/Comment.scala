package bean.messages.forum

import bean.AbstractMessage

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class Comment extends AbstractMessage {

  @BeanProperty
  protected var Topic: Topic = _

  def topic = Topic

  def topic(arg: Topic) = update {Topic = arg}
}
