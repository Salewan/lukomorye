package bean.messages.forum

import java.util.Date

import bean.{Bean, Clan}
import com.google.common.collect.Sets
import hibernate.HibernateSetAdapter

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class Folder extends Bean {

  @BeanProperty
  protected var Name: String = _

  @BeanProperty
  protected var Topics: java.util.Set[Topic] = _

  @BeanProperty
  protected var LastCommentTime: Date = _

  @BeanProperty
  protected var Deleted: java.lang.Boolean = false

  @BeanProperty
  protected var TypeId: java.lang.Integer = 0

  @BeanProperty
  protected var Clan: Clan = _

  @BeanProperty
  protected var NewsTopicId: java.lang.Integer = _

  @BeanProperty
  protected var NewsTime: Date = _

  lazy val topics = new HibernateSetAdapter[Topic]({
    if (Topics == null) Topics = Sets.newLinkedHashSet()
    Topics
  })

  // Getters
  def name = Name
  def lastCommentTime: Option[Long] = Option(LastCommentTime) map (_.getTime)
  def deleted: Boolean = Deleted
  def typeId: Int = TypeId
  def clan: Option[Clan] = Option(Clan)
  def newsTopicId: Option[Int] = Option(NewsTopicId)
  def newsTime: Option[Long] = Option(NewsTime) map (_.getTime)

  // Setters
  def name(arg: String) = update {Name = arg}
  def lastCommentTime(arg: Long) = update {LastCommentTime = new Date(arg)}
  def deleted(arg: Boolean) = update {Deleted = arg}
  def typeId(arg: Int) = update {TypeId = arg}
  def clan(arg: Clan) = update {Clan = arg}
  def newsTopicId(arg: Int) = update {NewsTopicId = arg}
  def newsTime(arg: Long) = update {NewsTime = new Date(arg)}

  def clearLastCommentTime() = update {LastCommentTime = None.orNull}
  def clearClan() = update {Clan = None.orNull}
  def clearNewsTopicId() = update {NewsTopicId = None.orNull}
  def clearNewsTime() = update {NewsTime = None.orNull}


  def folderType: FolderType = FolderType(typeId)
  def folderType(arg: FolderType) = update {typeId(arg.id)}
  def folderType(code: String) = update {typeId(FolderType(code).id)}

  def isPersistent = id > 0
  def isAdmin = folderType == FolderType.ADMIN

  def updateLastCommentDate(): Unit = {
    var max = 0L
    topics.seq.foreach {t =>
      if (!t.deleted && t.lastCommentTime.exists(_ > max))
        max = t.lastCommentTime.get
    }
    lastCommentTime(max)
  }

  override def toString: String = id + "#" + name
}
