package bean.messages.forum

import java.util.Date

import bean.{BeanTemplate, User}

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class FolderMark extends BeanTemplate {

  @BeanProperty
  protected var Id: FolderMark.ID = _

  @BeanProperty
  protected var LastViewTime: Date = _

  @BeanProperty
  protected var LastMarkAllReadTime: Date = _

  // Getters
  def id: FolderMark.ID = Id
  def lastViewTime: Long = LastViewTime.getTime
  def lastMarkAllReadTime: Option[Long] = Option(LastMarkAllReadTime) map (_.getTime)

  // Setters
  def id(arg: FolderMark.ID) = update {Id = arg}
  def id(user: User, folder: Folder) = update {Id = new FolderMark.ID(user, folder)}
  def lastViewTime(arg: Long) = update {LastViewTime = new Date(arg)}
  def lastMarkAllReadTime(arg: Long) = update {LastMarkAllReadTime = new Date(arg)}

  def clearLastMarkAllReadTime() = update {LastMarkAllReadTime = None.orNull}
}

object FolderMark {
  class ID extends java.io.Serializable {

    @BeanProperty
    var UserId: java.lang.Integer = _

    @BeanProperty
    var FolderId: java.lang.Integer = _

    def this(user: User, folder: Folder) = {
      this()
      this.UserId = user.id
      this.FolderId = folder.id
    }

    private def canEqual(a: Any) = a.isInstanceOf[FolderMark.ID]

    override def equals(that: Any): Boolean = that match {
      case that: FolderMark.ID =>
        that.canEqual(this) && this.hashCode == that.hashCode
      case _ => false
    }

    override def hashCode(): Int = 31 * UserId + FolderId
  }
}
