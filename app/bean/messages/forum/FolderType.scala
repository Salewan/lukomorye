package bean.messages.forum

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class FolderType(val id: Int, val code: String)

object FolderType {

  case object NORMAL extends FolderType(0, "normal")
  case object NEWS extends FolderType(1, "news")
  case object ADMIN extends FolderType(2, "admin")
  case object MEMBERS extends FolderType(3, "opened")
  case object CHIEFS extends FolderType(4, "closed")
  case object GUESTS extends FolderType(5, "guests")

  private val all: Array[FolderType] = Array(NORMAL, NEWS, ADMIN, MEMBERS, CHIEFS, GUESTS)
  private val map: Map[Int, FolderType] = all map (f => f.id -> f) toMap
  private val type2code: Map[String, FolderType] = all map (f => f.code -> f) toMap

  def find(id: Int): Option[FolderType] = map.get(id)

  def apply(id: Int): FolderType = map(id)

  def apply(code: String): FolderType = type2code(code)

  def clanCodes = Array(MEMBERS, CHIEFS, GUESTS) map (_.code)

  def commonCodes = Array(NORMAL, NEWS, ADMIN) map (_.code)
}
