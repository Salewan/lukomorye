package bean.messages.forum

import java.util.Date

import bean.{Bean, User}
import com.google.common.collect.Sets
import components.HibernateSessionAware
import dao.MessageDAO
import hibernate.HibernateSetAdapter

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class Topic extends Bean {

  @BeanProperty
  protected var CreationTime: Date = _

  @BeanProperty
  protected var Author: User = _

  @BeanProperty
  protected var Subject: String = _

  @BeanProperty
  protected var Body: String = _

  @BeanProperty
  protected var Folder: Folder = _

  @BeanProperty
  protected var Comments: java.util.Set[Comment] = _

  @BeanProperty
  protected var LastCommentTime: Date = _

  @BeanProperty
  protected var CommentsCount: java.lang.Integer = 0

  @BeanProperty
  protected var Pinned: java.lang.Boolean = false

  @BeanProperty
  protected var Closed: java.lang.Boolean = false

  @BeanProperty
  protected var Deleted: java.lang.Boolean = false

  @BeanProperty
  protected var Moderator: java.lang.Boolean = false

  lazy val comments = new HibernateSetAdapter[Comment]({
    if (Comments == null) Comments = Sets.newLinkedHashSet()
    Comments
  })

  // Getters
  def creationTime: Long = CreationTime.getTime
  def author: User = Author
  def subject: String = Subject
  def body: String = Body
  def folder: Folder = Folder
  def lastCommentTime: Option[Long] = Option(LastCommentTime) map (_.getTime)
  def commentsCount: Int = Option(CommentsCount) map Int.unbox getOrElse 0
  def pinned: Boolean = Pinned
  def closed: Boolean = Closed
  def deleted: Boolean = Deleted
  def moderator: Boolean = Moderator

  // Setters
  def creationTime(arg: Long) = update {CreationTime = new Date(arg)}
  def author(arg: User) = update {Author = arg}
  def subject(arg: String) = update {Subject = arg}
  def body(arg: String) = update {Body = arg}
  def folder(arg: Folder) = update {Folder = arg}
  def lastCommentTime(arg: Long) = update {LastCommentTime = new Date(arg)}
  def commentsCount(arg: Int) = update {CommentsCount = arg}
  def pinned(arg: Boolean) = update {Pinned = arg}
  def closed(arg: Boolean) = update {Closed = arg}
  def deleted(arg: Boolean) = update {Deleted = arg}
  def moderator(arg: Boolean) = update {Moderator = arg}

  def clearLastCommentTime() = update {LastCommentTime = None.orNull}
}

object Topic {
  implicit class TopicOps(self: Topic)(implicit hib: HibernateSessionAware) {
    def isNew(user: User): Boolean = {
      val dao = new MessageDAO(hib.hibernateSession)
      val folderMark = dao.getFolderMark(user, self.folder)
      val isOld = for(markAllTime <- folderMark.lastMarkAllReadTime; lastCommentTime <- self.lastCommentTime)
        yield markAllTime > lastCommentTime

      !isOld.contains(true) && {
        val mark = dao.getTopicMark(user, self)

        val _isNew = for(lastCommentTime <- self.lastCommentTime) yield lastCommentTime > mark.lastViewedCommentTime

        _isNew.contains(true)
      }
    }

    def icon(user: User): String = {
      val isNew = this.isNew(user)
      if (self.closed && !self.pinned) "topic_closed.png"
      else if (isNew && self.pinned) "topic_attached_new.png"
      else if (!isNew && self.pinned) "topic_attached.png"
      else if (isNew) "topic_new.png"
      else "topic.png"
    }

    def updateLastCommentDate(): Unit = {
      val dao = new MessageDAO(hib.hibernateSession)
      val lastComment = dao.getLastComment(self)
      if (lastComment.isDefined) self.lastCommentTime(lastComment.get.creationTime)
      else self.lastCommentTime(self.creationTime)
    }
  }
}
