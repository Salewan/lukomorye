package bean.messages.forum

import java.util.Date

import bean.{BeanTemplate, User}

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class TopicMark extends BeanTemplate {

  @BeanProperty
  protected var Id: TopicMark.ID = _

  @BeanProperty
  protected var LastViewedCommentTime: Date = _

  @BeanProperty
  protected var PageNum: java.lang.Integer = 0

  @BeanProperty
  protected var FolderId: java.lang.Integer = _

  // Getters
  def id: TopicMark.ID = Id
  def lastViewedCommentTime: Long = LastViewedCommentTime.getTime
  def pageNum: Int = PageNum
  def folderId: Int = FolderId

  // Setters
  def id(arg: TopicMark.ID) = update {Id = arg}
  def id(user: User, topic: Topic) = update {Id = new TopicMark.ID(user, topic)}
  def lastViewedCommentTime(arg: Long) = update {LastViewedCommentTime = new Date(arg)}
  def pageNum(arg: Int) = update {PageNum = arg}
  def folderId(arg: Int) = update {FolderId = arg}
}

object TopicMark {
  class ID extends java.io.Serializable {

    @BeanProperty
    var UserId: java.lang.Integer = _

    @BeanProperty
    var TopicId: java.lang.Integer = _

    def this(user: User, topic: Topic) = {
      this()
      this.UserId = user.id
      this.TopicId = topic.id
    }

    private def canEqual(a: Any) = a.isInstanceOf[TopicMark.ID]

    override def equals(that: Any): Boolean = that match {
      case that: TopicMark.ID =>
        that.canEqual(this) && this.hashCode == that.hashCode
      case _ => false
    }

    override def hashCode(): Int = 31 * UserId + TopicId
  }
}
