package bean.messages.personal

import java.util.Date

import bean.{Bean, User}
import com.google.common.collect.Sets
import hibernate.HibernateSetAdapter

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 26.01.2017.
  */
class PrivateDialog extends Bean {

  @BeanProperty
  protected var Owner: User = _

  @BeanProperty
  protected var Target: User = _

  @BeanProperty
  protected var LastViewDate: Date = _

  @BeanProperty
  protected var LastMessageDate: Date = _

  @BeanProperty
  protected var Messages: java.util.Set[PrivateMessage] = _

  @BeanProperty
  protected var Deleted: java.lang.Boolean = _

  lazy val messages = new HibernateSetAdapter[PrivateMessage]({
    if (Messages == null) Messages = Sets.newLinkedHashSet()
    Messages
  })

  // Getters
  def owner = Owner
  def target = Target
  def lastViewDate: Long = LastViewDate.getTime
  def lastMessageDate: Long = LastMessageDate.getTime
  def deleted: Boolean = Deleted

  // Setters
  def owner(arg: User) = update {Owner = arg}
  def target(arg: User) = update {Target = arg}
  def lastViewDate(arg: Long) = update {LastViewDate = new Date(arg)}
  def lastMessageDate(arg: Long) = update {LastMessageDate = new Date(arg)}
  def deleted(arg: Boolean) = update {Deleted = arg}

  def hasNewMessages = lastMessageDate > lastViewDate

  def remove(message: PrivateMessage): Unit = {
    messages -= message
    if (messages.isEmpty) destroy()
  }

  def destroy(): Unit = deleted(true)

  def removeDialog(): Unit = {
    messages.clear()
    destroy()
  }

  def messageIcon(lastMessage: Option[PrivateMessage]): String =
    if (hasNewMessages) "mail_yellow.png"
    else if (lastMessage.isEmpty) "mail_white.png"
    else if (lastMessage.exists(_.author == target)) "mail_incoming.png"
    else "mail_outgoing.png"
}
