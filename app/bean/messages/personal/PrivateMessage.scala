package bean.messages.personal

import bean.{AbstractMessage, Mission, Task, User}
import components.HibernateSessionAware
import model.messaging._

import scala.beans.BeanProperty

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.11.2016.
  */
class PrivateMessage extends AbstractMessage {

  @BeanProperty
  protected var Target: User = _

  @BeanProperty
  protected var WasRead: java.lang.Boolean = false

  @BeanProperty
  protected var Dialog: PrivateDialog = _

  @BeanProperty
  protected var InboxType: java.lang.Integer = _

  @BeanProperty
  protected var Param: java.lang.Integer = _

  // Getters
  def target = Option(Target)
  def wasRead: Boolean = WasRead
  def dialog = Option(Dialog)
  def inboxType: Option[InboxType] = Option(InboxType).map(Int.unbox).flatMap(Inbox.findInboxType)
  def param: Option[Int] = Option(Param).map(Int.unbox)

  // Setters
  def target(arg: User) = update {Target = arg}
  def clearTarget() = update {Target = None.orNull}
  def wasRead(arg: Boolean) = update {WasRead = arg}
  def dialog(arg: PrivateDialog) = update {Dialog = arg}
  def clearDialog() = update {Dialog = None.orNull}
  def inboxType(arg: Option[InboxType]) = update {InboxType = arg.map(Int box _.typeId).orNull}
  def param(arg: Option[Int]) = update {Param = arg.map(Int.box).orNull}

  def checkIfExpired()(implicit hib: HibernateSessionAware): Unit = for (
    it <- inboxType if it == EnvelopeMission;
    mid <- param;
    mission <- hib.hibernateSession.get(classOf[Mission], mid) if mission.isExpired;
    sender <- mission.originalOwner
  ) {
    inboxType(Some(EnvelopeMission_Expired))
    param(Some(sender.id))
  }

  def checkIfExpired_Task()(implicit hib: HibernateSessionAware): Unit = for (
    it <- inboxType if it == EnvelopeTask;
    tid <- param;
    task <- hib.hibernateSession.get(classOf[Task], tid) if task.isExpired;
    sender <- task.originalOwner
  ) {
    inboxType(Some(EnvelopeTask_Expired))
    param(Some(sender.id))
  }

  def markMissionAccepted(senderId: Option[Int]): Unit = {
    inboxType(Some(EnvelopeMission_Accepted))
    param(senderId)
  }

  def markTaskAccepted(senderId: Option[Int]): Unit = {
    inboxType(Some(EnvelopeTask_Accepted))
    param(senderId)
  }

  def markEnvelopeMissionDeleted(senderId: Option[Int]): Unit = {
    inboxType(Some(EnvelopeMission_Removed))
    param(senderId)
  }

  def markEnvelopeTaskDeleted(senderId: Option[Int]): Unit = {
    inboxType(Some(EnvelopeTask_Removed))
    param(senderId)
  }
}
