package bean.state

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.11.2016.
  */
object ClanHistoryRecordType extends Enumeration {

  type ClanHistoryRecordType = Value

  val CREATION, USER_IN, USER_OUT, USER_KICKED,
  ROLE_UP, ROLE_DOWN, OWNER_CHANGED, RENAME, USER_LEFT = Value

}
