package bean.state

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.11.2016.
  */
trait ClanJoinResult

object ClanJoinResult {
  case object OK extends ClanJoinResult
  case object CLAN_IS_FULL extends ClanJoinResult
  case object USER_IS_IN_CLAN extends ClanJoinResult
  case object LOW_LEVEL extends ClanJoinResult
}
