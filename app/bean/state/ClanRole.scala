package bean.state

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.11.2016.
  */
trait ClanRole {
  val id: Int
  val name: String

  def getNext: ClanRole = ClanRole.roles.sortBy(_.id).find(_.id > id) getOrElse ClanRole.LEADER

  def getPrev: ClanRole = ClanRole.roles.sortBy(_.id).reverse.find(_.id < id) getOrElse ClanRole.NONE
}

object ClanRole {

  case object LEADER extends ClanRole {
    override val id: Int = 100
    override val name: String = "княже"
  }
  case object OFFICER extends ClanRole {
    override val id: Int = 50
    override val name: String = "бояре"
  }
  case object ORDINARY extends ClanRole {
    override val id: Int = 10
    override val name: String = "дворяне"
  }
  case object NONE extends ClanRole {
    override val id: Int = 0
    override val name: String = ""
  }

  val preLeader = OFFICER

  val prePreLeader = ORDINARY

  val roles = Array(LEADER, OFFICER, ORDINARY, NONE)

  val roleMap: Map[Int, ClanRole] = roles map (r => r.id -> r) toMap

  def apply(id: Int): ClanRole = roleMap.getOrElse(id, NONE)
}
