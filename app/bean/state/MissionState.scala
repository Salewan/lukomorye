package bean.state

/**
  * @author Sergey Lebedev (salewan@gmail.com) 17.10.2016.
  */
trait MissionState

object MissionState {

  case object NEW extends MissionState

  case object IN_PROGRESS extends MissionState

  case object COMPLETE extends MissionState

  case object CLOSED extends MissionState

}
