package bean.state

/**
  * @author Sergey Lebedev (salewan@gmail.com) 08.11.2016.
  */
trait TransformState
object TransformState {

  case object Partial extends TransformState // частично установленные ингредиенты, кнопка неактивна
  case object NoRecipe extends TransformState // такого рецепта нет, кнопка неактивна
  case object Available extends TransformState // рецепт есть и доступен игроку, кнопка активна
  case object NotAvailable extends TransformState // рецепт есть но не доступен игроку, кнопка активна но с подсказкой
}
