package components

import play.api.mvc.QueryStringBindable

import scala.util.Right

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.04.2016.
  */
trait Direction {
  val value: Int
}
object Direction {

  implicit def queryStringBinder(implicit intBinder: QueryStringBindable[Int]) = new QueryStringBindable[Direction] {

    override def bind(key: String, params: Map[String, Seq[String]]): Option[Either[String, Direction]] = {
      for {
        id <- intBinder.bind(key, params)
      } yield {
        id match {
          case Right(i) => Direction(i)
          case _ => Left("Unable to bind a Direction")
        }
      }
    }

    override def unbind(key: String, value: Direction): String = intBinder.unbind(key, value.value)
  }

  def apply(c: Int): Either[String, Direction] = c match {
    case 1 => Right(LEFT)
    case 2 => Right(RIGHT)
    case x => Left("Unable to bind a Direction")
  }
}
case object LEFT extends Direction {
  override val value: Int = 1
}
case object RIGHT extends Direction {
  override val value: Int = 2
}
