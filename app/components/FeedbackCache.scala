package components

import java.util.concurrent.ConcurrentMap

import com.google.common.collect.Maps
import play.twirl.api.Html

class Lock

object FeedbackCache {
  private val locks: ConcurrentMap[Int, Lock] = Maps.newConcurrentMap[Int, Lock]()
  private val feedbacks = Maps.newConcurrentMap[Int, Seq[Html]]()

  private def getLock(key: Int) = Option apply locks.putIfAbsent(key, new Lock) getOrElse locks.get(key)

  def add(key: Int, notif: Html): Unit = getLock(key).synchronized {
    feedbacks.put(key, feedbacks.getOrDefault(key, Seq()) :+ notif)
    ()
  }

  def get(key: Int): Seq[Html] = getLock(key).synchronized(Option apply feedbacks.remove(key) getOrElse Seq())

  def isEmpty(key: Int): Boolean = feedbacks.getOrDefault(key, Seq()).isEmpty
}
