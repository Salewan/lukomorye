package components

import play.twirl.api.Html

/**
 * @author Sergey Lebedev (salewan@gmail.com) 22.03.2016.
 */
trait FeedbackView {
  
  def feedbackSeq: Seq[Html]
}
