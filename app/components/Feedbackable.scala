package components

import play.twirl.api.Html

/**
 * @author Sergey Lebedev (salewan@gmail.com) 22.03.2016.
 */
trait Feedbackable extends FeedbackView {

  val keySuffix: Int

  override def feedbackSeq: Seq[Html] = FeedbackCache.get(keySuffix)

  def addFeedback(html: Html): Unit = FeedbackCache.add(keySuffix, html)

  def isEmptyFeedback: Boolean = FeedbackCache.isEmpty(keySuffix)
}
