package components

import hibernate.HibernateSession

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
trait HibernateSessionAware {

  val hibernateSession: HibernateSession
}

object HibernateSessionAware {
  def apply(session: HibernateSession) = new HibernateSessionAware {
    override val hibernateSession: HibernateSession = session
  }
}
