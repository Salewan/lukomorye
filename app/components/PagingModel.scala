package components

import dao.paging.PagingDAO
import play.api.mvc.Call

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.10.2016.
  */
class PagingModel[T](val currentPage: Int, pageCount: Int, val page: Iterable[T], f: Int => Call) {

  def numbers: List[Int] = leftPage to rightPage toList

  def leftPage: Int = if ((currentPage - margin) + margin * 2 > maxPage) Math.max(0, maxPage - margin * 2) else Math.max(0, currentPage - margin)

  def rightPage: Int = Math.min(maxPage, leftPage + margin * 2)

  def maxPage: Int = if (pageCount == 1) 0 else Math.max(0, pageCount - 1)

  def margin = 2

  def isFirst = currentPage == 0 || pageCount <= 1

  def isLast = currentPage >= maxPage

  def isVisible = pageCount > 1

  def callFirst = f(0)

  def call(num: Int) = f(num)

  def callLast = f(maxPage)

  def callPrev = f((pageCount - 2) min (currentPage - 1))

  def callNext = f(currentPage + 1)

  def isFirstElement(t: T) = page.head == t

  def isEmpty = pageCount <= 0

  def nonEmpty = !isEmpty
}

object PagingModel {
  def apply[T](page: Int, pagingDAO: PagingDAO[T], ref: Int => Call) =
    new PagingModel[T](page, pagingDAO.pageCount, pagingDAO.page(page), ref)
}
