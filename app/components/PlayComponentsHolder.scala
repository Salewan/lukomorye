package components

import javax.inject.{Inject, Singleton}

import modules.EagerInitee
import play.api.Environment
import play.api.Mode.Mode
import play.api.cache.CacheApi
import play.api.i18n.MessagesApi

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
@Singleton
class PlayComponentsHolder @Inject() (cacheApi: CacheApi,
                                      messagesApi: MessagesApi,
                                      environment: Environment) extends EagerInitee {
  PlayComponentsHolder.innerCache = cacheApi
  PlayComponentsHolder.innerMessages = messagesApi
  PlayComponentsHolder.innerMode = environment.mode
}

object PlayComponentsHolder {

  private var innerCache: CacheApi = _
  private var innerMessages: MessagesApi = _
  private var innerMode: play.api.Mode.Mode = _

  lazy val cacheApi: CacheApi = innerCache
  lazy val messagesApi: MessagesApi = innerMessages
  lazy val mode: Mode = innerMode
}
