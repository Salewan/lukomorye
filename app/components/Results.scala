package components

import play.api.mvc.Results.Redirect
import controllers.routes
/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
object Results {

  val IndexPage = Redirect(routes.Application.index())

  val GamePage = Redirect(routes.Application.city())

  val NotFoundPage = Redirect(routes.Application.notFound())

  val AccessDeniedPage = Redirect(routes.Application.accessDenied())

  val BusyPage = Redirect(routes.Application.busy())
}
