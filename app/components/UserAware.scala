package components

import bean._
import model._

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
trait UserAware {

  def userIdOptional: Option[Int]

  def userOptional: Option[User]

  def gameUserOptional: Option[GameUser]

  def avatarOptional: Option[Avatar]

  def profileOptional: Option[Profile]

  def settingsOptional: Option[Settings]
}
