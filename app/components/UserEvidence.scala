package components

import bean.User

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
trait UserEvidence {
  this: UserAware =>

  lazy val user: User = userOptional.getOrElse(throw new RuntimeException("User not found."))

  val userId = userIdOptional.getOrElse(throw new RuntimeException("User Id not found."))

  lazy val gameUser = gameUserOptional.getOrElse(throw new RuntimeException("Gameuser is not found."))

  lazy val avatar = avatarOptional.getOrElse(throw new RuntimeException("Avatar is not found."))

  lazy val profile = profileOptional.getOrElse(throw new RuntimeException("Profile is not found."))

  lazy val settings = settingsOptional.getOrElse(throw new RuntimeException("Settings is not found."))
}
