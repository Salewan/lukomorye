package components

/**
  * @author Sergey Lebedev (salewan@gmail.com) 21.10.2016.
  */
trait WindowWidth {
  val value: String
  val code: Int
}

object WindowWidth {
  def apply(arg: Int) = if (arg > 0) new WindowWidth {
    override val value: String = arg + "px"
    override val code: Int = arg
  } else DefaultWidth
}

case object DefaultWidth extends WindowWidth {
  override val value: String = "default"
  override val code: Int = 0
}

