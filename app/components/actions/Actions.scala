package components.actions

import components.actions.wizard.{BuyForCoinTransformer, BuyForRubyTransformer, ConfirmationTransformer, TimeReductionTransformer}
import components.context._
import model.TransactionType
import model.price.{CoinPrice, RubyPrice}
import play.api.mvc.{ActionBuilder, ActionFunction, Request}

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
object Actions {

  val FreeOdklAction: ActionBuilder[PlayContext] = {
    ExceptionAbsorber andThen
    NotLoggedInFilter andThen
    PlayContextMaker
  }

  val FreeOnlyAction: ActionBuilder[PlayContext] = {
    ExceptionAbsorber andThen
      new OdnoklassnikiFilter andThen
      NotLoggedInFilter andThen
      PlayContextMaker
  }

  val OdnoklassnikiLoginAction: ActionBuilder[PlayContext] = ExceptionAbsorber andThen PlayContextMaker

  val AnonymousAction: ActionBuilder[PlayContext] =
    ExceptionAbsorber andThen
      PlayContextMaker andThen
      new OdnoklassnikiFilter andThen
      AnonymousFilter andThen
      new TutorialFilter[PlayContext] andThen
      new UserBusyFilter[PlayContext]

  val PlayAction: ActionBuilder[PlayContext] =
    ExceptionAbsorber andThen
      PlayContextMaker andThen
      new OdnoklassnikiFilter andThen
      new UserBusyFilter[PlayContext]()

  private def BaseGameAction(): ActionBuilder[GameContext] =
    ExceptionAbsorber andThen
      LoggedInFilter andThen
      new UserBusyFilter[Request] andThen
      GameContextMaker andThen
      new OdnoklassnikiFilter andThen
      RegisterFilter andThen
      ExileFilter

  def LevelRequiredAction(requiredLevel: Int = 1) =
    BaseGameAction() andThen
      new UserLevelFilter(requiredLevel) andThen
      new TutorialFilter[GameContext] andThen
      UserDomainFilter

  /**
    * Простой экшн с подтверждением.
    *
    * @param h проверочный код
    */
  def ConfirmationAction(h: Option[String]) = new ConfirmationTransformer(h, _ => true)

  /**
    * Экшн для покупки за рубины.
    *
    * @param h проверочный код
    * @param price цена
    * @param tr тип транзакции, которая будет залоггирована
    * @param confirm показывать ли игроку подтверждение;
    *                если не указано будет решаться по настройкам игрока, если текущие траты это "малые покупки"
    */
  def BuyForRubyAction(
                        h: Option[String],
                        price: GameContext[_] => RubyPrice,
                        tr: TransactionType,
                        confirm: Option[GameContext[_] => Boolean] = None
                      ): ActionFunction[GameContext, BuyForRubyContext] = {
    val willConfirm = (ctx: GameContext[_]) =>
      ctx.settings.confirmSmallSpendings || price(ctx).valueCeil(ctx) > config.SMALL_SPENDING_UP_TO
    new ConfirmationTransformer(h, confirm getOrElse willConfirm) andThen
    new BuyForRubyTransformer(h, price, tr)
  }

  /**
    * Экшн для покупки за монеты.
    *
    * @param h проверочный код
    * @param price цена
    * @param tr1 тип основной транзакции для логгирования
    * @param tr2 тип транзакции при докупки за рубины
    */
  def BuyForCoinAction(
                        h: Option[String],
                        price: GameContext[_] => CoinPrice,
                        tr1: TransactionType,
                        tr2: GameContext[_] => Option[TransactionType]
                      ): ActionFunction[GameContext, BuyForCoinContext] =
    new ConfirmationTransformer(h, _ => false) andThen
    new BuyForCoinTransformer(h, price, tr1, tr2)


  /**
    * Экшн для ускорения чего-либо
    *
    * @param h проверочный код
    * @param amountOfMillisToReduce количество времени которое ускоряется, в миллисекундах
    */
  def TimeReduceAction(
                        h: Option[String],
                        tr: TransactionType,
                        amountOfMillisToReduce: GameContext[_] => Long
                      ): ActionFunction[GameContext, TimeReductionContext] =
    new ConfirmationTransformer(h, _ => true) andThen
    new TimeReductionTransformer(h, amountOfMillisToReduce, tr)
}
