package components.actions

import components.Results.AccessDeniedPage
import components.context.GameContext
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future

/**
  * Created by Salewan on 29.05.2017.
  */
object AdminOnlyFilter extends ActionFilter[GameContext] {
  override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future successful {
    if (request.user.isAdmin) None else Some(AccessDeniedPage)
  }
}
