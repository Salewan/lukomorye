package components.actions

import components.context.PlayContext
import controllers.routes
import play.api.mvc.Results.Redirect
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 22.03.2016.
 */
object AnonymousFilter extends ActionFilter[PlayContext] {
  override protected def filter[A](request: PlayContext[A]): Future[Option[Result]] = Future successful {
    request.avatarOptional.flatMap(avatar => if (avatar.isAnonymous) None else Some(Redirect(routes.Application.notFound())))
  }
}
