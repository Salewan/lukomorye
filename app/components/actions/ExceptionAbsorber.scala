package components.actions

import controllers.routes
import play.api.Logger
import play.api.mvc.Results._
import play.api.mvc.{Action, ActionBuilder, BodyParser, Request, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */

// TODO remove, see ErrorHandler instead
object ExceptionAbsorber extends ActionBuilder[Request] {

  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = block(request)

  override protected def composeAction[A](action: Action[A]): Action[A] = new ErrorCatcher(action)
}

private class ErrorCatcher[A](action: Action[A]) extends Action[A] {

  override def apply(request: Request[A]): Future[Result] = action(request)

  override def parser: BodyParser[A] = action.parser
}
