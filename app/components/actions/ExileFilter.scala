package components.actions

import components.context.GameContext
import controllers.routes
import play.api.mvc.Results.Redirect
import play.api.mvc.{ActionFilter, Result}
import util.Implicits._

import scala.concurrent.Future
import scala.language.postfixOps

object ExileFilter extends ActionFilter[GameContext] {
  override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful {
    if (request.user.banEndTime.exists(_ > System.currentTimeMillis() + (1 year))) {
      Some(Redirect(routes.ExileController.exiledPage()))
    } else None
  }
}
