package components.actions

import bean.{Profile, UserReturn}
import components.context.GameContext
import play.api.Logger
import play.api.mvc._
import util.OnlineUtil
import util.Implicits._
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.Try

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
object GameContextMaker extends ActionTransformer[Request, GameContext] {
  override protected def transform[A](request: Request[A]): Future[GameContext[A]] = {
    val ctx = new GameContext[A](request)
    val user = ctx.user
    val profile = ctx.profile
    val gameUser = ctx.gameUser

    OnlineUtil.register(user)(ctx)

    val ip = ctx.getIp
    if (!profile.lastIp.equals(ip))
      profile.lastIp(ip)

    val prevUA = profile.userAgent
    setUserAgent(profile, ctx.headers)

    ctx.cookies get config.VIRUS_COOKIE_NAME flatMap (Try apply _.value.toInt toOption) match {
      case Some(virusId) =>
        if (virusId != profile.virusId) {
          Logger.info(s"Changing virus id of #${user.id} from ${profile.virusId} to $virusId, IP=$ip" +
            s" UA=${profile.userAgent}, prevUA=$prevUA")
          profile.virusId(Some(virusId))
        }
      case None =>
        ctx.processResult = result =>
          result.withCookies(Cookie(config.VIRUS_COOKIE_NAME, profile.virusId.toString, Some(Int.MaxValue), "/"))
    }

    if (UserReturn.isReturnedUser(gameUser)) {
      Logger.info(s"Returned user found #${gameUser.id}")
      ctx.hibernateSession.persist(UserReturn(gameUser))
    }

    val now = System.currentTimeMillis()
    if (now - gameUser.lastActionTime.getOrElse(0L) > (1 minute)) gameUser.lastActionTime(now)

    Future.successful(ctx)
  }

  private def setUserAgent(profile: Profile, headers: Headers): Unit = {
    profile.userAgent(headers.get("User-Agent"))
    profile.operaUserAgent(
      headers.get("X-OperaMini-Phone-UA") orElse
      headers.get("X-Original-User-Agent") orElse
      headers.get("X-Device-User-Agent") orElse
      headers.get("Device-Stock-UA")
    )
  }
}
