package components.actions

import components.Results
import play.api.mvc.{ActionFilter, Request, Result}

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
object LoggedInFilter extends ActionFilter[Request] {
  override protected def filter[A](request: Request[A]): Future[Option[Result]] = Future.successful {
    if (request.session.get(config.COOKIE_ID_NAME).exists(_.toInt > 0)) None
    else Some(Results.IndexPage)
  }
}
