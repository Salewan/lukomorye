package components.actions

import controllers.OdnoklassnikiController
import play.api.Logger
import play.api.mvc.{ActionFilter, Request, Result}
import play.api.mvc.Results.Redirect

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.higherKinds
import OdnoklassnikiFilter.logger

class OdnoklassnikiFilter[T[X] <: Request[X]] extends ActionFilter[T] {

  override protected def filter[A](request: T[A]): Future[Option[Result]] = Future {
    if (request.host.toLowerCase.contains(OdnoklassnikiController.ODKL_DOMAIN) &&
        !request.session.get(config.COOKIE_ID_NAME).exists(_.toInt > 0)) {
      logger.debug("Redirecting to OdnoklassnikiController.ODKL_APP_URL")
      Some(Redirect(OdnoklassnikiController.ODKL_APP_URL))
    } else None
  }
}

object OdnoklassnikiFilter {
  val logger = Logger(OdnoklassnikiFilter.getClass.getName.stripSuffix("$"))
}
