package components.actions

import components.Results.AccessDeniedPage
import controllers.OdnoklassnikiController
import play.api.mvc.{ActionFilter, Request, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.higherKinds

class OdnoklassnikiProhibitedFilter[T[X] <: Request[X]] extends ActionFilter[T] {

  override protected def filter[A](request: T[A]): Future[Option[Result]] = Future {
    if (request.host.toLowerCase.contains(OdnoklassnikiController.ODKL_DOMAIN))
      Some(AccessDeniedPage)
    else None
  }
}