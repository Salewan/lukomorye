package components.actions

import components.context.PlayContext
import play.api.mvc.{Request, ActionTransformer}

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
object PlayContextMaker extends ActionTransformer[Request, PlayContext] {
  override protected def transform[A](request: Request[A]): Future[PlayContext[A]] =
    Future.successful(new PlayContext[A](request))
}
