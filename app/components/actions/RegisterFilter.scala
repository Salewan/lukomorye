package components.actions

import components.context.GameContext
import controllers.routes
import play.api.mvc.Results.Redirect
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future

object RegisterFilter extends ActionFilter[GameContext] {

  override def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful {

    if (request.gameUser.level >= config.REG_LEVEL && request.avatar.isAnonymous) {
      if (request.user.isOdnoklassnikiUser && !request.uri.contains("changeRegData"))
        Some(Redirect(routes.SettingsController.changeRegData(2)))
      else if (!request.user.isOdnoklassnikiUser && !request.uri.contains("mandatoryregistration"))
        Some(Redirect(routes.RegistrationController.registrationPageMandatory()))
      else
        None

    } else None
  }
}
