package components.actions

import components.Results.AccessDeniedPage
import components.context.GameContext
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future

object SupporterFilter extends ActionFilter[GameContext] {
  override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future successful {
    if (request.user.isAdminOrSupporter) None else Some(AccessDeniedPage)
  }
}
