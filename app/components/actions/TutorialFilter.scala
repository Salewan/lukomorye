package components.actions

import components.context.PlayContext
import controllers.routes
import play.api.mvc.Results._
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future
import scala.language.higherKinds

/**
 * @author Sergey Lebedev (salewan@gmail.com) 24.03.2016.
 */
class TutorialFilter[T[_] <: PlayContext[_]] extends ActionFilter[T] {

  override protected def filter[A](request: T[A]): Future[Option[Result]] = Future.successful {
    lazy val gameUser = request.gameUserOptional.get
    if (!request.isLoggedIn) None
    else {
      if (gameUser.isTutorialCompleted) {
        val game = Some(Redirect(routes.Application.city().unique()))
        if (request.path.startsWith("/tutorial")) game
        else None
      }
      else {
        val tutorial = Some(Redirect(routes.TutorialController.tutorial(gameUser.tutorialStep).unique()))
        if (request.path.startsWith("/tutorial") && request.getQueryString("step").exists(_.equals(gameUser.tutorialStep.toString))) None
        else tutorial
      }
    }
  }
}
