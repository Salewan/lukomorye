package components.actions

import components.PlayComponentsHolder
import components.Results.BusyPage
import play.api.mvc.{ActionFilter, Request, Result}

import scala.concurrent.Future
import scala.language.higherKinds

/**
  * Created by Salewan on 07.06.2017.
  */
class UserBusyFilter[T[_] <: Request[_]] extends ActionFilter[T] {

  override protected def filter[A](request: T[A]): Future[Option[Result]] = Future.successful {
    request.session.get(config.COOKIE_ID_NAME).map(_.toInt).
      flatMap(uid => PlayComponentsHolder.cacheApi.get[Long](s"lock.user.${uid.toString}")).
      filter(_ != request.id).
      map(_ => BusyPage)
  }
}
