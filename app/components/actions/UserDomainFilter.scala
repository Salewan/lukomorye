package components.actions

import components.context.GameContext
import play.api.mvc.{ActionFilter, Result}
import controllers.routes
import org.apache.commons.lang3.StringUtils
import play.api.mvc.Results.Redirect
import play.api.http.Status.SEE_OTHER
import scala.concurrent.Future
import play.api.Logger

/**
  * Created by Salewan on 05.06.2017.
  */
object UserDomainFilter extends ActionFilter[GameContext] {
  override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful{
    val profile = request.profile
    val user = request.user
    val domain = request.getDomain
    if (
      StringUtils.isBlank(profile.domain) ||
      profile.domain.equalsIgnoreCase(domain) ||
      StringUtils.prependIfMissingIgnoreCase(profile.domain, "www.", null).
        equalsIgnoreCase(StringUtils.prependIfMissingIgnoreCase(domain, "www.", null))
    ) None
    else {
      val url = s"http://${profile.domain}"
      Logger.debug(s"DOMAIN FILTER ${user.id}#user's domain@${profile.domain} doesn't match the actual one@$domain, ...redirecting to $url")
      Some(Redirect(url, SEE_OTHER))
    }
  }
}
