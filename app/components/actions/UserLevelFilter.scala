package components.actions

import components.Results
import components.context.GameContext
import play.api.mvc.{ActionFilter, Result}

import scala.concurrent.Future

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.01.2017.
  */
class UserLevelFilter(requiredLevel: Int) extends ActionFilter[GameContext] {
  override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful {
    if (request.gameUser.level >= requiredLevel) None else Some(Results.NotFoundPage)
  }
}
