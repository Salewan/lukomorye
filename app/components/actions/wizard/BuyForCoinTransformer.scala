package components.actions.wizard

import components.PlayComponentsHolder
import components.context.{BuyForCoinContext, ConfirmationContext, GameContext}
import model.TransactionType
import model.price.Price
import org.apache.commons.lang3.RandomStringUtils
import play.api.mvc.ActionTransformer

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
class BuyForCoinTransformer(
                             hash: Option[String],
                             price: GameContext[_] => Price,
                             tr: TransactionType,
                             payRest: GameContext[_] => Option[TransactionType])

  extends ActionTransformer[ConfirmationContext, BuyForCoinContext] {

  private val cache = PlayComponentsHolder.cacheApi

  override protected def transform[A](request: ConfirmationContext[A]): Future[BuyForCoinContext[A]] = {

    implicit val ctx = request

    val name = "BuyForCoin." + request.userId
    val prevState = request.wizardState
    val gameUser = ctx.gameUser
    val p = price(ctx)

    val wizardState =
    prevState match {
      case x@Confirm(h) => PayPriceConfirm(h, p)
      case Success =>
        if (gameUser.buy(p, tr)) {
          cache.remove(request.name)
          cache.remove(name)
          Success
        }
        else {
          val willPayRest = payRest(request)
          if (willPayRest.isEmpty) {
            cache.remove(request.name)
            cache.remove(name)
            NotEnoughCoin(gameUser.deficit(p))
          }
          else {
            val step = cache.get[WizardStep](name).getOrElse(new WizardStep(name, None, false))

            if (hash.isEmpty || step.hash.isEmpty || !hash.get.equals(step.hash.get)) {
              step.hash = Some(RandomStringUtils.randomAlphanumeric(8))
              cache.set(name, step, config.WIZARD_STATE_CACHE_STORE_TIME)
              PayRestConfirm(step.hash, p, gameUser.deficit(p), gameUser.rubyRest(p))
            }
            else {
              cache.remove(request.name)
              cache.remove(name)
              if (gameUser.buyWithBothCurrencies(p, tr, willPayRest.get)) Success
              else NotEnoughRuby(gameUser.restDeficit(p))
            }
          }
        }
    }

    Future.successful(new BuyForCoinContext[A](request, wizardState))
  }
}
