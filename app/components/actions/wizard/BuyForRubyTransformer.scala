package components.actions.wizard

import components.PlayComponentsHolder
import components.context.{BuyForRubyContext, ConfirmationContext, GameContext}
import model.price.RubyPrice
import model.TransactionType
import play.api.mvc.ActionTransformer

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
class BuyForRubyTransformer(hash: Option[String], price: GameContext[_] => RubyPrice, tr: TransactionType)
extends ActionTransformer[ConfirmationContext, BuyForRubyContext] {
  private val cache = PlayComponentsHolder.cacheApi

  override protected def transform[A](request: ConfirmationContext[A]): Future[BuyForRubyContext[A]] = {

    implicit val ctx = request

    val prevState = request.wizardState
    val gameUser = ctx.gameUser
    val p = price(ctx)

    val wizardState =
    prevState match {
      case x@Confirm(h) => PayPriceConfirm(h, p)
      case Success =>
        cache.remove(request.name)
        if (gameUser.buy(p, tr)) Success
        else NotEnoughRuby(gameUser.deficit(p))
    }
    Future.successful(new BuyForRubyContext[A](request, wizardState))
  }
}
