package components.actions.wizard

import components.PlayComponentsHolder
import components.context.{ConfirmationContext, GameContext}
import org.apache.commons.lang3.RandomStringUtils
import play.api.mvc.ActionTransformer

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
class ConfirmationTransformer(hash: Option[String], confirmationRequired: GameContext[_] => Boolean)
extends ActionTransformer[GameContext, ConfirmationContext] {
  private val cache = PlayComponentsHolder.cacheApi

  override protected def transform[A](request: GameContext[A]): Future[ConfirmationContext[A]] = {
    val name = "Confirmation." + request.userId
    val step = cache.get[WizardStep](name).getOrElse(new WizardStep(name, None, false))

    val wizardState =
    if (confirmationRequired(request) && (hash.isEmpty || step.hash.isEmpty || (!hash.get.equals(step.hash.get) && !step.passed) )) {
      step.hash = Some(RandomStringUtils.randomAlphanumeric(8))
      step.passed = false
      Confirm(step.hash)
    }
    else {
      step.passed = true
      Success
    }

    cache.set(name, step, config.WIZARD_STATE_CACHE_STORE_TIME)
    Future.successful(new ConfirmationContext[A](request, name, wizardState))
  }
}
