package components.actions.wizard

import components.PlayComponentsHolder
import components.context.{ConfirmationContext, GameContext, TimeReductionContext}
import model.TransactionType
import play.api.mvc.ActionTransformer
import util.TimeReduction

import scala.concurrent.Future
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 29.11.2016.
  */
class TimeReductionTransformer(hash: Option[String], time: GameContext[_] => Long, tr: TransactionType)
extends ActionTransformer[ConfirmationContext, TimeReductionContext] {

  val cache = PlayComponentsHolder.cacheApi

  override protected def transform[A](request: ConfirmationContext[A]): Future[TimeReductionContext[A]] = {

    implicit val ctx = request

    val prevState = request.wizardState
    val gameUser = ctx.gameUser

    // одна минута ускорения за одно "зелье времени"
    val p = TimeReduction.price(time(ctx))

    val wizardState = prevState match {
      case x@Confirm(h) => PayPriceConfirm(h, p)
      case Success =>
        cache.remove(request.name)
        if (gameUser.buy(p, tr)) Success
        else NotEnoughDreamDust(gameUser.deficit(p))
    }
    Future.successful(new TimeReductionContext[A](request, wizardState))
  }
}
