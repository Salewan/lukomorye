package components.actions.wizard

import model.price.Price
import model.Deficit

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
trait WizardState
case object Success extends WizardState
case class Confirm(hash: Option[String]) extends WizardState
case class NotEnoughRuby(deficit: Deficit) extends WizardState
case class NotEnoughDreamDust(deficit: Deficit) extends WizardState
case class NotEnoughCoin(deficit: Deficit) extends WizardState
case class PayPriceConfirm(hash: Option[String], price: Price) extends WizardState
case class PayRestConfirm(hash: Option[String], originalPrice: Price, deficit: Deficit, rubyRest: Long) extends WizardState

