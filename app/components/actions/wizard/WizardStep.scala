package components.actions.wizard

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
class WizardStep(val name: String, var hash: Option[String], var passed: Boolean) {

}
