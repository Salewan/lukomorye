package components.context

import components.actions.wizard.{NotEnoughCoin, NotEnoughRuby, PayPriceConfirm, PayRestConfirm, Success, WizardState}
import model.Deficit

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
class BuyForCoinContext[A](request: ConfirmationContext[A], val wizardState: WizardState) extends GameContext[A](request) {

  def fold[R](
               success: => R,
               payConfirm: PayPriceConfirm => R,
               coinDeficit: Deficit => R,
               payRestConfirm: PayRestConfirm => R,
               rubyDeficit: Deficit => R ) = wizardState match {

    case x@PayPriceConfirm(_,_) => payConfirm(x)
    case Success => success
    case NotEnoughCoin(d) => coinDeficit(d)
    case x@PayRestConfirm(_,_,_,_) => payRestConfirm(x)
    case NotEnoughRuby(d) => rubyDeficit(d)
    case x => throw new RuntimeException("I do not know that case " + x.toString)
  }
}
