package components.context

import components.actions.wizard.{NotEnoughRuby, PayPriceConfirm, Success, WizardState}
import model.Deficit

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
class BuyForRubyContext[A](request: ConfirmationContext[A], val wizardState: WizardState) extends GameContext[A](request) {

  def fold[R](bought: => R, confirmation: PayPriceConfirm => R, deficit: Deficit => R): R =
    wizardState match {
      case x@PayPriceConfirm(_,_) => confirmation(x)
      case Success =>
        bought
      case NotEnoughRuby(d) =>
        deficit(d)
      case x => throw new RuntimeException("I do not know that case " + x.toString)
    }

}
