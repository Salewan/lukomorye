package components.context

import components.PlayComponentsHolder
import components.actions.wizard.{Confirm, Success, WizardState}
import play.api.mvc.Request

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.03.2016.
 */
class ConfirmationContext[A](request: Request[A], val name: String, val wizardState: WizardState) extends GameContext[A](request) {

  val cache = PlayComponentsHolder.cacheApi

  def fold[R](confirmed: => R, noConfirmed: Option[String] => R) =
    wizardState match {
      case Confirm(hash) => noConfirmed(hash)
      case Success =>
        cache.remove(name)
        confirmed
      case x => throw new RuntimeException("I don't know that case. " + x.toString)
    }
}
