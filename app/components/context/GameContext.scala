package components.context

import bean.state.MissionState
import components.{Feedbackable, UserEvidence}
import dao.{MissionDAO, TaskDAO}
import model.clan.ClanBonus
import play.api.mvc.Request

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
class GameContext[A](request: Request[A]) extends PlayContext[A](request) with UserEvidence with Feedbackable {

  override val keySuffix: Int = userId

  if (gameUser.isTutorialCompleted) {

    // удаляем просроченные задания
    gameUser.tasks.toSeq.filter(_.isExpired).foreach { expired =>
      expired.delete()
      TaskDAO(hibernateSession).generateTasks(gameUser, 1) // тут же генерация
    }

    // генерируем новые
    val user = gameUser
    val now = System.currentTimeMillis()
    val taskGenInterval = config.TASKS_GENERATION_INTERVAL -
      user.clan.map(_.valueOf(ClanBonus.TaskGenSpeedup).asInstanceOf[Long]).getOrElse(0L)

    GameContext.processAccumulatedUpdates(user.tasksGenerationTime,
      taskGenInterval,
      user.tasks.size,
      user.tasksCountMax,
      count => {
        TaskDAO(hibernateSession).generateTasks(user, count)
        if (user.tasks.size >= user.tasksCountMax) user.tasksGenerationTime(now)
        else user.tasksGenerationTime(user.tasksGenerationTime + count * taskGenInterval)
        hibernateSession.flush()
      })

    // удаляем просроченные поручения
    gameUser.missions.toSeq.filter(m => m.state == MissionState.NEW && m.isExpired && !m.inScare).foreach { expired =>
      expired.closed(true)
      expired.delete()
      MissionDAO(hibernateSession).generateMissions(gameUser, 1) // тут же генерация
    }

    val missionGenInterval = config.MISSIONS_GENERATION_INTERVAL -
      user.clan.map(_.valueOf(ClanBonus.MissionGenSpeedup).asInstanceOf[Long]).getOrElse(0L)
    // генерируем новые поручения
    GameContext.processAccumulatedUpdates(user.missionsGenerationTime,
      missionGenInterval,
      user.missions.count(_.isNew),
      user.missionsCountMax,
      count => {
        MissionDAO(hibernateSession).generateMissions(user, count)
        if (user.missions.count(_.isNew) >= user.missionsCountMax) user.missionsGenerationTime(now)
        else user.missionsGenerationTime(user.missionsGenerationTime + count * missionGenInterval)
        user.missionsGenerationTime(now)
        hibernateSession.flush()
      })
  }
}

object GameContext {
  private def accumulatedItemsCount(since: Long, interval: Long): Int = {
    if (interval <= 0) 0
    else ((System.currentTimeMillis() - since) / interval).toInt
  }

  private def availableAccumulatedCount(accumulated: Int, have: Int, maximum: Int): Int = {
    math.min(accumulated, math.max(0, maximum - have))
  }

  def processAccumulatedUpdates(since: Long, byInterval: Long, have: Int, max: Int, action: Int => Unit): Unit = {
    val n = availableAccumulatedCount(accumulatedItemsCount(since, byInterval), have, max)
    if (n > 0) action(n)
  }
}
