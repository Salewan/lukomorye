package components.context

import java.util.{Date, TimeZone}

import bean._
import components._
import controllers.OdnoklassnikiController
import dao.MessageDAO
import hibernate.HibernateSession
import org.joda.time.{DateTime, DateTimeZone, LocalDate}
import play.api.Mode
import play.api.mvc.{Request, Result, WrappedRequest}
import play.twirl.api.Html
import util.{ExpMoneyUnits, HelperMethods}

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
class PlayContext[A](request: Request[A]) extends WrappedRequest[A](request) with HibernateSessionAware with UserAware with FeedbackView {

  val since = System.currentTimeMillis()

  var refreshUri: String = HelperMethods.uniquify(HelperMethods.deuniqify(uri))

  private def loadTypedEntity[T](clazz: Class[T]): Option[T] = userIdOptional.flatMap(userId => hibernateSession.get(clazz, userId))

  override val hibernateSession: HibernateSession =
    PlayComponentsHolder.cacheApi.get[HibernateSession](request.id.toString).
    getOrElse(throw new RuntimeException("Hibernate session does not exists."))

  override def feedbackSeq: Seq[Html] = Seq()

  override def userIdOptional: Option[Int] = session.get(config.COOKIE_ID_NAME).map(_.toInt)

  override def userOptional: Option[User] = loadTypedEntity(classOf[User])

  override def gameUserOptional: Option[GameUser] = loadTypedEntity(classOf[GameUser])

  override def avatarOptional: Option[Avatar] = loadTypedEntity(classOf[Avatar])

  override def profileOptional: Option[Profile] = loadTypedEntity(classOf[Profile])

  override def settingsOptional: Option[Settings] = loadTypedEntity(classOf[Settings])

  def isLoggedIn: Boolean = userIdOptional.exists(_ > 0)

  def requestTime: Long = System.currentTimeMillis() - since

  def systemTime: String = config.FOOTER_DATE_FORMAT.format(new Date())

  // процент экспы от 0 до 100%
  def expAbs: Option[Double] = gameUserOptional.map { user =>
    val curr = user.currentLevelExperience
    val nextLvlExp = ExpMoneyUnits.experienceToNextLevel(user.level)
    100.0 min (1.0 * curr / nextLvlExp * 100)
  }

  // процент экспы от 2.5 до 98%, усечение до 2х знаков после запятой
  def expShifted: Double = expAbs.map {exp =>
    val shift = exp + 2.5 - exp * 0.045
    (math floor shift * 100) / 100
  } getOrElse 2.5

  def exp: String = expShifted + "%"

  def currentExpPercent: Int = {
    gameUserOptional.map { gameUser =>
      val x = 1.0 * gameUser.currentLevelExperience / ExpMoneyUnits.experienceToNextLevel(gameUser.level) * 100
      Math.min(100, Math.round(x)).asInstanceOf[Int]
    } getOrElse 0
  }

  def windowWidth: WindowWidth = settingsOptional map (_.windowWidth()) getOrElse DefaultWidth

  def getDomain: String = host

  def isOdnoklassnikiDomain: Boolean = getDomain.equalsIgnoreCase(OdnoklassnikiController.ODKL_DOMAIN)

  def getIp: String = remoteAddress

  def promoChannel: Option[String] = request.cookies.get("promo").map(promo => promo.value)

  private var preventNewMessagesFeedback = false
  def preventNewMessagesFeedback(arg: Boolean): Unit = {
    preventNewMessagesFeedback = arg
    ()
  }
  def showNewMessagesFeedback: Boolean = !preventNewMessagesFeedback && {
    for (user <- userOptional; lastIncome <- user.lastMessageTime; settings <- settingsOptional)
      yield user.newMessagesCount > 0 && lastIncome > user.lastMessagesViewTime && settings.showNewMailNotifications
  }.contains(true)

  private var preventNewsFeedback = false
  def preventNewsFeedback(arg: Boolean): Unit = {
    preventNewsFeedback = arg
    ()
  }
  def newsFeedback(): Option[Html] = if (preventNewsFeedback) None else {
    for {
      gameUser <- gameUserOptional if gameUser.level >= config.MESSAGES_ALLOWED_LEVEL && gameUser.isTutorialCompleted
      user <- userOptional
      newsForum <- new MessageDAO(hibernateSession).getNewsForum
      if newsForum.newsTime.exists(user.newsViewed.isEmpty || user.newsViewed.get < _)

    } yield {
      user.newsViewed(since)
      newsForum
    }
  } map (views.html.forum.NewsFeedbackPanel(_))

  var processResult: Result => Result = x => x

  def isDevEnvironment: Boolean = PlayComponentsHolder.mode == Mode.Dev
  def isProdEnvironment: Boolean = PlayComponentsHolder.mode == Mode.Prod

  def waplogCounter: Html = {
    if (request.uri.equals("/") || request.uri.startsWith("/?"))
      Html("<a href=\"http://waplog.net/c.shtml?617380\"><img src=\"http://c.waplog.net/617380.cnt\" alt=\"waplog\" /></a>")
    else
      Html("<a href=\"http://waplog.net/c.shtml?617381\"><img src=\"http://c.waplog.net/617381.cnt\" alt=\"waplog\" /></a>")
  }

  def mobtopCounter: Html = {
    if (request.uri.equals("/") || request.uri.startsWith("/?"))
      Html("<script type=\"text/javascript\" src=\"http://mobtop.ru/c/118693.js\"></script><noscript><a href=\"http://mobtop.ru/in/118693\"><img src=\"http://mobtop.ru/118693.gif\" alt=\"MobTop.Ru - Рейтинг и статистика мобильных сайтов\"/></a></noscript>")
    else
      Html("<script type=\"text/javascript\" src=\"http://mobtop.ru/c/118694.js\"></script><noscript><a href=\"http://mobtop.ru/in/118694\"><img src=\"http://mobtop.ru/118694.gif\" alt=\"MobTop.Ru - Рейтинг и статистика мобильных сайтов\"/></a></noscript>")
  }

  def getLogo: String = {
    if (getDomain.contains("skazka.mobi")) "skazka.jpg"
    else "logo.jpg"
  }

  def getName: String = {
    if (getDomain.contains("skazka.mobi")) "Сказка"
    else "Лукоморье"
  }

  def isDayOfMay(date: Int): Boolean = new DateTime(2018, 5, date, 0, 0).toLocalDate.equals(new LocalDate())

  def victoryDayToday: Boolean = isDayOfMay(9) || isDayOfMay(10)

  def victoryFeedbackShown: Boolean = victoryDayToday && userOptional.exists {user =>
    user.gameUser(this).isTutorialCompleted && user.victoryGiftGot.isEmpty
  }
}
