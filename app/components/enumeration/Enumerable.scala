package components.enumeration

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.03.2017.
  */
trait Enumerable[T] {
  val typeId: T
}
