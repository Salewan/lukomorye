package components.enumeration

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.03.2017.
  */
trait EnumerationBoilerplate[T, E <: Enumerable[T]] {

  def entries: IndexedSeq[E]

  private lazy val map = entries.map(t => t.typeId -> t).toMap

  def withTypeOption(tid: T) = map.get(tid)

  def withType(tid: T) = map(tid)
}
