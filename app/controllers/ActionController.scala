package controllers

import components.actions.Actions.LevelRequiredAction
import components.actions.AdminOnlyFilter
import model.action.ActionType
import model.action.ActionType._

/**
  * Created by Salewan on 30.06.2017.
  */
class ActionController extends BaseController {


  override val GameAction = LevelRequiredAction() andThen AdminOnlyFilter

  val index = routes.ActionController.changeAction()

  def changeAction = GameAction {implicit ctx =>
    val action = actionDAO.getCurrentAction.getOrElse(actionDAO.createAction(getUser, ACTION_OFF, false))
    Ok(views.html.action.ChangeActionPage(action, actionDAO.getQueuedActions))
  }

  def switchOn(ac: Int) = GameAction {implicit ctx =>
    withTypeOption(ac).foreach(actionType => actionDAO.createAction(getUser, actionType, queued = false))
    Redirect(index)
  }

  def enqueue(ac: Int) = GameAction {implicit ctx =>
    withTypeOption(ac).foreach(actionType => actionDAO.createAction(getUser, actionType, queued = true))
    Redirect(index)
  }

  def clearQueue = GameAction {implicit ctx =>
    actionDAO.cleanQueue()
    Redirect(index)
  }

  def switchOff = GameAction {implicit ctx =>
    actionDAO.createAction(getUser, ACTION_OFF, false)
    Redirect(index)
  }
}

object ActionController {
  val actions: IndexedSeq[ActionType] = ActionType.entries.filterNot(_ == ActionType.ACTION_OFF)
}
