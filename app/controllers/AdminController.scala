package controllers

import bean.{Avatar, Chest, GameUser, Profile, Reputation, Spam, User, UserMoney}
import components.PagingModel
import components.Results._
import components.actions.Actions.LevelRequiredAction
import components.actions.AdminOnlyFilter
import dao.paging.CollectionPagingDAOProxy
import model.{ReputationModel, TransactionGenerator}
import play.api.mvc.Action
import components.Results.NotFoundPage
import play.twirl.api.Html
import play.api.data.Forms._
import play.api.data._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 25.04.2016.
  */
class AdminController extends BaseController {

  override val GameAction = LevelRequiredAction() andThen AdminOnlyFilter

  def levelPlus(foreignerId: Int) = GameAction { implicit ctx =>
    session.get(classOf[GameUser], foreignerId).foreach { foreigner =>
      foreigner.incLevel()
    }
    Redirect(routes.ProfileController.profilePage(Some(foreignerId)))
  }

  def levelMinus(foreignerId: Int) = GameAction { implicit ctx =>
    session.get(classOf[GameUser], foreignerId).foreach { foreigner =>
      foreigner.level(foreigner.level - 1)
    }
    Redirect(routes.ProfileController.profilePage(Some(foreignerId)))
  }

  def incrementRuby(foreignerId: Int, x: Int) = GameAction {implicit ctx =>
    session.get(classOf[UserMoney], foreignerId).foreach { money =>
      money.addRubies(x, TransactionGenerator.ADMIN_WISHES_TX.get)
    }
    Redirect(routes.ProfileController.profilePage(Some(foreignerId)))
  }

  def incrementCoin(foreignerId: Int, x: Int) = GameAction {implicit ctx =>
    session.get(classOf[UserMoney], foreignerId).foreach { money =>
      money.addCoins(x, TransactionGenerator.ADMIN_WISHES_TX.get)
    }
    Redirect(routes.ProfileController.profilePage(Some(foreignerId)))
  }

  def incrementDreamdust(foreignerId: Int, x: Int) = GameAction {implicit ctx =>
    session.get(classOf[UserMoney], foreignerId).foreach { money =>
      money.addDreamDust(x, TransactionGenerator.ADMIN_WISHES_TX.get)
    }
    Redirect(routes.ProfileController.profilePage(Some(foreignerId)))
  }

  def logout = Action { IndexPage.withNewSession }

  /**
    * Страница спама
    */
  def spamPage(userId: Option[Int], p: Int) = LevelRequiredAction() {implicit ctx =>
    val user = getUser

    if (!user.isAdminOrSupporter) AccessDeniedPage
    else {
      val optionalSender = userId.flatMap(id => session.get[User](classOf[User], id))
      val spamIds = spamDAO.getSpamIds(optionalSender)
      val dao = new CollectionPagingDAOProxy[Int, Spam](spamIds, session.get(classOf[Spam], _).get, 20)
      val pg = PagingModel(p, dao, routes.AdminController.spamPage(userId, _))
      Ok(views.html.spam.SpamPage(pg, userId))
    }
  }

  /**
    * Удалить из спама сообщение
    */
  def spamDelete(userId: Option[Int], spamId: Int, p: Int) = LevelRequiredAction() {implicit ctx =>
    val user = getUser

    if (!user.isAdminOrSupporter) AccessDeniedPage
    else {
      session.get(classOf[Spam], spamId).foreach {spam =>
        session.delete(spam)
        session.flush()
      }
      Redirect(routes.AdminController.spamPage(userId, p))
    }
  }

  /**
    * Удалить страницу спама
    */
  def spamDeletePage(userId: Option[Int], p: Int) = LevelRequiredAction() {implicit ctx =>
    val user = getUser

    if (!user.isAdminOrSupporter) AccessDeniedPage
    else {
      val optionalSender = userId.flatMap(id => session.get[User](classOf[User], id))
      val spamIds = spamDAO.getSpamIds(optionalSender)
      val dao = new CollectionPagingDAOProxy[Int, Spam](spamIds, session.get(classOf[Spam], _).get, 20)
      val pg = PagingModel(p, dao, routes.AdminController.spamPage(userId, _))
      spamDAO.deleteSpam(pg.page.map(_.id).toSeq)
      Redirect(routes.AdminController.spamPage(userId, p))
    }
  }

  /**
    * Скрыть/показать админскую панельку в профиле.
    */
  def toggleAdminPanel(profileId: Int) = GameAction {implicit ctx =>
    val user = getUser
    if (!user.isAdminOrSupporter) AccessDeniedPage
    else {
      user.settings.toggleShowAdminPanel()
      Redirect(routes.ProfileController.profilePage(Some(profileId)))
    }
  }

  /**
    * Скрыть/показать саппорт-панель в профиле.
    */
  def toggleSupportPanel(profileId: Int) = LevelRequiredAction() {implicit ctx =>
    val user = getUser
    if (!user.isAdminOrSupporter) AccessDeniedPage
    else {
      user.settings.toggleShowSupportPanel()
      Redirect(routes.ProfileController.profilePage(Some(profileId)))
    }
  }

  /**
    * Скрыть/показать девелоперскую панель в профиле.
    */
  def toggleDeveloperPanel(profileId: Int) = GameAction {implicit ctx =>
    val user = getUser
    if (!user.isAdmin) AccessDeniedPage
    else {
      user.settings.toggleDeveloperPanel()
      Redirect(routes.ProfileController.profilePage(Some(profileId)))
    }
  }

  def resetCache(id: Int) = GameAction {implicit ctx =>
    session.resetCache(classOf[GameUser], id)
    session.resetCache(classOf[User], id)
    session.resetCache(classOf[Avatar], id)
    session.resetCache(classOf[Profile], id)
    session.get(classOf[GameUser], id).foreach { user =>
      user.reputation.map(_._2).foreach(rep => session.resetCache(classOf[Reputation], rep.id))
    }
    session.resetCollectionCache(classOf[GameUser], id, "Reputation")
    Redirect(routes.ProfileController.profilePage(Some(id)))
  }

  def changeRole(id: Int, role: Char) = GameAction {implicit ctx =>
    val roles = Map('u' -> "игрок", 'm' -> "модератор", 's' -> "техподдержка")
    session.get(classOf[User], id).foreach {user =>
      user.role(role)
      addFeedback(Html(user.avatar.visualNick + " теперь " + roles.getOrElse(role, "") + "."))
    }
    Redirect(routes.ProfileController.profilePage(Some(id)))
  }

  def addReputation(profileId: Int, rep: Int, x: Int) = GameAction {implicit ctx =>
    {
      for(user <- session.get(classOf[GameUser], profileId); reputation <- ReputationModel.withTypeOption(rep))
      yield (user, reputation)

    }.map {case (user, reputation) =>
      if (x > 0) {
        addFeedback(user.addReputation(reputation, x))
      } else user.reputation.find(_._2.model == reputation).foreach { case (_, rep) =>
        rep.value(rep.value + x).repForLvl(rep.repForLvl + x).repForRating(rep.repForRating + x)
      }

      Redirect(routes.ProfileController.profilePage(Some(profileId)))
    } getOrElse NotFoundPage

  }

  def addChest(profileId: Int) = GameAction { implicit ctx =>
    session.get(classOf[GameUser], profileId).foreach {user =>
      user.chests += new Chest().expireTime(now + Chest.DEFAULT_LIFETIME)
    }
    Redirect(routes.ProfileController.profilePage(Some(profileId)))
  }

  def addKey(profileId: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], profileId).foreach {user =>
      user.addKeys(1)
    }
    Redirect(routes.ProfileController.profilePage(Some(profileId)))
  }

  def changeEmail(id: Option[Int]) = GameAction {implicit ctx =>
    id.flatMap(uid => session.get(classOf[Profile], uid)).foreach {profile =>
      Form(single("email" -> optional(email))).bindFromRequest().fold(formWithErrors => {
        addFeedback(Html("Не удалось сохранить email."))
      }, success => {
        profile.email(success)
        addFeedback(Html("Email обновлён."))
      })
    }
    Redirect(routes.ProfileController.profilePage(id))
  }

  def adminPayment(id: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], id).foreach { gameUser =>
      billingDAO.successPayment(gameUser, 100, "", None, None, 0.0)
    }
    Redirect(routes.ProfileController.profilePage(Some(id)))
  }

  def admin = GameAction {implicit ctx =>
    Ok(views.html.admin.AdminPage())
  }

  def giveMission(id: Int) = GameAction { implicit ctx =>
    session.get(classOf[GameUser], id).map {gu =>
      missionDAO.generateMissions(gu, 3)
      Redirect(routes.ProfileController.profilePage(Some(id)))
    } getOrElse Refresh
  }
}
