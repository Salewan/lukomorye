package controllers

import bean.Reputation
import components.actions.Actions.LevelRequiredAction
import components.actions.AdminOnlyFilter
import components.context.GameContext
import play.api.mvc.ActionBuilder
import scala.collection.JavaConverters._

class AdminReputationController extends BaseController {

  override val GameAction: ActionBuilder[GameContext] = LevelRequiredAction() andThen AdminOnlyFilter

}
