package controllers

import components.actions.Actions._
import components.actions.{ExceptionAbsorber, OdnoklassnikiProhibitedFilter, PlayContextMaker}
import components.context.GameContext
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import play.twirl.api.Html

class Application extends BaseController {

  def index(c: Option[String]) = FreeOnlyAction { implicit ctx =>
    c.map { channel =>
      Redirect(routes.Application.index()).withCookies(Cookie("promo", channel, path = "/"))
    } getOrElse Ok(views.html.index())
  }

  def game(c: Option[String]) = FreeOnlyAction { implicit ctx =>
    val user = userDAO.createUser().provideContextInfo()
    user.promoChannel(user.promoChannel orElse c)
    Redirect(routes.TaskController.taskList()).createGameSession(user.id)
  }

  def quickStart(c: Option[String]) = FreeOnlyAction {implicit ctx =>
    val user = userDAO.createUser().provideContextInfo()
    user.promoChannel(user.promoChannel orElse c)
    user.gameUser.finishTutorial()
    Redirect(routes.MissionController.missionList(0, true)).createGameSession(user.id)
  }

  def city = GameAction {implicit ctx =>
    Ok(views.html.gameplay.CityPage())
  }

  def busy = (ExceptionAbsorber andThen PlayContextMaker) { implicit ctx => Ok(views.html.BusyPage())}

  def internalError = PlayAction(implicit ctx => Ok(views.html.common.internalErrorPage()))

  def notFound = PlayAction(implicit ctx => NotFound(views.html.common.notFoundPage()))

  def accessDenied = PlayAction(implicit ctx => Ok(views.html.common.AccessDeniedPage()))

  def exit(hash: Option[String]) =
  (GameAction andThen new OdnoklassnikiProhibitedFilter andThen ConfirmationAction(hash))
  {implicit ctx =>
    ctx.fold({
      Redirect(routes.Application.index()).withNewSession
    },
    newHash => {
      val accept = routes.Application.exit(newHash)
      val toSettings = routes.SettingsController.settings()
      addFeedback(Html("Выйти из игры?"), accept, toSettings)
      Redirect(toSettings)
    })
  }

  def pinExit = Action (play.api.mvc.Results.Redirect(routes.Application.index()).withNewSession)

  val pincodeForm = Form (single("pincode" -> nonEmptyText))

  def begin = Action(play.api.mvc.Results.Redirect(routes.Application.index()).withNewSession)

  def rules = PlayAction {implicit ctx =>
    Ok(views.html.Rules())
  }

  def border = GameAction {implicit ctx =>
    Ok(views.html.misc.BorderPage())
  }

  def refresh = PlayAction {implicit ctx => Refresh}

//  def doError: Action[AnyContent] = Action(r => throw new RuntimeException("some message of deliberate exception impacting"))
//
//  def patchPage: Action[AnyContent] = Action {implicit r =>
//    play.api.mvc.Results.Ok(views.html.common.PatchPage())
//  }
}
