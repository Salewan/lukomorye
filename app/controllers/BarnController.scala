package controllers

import components.actions.Actions.BuyForRubyAction
import model.TransactionGenerator
import model.item.Ingredient
import model.price.RubyPrice
import play.twirl.api.Html

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.10.2016.
  */
class BarnController extends BaseController {

  def barnPage = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val t = if (getUser.isAdmin) -1 else 0
    val asc = !getSettings.barnOrdering
    val barn = gameUser.barn.domainKeyMap.toSeq.
      filter {case (ing, amount) => !ing.unlimited && amount > t}.
      sortWith((a,b) => {
        val ai = a._1
        val bi = b._1

        if (ai.level == bi.level) ai.typeId > bi.typeId
        else if (asc) ai.level < bi.level
        else ai.level > bi.level
      })
    Ok(views.html.gameplay.barn.BarnPage(barn))
  }

  def barnInc(typeId: Int, amount: Int) = GameAction {implicit ctx =>
    if (getUser.isAdmin) {
      Ingredient.ingredientsMap.get(typeId) foreach {ing =>
        addFeedback(getGameUser.addIngredient(ing, amount))
      }
    }
    Redirect(routes.BarnController.barnPage())
  }

  /**
    * Проинициализировать амбар (Админский режим)
    */
  def barnInit = GameAction {implicit ctx =>
    if (getUser.isAdmin) {
      val gameUser = getGameUser
      val barn = gameUser.barn
      Ingredient.all.filter(_.level <= gameUser.level).foreach { gi =>
        if(barn.get(gi.typeId).isEmpty) barn += gi.typeId -> 0
      }
    }
    Redirect(routes.BarnController.barnPage())
  }

  /**
    * Краткий/подробный вид амбара
    */
  def toggleVisualOption = GameAction {implicit ctx =>
    getSettings.toggleInventar()
    Redirect(routes.BarnController.barnPage())
  }

  /**
    * Увеличить объем амбара
    */
  def upgradeLevel(h: Option[String]) =
  (GameAction andThen BuyForRubyAction(h, _.gameUser.barnUpgradePrice.getOrElse(RubyPrice(0)), TransactionGenerator.BARN_UPGRADE_TX.get))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.BarnController.barnPage

    ctx.fold({
      if (user.barnUpgradePrice.isEmpty) {
        session.getTransaction.rollback()
      } else {
        user.incBarnLevel()
        addFeedback(Html(s"Поздравляем! Ваш амбар увеличен до ${user.barnSize} мест!"))
      }
      Redirect(back)
    }, confirm => {
      addFeedback(confirm, accept = routes.BarnController.upgradeLevel(confirm.hash), decline = back)
      Redirect(back)
    }, deficit => {
      addFeedback(deficit)
      Redirect(back)
    })
  }

  def toggleOrdering = GameAction {implicit ctx =>
    getSettings.toggleBarnOrdering()
    Redirect(routes.BarnController.barnPage())
  }
}
