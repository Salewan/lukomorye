package controllers

import scala.concurrent.Future
import components.Feedbackable
import components.PlayComponentsHolder
import components.actions.wizard.PayPriceConfirm
import components.actions.wizard.PayRestConfirm
import components.context.GameContext
import components.context.PlayContext
import model.Deficit
import model.Reward
import model.price.RubyPrice
import play.api.Logger
import play.api.mvc.{AnyContent, Call, Controller, DiscardingCookie, Result, Results, Session}
import play.twirl.api.Html
import util.HelperMethods

import scala.language.implicitConversions

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
class BaseController extends Controller with DaoMethods with ContextMethods {

  val GameAction = components.actions.Actions.LevelRequiredAction()

  def Redirect(call: Call)(implicit ctx: PlayContext[AnyContent]): Result = ctx.processResult(super.Redirect(call.unique()) )

  private def cacheUri()(implicit ctx: PlayContext[AnyContent]): Unit = ctx.userIdOptional.foreach(userId =>
    PlayComponentsHolder.cacheApi.set("uri." + userId, HelperMethods.deuniqify(ctx.uri))
  )

  def Ok(content: Html)(implicit ctx: PlayContext[AnyContent]) = {
    cacheUri()
    ctx.processResult( Results.Ok(content) )
  }

  def future(result: Result): Future[Result] = Future.successful(result)

  private def restoreUri(implicit ctx: PlayContext[AnyContent]): String = {
    ctx.userIdOptional.flatMap(userId => PlayComponentsHolder.cacheApi.get[String]("uri." + userId)).getOrElse("/city")
  }

  def Refresh(implicit ctx: PlayContext[AnyContent]): Result = Status(SEE_OTHER).withHeaders(LOCATION -> HelperMethods.uniquify(restoreUri))

  def addFeedback(userId: Int, content: Html): Unit = addFeedback(content)(new Feedbackable {
    override val keySuffix: Int = userId
  })

  def addFeedback(content: Html)(implicit fdb: Feedbackable): Unit = fdb.addFeedback(content)

  def addFeedback(content: Html, accept: Call, decline: Call)(implicit ctx: GameContext[AnyContent]): Unit =
    addFeedback(views.html.misc.basicConfirmationView(content, accept, decline))

  def addFeedback(text: String, accept: Call, decline: Call)(implicit ctx: GameContext[AnyContent]): Unit =
    addFeedback(Html(text), accept, decline)

  def addFeedback(deficit: Deficit)(implicit fdb: Feedbackable): Unit =
    addFeedback(views.html.misc.deficit(deficit))

  def addFeedback(info: PayPriceConfirm, accept: Call, decline: Call)(implicit ctx: GameContext[AnyContent]): Unit =
    info.price match {
      case p: RubyPrice => addFeedback(views.html.misc.PayForRubyAskView(p, accept, decline))
      case _ => addFeedback(views.html.misc.payPriceAsk(info), accept, decline)
    }

  def addFeedback(info: PayRestConfirm, accept: Call, decline: Call)(implicit ctx: GameContext[AnyContent]): Unit =
    addFeedback(views.html.misc.payRestAsk(info), accept, decline)

  def addFeedback(reward: Reward)(implicit fdb: Feedbackable): Unit = if (reward.nonEmpty) addFeedback(views.html.misc.reward(reward))

  def info(message: => String): Unit = Logger.info(message)

  def createSession(id: Int): Session = Session(Map(config.COOKIE_ID_NAME -> id.toString))

  implicit def wrapResult(result: Result): WrappedResult = WrappedResult(result)
}

case class WrappedResult(result: Result) {
  def createGameSession(id: Int): Result = {
    result.withSession(Session(Map(config.COOKIE_ID_NAME -> id.toString))).discardingCookies(DiscardingCookie("promo"))
  }
}
