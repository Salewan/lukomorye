package controllers

import components.actions.Actions.LevelRequiredAction
import components.actions.Actions.BuyForRubyAction
import model.TransactionGenerator._
import model.buff.BuffType
import play.twirl.api.Html

/**
  * Created by Salewan on 28.06.2017.
  */
class BuffController extends BaseController {

  override val GameAction = LevelRequiredAction(config.BUFFS_LEVEL)

  val home = routes.BuffController.buffs()

  def buffs = GameAction {implicit ctx =>
    val user = getGameUser
    if (user.level >= config.BUFFS_LEVEL && !user.startingBuffEarned) {
      user.startingBuffEarned(true)
      user.getBuff(BuffType.HeroExp50.typeId).prolong()
      addFeedback(Html("Поздравляем! Вам теперь доступны покупки в магазине, они увеличивают получаемый Игроком или Героями опыт. " +
        "В награду Вы получаете улучшение на опыт Героям!"))
    }
    Ok(views.html.buff.BuffPage())
  }

  def buyAction(typeId: Int, h: Option[String]) = (GameAction andThen BuyForRubyAction(
    h, _ => BuffType.withType(typeId).price, BUFF_TX.get(typeId)
  )) { implicit ctx =>
    val user = getGameUser
    user.findBuff(typeId).foreach { buff =>
      ctx.fold({
        if (!buff.canBuy) session.getTransaction.setRollbackOnly()
        else {
          buff.prolong()
          addFeedback(Html(s"""Куплен бонус "${buff.buffType.name}"."""))
        }
      }, confirm => {
        addFeedback(confirm, accept = routes.BuffController.buyAction(typeId, confirm.hash), decline = home)
      }, deficit => {
        addFeedback(deficit)
      })
    }
    Redirect(home)
  }
}
