package controllers

/**
  * @author Sergey Lebedev (salewan@gmail.com) 11.01.2017.
  */
class BuildingController extends BaseController {

  def buildingList = GameAction {implicit ctx =>
    Redirect(routes.OgorodController.ogorodPage())
  }

  def productDetailsToggle(productionType: Int) = GameAction {implicit ctx =>
    ctx.settings.productDetailsToggle()
    Redirect(routes.ProductionController.productionPage(productionType))
  }
}
