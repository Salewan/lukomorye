package controllers

import bean.{Avatar, Clan}
import bean.state.ClanRole
import components.PagingModel
import components.Results.AccessDeniedPage
import components.Results.NotFoundPage
import components.actions.Actions
import components.actions.Actions.BuyForRubyAction
import dao.paging.CollectionPagingDAOProxy
import model.TransactionGenerator
import model.price.RubyPrice
import model.rating.{RatingEntity, RatingType}
import play.api.mvc.Call
import play.twirl.api.Html
import util.RuGrammar

/**
  * Created by Salewan on 27.08.2017.
  */
class CashDeskController extends BaseController {

  def index(clanId: Int, p: Int = 0) = routes.CashDeskController.cashDeskPage(clanId, p)

  def cashDeskPage(clanId: Int, p: Int, ratingType: Option[Int]) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      val rt = ratingType.flatMap(RatingType.findBankRatings).getOrElse(RatingType.CLAN_INNER_RUBIES)
      val rating = rt.getRating(clan)
      val dao = new CollectionPagingDAOProxy[RatingEntity, (Avatar, Long)](
        rating.entries,
        e => session.get(classOf[Avatar], e.id).get -> e.value,
        10
      )
      val pg = PagingModel(p, dao, routes.CashDeskController.cashDeskPage(clanId, _, ratingType))

      if (getGameUser.clan.exists(_.id == clanId) || getUser.isAdminOrSupporter)
        Ok(views.html.gameplay.clan.CashDeskPageView(clan, rating, pg, rt))
      else AccessDeniedPage

    } getOrElse NotFoundPage
  }

  def fund1Ruby(clanId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val call = (hash: Option[String]) => routes.CashDeskController.fund1Ruby(clanId, p, hash)
    delegateFunding(clanId, 1, p, h, call).apply(ctx)
  }

  def fund10Rubies(clanId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val call = (hash: Option[String]) => routes.CashDeskController.fund10Rubies(clanId, p, hash)
    delegateFunding(clanId, 10, p, h, call).apply(ctx)
  }

  def fund100Rubies(clanId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val call = (hash: Option[String]) => routes.CashDeskController.fund100Rubies(clanId, p, hash)
    delegateFunding(clanId, 100, p, h, call).apply(ctx)
  }

  def fund1000Rubies(clanId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val call = (hash: Option[String]) => routes.CashDeskController.fund1000Rubies(clanId, p, hash)
    delegateFunding(clanId, 1000, p, h, call).apply(ctx)
  }

  def fund10000Rubies(clanId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val call = (hash: Option[String]) => routes.CashDeskController.fund10000Rubies(clanId, p, hash)
    delegateFunding(clanId, 10000, p, h, call).apply(ctx)
  }

  private def delegateFunding(clanId: Int, rubies: Int, p: Int, h: Option[String], call: Option[String] => Call) =
  GameAction.async { implicit ctx =>

    session.get(classOf[Clan], clanId).map {clan =>
      val user = getGameUser

      if (!clan.canAddRubies(user, rubies)) {
        addFeedback(Html("Действует ограничение на взнос рубинов в кассу княжества."))
        future(Redirect(routes.CashDeskController.cashDeskPage(clanId, p)))
      } else if (user.clan.exists(_.id == clanId)) {
        fundRubies(clan, rubies, p, h, call).apply(ctx)
      } else future(AccessDeniedPage)

    }.getOrElse(future(NotFoundPage))
  }

  private def fundRubies(clan: Clan, rubies: Int, p: Int, h: Option[String], call: Option[String] => Call) = (
  GameAction andThen BuyForRubyAction(h, _ => RubyPrice(rubies), TransactionGenerator.CLAN_FUND_TX.get, Some(_ => true)))
  { implicit ctx =>

    val user = getGameUser
    val clanId = clan.id
    val root = index(clanId, p)

    ctx.fold({
      clan.addRubies(user, rubies, TransactionGenerator.CLAN_CASHDESK_REFILL.get)
      addFeedback(Html(s"Касса пополнена на $rubies рубинов."))
    }, confirm => {
      addFeedback(Html(s"Вы действительно хотите пополнить кассу на $rubies ${RuGrammar.rubyPayment(rubies)}?"),
        accept = call(confirm.hash), decline = index(clanId, p))
    }, deficit => {
      addFeedback(deficit)
    })

    Redirect(root)
  }

  def upgradeMembersCount(clanId: Int, h: Option[String]) = (GameAction andThen Actions.ConfirmationAction(h)) { implicit ctx =>
    val gameUser = getGameUser
    for(clan <- gameUser.clan if clan.id == clanId && gameUser.clanRole == ClanRole.LEADER;
        price <- clan.membersCountUpgradePrice) {
      ctx.fold({
        if (price.value > clan.clanMoney().rubies)
          addFeedback(views.html.gameplay.clan.NotEnoughClanRubies(price.value - clan.clanMoney().rubies))
        else {
          clan.changeRuby(-price.value, TransactionGenerator.CLAN_MEMBERS_UPGRADE.get)
          clan.membersCountUpgrade(clan.membersCountUpgrade + 1)
        }
      }, hh => {
          addFeedback(views.html.gameplay.clan.UpgradeMembersAsk(price.value),
            accept = routes.CashDeskController.upgradeMembersCount(clanId, hh),
            decline = routes.ClanController.clan(clanId))
      })
    }
    Redirect(routes.ClanController.clan(clanId))
  }
}
