package controllers

import bean.{Avatar, ChatMessage, Clan}
import components.PagingModel
import components.Results.{AccessDeniedPage, NotFoundPage}
import components.context.GameContext
import controllers.helpers.ChatInfo
import dao.paging.CollectionPagingDAO
import org.apache.commons.lang3.StringUtils
import play.api.data.Forms._
import play.api.data._
import play.api.mvc.AnyContent
import util.MessagesUtil


class ChatController extends BaseController {

  private def hasAccess(clan: Clan)(implicit ctx : GameContext[AnyContent]): Boolean = {
    getUser.isAdminOrSupporter || getGameUser.clan.exists(_.id == clan.id)
  }

  private def createChatInfo(clan: Clan, p: Int)(implicit ctx: GameContext[AnyContent]): ChatInfo = {
    val pg = PagingModel(
      p, new CollectionPagingDAO[ChatMessage](clan.chat().messages.iter()),
      routes.ChatController.chatPage(clan.id, _)
    )
    new ChatInfo(clan, pg)
  }

  private def messageForm(implicit ctx: GameContext[AnyContent]): Form[String] = Form(
    single("text" -> text)
  )






  def chatPage(clanId: Int, p: Int, reply: Option[Int]) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map {clan =>
      if (!hasAccess(clan)) AccessDeniedPage
      else {
        getGameUser.lastClanChatVisitTime(clan.chat().lastMessageTime)
        val theForm = reply.
          flatMap(session.get(classOf[Avatar], _)).
          map(ava => messageForm.fill(ava.visualNick + ", ")).
          getOrElse(messageForm)
        Ok(views.html.gameplay.clan.ClanChatPage(createChatInfo(clan, p), theForm))
      }
    }.getOrElse(NotFoundPage)
  }

  def chatPost(clanId: Int, p: Int) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map {clan =>
      if (!hasAccess(clan)) AccessDeniedPage
      else {
        messageForm.bindFromRequest().fold(formWithErrors => {
          Ok(views.html.gameplay.clan.ClanChatPage(createChatInfo(clan, p), formWithErrors))
        }, success => {
          val author = getUser
          if (!author.isBanned || author.isAdminOrSupporter) {
            var text = StringUtils.left(success, 500)
            if (StringUtils.isNotBlank(text)) {
              if (!author.isAdminOrSupporter) text = MessagesUtil.filteredText(text, ctx.domain)
              val chatMessage = new ChatMessage
              chatMessage.text(text)
              chatMessage.creationTime(now)
              chatMessage.author(author)
              chatMessage.moderator(getUser.isAdminOrSupporterOrModerator)
              clan.chat().addMessage(chatMessage)
              author.gameUser.lastClanChatVisitTime(Some(now))
            }
          }
          Redirect(routes.ChatController.chatPage(clanId, p))
        })
      }
    }.getOrElse(NotFoundPage)
  }

  def deleteMessage(id: Int, p: Int) = GameAction {implicit ctx =>
    val user = getGameUser

    {
      for {
        clan <- user.clan
        chatMessage <- session.get(classOf[ChatMessage], id) if chatMessage.canDelete(user)
      } yield (clan, chatMessage)

    } map {case (clan, chatMessage) =>
        clan.chat().messages -= chatMessage
        session.delete(chatMessage)
        session.flush()
        Redirect(routes.ChatController.chatPage(clan.id, p))

    } getOrElse AccessDeniedPage

  }

  def toggleModerate(id: Int, p: Int) = GameAction {implicit ctx =>
    getGameUser.settings.toggleChatModerationMode()
    Redirect(routes.ChatController.chatPage(id, p))
  }
}
