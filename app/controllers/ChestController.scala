package controllers

import bean.Chest
import components.PagingModel
import dao.paging.CollectionPagingDAO
import components.actions.Actions.BuyForRubyAction
import model.TransactionGenerator
import model.price.RubyPrice

import scala.concurrent.Future

/**
  * Created by Salewan on 22.08.2017.
  */
class ChestController extends BaseController {

  def index(p: Int = 0) = routes.ChestController.chestsPage(p)

  val sortByLevel: (Chest, Chest) => Boolean = (a,b) => {
    val lvl_a = a.missionGenerator.map(_.level).getOrElse(0)
    val lvl_b = b.missionGenerator.map(_.level).getOrElse(0)
    if (lvl_a == lvl_b) a.expireTime < b.expireTime
    else lvl_a > lvl_b
  }

  val sortByExpire: (Chest, Chest) => Boolean = (a, b) => {
    a.expireTime < b.expireTime
  }

  def chestsPage(p: Int, sort: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    user.chests.toSeq.foreach(chest => if (chest.isExpired) user.chests -= chest)

    val sorter = if (sort == 1) sortByExpire else sortByLevel
    val col = user.chests.toSeq.sortWith(sorter)

    val pagingDao = new CollectionPagingDAO[Chest](col)
    val pg = PagingModel[Chest](p, pagingDao, routes.ChestController.chestsPage(_, sort))
    Ok(views.html.gameplay.chest.ChestsPageView(pg))
  }

  def openChest(chestId: Int, p: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val user = getGameUser
    session.get(classOf[Chest], chestId).filter(chest => !chest.isExpired && user.chests.contains(chest)).map {chest =>
      if (user.keys > 0) {
        addFeedback(user.open(chest))
        Future.successful(Redirect(index(p)))
      } else {
        buyKey(chest, p, h).apply(ctx)
      }
    }.getOrElse(Future.successful(Redirect(index(p))))
  }

  private def buyKey(chest: Chest, p: Int, h: Option[String]) =
    (GameAction andThen BuyForRubyAction(h, _ => RubyPrice(10), TransactionGenerator.BUY_KEY_TX.get)) {implicit ctx =>
      val user = getGameUser
      ctx.fold({
        if (user.keys > 0) {
          session.getTransaction.setRollbackOnly()
          Redirect(index(p))
        } else {
          addFeedback(user.open(chest, true))
          Redirect(index(p))
        }
      }, confirm => {
        addFeedback(
          views.html.misc.BuyKeyAsk(
            confirm.price,
            accept = routes.ChestController.openChest(chest.id, p, confirm.hash),
            decline = index(p) ))
        Redirect(index(p))
      }, deficit => {
        addFeedback(deficit)
        Redirect(index(p))
      })
    }
}
