package controllers

import bean.state.ClanRole
import components.Results.AccessDeniedPage
import components.Results.NotFoundPage
import components.actions.Actions.ConfirmationAction
import model.ReputationModel
import play.twirl.api.Html
import util.TimeUtil

class ClanAllianceController extends BaseController {

  def index = routes.ClanAllianceController.alliancePage()

  def alliancePage = GameAction { implicit ctx =>
    val user = getGameUser
    user.clan.map {clan =>
      if (user.clanRole != ClanRole.LEADER) AccessDeniedPage
      else Ok(views.html.gameplay.clan.AlliancePage(clan))
    } getOrElse NotFoundPage
  }

  def enterAlliance(repType: Option[Int], h: Option[String]) = (GameAction andThen ConfirmationAction(h)) {implicit ctx =>
    {

      for {
        user <- ctx.gameUserOptional if user.clanRole == ClanRole.LEADER
        clan <- user.clan
      } yield (user, clan)

    } map {case (user, clan) =>

      val alliance = repType.flatMap(ReputationModel.withTypeOption)
      if (clan.allianceChangeCooldown.nonEmpty)
        addFeedback(Html(
          s"Сменить союз можно будет через ${TimeUtil.formatTime(clan.allianceChangeCooldown.get)}"
        ))
      else
        ctx.fold({
          clan.changeAlliance(alliance)
          addFeedback(Html("Союз изменён."))
        }, newHash => {
          addFeedback(
            Html("Вы действительно хотите изменить союз?"),
            accept = routes.ClanAllianceController.enterAlliance(repType, newHash),
            decline = index
          )
        })

      Redirect(index)
    } getOrElse NotFoundPage
  }

}
