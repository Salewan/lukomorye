package controllers

import bean.Clan
import bean.state.ClanRole
import components.Results.NotFoundPage
import model.clan.ClanBonus
import components.actions.Actions.ConfirmationAction
import components.context.GameContext
import model.TransactionGenerator
import play.api.mvc.{ActionBuilder, ActionFilter, Result}
import play.twirl.api.Html
import components.actions.Actions.LevelRequiredAction

import scala.concurrent.Future

class ClanBonusesController extends BaseController {

  def GameAction(clanId: Int): ActionBuilder[GameContext] = LevelRequiredAction().andThen(new ActionFilter[GameContext] {
    override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful {
      implicit val ct = request
      (for {
        c1 <- request.gameUser.clan
        c2 <- session.get(classOf[Clan], clanId)
      } yield {
        if (c1.id == c2.id && request.gameUser.clanRole == ClanRole.LEADER) None
        else Some(NotFoundPage)
      }) getOrElse Some(NotFoundPage)
    }
  })

  def clanBonuses(clanId: Int) = GameAction(clanId) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      Ok(views.html.gameplay.clan.ClanBonusPage(clan.bonusRegistry))
    } getOrElse NotFoundPage
  }

  def edit(clanId: Int) = GameAction(clanId) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      clan.bonusRegistry.edit()
      Redirect(routes.ClanBonusesController.clanBonuses(clanId))
    } getOrElse NotFoundPage
  }

  def clear(clanId: Int, h: Option[String]) = GameAction(clanId).andThen(ConfirmationAction(h)) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      if (clan.canRedistributeBonuses) {
        ctx.fold({
          clan.bonusRegistry.clear()
        }, newHash => {
          addFeedback(Html("Перераспределить печати?"), accept = routes.ClanBonusesController.clear(clanId, newHash),
            decline = routes.ClanBonusesController.clanBonuses(clanId))
        })
      } else {
        addFeedback(Html("В данный момент перераспределение княжеских печатей невозможно."))
      }
      Redirect(routes.ClanBonusesController.clanBonuses(clanId))
    } getOrElse NotFoundPage
  }

  def cancel(clanId: Int) = GameAction(clanId) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      clan.bonusRegistry.cancel()
      Redirect(routes.ClanBonusesController.clanBonuses(clanId))
    } getOrElse NotFoundPage
  }

  def commit(clanId: Int) = GameAction(clanId) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      clan.bonusRegistry.submit()
      Redirect(routes.ClanBonusesController.clanBonuses(clanId))
    } getOrElse NotFoundPage
  }

  def increment(clanId: Int, bonusId: Int) = GameAction(clanId) {implicit ctx =>

    for {
      clan <- session.get(classOf[Clan], clanId)
      bonus <- ClanBonus.find(bonusId)
    } {
      val reg = clan.bonusRegistry
      val curLvl = reg.currentLevel(bonus)
      if (bonus.maxLevel <= curLvl) addFeedback(Html("Достигнут максимальный уровень бонуса."))
      else if (reg.pointsRest < bonus.price(curLvl + 1)) addFeedback(Html("Не хватает печатей."))
      else clan.bonusRegistry.increment(bonus)
    }

    Redirect(routes.ClanBonusesController.clanBonuses(clanId))
  }

  def buyStamp(clanId: Int, h: Option[String]) = GameAction(clanId).andThen(ConfirmationAction(h)) {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      ctx.fold({
        if (clan.stamps >= clan.maxStamps) addFeedback(Html("Чтобы покупать больше печатей нужен более высокий уровень княжества."))
        else clan.stampPrice.foreach {price =>
          val deficit = price.value - clan.clanMoney().rubies
          if (deficit > 0) addFeedback(views.html.gameplay.clan.NotEnoughClanRubies(deficit))
          else {
            clan.clanMoney().changeRuby(-price.value, TransactionGenerator.CLAN_BUY_STAMP.get)
            clan.extraStamps(clan.extraStamps + 1)
          }
        }
        Redirect(routes.ClanBonusesController.clanBonuses(clanId))
      }, newHash => {
        addFeedback(Html("Купить печать?"), accept = routes.ClanBonusesController.buyStamp(clanId, newHash), decline = routes.ClanBonusesController.clanBonuses(clanId))
        Redirect(routes.ClanBonusesController.clanBonuses(clanId))
      })
    } getOrElse NotFoundPage
  }

  def toggleDetails(clanId: Int) = GameAction(clanId) {implicit ctx =>
    getSettings.clanBonusesDetails(!getSettings.clanBonusesDetails)
    Redirect(routes.ClanBonusesController.clanBonuses(clanId))
  }
}
