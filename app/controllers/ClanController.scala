package controllers

import java.util.Date

import bean.state.ClanRole
import bean._
import components.Results._
import components.actions.Actions._
import components.context.GameContext
import components.{HibernateSessionAware, PagingModel}
import dao.GameDAO
import dao.paging.{CollectionPagingDAO, CollectionPagingDAOProxy, DatabasePagingDAO}
import model.{ReputationModel, TransactionGenerator}
import model.price.RubyPrice
import model.rating.{RatingEntity, RatingType}
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.mvc.AnyContent
import play.twirl.api.Html
import util.{MessagesUtil, SingleLanguageValidator}

/**
  * @author Sergey Lebedev (salewan@gmail.com) 09.11.2016.
  */
class ClanController extends BaseController {

  val clanPrice = RubyPrice(1000)
  def clanForm(implicit hs: HibernateSessionAware) = Form(single("name", text.verifying(checkForm)))

  def checkForm(implicit hs: HibernateSessionAware): Constraint[String] = Constraint("") { plainText =>
    if (plainText.isEmpty)
      Invalid(ValidationError("error.clan.NameShouldNotBeEmpty"))
    else if (!plainText.trim.matches(config.NICK_PATTERN) || !SingleLanguageValidator.isValid(plainText))
      Invalid(ValidationError("error.clan.NamePattern"))
    else if (clanDAO.getClanByName(plainText).isDefined)
      Invalid(ValidationError("error.clan.NameOccupied"))
    else Valid
  }

  /**
    * Страница с формой создать княжество
    */
  def clanCreatePage = GameAction {implicit ctx =>
    Ok(views.html.gameplay.clan.ClanCreatePage(clanForm, clanPrice))
  }

  /**
    * Создать княжество
    */
  def clanCreate = GameAction {implicit ctx =>
    val back = routes.ClanController.clanCreatePage()

    clanForm.bindFromRequest().fold(

      formWithErrors => {
        Ok(views.html.gameplay.clan.ClanCreatePage(formWithErrors, clanPrice))
      },

      validName => {
        val gameUser = getGameUser
        if (gameUser.clan.isDefined) {
          addFeedback(Html("Вам нужно покинуть текущее княжество."))
          Redirect(back)
        } else if (!gameUser.buy(clanPrice, TransactionGenerator.CLAN_TX.get)) {
          addFeedback(gameUser.deficit(clanPrice))
          Redirect(back)
        } else {
          val clan = clanDAO.createClan(validName, gameUser)
          gameUser.clearOffers()
          Redirect(routes.ClanController.clan(clan.id))
        }
      })
  }


  /**
    * Страница Княжества
    */
  def clan(id: Int, p: Int) = GameAction {implicit ctx =>
    session.get(classOf[Clan], id).map {clan =>

      val pg = PagingModel(p, new CollectionPagingDAO(clan.members), routes.ClanController.clan(id, _))

      Ok(views.html.gameplay.clan.ClanPage(clan, pg))
    } getOrElse NotFoundPage
  }


  /**
    * Управление панелью Участники в княжестве
    */
  def clanMembersPanelToggle(id: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    gameUser.settings.clanMembersShownToggle()
    Redirect(routes.ClanController.clan(id))
  }


  /**
    * Отправить приглашение игроку вступить в княжество
    */
  def clanInviteTo(userId: Int) = GameAction { implicit ctx =>
    val me = getGameUser

    session.get(classOf[GameUser], userId).map { candidate =>
      me.clan.map { clan =>

        if (me.canInviteToClan(candidate.user)) {
          if (clanDAO.isCandidateOfferedByClan(clan, candidate)) {
            for (offer <- clan.offers) {

              // если есть заявка или приглашение для этого игрока
              if (offer.candidate == candidate) {
                if (offer.isOffer) addFeedback(Html("Игроку уже отправлено приглашение в ваше княжество."))
                else addFeedback(Html("Есть заявка от игрока. Можно взять в княжество."))
              }
            }
          } else {
            val offer = (new Offer).clan(clan).candidate(candidate).date(now).master(me)
            session.persist(offer)
            candidate.offers += offer
            clan.offers += offer

            // TODO попровить урлы
            MessagesUtil.sendNotificationMessage(
              candidate.user,
              s"Вы получили приглашение в княжество " +
                s"[url=/clanPage?id=${clan.id}]${clan.visualName}[/url]. " +
                s"Просмотреть информацию по вашим приглашениям можно " +
                s"[url=/profileOffers?userId=${candidate.id}]тут[/url].",
              GameDAO.ADMINISTRATION_LOGIN)

            val candAva = candidate.avatar
            val fdb = s"${candAva.visualNick} ${if (candAva.isFemale) " приглашена" else " приглашен"} в княжество."
            addFeedback(Html(fdb))
          }
        }
        Redirect(routes.ClanController.clan(clan.id))

      } getOrElse NotFoundPage
    } getOrElse NotFoundPage
  }


  /**
    * Отправить запрос на вступление в княжество
    */
  def clanSendRequest(clanId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map {clan =>

      val me = getGameUser
      if (!me.avatar.isAnonymous && me.canSendClanRequest) {
        if (clan.requestsDisabled)
          addFeedback(Html("Это княжество не принимает заявки на вступление."))
        else if (clanDAO.isCandidateOfferedByClan(clan, me))
          addFeedback(Html("У Вас уже есть заявка в это княжество."))
        else if (clan.isFull)
          addFeedback(Html("В этом княжестве нет свободных мест."))
        else {
          me.clanRequests += clan.id -> new Date()
          val offer = (new Offer).clan(clan).candidate(me).date(now)
          session.persist(offer)
          me.offers += offer
          clan.offers += offer

          // TODO попровить урлы
          MessagesUtil.sendNotificationMessage(
            me.user,
            s"Вы оставили заявку в княжество " +
              s"[url=/clanPage?id=${clan.id}]${clan.visualName}[/url]. " +
              s"Просмотреть информацию по вашим заявкам можно " +
              s"[url=/profileOffers?userId=${me.id}]тут[/url].",
            GameDAO.ADMINISTRATION_LOGIN)

          addFeedback(Html(s"Вы подали заявку в княжество ${clan.visualName}"))
        }
      }

      Redirect(routes.ClanController.clan(clanId))
    } getOrElse NotFoundPage
  }


  /**
    * Страница Заявки
    */
  def clanOffersPage(clanId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map { clan =>
      if (clan.checkRoles(getGameUser, ClanRole.LEADER, ClanRole.OFFICER)) clan.lastOffersViewDate(Some(now))

      val f = form.fill(clan.requestsDisabled)
      Ok(views.html.gameplay.clan.ClanOffersPage(clan.id, clan.offers, f))
    } getOrElse NotFoundPage
  }

  def form = Form(single("req_dis", boolean))

  /**
    * Разрешить/запретить отправлять заявки в княжество
    */
  def clanRequestsDisabledToggle(clanId: Int) = GameAction {implicit ctx =>
    val back = routes.ClanController.clan(clanId)
    form.bindFromRequest().fold(formWithErrors => {
      ()
    }, checked => {
      { for (clan <- session.get(classOf[Clan], clanId) if getGameUser.canManageClanSettings(clan))
        yield clan } foreach (_.requestsDisabled(checked))
    })
    Redirect(back)
  }

  /**
    * Принять заявку в княжество
    */
  def clanAcceptOffer(offerId: Int) = GameAction {implicit ctx =>

    session.get(classOf[Offer], offerId).map { offer =>
      val clan = offer.clan
      if (offer.isRequest && hasAccess(getUser, offer)) {
        if (clan.isFull) addFeedback(Html("В княжестве уже максимальное количество участников."))
        else {
          val candidate = offer.candidate
          if (candidate.isInClan) addFeedback(Html(s"${candidate.avatar.visualNick} уже состоит в княжестве."))
          else {
            clan.join(candidate, Some(getGameUser))
            candidate.clearOffers()
          }
        }
      }

      Redirect(routes.ClanController.clanOffersPage(clan.id))
    } getOrElse NotFoundPage
  }


  /**
    * Отклонить заявку
    */
  def clanDeclineOffer(offerId: Int) = GameAction {implicit ctx =>

    session.get(classOf[Offer], offerId).map { offer =>
      val clanId = offer.clan.id
      if (hasAccess(getUser, offer)) {
        offer.clan.offers -= offer
        offer.candidate.offers -= offer
        session.delete(offer)
      }
      Redirect(routes.ClanController.clanOffersPage(clanId))
    } getOrElse NotFoundPage
  }


  /**
    * Может ли user оперировать с заявкой в этом княжестве
    */
  def hasAccess(user: User, offer: Offer)(implicit hib: HibernateSessionAware): Boolean = {
    user.isAdminOrSupporterOrModerator ||
      offer.clan.checkRoles(user.gameUser, ClanRole.LEADER, ClanRole.OFFICER) ||
      user.isAdmin
  }

  /**
    * Повысить в должности
    */
  def clanMemberPromote(memberId: Int, p: Int) = GameAction(implicit ctx => _f(memberId, p, _.promote))

  /**
    * Сделать лидером княжества
    */
  def clanMemberMakeLiader(memberId: Int, hash: Option[String], p: Int) =
  (GameAction andThen ConfirmationAction(hash))
  {implicit ctx =>
    ctx.fold({
      _f(memberId, p, _.makeLeader)
    }, h => {

      getGameUser.clan.map { c =>
        val back = routes.ClanController.clan(c.id)
        addFeedback("Вы уверены, что хотите отдать княжение?",
          accept = routes.ClanController.clanMemberMakeLiader(memberId, h), decline = back)
        Redirect(back)
      } getOrElse NotFoundPage

    })
  }

  /**
    * Понизить в должности
    */
  def clanMemberDemote(memberId: Int, p: Int) = GameAction(implicit ctx => _f(memberId, p, _.demote))

  /**
    * Кикнуть из княжества
    */
  def clanMemberKick(memberId: Int, hash: Option[String], p: Int) = (GameAction andThen ConfirmationAction(hash))
  {implicit ctx =>
    val tpl = for (clan <- getGameUser.clan; member <- session.get(classOf[Avatar], memberId)) yield (clan, member)

    tpl.map { case (clan, member) =>
      ctx.fold({
        val res = _f(memberId, p, _.kick)
        val rem = if (member.isFemale) "исключена" else "исключён"
        addFeedback(Html(s"${member.visualNick} $rem из княжества"))
        res
      }, confirm => {
        val back = routes.ClanController.clan(clan.id)
        addFeedback(Html(s"Прогнать из княжества ${member.visualNick}?"), accept = routes.ClanController.clanMemberKick(memberId, confirm),
          decline = back)
        Redirect(back)
      })
    } getOrElse NotFoundPage
  }


  def _f(memberId: Int, page: Int, func: GameUser => GameUser => Unit)(implicit ctx: GameContext[AnyContent]) = {
    val me = getGameUser
    me.clan.map { clan =>
      session.get(classOf[GameUser], memberId) foreach func(me)
      Redirect(routes.ClanController.clan(clan.id, page))
    } getOrElse NotFoundPage
  }

  def leaveClan(h: Option[String]) = (GameAction andThen ConfirmationAction(h)) {implicit ctx =>
    val user = getGameUser
    user.clan.map { clan =>
      ctx.fold({
        clan.leave(user)
        GamePage
      }, newHash => {
        addFeedback(Html("Вы действительно хотите покинуть княжество?"),
          accept = routes.ClanController.leaveClan(newHash), decline = routes.ClanController.clan(clan.id))
        Redirect(routes.ClanController.clan(clan.id))
      })
    } getOrElse GamePage
  }

  def toggleDetails(clanId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Clan], clanId).map {clan =>
      getSettings.toggleClanDetailsShown()
      Redirect(routes.ClanController.clan(clan.id))
    } getOrElse GamePage
  }

  def history(clanId: Int, p: Int) = GameAction {implicit ctx =>
    val PAGE_SIZE = 20
    session.get(classOf[Clan], clanId).filter(clan => getGameUser.clan.exists(_.id == clan.id)).map {clan =>
      val dao = new DatabasePagingDAO[ClanHistoryRecord] {

        override protected val pageSize: Int = PAGE_SIZE

        override def loadPage(p: Int): Iterable[ClanHistoryRecord] = clanDAO.getClanHistory(clan, p, PAGE_SIZE)

        override protected def allSize: Int = clanDAO.getClanHistoryPageCount(clan, PAGE_SIZE)
      }
      val pg = PagingModel(p, dao, routes.ClanController.history(clanId, _))
      Ok(views.html.gameplay.clan.ClanJournal(clan, pg))
    } getOrElse NotFoundPage
  }
}
