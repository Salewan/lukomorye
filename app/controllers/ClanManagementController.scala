package controllers

import _root_.util.MessagesUtil
import bean.state.ClanRole
import bean.{Clan, GameUser, Notification}
import components.Results
import components.context.GameContext
import model.NotificationType
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}

class ClanManagementController extends BaseController {

  private def getForm(implicit ctx: GameContext[_]): Form[(String, String)] = Form(tuple(
    "title" -> text,
    "text" -> text
  ) verifying Constraint[(String, String)]("") {case (title, text) =>
      val buf = collection.mutable.ArrayBuffer[String]()
      if (title.trim.isEmpty) buf += "error.clanManagement.titleRequired"
      if (title.trim.length > 255) buf += "error.clanManagement.titleTooLong"
      if (text.trim.isEmpty) buf += "error.clanManagement.messageTextRequired"
      if (text.trim.length > 500) buf += "error.clanManagement.messageTextTooLong"
      if (buf.nonEmpty) Invalid(buf.map(ValidationError(_)))
      else Valid
  })

  private def getMeAndClanSafe(implicit ctx: GameContext[_]): Option[(GameUser, Clan)] = for {
    gu <- ctx.gameUserOptional
    cl <- gu.clan if cl.checkRoles(gu, ClanRole.LEADER, ClanRole.preLeader)
  } yield (gu, cl)

  def manageClan = GameAction {implicit ctx =>

    getMeAndClanSafe.map { case (user, clan) =>
      Ok(views.html.gameplay.clan.ClanManagement(clan, getForm))
    } getOrElse Results.NotFoundPage

  }

  def sendNotice = GameAction { implicit ctx =>
    getMeAndClanSafe.map { case (user, clan) =>
      getForm.bindFromRequest().fold(
        formWithErrors => {
          Ok(views.html.gameplay.clan.ClanManagement(clan, formWithErrors))
        },
        { case (title, text) =>
          val body = MessagesUtil.processForumMessage(text, true, false).replace("\"", "\\\"")
          val jsonStr = s"""{"title": "$title", "text": "$body"}"""
          clan.members.foreach(member => {
            if (member.notifications.size > 1) member.notifications.clear()
            member.notifications += Notification(member, NotificationType.ClanNotice, jsonStr)
          })
          clan.noticeLastTime(now)
          Redirect(routes.ClanManagementController.manageClan())
        }
      )
    } getOrElse Results.NotFoundPage
  }

}
