package controllers

import java.util.Date

import bean._
import components.HibernateSessionAware
import components.context.{GameContext, PlayContext}
import hibernate.HibernateSession

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.01.2017.
  */
trait ContextMethods {

  val now = System.currentTimeMillis()

  val date = new Date()

  def session(implicit hibernateSessionAware: HibernateSessionAware): HibernateSession = hibernateSessionAware.hibernateSession

  def userId(implicit ctx: GameContext[_]) = ctx.userId

  def getUser(implicit ctx: GameContext[_]): User = ctx.user

  def getGameUser(implicit ctx: GameContext[_]): GameUser = ctx.gameUser

  def getAvatar(implicit ctx: GameContext[_]): Avatar = ctx.avatar

  def getProfile(implicit ctx: GameContext[_]): Profile = ctx.profile

  def getSettings(implicit ctx: GameContext[_]): Settings = ctx.settings

  def isLoggedIn(implicit ctx: PlayContext[_]): Boolean = ctx.userIdOptional.isDefined
}
