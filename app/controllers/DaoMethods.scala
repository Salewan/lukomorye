package controllers

import components.HibernateSessionAware
import dao._
import hibernate.HibernateSession

import scala.language.implicitConversions

/**
  * @author Sergey Lebedev (salewan@gmail.com) 09.11.2016.
  */
trait DaoMethods {

  private var udao: Option[UserDAO] = None
  def userDAO(implicit h: HibernateSessionAware): UserDAO = {
    udao = udao.orElse(Some(new UserDAO(h)))
    udao.get
  }

  private var _clanDao: Option[ClanDAO] = None
  def clanDAO(implicit h: HibernateSessionAware): ClanDAO = {
    _clanDao = _clanDao.orElse(Some(new ClanDAO(h)))
    _clanDao.get
  }

  private var _missionDao: Option[MissionDAO] = None
  def missionDAO(implicit h: HibernateSessionAware): MissionDAO = {
    _missionDao = _missionDao.orElse(Some(new MissionDAO(h)))
    _missionDao.get
  }

  private var _taskDao: Option[TaskDAO] = None
  def taskDAO(implicit h: HibernateSessionAware): TaskDAO = {
    _taskDao = _taskDao.orElse(Some(new TaskDAO(h)))
    _taskDao.get
  }

  private var _messageDao: Option[MessageDAO] = None
  def messageDAO(implicit h: HibernateSessionAware): MessageDAO = {
    _messageDao = _messageDao.orElse(Some(new MessageDAO(h)))
    _messageDao.get
  }

  private var _spamDao: Option[SpamDAO] = None
  def spamDAO(implicit h: HibernateSessionAware): SpamDAO = {
    _spamDao = _spamDao.orElse(Some(new SpamDAO(h)))
    _spamDao.get
  }

  private var _ticketDao: Option[TicketDAO] = None
  def ticketDAO(implicit h: HibernateSessionAware): TicketDAO = {
    _ticketDao = _ticketDao.orElse(Some(new TicketDAO(h)))
    _ticketDao.get
  }

  private var _actionDao: Option[ActionDAO] = None
  def actionDAO(implicit h: HibernateSessionAware): ActionDAO = {
    _actionDao = _actionDao.orElse(Some(new ActionDAO(h)))
    _actionDao.get
  }

  private var _billingDao: Option[BillingDAO] = None
  def billingDAO(implicit h: HibernateSessionAware): BillingDAO = {
    _billingDao = _billingDao.orElse(Some(new BillingDAO(h)))
    _billingDao.get
  }

  private var _moneyTransactionDao: Option[MoneyTransactionDAO] = None
  def moneyTransactionDAO(implicit h: HibernateSessionAware): MoneyTransactionDAO = {
    _moneyTransactionDao = _moneyTransactionDao.orElse(Some(new MoneyTransactionDAO(h)))
    _moneyTransactionDao.get
  }

  implicit def hibernateSession(hibernateSessionAware: HibernateSessionAware): HibernateSession = hibernateSessionAware.hibernateSession
}
