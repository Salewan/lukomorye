package controllers

import bean._
import bean.messages.personal.{PrivateDialog, PrivateMessage}
import com.google.common.collect.Lists
import components.PagingModel
import components.actions.Actions.ConfirmationAction
import components.context.GameContext
import dao.paging.{CollectionPagingDAO, CollectionPagingDAOProxy}
import model.TicketState
import model.messaging._
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.AnyContent
import play.twirl.api.Html
import util.Implicits._
import util.{MessagesUtil, SmilesUtil, TimeUtil}

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 26.01.2017.
  */
class DialogsController extends BaseController {

  val SPAM_PENALTY = 5 minutes

  /**
    * Список диалогов
    */
  def dialogsPage(page: Int) = GameAction {implicit ctx =>
    val user = getUser
    user.lastMessagesViewTime(now)
    val dialogs = user.dialogs.values
    val pageDialogs = dialogs.filterNot(_.deleted).toSeq.sortWith((d1, d2) => d1.lastMessageDate > d2.lastMessageDate)
    val dao = new CollectionPagingDAOProxy[PrivateDialog, (PrivateDialog, Option[PrivateMessage])](pageDialogs, d => {
      d -> messageDAO.getLastMessageId(d.id).flatMap(session.get(classOf[PrivateMessage], _))
    }, 10)
    val pg = PagingModel(page, dao, routes.DialogsController.dialogsPage)

    //if (dialogs.exists(d => !d.deleted && d.hasNewMessages)) user.newMessagesCount(0)
    user.newMessagesCount(0)

    val anonymous = user.avatar.isAnonymous
    val allowed = getGameUser.level >= config.MESSAGES_ALLOWED_LEVEL

    Ok(views.html.dialogs.PrivateDialogsPage(pg, anonymous, allowed))
  }

  /**
    * Новый диалог
    */
  def dialogsNewPage = dialogPage(None, 0)

  def makeForm(targetId: Option[Int])(implicit ctx: GameContext[AnyContent]) = Form (
    tuple(
      "target" -> text,
      "text" -> text
    ) verifying Constraint[(String, String)]("") { case (t, txt) =>
      
      val author = ctx.user
      val targetNick = t.trim.toLowerCase
      var error: String = ""
      var target: Option[User] = None
      
      if (targetId.isEmpty) {
        if (targetNick.isEmpty) error = "error.dialogs.TargetRequired"
        else target = userDAO.findByNick(targetNick).map(_.user)
      }
      else target = targetId.flatMap(session.get(classOf[User], _))

      if (txt.trim.isEmpty) error = "error.dialogs.TextEmpty"

      if (error.nonEmpty) Invalid(error)
      else if (target.isEmpty) Invalid("error.dialogs.TargetNotFound")
      else {
        val senderNick = author.avatar.nick
        val gameUser = author.gameUser
        if (author.spamPenaltyEndTime.exists(_ >= now)) Invalid("error.dialogs.YouSpammer")
        else if (!author.isAdminOrSupporterOrModerator && 
          target.exists(_.gameUser.ignore.contains(gameUser))) Invalid("error.dialogs.YouIgnored")
        else if (!author.isAdminOrSupporterOrModerator &&
          !senderNick.exists(config.ADMINISTRATION_LOGIN.equals) &&
          !senderNick.exists(config.POST_LOGIN.equals) &&
          target.exists(_.settings.onlyFriendsMessages) &&
          !target.exists(_.gameUser.friends.contains(gameUser)) &&
          !target.exists(_.gameUser.areCoClanners(gameUser))) Invalid("error.dialogs.TargetReceiveMailsFromFriendsOnly")
        else if (!target.map(_.gameUser).exists(gameUser.canWriteMessage)) Invalid("error.dialogs.CannotWriteMessage")
        else Valid
      }
    }
  )

  def generateDialogPageView(targetId: Option[Int], page: Int,
                             messageForm: Form[(String, String)], ticketId: Option[Int] = None)(implicit ctx: GameContext[AnyContent]) = {
    ctx.preventNewMessagesFeedback(true)
    val user = getUser
    val dialog = targetId.flatMap(user.dialogs.get)
    val messages = dialog.map(_.id).map(messageDAO.getMessagesIds).getOrElse(Iterable.empty)
    val pg = PagingModel(page, new CollectionPagingDAO[Int](messages),
      routes.DialogsController.dialogPage(targetId, _))
    val target = dialog.map(_.target.gameUser).orElse(targetId.flatMap(session.get(classOf[GameUser], _)))
    views.html.dialogs.PrivateDialogPage(messageForm, dialog, target, new Dialog(pg), ticketId)
  }

  private def getTicket(ticketId: Option[Int])(implicit ctx: GameContext[AnyContent]) = for {
    user <- ctx.userOptional if user.isAdminOrSupporter
    tid <- ticketId
    ticket <- session.get(classOf[Ticket], tid)
  } yield ticket

  /**
    * Диалог
    */
  def dialogPage(targetId: Option[Int], page: Int, ticketId: Option[Int] = None) = GameAction {implicit ctx =>
    var dialogForm = makeForm(targetId)

    getTicket(ticketId).foreach {ticket =>
      dialogForm = dialogForm.fill(ticket.sender.avatar.nick.getOrElse("Гость") -> s"""Здравствуйте, Вы писали: "${ticket.text}"""")
    }

    Ok(generateDialogPageView(targetId, page, dialogForm, ticketId))
  }

  /**
    * Сделать другом
    */
  def makeFriend(targetId: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val user = getUser
    user.dialogs.get(targetId).filter(d => user.canMakeFriend(d.target)).map(_.target).foreach { target =>
      gameUser.addToFriends(target.gameUser)
      addFeedback(Html(s"${target.avatar.visualNick} теперь твой друг."))
    }
    Redirect(routes.DialogsController.dialogPage(Some(targetId), p))
  }

  /**
    * В игнор
    */
  def toIgnore(targetId: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val user = getUser
    user.dialogs.get(targetId).filter(d => user.canIgnore(d.target)).map(_.target).foreach { target =>
      gameUser.addToIgnore(target.gameUser)
      addFeedback(Html(s"${target.avatar.visualNick} занесён в чёрный список."))
    }
    Redirect(routes.DialogsController.dialogPage(Some(targetId), p))
  }

  /**
    * Удалить диалог
    */
  def removeDialog(targetId: Int, h: Option[String], p: Int) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser
    ctx.fold({
      user.dialogs.get(targetId).filter(d => user.canRemoveDialog(d.target)).foreach(_.removeDialog())
      Redirect(routes.DialogsController.dialogsPage())
    }, confirm => {
      val back = routes.DialogsController.dialogPage(Some(targetId), p)
      user.dialogs.get(targetId).foreach {dialog =>
        addFeedback(Html(s"Удалить переписку с ${dialog.target.avatar.visualNick}?"),
          accept = routes.DialogsController.removeDialog(targetId, confirm, p),
          decline = back)
      }
      Redirect(back)
    })
  }

  /**
    * Написать сообщение
    */
  def writeMessage(targetId: Option[Int], page: Int, ticketId: Option[Int]) = GameAction {implicit ctx =>
    makeForm(targetId).bindFromRequest().fold(formWithErrors => {
      Ok(generateDialogPageView(targetId, page, formWithErrors, ticketId))
    }, {case (targetNick, messageText) =>
      var text = messageText
      val author = getUser
      val target = userDAO.findByNick(targetNick.toLowerCase).map(_.user).getOrElse(targetId.flatMap(session.get(classOf[User], _)).get)

      val localMessage = new PrivateMessage().text(text).creationTime(now).author(author).target(target).wasRead(true)
      val authorDialog = author.dialogWith(target)
      localMessage.dialog(authorDialog)
      authorDialog.lastMessageDate(now).lastViewDate(now).deleted(false).messages.+=(localMessage)
      if (target != author) {
        val remoteMessage = new PrivateMessage()
        if (!author.isAdminOrSupporterOrModerator) {
          // TODO text filtration
          // text = MessagesUtil.filteredText(text, getDomain)
        }
        remoteMessage.text(text).creationTime(now).author(author).target(target)
        val targetDialog = target.dialogWith(author)
        remoteMessage.dialog(targetDialog)
        targetDialog.lastMessageDate(now).lastViewDate(now - 1).deleted(false).messages.+=(remoteMessage)
        target.newMessagesCount(1).lastMessageTime(remoteMessage.creationTime)
      }
      getTicket(ticketId).map {ticket =>
        val currState = ticket.stateId
        ticket.resolveTime(Some(now)).resolver(ctx.userOptional).stateId(TicketState.RESOLVED.typeId)
        Redirect(routes.TicketsController.ticketsPage(None, Some(currState)))
      } getOrElse Redirect(routes.DialogsController.dialogPage(Some(target.id)))
    })
  }

  /**
    * Сообщить о спаме
    */
  def reportSpam(messageId: Int, p: Int, h: Option[String]) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    val user = getUser
    val dialogsRoute = routes.DialogsController.dialogsPage()
    ctx.fold({
      for (
        message <- session.get(classOf[PrivateMessage], messageId)
        if user.canReportSpam(message)
      ) {
        getGameUser.addToIgnore(message.author.gameUser)
//        message.author.spamPenaltyEndTime(now + SPAM_PENALTY)
        session.persist(new Spam(user, message))
        session.delete(message)
        session.flush()
        Logger.info(s"*** SPAM sender=${message.author.id} target=${message.target.get.id} text=${message.text}")
        addFeedback(Html("Спам удалён."))
      }
      Redirect(dialogsRoute)
    }, confirm => {
      for(message <- session.get(classOf[PrivateMessage], messageId) if user.canReportSpam(message))  yield {
        val sender = message.author
        val back = routes.DialogsController.dialogPage(Some(sender.id), p)
        addFeedback("Вы уверены?", accept = routes.DialogsController.reportSpam(messageId, p, confirm), back)
        Redirect(back)
      }
    } getOrElse Redirect(dialogsRoute) )
  }

  /**
    * Список друзей
    */
  def friendList(p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val model = new CollectionPagingDAOProxy[GameUser, Avatar](gameUser.friends.toSeq.sortBy(_.avatar.visualNick), _.avatar, 10)
    val pg = PagingModel(p, model, routes.DialogsController.friendList)
    Ok(views.html.dialogs.FriendsListPage(pg))
  }

  /**
    * Чёрный список
    */
  def ignoreList(p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val model = new CollectionPagingDAOProxy[GameUser, Avatar](gameUser.ignore.toSeq.sortBy(_.avatar.visualNick), _.avatar, 10)
    val pg = PagingModel(p, model, routes.DialogsController.ignoreList)
    Ok(views.html.dialogs.IgnoreListPage(pg))
  }

  /**
    * Удалить из списка друзей
    */
  def removeFriend(friendId: Int, p: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], friendId).foreach {friend =>
      getGameUser.removeFromFriends(friend)
      session.flush()
    }
    Redirect(routes.DialogsController.friendList(p))
  }

  /**
    * Удалить из черного списка
    */
  def removeIgnore(ignoreId: Int, p: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], ignoreId).foreach {ignorant =>
      getGameUser.removeFromIgnore(ignorant)
      session.flush()
    }
    Redirect(routes.DialogsController.ignoreList(p))
  }

  def hideNewMessagesFeedback = GameAction {implicit ctx =>
    getUser.lastMessagesViewTime(now)
    Refresh
  }
}

class Dialog(pg: PagingModel[Int])(implicit ctx: GameContext[AnyContent]) extends ContextMethods {
  def messages: Iterable[Message] = for (id <- pg.page) yield populateMessage(id)
  def pagingModel: PagingModel[Int] = pg
  def isEmpty: Boolean = pg.isEmpty

  def canUseBBCodes(author: User): Boolean =
    author.isAdminOrSupporterOrModerator || author.avatar.nick.exists(config.SPETIAL_USERS.contains)

  def canUseExternalLinks(author: User): Boolean = author.isAdminOrSupporterOrModerator

  private def getHtml(mess: PrivateMessage): Html = {
    for (inboxType <- mess.inboxType) yield {
      inboxType match {
        case EnvelopeMission | EnvelopeMission_Accepted | EnvelopeMission_Expired | EnvelopeMission_Removed =>

          val mission = Option(inboxType).filter(_ == EnvelopeMission).
            flatMap(_ => mess.param.flatMap(id => session.get(classOf[Mission], id)) ).
            map(mission => {mission.fakeOwner = Some(getGameUser); mission})

          val avatar = mission.flatMap(_.originalOwner.map(_.avatar)).
            orElse(mess.param.flatMap(id => session.get(classOf[Avatar], id)))

          views.html.gameplay.mission.EnvelopeMissionMessage(avatar, inboxType, mission, mess.id, pg.currentPage)
        case EnvelopeTask | EnvelopeTask_Accepted | EnvelopeTask_Expired | EnvelopeTask_Removed =>

          val task = Option(inboxType).filter(_ == EnvelopeTask).
            flatMap(_ => mess.param.flatMap(id => session.get(classOf[Task], id)) ).
            map(task => {task.fakeOwner = Some(getGameUser); task})

          val avatar = task.flatMap(_.originalOwner.map(_.avatar)).
            orElse(mess.param.flatMap(id => session.get(classOf[Avatar], id)))

          views.html.gameplay.task.EnvelopeTaskMessage(avatar, inboxType, task, mess.id, pg.currentPage)
        case _ => throw new RuntimeException("Not Implemented")
      }
    }
  } getOrElse {
    var messageText = MessagesUtil.
      processForumMessage(mess.text, canUseBBCodes(mess.author), canUseExternalLinks(mess.author))
    messageText = SmilesUtil.getInstance().processSmiles(messageText, Lists.newArrayList())
    Html(messageText)
  }


  def populateMessage(id: Int): Message = {
    val message = session.get(classOf[PrivateMessage], id).get
    val isIncoming = message.author == message.dialog.get.target
    val messageIcon = if (isIncoming) "mail_incoming.png" else "mail_outgoing.png"
    val time = TimeUtil.formatTime(now - message.creationTime)
    val author = message.author.avatar
    val textCss = author.textCssClass
    messagePopulated(message)
    Message(id, messageIcon, author, time, getHtml(message), textCss, getUser.canReportSpam(message))
  }

  def messagePopulated(message: PrivateMessage): Unit = {
    for (dialog <- message.dialog if dialog.owner == getUser) {
      message.wasRead(true)
      dialog.lastViewDate(dialog.lastViewDate max message.creationTime)
      message.checkIfExpired()
      message.checkIfExpired_Task()
    }
  }
}

case class Message(id: Int, messageIcon: String, author: Avatar, time: String, htmlFragment: Html, textCss: String,
                   reportSpamAllow: Boolean)
