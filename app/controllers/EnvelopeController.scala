package controllers

import bean.{Avatar, Mission, Task}
import bean.messages.personal.PrivateMessage
import components.Results.NotFoundPage
import components.actions.Actions.ConfirmationAction
import components.context.GameContext
import controllers.EnvelopeController.logger
import model.messaging.{EnvelopeMission, EnvelopeMission_Expired, EnvelopeTask}
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.mvc.{Action, AnyContent}
import play.twirl.api.Html
import util.MessagesUtil

import scala.concurrent.Future

class EnvelopeController extends BaseController {

  /**
    * Форма отправки поручения игроку
    */
  private def targetForm(missionId: Int)(implicit ctx: GameContext[_]) = Form(single("nick" -> text) verifying Constraint[String]("") {data =>
    val gameUser = getGameUser

    {
      for {
        ava <- userDAO.findByNick(data.trim.toLowerCase)
        mis <- gameUser.missions.find(_.id == missionId)
      } yield (ava, mis)

    } map {case (ava, mis) =>
      val target = ava.gameUser
        if (gameUser.id == target.id) Invalid(ValidationError("envelope.mission.selfSending"))
        else if (!gameUser.canSendMission) Invalid(ValidationError("envelope.mission.notAllowed"))
        else if (!mis.canBeenSent) Invalid(ValidationError("envelope.mission.cantBeenSent"))
        else if (!target.friendOrCoclaner(gameUser)) Invalid(ValidationError("envelope.mission.friendsOnly"))
        else if (target.level < mis.level) Invalid(ValidationError("envelope.mission.lowLevel"))
        else Valid
    } getOrElse {
      if (data.trim.nonEmpty) Invalid(ValidationError("envelope.mission.targetNotFound"))
      else Invalid(ValidationError("envelope.mission.targetRequired"))
    }

  })

  private def FormTasks(taskId: Int)(implicit ctx: GameContext[_]) = Form(single("nick" -> text) verifying Constraint[String]("") {data =>
    val gameUser = getGameUser

    {
      for {
        ava <- userDAO.findByNick(data.trim.toLowerCase)
        tas <- gameUser.tasks.find(_.id == taskId)
      } yield (ava, tas)

    } map {case (ava, tas) =>
      val target = ava.gameUser
      if (gameUser.id == target.id) Invalid(ValidationError("envelope.task.selfSending"))
      else if (!gameUser.canSendTask) Invalid(ValidationError("envelope.task.notAllowed"))
      else if (!tas.canBeenSent) Invalid(ValidationError("envelope.task.cantBeenSent"))
      else if (!target.friendOrCoclaner(gameUser)) Invalid(ValidationError("envelope.task.friendsOnly"))
      else if (target.level < tas.slots.map(_.gameItem.level).max) Invalid(ValidationError("envelope.task.lowLevel"))
      else Valid
    } getOrElse {
      if (data.trim.nonEmpty) Invalid(ValidationError("envelope.task.targetNotFound"))
      else Invalid(ValidationError("envelope.task.targetRequired"))
    }

  })

  /**
    * Страница отправки поручения
    */
  def sendMission(id: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    gameUser.missions.find(_.id == id).map {mission =>
      if (!gameUser.canSendMission) {
        Html("Вы не можете оправлять поручения.")
        Redirect(routes.MissionController.missionDescription(id, p))
      } else if (!mission.canBeenSent) {
        Html("Это поручение не может быть отправлено.")
        Redirect(routes.MissionController.missionDescription(id, p))
      } else {
        Ok(views.html.gameplay.mission.SendMissionPage(id, p, targetForm(id)))
      }
    } getOrElse Redirect(routes.MissionController.missionList())

  }

  /**
    * Оправить поручение
    */
  private def sendMissionInternal(frm: Form[String], missionId: Int, p: Int) = GameAction {implicit ctx =>
    frm.fold(formWithErrors => {
      Ok(views.html.gameplay.mission.SendMissionPage(missionId, p, formWithErrors))
    }, goodInput => {

      {
        for {
          ava <- userDAO.findByNick(goodInput.trim.toLowerCase)
          mis <- session.get(classOf[Mission], missionId)
        } yield (ava, ava.user, mis)
      }.map {case (targetAvatar, targetUser, mission) =>
        val me = getGameUser
        val copy = mission.copy
        copy.originalOwner(Some(me))
        copy.futureOwner(Some(targetUser.gameUser))
        session.persist(copy)
        mission.closed(true)
        mission.delete()
        MessagesUtil.sendNotificationMessage(targetUser, "", config.POST_LOGIN, false, Some(EnvelopeMission), Some(copy.id))
        logger.debug(s"envelope sending from#${me.id} to#${targetUser.id}, mission closed#${mission.id} created#${copy.id}")
        addFeedback(Html("Поручение успешно отправлено адресату."))
        me.addAddressee(targetAvatar)
        Redirect(routes.MissionController.missionList(p))
      } getOrElse NotFoundPage
    })
  }

  /**
    * Оправить поручение POST
    */
  def sendMissionAction(missionId: Int, p: Int): Action[AnyContent] = GameAction.async { implicit ctx =>
    val form = targetForm(missionId).bindFromRequest()
    sendMissionInternal(form, missionId, p).apply(ctx)
  }

  /**
    * Оправить поручение GET
    */
  def sendMissionByUserId(missionId: Int, targetId: Int, p: Int): Action[AnyContent] = GameAction.async { implicit ctx =>
    (for (ava <- session.get(classOf[Avatar], targetId); nick <- ava.nick) yield {
      val form = targetForm(missionId).bind(Map("nick" -> nick))
      sendMissionInternal(form, missionId, p).apply(ctx)
    }).getOrElse(Future.successful(Redirect(routes.MissionController.missionList(p))))
  }

  /**
    * Принять поручение
    */
  def acceptMission(privateMessageId: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val userId = gameUser.id
    val requiredData =
    for(
      mes <- session.get(classOf[PrivateMessage], privateMessageId);
      tip <- mes.inboxType if tip == EnvelopeMission;
      dia <- mes.dialog if dia.owner.id == userId;
      mis <- mes.param.flatMap(id => session.get(classOf[Mission], id))
    ) yield (mes, mis, dia)

    requiredData.map {case (mes, mis, dia) =>
        mes.checkIfExpired()
        if (mes.inboxType.contains(EnvelopeMission_Expired)) {
          addFeedback(Html("Время действия поручение истекло."))
          Redirect(routes.DialogsController.dialogPage(Some(dia.target.id), p))
        } else if (gameUser.dailyMissions.available(gameUser.dailyMissionsMax) <= 0) {
          addFeedback(Html(s"В день можно принимать не более ${gameUser.dailyMissionsMax} поручений."))
          Redirect(routes.DialogsController.dialogPage(Some(dia.target.id), p))
        } else {
          mes.markMissionAccepted(mis.originalOwner.map(_.id))
          mis.owner(gameUser)
          gameUser.missions += mis
          gameUser.dailyMissions.increment(gameUser.dailyMissionsMax)
          addFeedback(Html("Поручение добавлено в Ваш список."))
          Redirect(routes.MissionController.missionDescription(mis.id))
        }
    } getOrElse {
      addFeedback(Html("Поручение не найдено."))
      Redirect(routes.DialogsController.dialogsPage())
    }
  }

  /**
    * Удалить поручение
    */
  def deleteMission(privateMessageId: Int, p: Int, h: Option[String]): Action[AnyContent] = GameAction.andThen(
    ConfirmationAction(h)) {implicit ctx =>
    val gameUser = getGameUser
    val userId = gameUser.id
    val requiredData =
      for(
        mes <- session.get(classOf[PrivateMessage], privateMessageId);
        tip <- mes.inboxType if tip == EnvelopeMission;
        dia <- mes.dialog if dia.owner.id == userId;
        mis <- mes.param.flatMap(id => session.get(classOf[Mission], id))
      ) yield (mes, mis, dia)

    requiredData.map {case (mes, mis, dia) =>
      val deleteEnvelope = () => {
        mes.markEnvelopeMissionDeleted(mis.originalOwner.map(_.id))
        session.delete(mis)
      }
      val dialogsPage = routes.DialogsController.dialogPage(Some(dia.target.id), p)

      if (!getSettings.missionRemoveConfirmation) deleteEnvelope()
      else ctx.fold(deleteEnvelope(), newHash => {
          addFeedback(Html("Отказаться от предложения?"),
            routes.EnvelopeController.deleteMission(privateMessageId, p, newHash), dialogsPage)
      })
      Redirect(dialogsPage)
    } getOrElse NotFoundPage
  }

  /**
    * Страница отправки задания
    */
  def sendTask(id: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    gameUser.tasks.find(_.id == id).map {task =>
      if (!gameUser.canSendTask) {
        Html("Вы не можете оправлять задания.")
        Redirect(routes.TaskController.taskList(p))
      } else if (!task.canBeenSent) {
        Html("Это задание не может быть отправлено.")
        Redirect(routes.TaskController.taskList(p))
      } else {
        Ok(views.html.gameplay.task.SendTaskPage(id, p, FormTasks(id)))
      }
    } getOrElse Redirect(routes.TaskController.taskList(p))
  }

  /**
    * Отправить задание
    */
  private def sendTaskInternal(frm: Form[String], taskId: Int, p: Int) = GameAction {implicit ctx =>
    frm.fold(formWithErrors => {
      Ok(views.html.gameplay.task.SendTaskPage(taskId, p, formWithErrors))
    }, goodInput => {

      {
        for {
          ava <- userDAO.findByNick(goodInput.trim.toLowerCase)
          tas <- session.get(classOf[Task], taskId)
        } yield (ava, ava.user, tas)
      }.map {case (targetAvatar, targetUser, task) =>
        val me = getGameUser
        val copy = task.copy
        copy.originalOwner(Some(me))
        copy.futureOwner(Some(targetUser.gameUser))
        session.persist(copy)
        task.delete()
        MessagesUtil.sendNotificationMessage(targetUser, "", config.POST_LOGIN, false, Some(EnvelopeTask), Some(copy.id))
        addFeedback(Html("Задание успешно отправлено адресату."))
        me.addAddressee(targetAvatar)
        Redirect(routes.TaskController.taskList(p))
      } getOrElse NotFoundPage
    })
  }
  /**
    * Отправить задание POST
    */
  def sendTaskAction(taskId: Int, p: Int): Action[AnyContent] = GameAction.async { implicit ctx =>
    val form = FormTasks(taskId).bindFromRequest()
    sendTaskInternal(form, taskId, p).apply(ctx)
  }

  /**
    * Отправить задание, GET
    */
  def sendTaskByUserId(taskId: Int, targetId: Int, p: Int): Action[AnyContent] = GameAction.async { implicit ctx =>
    (for (ava <- session.get(classOf[Avatar], targetId); nick <- ava.nick) yield {
      val form = FormTasks(taskId).bind(Map("nick" -> nick))
      sendTaskInternal(form, taskId, p).apply(ctx)
    }).getOrElse(Future.successful(Redirect(routes.TaskController.taskList(p))))
  }

  /**
    * Принять задание
    */
  def acceptTask(privateMessageId: Int, p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val userId = gameUser.id
    val requiredData =
      for(
        mes <- session.get(classOf[PrivateMessage], privateMessageId);
        tip <- mes.inboxType if tip == EnvelopeTask;
        dia <- mes.dialog if dia.owner.id == userId;
        task <- mes.param.flatMap(id => session.get(classOf[Task], id))
      ) yield (mes, task, dia)

    requiredData.map {case (mes, task, dia) =>
      mes.checkIfExpired_Task()
      if (mes.inboxType.contains(EnvelopeMission_Expired)) {
        addFeedback(Html("Время действия задания истекло."))
        Redirect(routes.DialogsController.dialogPage(Some(dia.target.id), p))
      } else if (gameUser.dailyTasks.available(gameUser.dailyTasksMax) <= 0) {
        addFeedback(Html(s"В день можно принимать не более ${gameUser.dailyTasksMax} заданий."))
        Redirect(routes.DialogsController.dialogPage(Some(dia.target.id), p))
      } else {
        mes.markTaskAccepted(task.originalOwner.map(_.id))
        task.owner(gameUser)
        gameUser.tasks += task
        gameUser.dailyTasks.increment(gameUser.dailyTasksMax)
        addFeedback(Html("Задание добавлено в Ваш список."))
        Redirect(routes.DialogsController.dialogPage(Some(dia.target.id), p))
      }
    } getOrElse {
      addFeedback(Html("Задание не найдено."))
      Redirect(routes.DialogsController.dialogsPage())
    }
  }

  /**
    * Удалить задание
    */
  def deleteTask(privateMessageId: Int, p: Int, h: Option[String]): Action[AnyContent] = GameAction.andThen(
    ConfirmationAction(h)) {implicit ctx =>
    val gameUser = getGameUser
    val userId = gameUser.id
    val requiredData =
      for(
        mes <- session.get(classOf[PrivateMessage], privateMessageId);
        tip <- mes.inboxType if tip == EnvelopeTask;
        dia <- mes.dialog if dia.owner.id == userId;
        task <- mes.param.flatMap(id => session.get(classOf[Task], id))
      ) yield (mes, task, dia)

    requiredData.map {case (mes, task, dia) =>
      val deleteEnvelope = () => {
        mes.markEnvelopeTaskDeleted(task.originalOwner.map(_.id))
        session.delete(task)
      }
      val dialogsPage = routes.DialogsController.dialogPage(Some(dia.target.id), p)

      if (!getSettings.missionRemoveConfirmation) deleteEnvelope()
      else ctx.fold(deleteEnvelope(), newHash => {
        addFeedback(Html("Отказаться от предложения?"),
          routes.EnvelopeController.deleteTask(privateMessageId, p, newHash), dialogsPage)
      })
      Redirect(dialogsPage)
    } getOrElse NotFoundPage
  }
}

object EnvelopeController {
  val logger = Logger(classOf[EnvelopeController])
}
