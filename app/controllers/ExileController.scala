package controllers

import components.actions._
import play.api.mvc.{Action, AnyContent, Request}

class ExileController extends BaseController {

  def exiledPage: Action[AnyContent] =

    (ExceptionAbsorber andThen
    LoggedInFilter andThen
    new UserBusyFilter[Request] andThen
    GameContextMaker andThen
    new OdnoklassnikiFilter) { implicit ctx =>


      Ok(views.html.user.ExiledPage())
    }
}
