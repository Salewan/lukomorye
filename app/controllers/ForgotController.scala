package controllers

import java.util.UUID

import bean.User
import components.HibernateSessionAware
import components.actions.Actions.FreeOnlyAction
import components.context.PlayContext
import org.apache.commons.codec.digest.DigestUtils
import org.slf4j
import play.api.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.mvc.{AnyContent, Call, RequestHeader}
import util.MailUtil
import util.Implicits._

import scala.collection.mutable.ArrayBuffer
import scala.language.postfixOps

class ForgotController extends BaseController {

  import ForgotController._

  def forgotForm(implicit h: PlayContext[AnyContent]): Form[(String, String)] = Form(
    tuple("login" -> text, "email" -> text)
    verifying Constraint[(String, String)]("") { data =>
      val login = data._1.trim.toLowerCase
      val email = data._2.trim.toLowerCase
      val buffer: ArrayBuffer[String] = ArrayBuffer()
      if (login.isEmpty) buffer += "error.forgot.login.required"
      if (email.isEmpty) buffer += "error.forgot.email.required"
      if (buffer.isEmpty) {
         userDAO.getUserByNickAndEmail(login, email) match {
           case Some(user) =>
             MailUtil.send(user.profile.email.get, SUBJECT, generateText(user))
           case _ =>
             buffer += "error.forgot.not_found"
         }
      }
      if (buffer.nonEmpty) Invalid(buffer.map(err => ValidationError(err)))
      else Valid
    }
  )

  def forgot = FreeOnlyAction {implicit ctx =>
    Ok(views.html.forgot.ForgotPage(forgotForm, None))
  }

  def forgotPost = FreeOnlyAction {implicit ctx =>
    forgotForm.bindFromRequest().fold(formWithErrors => {
      Ok(views.html.forgot.ForgotPage(formWithErrors, None))
    }, success => {
      Ok(views.html.forgot.ForgotPage(
        forgotForm, Some("На Ваш Email отправлено письмо с ссылкой для восстановления пароля.")))
    })
  }
}

object ForgotController {
  val logger: slf4j.Logger = Logger(classOf[ForgotController]).underlyingLogger
  val SUBJECT = "Восстановление пароля"
  val SECRET_KEY = "40389mjgkHJhkHjAAA"

  def genCheck(key: String, expireTime: Long): String = DigestUtils.md5Hex(key + expireTime + SECRET_KEY)

  private def generateRestoreLink(user: User)(implicit rh: RequestHeader): String = {
    val expireTime = System.currentTimeMillis() + (1 day)
    val key = UUID.randomUUID().toString
    val code = genCheck(key, expireTime)
    user.restorePasswordKey(Some(key))
    logger.info(s"Restore password uri=/$key/$expireTime/$code")
    routes.RestoreController.restore(key, expireTime, code).absoluteURL()
  }

  private def generateText(user: User)(implicit rh: RequestHeader): String = {
    s"""Здравствуйте!

Вы отправили запрос на восстановление пароля от аккаунта в онлайн игре "Лукоморье". Для того, чтобы задать новый пароль, перейдите по ссылке ${generateRestoreLink(user)}

Пожалуйста, проигнорируйте данное письмо, если оно попало к Вам по ошибке.

С уважением,
Служба поддержки пользователей онлайн игры "Лукоморье"."""
  }
}
