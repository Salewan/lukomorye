package controllers

import bean.Clan
import bean.messages.forum.{Comment, Folder, FolderType, Topic}
import com.google.common.collect.ComparisonChain
import components.PagingModel
import components.Results.{AccessDeniedPage, NotFoundPage}
import components.actions.Actions.ConfirmationAction
import components.context.GameContext
import controllers.helpers.{ForumForms, ForumRestrictions, TopicInfo}
import dao.paging.CollectionPagingDAO
import play.api.Logger
import play.api.data.Form
import play.api.mvc.AnyContent
import play.twirl.api.Html
import util.MessagesUtil

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.02.2017.
  */
class ForumController extends BaseController with ForumRestrictions with ForumForms {

  val TOPIC_PAGE_SIZE = 10

  /**
    * Форумы
    */
  def forumMain(clanId: Option[Int], p: Int) = GameAction {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser
    val clan = clanId flatMap (session.get(classOf[Clan], _))
    val withAdmin = user.isAdminOrSupporterOrModerator
    val folders = clan map (_.getNotDeletedFolders(gameUser)) getOrElse messageDAO.getRootFolders(withAdmin)
    val pg = PagingModel(p, new CollectionPagingDAO[Folder](folders), routes.ForumController.forumMain(clanId, _))
    Ok(views.html.forum.ForumMainPage(clan, pg))
  }

  /**
    * Пометить все княжеские форумы прочитанными
    */
  def markAllFolders(clanId: Option[Int], p: Int) = GameAction {implicit ctx =>
    clanId flatMap (session.get(classOf[Clan], _)) foreach {clan =>
      for(folder <- clan.getNotDeletedFolders(getGameUser)) {
        messageDAO.getFolderMark(getUser, folder).lastViewTime(now).lastMarkAllReadTime(now)
      }
    }
    Redirect(routes.ForumController.forumMain(clanId, p))
  }

  /**
    * Пометить все топики прочитанными
    */
  def markAllTopics(folderId: Int, p: Int) = GameAction {implicit ctx =>
    checkFolderPageAccess(folderId) match {

      case (Some(folder), None) =>
        messageDAO.getFolderMark(getUser, folder).
          lastViewTime(now).
          lastMarkAllReadTime(now)
        Redirect(routes.ForumController.folder(folderId, p))

      case (_, Some(errorResult)) => errorResult

      case _ => Redirect(routes.ForumController.forumMain())
    }
  }

  /**
    * Форум. Список топиков.
    */
  def folder(folderId: Int, p: Int) = GameAction {implicit ctx =>
    val user = getUser

    checkFolderPageAccess(folderId) match {

      case (Some(folder), None) =>
        val topics = folder.topics.toSeq.sortWith((o1, o2) =>
          ComparisonChain.start().
            compareFalseFirst(o2.pinned, o1.pinned).
            compare(o2.lastCommentTime.getOrElse(0L), o1.lastCommentTime.getOrElse(0L)).
            result() < 0)
        val pg = PagingModel(p, new CollectionPagingDAO[Topic](topics), routes.ForumController.folder(folderId, _))
        val hasNewTopics = pg.page.exists(_.isNew(user))
        messageDAO.getFolderMark(user, folder).lastViewTime(now)
        Ok(views.html.forum.FolderPage(folder, pg, hasNewTopics))

      case (_, Some(errorResult)) => errorResult

      case _ => Redirect(routes.ForumController.forumMain())
    }
  }

  /**
    * Удалить раздел форума.
    */
  def removeFolder(folderId: Int, p: Int, h: Option[String]) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    val gameUser = getGameUser
    session.get(classOf[Folder], folderId).map {folder =>
      if (!gameUser.canRemoveFolder(folder)) AccessDeniedPage
      else {
        ctx.fold({
          folder.deleted(true)
          Logger.info(s"$getUser deleted forum folder $folder")
          Redirect(routes.ForumController.forumMain(folder.clan.map(_.id)))
        }, hash => {
          val folderPage = routes.ForumController.folder(folderId, p)
          addFeedback(Html("Удалить этот раздел форума?"),
            accept = routes.ForumController.removeFolder(folderId, p, hash),
            decline = folderPage)
          Redirect(folderPage)
        })
      }
    } getOrElse NotFoundPage
  }

  /**
    * Форма создать/редактировать форум
    */
  def folderForm(folderId: Option[Int], clanId: Option[Int]) = GameAction {implicit ctx =>
    checkFolderFormAccess(folderId, clanId) match {

      case (_, _, Some(result)) => result // access error

      case (folder, clan, None) => // access ok
        val form = makeFolderForm(folder, clan).fill((folder.name, folder.folderType.code))
        Ok(views.html.forum.FolderFormPage(folder, clan, form))

      case _ => NotFoundPage
    }
  }

  /**
    * Обработка POST формы создания форума
    */
  def folderEdit(folderId: Option[Int], clanId: Option[Int]) = GameAction {implicit ctx =>
    checkFolderFormAccess(folderId, clanId) match {

      case (_, _, Some(result)) => result // access error

      case (folder, clan, None) => // access ok

        makeFolderForm(folder, clan).bindFromRequest().fold(formWithErrors => {
          Ok(views.html.forum.FolderFormPage(folder, clan, formWithErrors))
        }, {case (folderName, folderOption) =>

          folder.name(folderName).
            folderType(folderOption)

          if (!folder.isPersistent) {
            session.persist(folder)
            clan.foreach(_.folders.+=(folder))
          }

          Redirect(routes.ForumController.folder(folder.id))
        })

      case _ => NotFoundPage
    }
  }

  /**
    * Форма создания топика.
    */
  def topicForm(folderId: Int, folderPage: Int) = GameAction {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser

    checkTopicCreation(folderId) match {
      case (Some(folder), None) =>
        Ok(views.html.forum.TopicFormPage(folder, makeTopicForm, folderPage))
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Обработка POST создания топика.
    */
  def topicCreate(folderId: Int, folderPage: Int) = GameAction {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser

    checkTopicCreation(folderId) match {

      case (Some(folder), None) =>
        makeTopicForm.bindFromRequest().fold(formWithErrors => {
          Ok(views.html.forum.TopicFormPage(folder, formWithErrors, folderPage))
        }, { case (subject, body) =>

          val topic = new Topic().
            author(user).
            creationTime(now).
            lastCommentTime(now).
            moderator(user.isAdminOrSupporterOrModerator).
            folder(folder).
            subject(subject).
            body(body)

          session.persist(topic)

          folder.lastCommentTime(now).topics.+=(topic)
          messageDAO.getFolderMark(user, folder).lastViewTime(now)

          if (folder.folderType == FolderType.NEWS) folder.newsTime(now).newsTopicId(topic.id)

          Redirect(routes.ForumController.topic(topic.id))
        })
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  //
  private def renderTopic(topic: Topic, commentForm: Form[String], folderPage: Int, p: Option[Int])
                         (implicit ctx: GameContext[AnyContent]) = {
    val user = getUser
    var currentPage = p.getOrElse(0)

    if (p.isEmpty) {
      val mark = messageDAO.getTopicMark(user, topic)
      val folderMark = messageDAO.getFolderMark(user, topic.folder)

      {
        for(lm <- folderMark.lastMarkAllReadTime;
            lc <- topic.lastCommentTime;
            lv <- Some(mark.lastViewedCommentTime))
          yield (lm, lc, lv)

      } map { case (lastMark, lastComment, lastViewed) =>
        lastMark > lastComment && lastViewed <= lastMark

      } match {
        case Some(true) =>
          val page = messageDAO.getTopicCommentsPage(topic.id, mark.lastViewedCommentTime, TOPIC_PAGE_SIZE)
          mark.lastViewedCommentTime(now)
          mark.pageNum(page)
          currentPage = page

        case _ =>
          if (mark.pageNum == 0)
            mark pageNum messageDAO.getTopicCommentsPage(topic.id, mark.lastViewedCommentTime, TOPIC_PAGE_SIZE)

          currentPage = mark.pageNum
      }

    } else {
      val mark = messageDAO.getTopicMark(user, topic, persist = true)
      mark.pageNum(p.get)
    }


    val commentIds = messageDAO.getTopicCommentsIds(topic.id)
    val dao = new CollectionPagingDAO[Int](commentIds)
    val pg = PagingModel[Int](
      currentPage, dao, pp => routes.ForumController.topic(topic.id, None, folderPage, Some(pp)) )


    val mark = messageDAO.getTopicMark(user, topic, persist = true)
    if (topic.creationTime > mark.lastViewedCommentTime)
      mark.lastViewedCommentTime(topic.creationTime)

    views.html.forum.TopicPage(topic, new TopicInfo(topic, pg), commentForm, folderPage)
  }

  /**
    * Страница топика.
    */
  def topic(topicId: Int, replyNick: Option[String], folderPage: Int, p: Option[Int]) = GameAction {implicit ctx =>

    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        val commentForm = makeTopicWriteForm(topic).fill(replyNick.map(_ + ", ").getOrElse(""))
        Ok(renderTopic(topic, commentForm, folderPage, p))

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Обработка POST. Написать коммент в топике.
    */
  def doComment(topicId: Int, folderPage: Int, p: Int) = GameAction {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser

    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>

        makeTopicWriteForm(topic).bindFromRequest().fold(formWithErrors => {
          Ok(renderTopic(topic, formWithErrors, folderPage, Some(p)))

        }, { t =>
          var text = t
          val comment = new Comment

          if (!user.isAdminOrSupporterOrModerator)
            text = MessagesUtil.filteredText(text, ctx.getDomain)

          comment.text(text).creationTime(now).author(user).topic(topic).moderator(user.isAdminOrSupporterOrModerator)
          session.persist(comment)
          topic.lastCommentTime(now).comments.+=(comment)
          session.flush()
          topic.commentsCount(topic.comments.size)
          topic.folder.lastCommentTime(now)
          messageDAO.getFolderMark(user, topic.folder).lastViewTime(now)

          val cc = topic.commentsCount
          val lastPage = cc / TOPIC_PAGE_SIZE + {if (cc % TOPIC_PAGE_SIZE == 0) -1 else 0}
          Redirect(routes.ForumController.topic(topic.id, None, folderPage, Some(lastPage)))
        })

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Скрыть/показать кнопки управления топиком
    */
  def manageToggle(topicId: Int, folderPage: Int, p: Int) = GameAction {implicit ctx =>
    getUser.settings.toggleManageBlockExpanding()
    Redirect(routes.ForumController.topic(topicId, None, folderPage, Some(p)))
  }

  /**
    * Закрыть топик
    */
  def closeTopic(topicId: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        if (!getGameUser.canOpenClose(topic)) AccessDeniedPage
        else {
          topic.closed(true)
          Redirect(routes.ForumController.folder(topic.folder.id, folderPage))
        }
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Открыть топик
    */
  def openTopic(topicId: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        if (!getGameUser.canOpenClose(topic)) AccessDeniedPage
        else {
          topic.closed(false)
          Redirect(routes.ForumController.folder(topic.folder.id, folderPage))
        }
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Удалить топик
    */
  def deleteTopic(topicId: Int, folderPage: Int, p: Int, h: Option[String]) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        ctx.fold({
          if (!getGameUser.canDelete(topic)) AccessDeniedPage
          else {
            topic.deleted(true)
            topic.folder.topics -= topic
            topic.folder.updateLastCommentDate()
            Redirect(routes.ForumController.folder(topic.folder.id, folderPage))
          }
        }, hash => {
          val goTopic = routes.ForumController.topic(topicId, None, folderPage, Some(p))
          addFeedback("Удалить топик?", accept = routes.ForumController.deleteTopic(topic.id, folderPage, p, hash),
            decline = goTopic)
          Redirect(goTopic)
        })
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Прикрепить топик
    */
  def pinTopic(topicId: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        if (!getGameUser.canPinUnpin(topic)) AccessDeniedPage
        else {
          topic.pinned(true)
          Redirect(routes.ForumController.folder(topic.folder.id, folderPage))
        }
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Открепить топик
    */
  def unpinTopic(topicId: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicAccess(topicId) match {
      case (Some(topic), None) =>
        if (!getGameUser.canPinUnpin(topic)) AccessDeniedPage
        else {
          topic.pinned(false)
          Redirect(routes.ForumController.folder(topic.folder.id, folderPage))
        }
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  //
  private def getTargetFoldersForMoveTo(topic: Topic)(implicit ctx: GameContext[AnyContent]): Seq[Folder] = {
    val folder = topic.folder
    if (folder.clan.isEmpty) messageDAO.getRootFolders(true)
    else folder.clan.get.getNotDeletedFolders(getGameUser)
  }

  private def renderTopicMovePage(topic: Topic, frm: Form[Int], folderPage: Int, topicPage: Int)
                                 (implicit ctx: GameContext[AnyContent]) = {
    val opts = getTargetFoldersForMoveTo(topic).map(f => f.id.toString -> f.name)
    views.html.forum.TopicMovePage(topic, frm, opts, folderPage, topicPage)
  }

  /**
    * Страница с формой перемещения топика
    */
  def moveTopicPage(topicId: Int, folderPage: Int, topicPage: Int) = GameAction {implicit ctx =>
    checkMoveAccess(topicId) match {
      case (Some(topic), None) =>
        val frm = moveForm(topic).fill(topic.folder.id)
        Ok(renderTopicMovePage(topic, frm, folderPage, topicPage))

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * POST. Переместить топик.
    */
  def moveTopic(topicId: Int, folderPage: Int, topicPage: Int) = GameAction {implicit ctx =>
    checkMoveAccess(topicId) match {
      case (Some(topic), None) =>

        moveForm(topic).bindFromRequest().fold(formWithErrors => {
          Ok(renderTopicMovePage(topic, formWithErrors, folderPage, topicPage))

        }, { newFolderId =>
          val newFolder = session.get(classOf[Folder], newFolderId).get
          val oldFolder = topic.folder
          oldFolder.topics -= topic
          oldFolder.updateLastCommentDate()
          topic.folder(newFolder)
          newFolder.topics += topic
          newFolder.updateLastCommentDate()
          Redirect(routes.ForumController.folder(newFolder.id))
        })

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * Страница редактирования топика.
    */
  def editTopicPage(topicId: Int, topicPage: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicEditAccess(topicId) match {
      case (Some(topic), None) =>
        val editForm = makeTopicForm.fill((topic.subject, topic.body))
        Ok(views.html.forum.TopicEditPage(topic, editForm, topicPage, folderPage))
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * POST. Редактирование топика.
    */
  def editTopic(topicId: Int, topicPage: Int, folderPage: Int) = GameAction {implicit ctx =>
    checkTopicEditAccess(topicId) match {

      case (Some(topic), None) =>
        makeTopicForm.bindFromRequest().fold(formWithErrors => {
          Ok(views.html.forum.TopicEditPage(topic, formWithErrors, topicPage, folderPage))
        },

        { case (s, b) =>
          var subject = s.trim
          var body = b.trim

          if (!topic.author.isAdminOrSupporterOrModerator) {
            subject = MessagesUtil.filteredText(subject, ctx.getDomain)
            body = MessagesUtil.filteredText(body, ctx.getDomain)
          }

          topic.subject(subject)
          topic.body(body)
          Redirect(routes.ForumController.topic(topic.id, None, folderPage, Some(topicPage)))
        })

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }


  /**
    * Удалить коментарий
    */
  def deleteComment(topicId: Int, commentId: Int, folderPage: Int, topicPage: Int) = GameAction {implicit ctx =>
    session.get(classOf[Comment], commentId).
      filter(c => c.topic.id == topicId && getGameUser.canDelete(c.topic)).
      foreach {comment =>
        comment.deleted(true)
        session.flush()
        val topic = comment.topic
        topic.commentsCount(topic.comments.size)
        topic.updateLastCommentDate()
        topic.folder.updateLastCommentDate()
      }
    Redirect(routes.ForumController.topic(topicId, None, folderPage, Some(topicPage)))
  }

  private def renderBanCommentPage(comment: Comment, banForm: Form[(Int, Int)], folderPage: Int, topicPage: Int)
                                  (implicit ctx: GameContext[AnyContent]) = {
    val timeOpts = getBanTimes(getUser) map (_._1.toString) zip banTimesName
    val reasonOpts = orderedReasons map (f => f._1.toString -> f._2)
    views.html.forum.BanCommentPage(comment, banForm, timeOpts, reasonOpts, folderPage, topicPage)
  }

  /**
    * Бан страница
    */
  def banCommentPage(commentId: Int, folderPage: Int, topicPage: Int) = GameAction {implicit ctx =>
    checkBanCommentAccess(commentId) match {
      case (Some(comment), None) =>
        val banForm = makeBanForm.fill((0, 23))
        Ok(renderBanCommentPage(comment, banForm, folderPage, topicPage))

      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }

  /**
    * POST. Забанить
    */
  def banComment(commentId: Int, folderPage: Int, topicPage: Int) = GameAction {implicit ctx =>
    checkBanCommentAccess(commentId) match {

      case (Some(comment), None) =>
        makeBanForm.bindFromRequest().fold(formWithErrors => {
          Ok(renderBanCommentPage(comment, formWithErrors, folderPage, topicPage))

        }, { case (timeOpt, reasonOpt) =>
          val user = getUser
          val target = comment.author
          val time = getBanTimes(user).toMap.apply(timeOpt)
          val reason = orderedReasons.toMap.apply(reasonOpt)
          target.ban(time, user, reason, comment.text)

          comment.deleted(true)
          session.flush()
          val topic = comment.topic
          topic.commentsCount(topic.comments.size)
          topic.updateLastCommentDate()
          topic.folder.updateLastCommentDate()

          Logger.info(s"User #${user.id} removed forum comment #${comment.id} text=${comment.text}")
          Redirect(routes.ForumController.topic(comment.topic.id, None, folderPage, Some(topicPage)))
        })
      case (_, Some(errorResult)) => errorResult
      case _ => throw new Error
    }
  }
}
