package controllers

import bean.Hero
import components.PagingModel
import components.Results.NotFoundPage
import components.actions.Actions._
import components.context.GameContext
import dao.paging.CollectionPagingDAO
import model.hero.{Ability, HeroTemplate}
import model.{TransactionGenerator, TransactionType}
import model.TransactionGenerator._
import play.api.mvc.{Action, ActionBuilder, AnyContent, Call}
import play.twirl.api.Html
import model.price.RubyPrice
import model.price.CoinPrice
import components.actions.Actions.LevelRequiredAction
import HeroController.TavernaAction
import components.actions.AdminOnlyFilter

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.10.2016.
 */
class HeroController extends BaseController {

  /**
    * Страница списка героев
    */
  def heroList(p: Int) = GameAction { implicit ctx =>
    val gameUser = getGameUser
    val sorter = (o1: Hero, o2: Hero) => {
      if ((o1.pendingLevel > 0 && o2.pendingLevel > 0) || (o1.pendingLevel == 0 && o2.pendingLevel == 0)) {
        if (o1.totalExperience == o2.totalExperience) {
          o1.id < o2.id
        } else o1.totalExperience > o2.totalExperience
      } else o1.pendingLevel > o2.pendingLevel
    }
    val heroes = gameUser.heroes.values.sortWith(sorter)

    val pg = PagingModel(p, new CollectionPagingDAO[Hero](heroes), routes.HeroController.heroList)
    Ok(views.html.gameplay.hero.HeroesPageView(pg))
  }

  private def _price(implicit ctx: GameContext[_]) = {
    getGameUser.activeHeroesUpgradePrice.getOrElse(RubyPrice(0L))
  }

  /**
    * Увеличить максимальное число активных героев
    */
  def upgradeMaxActive(p: Int, h: Option[String], backUrl: Option[String]): Action[AnyContent] = (GameAction andThen
    BuyForRubyAction(h, c => _price(c), ACTIVE_HEROES_TX.get)) { implicit ctx =>
    val user = getGameUser
    val back = backUrl match {
      case Some(s) if s.equalsIgnoreCase("missions") => routes.MissionController.missionList(p)
      case _ => routes.HeroController.heroShop()
    }

    ctx.fold({
      if (_price.raw == 0) {
        session.getTransaction.setRollbackOnly()
      } else {
        user.incAdditionalActiveHeroes()
        addFeedback(Html("Максимальное число активных героев увеличено."))
      }
    }, confirm => {
      addFeedback(confirm, accept = routes.HeroController.upgradeMaxActive(p, confirm.hash, backUrl), decline = back)
    }, deficit => {
      addFeedback(deficit)
    })
    Redirect(back)
  }

  /**
    * Покупка героев
    */
  def heroShop = TavernaAction { implicit ctx =>
    Ok(views.html.gameplay.HeroShop(HeroTemplate.forSale(getGameUser)))
  }

  /**
    * Увеличить уровень у героя
    */
  def promoteLevel(heroId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Hero], heroId)
      .filter(h => h.owner == getGameUser && h.pendingLevel > 0)
      .foreach(hero => addFeedback(hero.commitLevel()))

    Redirect(routes.HeroController.heroList())
  }

  /**
    * Купить героя
    */
  def buyHero(templateId: Int, h: Option[String]) = TavernaAction.async {implicit ctx =>
    val user = getGameUser
    (for {
      template <- HeroTemplate.withTypeOption(templateId)
      if !user.heroes.containsKey(templateId) && template.canBuy(user)

      price <- Option(template.price)
    } yield price -> template) match {
      case Some((x: RubyPrice, template)) =>
        buyHeroForRuby(template, x, BUY_HERO_TX.get(templateId), h).apply(ctx)
      case Some((x: CoinPrice, template)) =>
        buyHeroForCoin(template, x, BUY_HERO_TX.get(templateId),
          Some(REST_HERO.get(templateId)), h).apply(ctx)
      case _ => Future.successful(Redirect(routes.HeroController.heroList()))
    }
  }

  // купить героя за монеты
  private def buyHeroForCoin(template: HeroTemplate,
                             coinPrice: CoinPrice,
                             tr1: TransactionType,
                             tr2: Option[TransactionType],
                             h: Option[String]) =
  (GameAction andThen BuyForCoinAction(h, _ => coinPrice, tr1, _ => tr2))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.HeroController.heroShop
    val accept: Option[String] => Call = x => routes.HeroController.buyHero(template.typeId, x)

    ctx.fold({
      user.heroes += (template.typeId -> Hero(user, template))
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
    }, coinDeficit => {
      addFeedback(coinDeficit)
    }, restConfirm => {
      addFeedback(restConfirm, accept(restConfirm.hash), decline = back)
    }, rubyDeficit => {
      addFeedback(rubyDeficit)
    })
    Redirect(back)
  }

  // купить героя за рубины
  private def buyHeroForRuby(template: HeroTemplate,
                             rubyPrice: RubyPrice,
                             tr1: TransactionType,
                             h: Option[String]) =
  (GameAction andThen BuyForRubyAction(h, _ => rubyPrice, tr1))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.HeroController.heroShop
    val accept: Option[String] => Call = x => routes.HeroController.buyHero(template.typeId, x)

    ctx.fold({
      user.heroes += (template.typeId -> Hero(user, template))
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
    }, deficit => {
      addFeedback(deficit)
    })
    Redirect(back)
  }

  /**
    * Получение новой способности на 20 уровне
    */
  def upgrade(id: Int, h: Option[String]) = (GameAction andThen
    BuyForRubyAction(h, _ => HeroController.upgradePrice, TransactionGenerator.UPGRADE_HERO_TX.get(id))) { implicit ctx =>
    val user = getGameUser
    session.get(classOf[Hero], id).foreach {hero =>
      ctx.fold({
        if (hero.owner != user || !hero.canUpgraded) session.getTransaction.setRollbackOnly()
        else {
          hero.addRandomAbility()
          addFeedback(Html("Герой получил новую способность."))
        }
      }, confirm => {
        addFeedback(confirm, accept = routes.HeroController.upgrade(id, confirm.hash),
          decline = routes.HeroController.heroList())
      }, deficit => {
        addFeedback(deficit)
      })
    }
    Redirect(routes.HeroController.heroList())
  }

  /**
    * АДМИНСКИЙ РЕЖИМ
    */
  def skillsManagement(id: Int) = GameAction.andThen(AdminOnlyFilter) { implicit ctx =>
    session.get(classOf[Hero], id).map {hero =>
      val has = hero.abilities.map(Ability.withType).toSet
      val skillsToAdd = Ability.values.toSet -- has
      Ok(views.html.gameplay.hero.SkillsManagementView(hero, skillsToAdd.toSeq.sortBy(_.typeId)))
    } getOrElse NotFoundPage
  }

  def addSkill(id: Int, typeId: Int) = GameAction.andThen(AdminOnlyFilter) {implicit ctx =>
    session.get(classOf[Hero], id).map {hero =>
      hero.abilities += typeId
      Redirect(routes.HeroController.heroList())
    } getOrElse NotFoundPage
  }

  def deleteSkill(id: Int, typeId: Int) = GameAction.andThen(AdminOnlyFilter) {implicit ctx =>
    session.get(classOf[Hero], id).map {hero =>
      hero.abilities -= typeId
      Redirect(routes.HeroController.heroList())
    } getOrElse NotFoundPage
  }

  def incrementLevel(id: Int, value: Int) = GameAction.andThen(AdminOnlyFilter) {implicit ctx =>
    session.get(classOf[Hero], id).map {hero =>
      hero.incLevel(value)
      Redirect(routes.HeroController.heroList())
    } getOrElse NotFoundPage
  }
}

object HeroController {
  val TavernaAction: ActionBuilder[GameContext] = LevelRequiredAction(config.TAVERNA_LEVEL)

  val upgradePrice = RubyPrice(30)
}
