package controllers

import bean.Hero
import components.Results.NotFoundPage
import components.Results.AccessDeniedPage

/**
  * Created by Salewan on 21.08.2017.
  */
class HeroLogController extends BaseController {

  def logPage(heroId: Int) = GameAction {implicit ctx =>
    if (getUser.isAdmin) {
      session.get(classOf[Hero], heroId).map { hero =>
        Ok(views.html.herolog.LogPageView(hero))
      } getOrElse NotFoundPage
    } else AccessDeniedPage
  }
}
