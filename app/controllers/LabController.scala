package controllers

import model.item.Ingredient
import components.actions.Actions.LevelRequiredAction
import model.Reward
import model.TransactionGenerator.KOSHEI_DUST_CLAIM_TX

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.04.2017.
  */
class LabController extends BaseController {


  override val GameAction = LevelRequiredAction(config.LABORATORY_LEVEL)

  /**
    * Страница лаборатории
    */
  def labPage = GameAction {implicit ctx =>
    val user = getGameUser
    val asc = !getSettings.labOrdering
    val barn = user.barn.domainKeyMap.toSeq.
      filter {case (ing, amount) => !ing.unlimited && amount > 0}.
      sortWith((a,b) => {
        val ai = a._1
        val bi = b._1

        if (ai.level == bi.level) ai.typeId > bi.typeId
        else if (asc) ai.level < bi.level
        else ai.level > bi.level
      })
    Ok(views.html.gameplay.lab.LabView(barn))
  }

  /**
    * Сжечь
    */
  def burn(ing: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    Ingredient.find(ing).filter(i => user.barn.get(ing).exists(_ > 0)).foreach {ingredient =>
      user.burnIngredient(ingredient, 1)
    }
    Redirect(routes.LabController.labPage())
  }

  /**
    * Изменить сортировку
    */
  def toggleOrdering = GameAction {implicit ctx =>
    getSettings.toggleLabOrdering()
    Redirect(routes.LabController.labPage())
  }

  /**
    * Получить бесплатную пыль
    */
  def claim = GameAction {implicit ctx =>
    val user = getGameUser
    val allowed = user.getAllowedFreeDust
    if (allowed > 0 && user.dustBankEnabled) {
      user.money.addDreamDust(allowed, KOSHEI_DUST_CLAIM_TX.get)
      user.lastFreeDustCollectTime(now)
      addFeedback(Reward().addDreamDust(allowed))
    }
    Redirect(routes.LabController.labPage())
  }
}
