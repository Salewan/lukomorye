package controllers

import components.HibernateSessionAware
import org.apache.commons.lang3.StringUtils
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.Session
import util.{AuthorizationUtil, WebFormsUtil}
import components.actions.Actions.FreeOnlyAction
/**
 * @author Sergey Lebedev (salewan@gmail.com) 15.03.2016.
 */
class LoginController extends BaseController {

  def userFormTuple(implicit hs: HibernateSessionAware) = Form (
    tuple(
      "nick" -> text.verifying(require("error.NickRequired")),
      "pass" -> text.verifying(require("error.PasswordRequired"))
    ) verifying Constraint[(String,String)]("") { t =>

      val checkNick = WebFormsUtil.nickOf(t._1)
      val checkPass = AuthorizationUtil.md5WithSalt(t._2)
      val avatar = userDAO.findByNick(checkNick)
      if (avatar.isEmpty || !avatar.get.profile.password.equals(checkPass)) Invalid("error.LoginFail")
      else Valid
    }
  )

  def require(errorKey: String): Constraint[String] = Constraint("") {
    pass => if (StringUtils.isNoneBlank(pass)) Valid else Invalid(errorKey)
  }

  def loginPage = FreeOnlyAction { implicit ctx =>
    Ok(views.html.auth.login(userFormTuple))
  }

  def login = FreeOnlyAction { implicit ctx =>
    userFormTuple.bindFromRequest().fold(formWithErrors => {
      Ok(views.html.auth.login(formWithErrors))
    }, data => {
      val nick = WebFormsUtil.nickOf(data._1)
      userDAO.findByNick(nick).map { a =>
        Redirect(routes.Application.city()).withSession(Session(Map(config.COOKIE_ID_NAME -> a.id.toString)))
      }.getOrElse(Redirect(routes.Application.notFound()))
    })
  }
}
