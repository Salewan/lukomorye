package controllers

import bean.state.MissionState
import bean.{Mission, MissionSlot, Task}
import components.PagingModel
import components.Results.NotFoundPage
import components.actions.Actions._
import components.context.GameContext
import dao.paging.{CollectionPagingDAO, HeroesPagingDAO}
import model.TransactionGenerator.ENFORCE_MISSION
import model.TransactionGenerator._
import play.api.mvc.AnyContent
import play.twirl.api.Html
import util.RandomUtil
import model.price.RubyPrice
import model.price.CoinPrice

import scala.concurrent.Future
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.10.2016.
  */
class MissionController extends BaseController {

  def sortModes(mode: Int)(implicit ctx: GameContext[AnyContent]): Option[Mission => Double] = mode match {
    case 0 => Some (mission => - 1 * mission.idealizedChance)
    case 1 => Some (mission => mission.duration.toDouble)
    case 2 => Some (mission => - mission.duration.toDouble)
  }

  /**
    * Страница списка миссий для игрока.
    */
  def missionList(p: Int, start: Boolean) = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val sortMode = sortModes(getSettings.missionSortMode)
    val sortFunc = (mission: Mission) =>
      if (mission.isNew && mission.slots.toSeq.exists(_.hero.nonEmpty)) - (Long.MaxValue - mission.id)
      else if (mission.complete.nonEmpty) -(Long.MaxValue - mission.complete.get)
      else if (sortMode.nonEmpty) sortMode.get.apply(mission)
      else 100 + mission.id

    val seq = gameUser.missions.toSeq.filter(_.state == MissionState.NEW).sortBy(sortFunc)

    val dao = new CollectionPagingDAO[Mission](seq)

    val pg = PagingModel(p, dao, routes.MissionController.missionList(_))

    if (start) addFeedback(Html("Отправьте Ваших героев на новые поручения!"))

    Ok(views.html.gameplay.mission.MissionList(pg))
  }

  /**
    * Страница запущенных поручений.
    */
  def inProgressList(p: Int) = GameAction {implicit ctx =>
    val user = getGameUser

    val seq = user.missions.toSeq.filter(_.state != MissionState.NEW).sortBy(_.complete.getOrElse(0L))
    val dao = new CollectionPagingDAO[Mission](seq)
    val pg = PagingModel(p, dao, routes.MissionController.inProgressList)
    Ok(views.html.gameplay.mission.InProgressMissions(pg))
  }

  private def getMission(missionId: Int)(implicit ctx: GameContext[_]): Option[Mission] = for {
    mission <- session.get(classOf[Mission], missionId)
    if mission.closed == false && (mission.isExpired == false || mission.state != MissionState.NEW || mission.inScare) && mission.owner == getGameUser
  } yield mission

  /**
    * Страница миссии когда кликаем на неё из списка см. выше.
    */
  def missionDescription(missionId: Int, page: Int) = GameAction {implicit ctx =>
    val user = getGameUser

    getMission(missionId).filter(_.isNew).map {mission =>
      // очистить слоты в незапущенных миссиях
      user.missions.toSeq.filter(mis => mis.id != missionId && mis.isNew).foreach(_.slots.toSeq.foreach(_.clear()))

      mission.generateEmptySlots()

      val heroes = user.getFreeHeroes.toSeq.sortWith((o1, o2) => {
        val w1 = mission.contribution(o1)
        val w2 = mission.contribution(o2)
        w1 > w2 || (w1 == w2 && o1.id > o2.id)
      })
      val pg = PagingModel(page, new HeroesPagingDAO(heroes),
        routes.MissionController.missionDescription(missionId, _))

      Ok(views.html.gameplay.mission.MissionPage(mission, pg))

    } getOrElse Redirect(routes.MissionController.missionList() )
  }

  /**
    * Клик по слоту
    */
  def missionSelectSlot(missionId: Int, select: Int, page: Int) = GameAction {implicit ctx =>

    getMission(missionId) map { mission =>
      mission.getSlot(select).foreach { slot =>
        if (slot.nonEmptySlot) slot.clear() // очистить слот если в нём находится герой
        else if (!mission.setupBestHero(slot)) addFeedback(views.html.gameplay.mission.ActiveLimitExceed()) // иначе поставить туда героя
      }
      Redirect(routes.MissionController.missionDescription(missionId, page))
    } getOrElse NotFoundPage
  }

  /**
    * Очистить все слоты
    */
  def missionClear(missionId: Int, page: Int) = GameAction {implicit ctx =>
    getMission(missionId) map {mission =>
      mission.slots.foreach(_.clear())
      mission.activeSlot(1)
      Redirect(routes.MissionController.missionDescription(missionId, page))
    } getOrElse NotFoundPage
  }

  /**
    * Страница списка героев которых можно отправить на задание
    */
  def missionHeroesList(missionId: Int, page: Int) = GameAction {implicit ctx =>
    getMission(missionId) map { mission =>
      val heroes = getGameUser.getFreeHeroes.toSeq.sortBy(hero => -mission.contribution(hero))
      Ok(views.html.gameplay.mission.MissionHeroes(mission, heroes, page))
    } getOrElse NotFoundPage
  }

  /**
    * Экшн помещает выбранного героя в свободный или лучший слот
    * Редирект на страницу описания миссии.
    */
  def missionSlotApply(missionId: Int, heroId: Int, page: Int) =
  GameAction { implicit ctx =>
    val user = getGameUser

    {
      for {
        mission <- getMission(missionId)
        hero <- user.getFreeHeroes.find(_.id == heroId)
      } yield (mission, hero)

    } map { case (mission, hero) =>
        if (!mission.insert(hero)) addFeedback(views.html.gameplay.mission.ActiveLimitExceed())
        Redirect(routes.MissionController.missionDescription(missionId, page))

    } getOrElse Redirect(routes.MissionController.missionList())
  }

  /**
    * Старт миссии
    */
  def missionStart(missionId: Int) = GameAction { implicit ctx =>
    val user = getGameUser
    val listOfMissions = routes.MissionController.missionList()

    getMission(missionId) map { mission =>
      if (mission.participatedHeroesCount > 0) {
        mission.start()
      }
      Redirect(listOfMissions)
    } getOrElse {
      addFeedback(Html("Поручение не найдено."))
      Redirect(listOfMissions)
    }
  }

  def _happyPath(mission: Mission, task: Task)(implicit ctx: GameContext[_]): Unit = {
    val user = getGameUser
    mission.closed(true)
    addFeedback(Html("Успех в поручении!"))

  }

  /**
    * Завершить поручение.
    */
  def missionComplete(missionId: Int, p: Int, redirectToTasks: Boolean) = GameAction {implicit ctx =>
    val user = getGameUser

    {
      for(mission <- getMission(missionId)
          if mission.owner == user && !mission.closed && mission.isCompleted) yield mission
    } map { mission =>

      val chance = mission.chance
      val win = RandomUtil.throwTheDice(chance)
      val bonus = RandomUtil.throwTheDice(chance - 1.0)

      if (win && mission.artefact.nonEmpty && user.barnFreeSpace < mission.generator.ingNum)
        addFeedback(views.html.misc.FullBarn())
      else {
        if (win) addFeedback(Html("Успех в поручении!"))
        else addFeedback(Html("Неудача при выполнении поручения."))

        addFeedback(mission.generator.reward(mission, win, bonus))
        user.complete(mission)
      }

      Redirect(routes.MissionController.inProgressList(p))
    } getOrElse NotFoundPage
  }

  /**
    * Ускорить поручение за деньги
    */
  def missionEnforceCompletion(missionId: Int, hash: Option[String], page: Int) =
    (GameAction andThen TimeReduceAction(hash, ENFORCE_MISSION.get, _time(missionId)(_))) { implicit ctx =>
    val back = routes.MissionController.inProgressList(page)
    ctx.fold({
      getMission(missionId).map { mission =>
        if (mission.state != MissionState.IN_PROGRESS || mission.owner != getGameUser) {
          session.getTransaction.setRollbackOnly()
          Redirect(back)
        } else {
          mission.complete(now)
          Redirect(routes.MissionController.missionComplete(mission.id))
        }
      } getOrElse NotFoundPage

    }, confirm => {
      val accept = routes.MissionController.missionEnforceCompletion(missionId, confirm.hash, page)
      addFeedback(confirm, accept, decline = back)
      Redirect(back)

    }, deficit => {
      addFeedback(deficit)
      Redirect(back)
    })
  }

  def _time(missionId: Int)(implicit ctx: GameContext[_]): Long = {
    getMission(missionId).flatMap { mission =>
      mission.complete.map(complete => (complete - now) max 0) map (_ * mission.participatedHeroesCount)
    } getOrElse 0
  }

  /**
    * Удалить поручение
    */
  def missionDrop(missionId: Int, p: Int, h: Option[String]) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    val user = getGameUser
    val home = routes.MissionController.missionList(p)
    getMission(missionId).filter(mission => mission.state == MissionState.NEW).map {mission =>
      if (!getSettings.missionRemoveConfirmation) {
        mission.closed(true)
        mission.delete()
      } else {
        ctx.fold({
          mission.closed(true)
          mission.delete()
        }, confirm => {
          addFeedback(Html("Удалить поручение?"), accept = routes.MissionController.missionDrop(missionId, p, confirm), decline = home)
        })
      }
      Redirect(home)
    } getOrElse {
      addFeedback(Html("Поручение не найдено."))
      Redirect(home)
    }
  }

  private def doLevelUpgrade()(implicit ctx: GameContext[AnyContent]): Unit = {
    val user = getGameUser
    user.incMissionsLevel()
    addFeedback(Html(s"Поздравляем! Максимальное количество поручений увеличено до ${user.missionsCountMax}!"))
  }

  private def upgradeMissionsLevelForRuby(price: RubyPrice, h: Option[String]) =
  (GameAction andThen BuyForRubyAction(h, _ => price, MISSIONS_MAX_TX.get))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.MissionController.missionList()

    ctx.fold({
      doLevelUpgrade()
    }, confirm => {
      addFeedback(confirm, accept = routes.MissionController.upgradeMissionsLevel(confirm.hash), decline = back)
    }, deficit => {
      addFeedback(deficit)
    })
    Redirect(back)
  }

  private def upgradeMissionsLevelForCoin(price: CoinPrice, h: Option[String]) = (GameAction andThen
    BuyForCoinAction(h, _ => price, UPGRADE_MISSIONS_LEVEL_TX.get, _ => Some(REST_UPGRADE_MISSIONS_LEVEL_TX.get)))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.MissionController.missionList()
    val accept = (hh: Option[String]) => routes.MissionController.upgradeMissionsLevel(hh)

    ctx.fold({
      doLevelUpgrade()
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
    }, coinDeficit => {
      addFeedback(coinDeficit)
    }, payRest => {
      addFeedback(payRest, accept(payRest.hash), decline = back)
    }, rubyDeficit => {
      addFeedback(rubyDeficit)
    })
    Redirect(back)
  }

  /**
    * Увеличить список поручений
    */
  def upgradeMissionsLevel(h: Option[String]) = GameAction.async {implicit ctx =>
    getGameUser.priceForMissionsCountUpgrade.map {
      case x: RubyPrice => upgradeMissionsLevelForRuby(x, h).apply(ctx)
      case x: CoinPrice => upgradeMissionsLevelForCoin(x, h).apply(ctx)
    } getOrElse Future.successful(Redirect(routes.MissionController.missionList()))
  }

  def sortMode(mode: Int) = GameAction {implicit ctx =>
    getSettings.missionSortMode(mode)
    Redirect(routes.MissionController.missionList())
  }

  def showDetails(missionId: Int, page: Int) = GameAction {implicit ctx =>
    getSettings.toggleMissionDetails()
    Redirect(routes.MissionController.missionDescription(missionId, page))
  }
}
