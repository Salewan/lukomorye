package controllers

import java.net.URLEncoder

import billing.RUBIES_REAL_AMOUNT
import components.Results.GamePage
import components.actions.Actions.{OdnoklassnikiLoginAction, FreeOdklAction}
import components.context.GameContext
import controllers.OdnoklassnikiController._
import controllers.payment.OdnoklassnikiPaymentMenuItem
import org.joda.time.DateTime
import play.api.Logger
import play.api.mvc.Call
import util.AuthorizationUtil
import util.Implicits._

import scala.xml.Elem

class OdnoklassnikiController extends BaseController {

  def login = OdnoklassnikiLoginAction {implicit ctx =>
    val queryString = ctx.queryString
    val checkSig = checkSigCorrect(queryString)
    val userIdOpt = queryString.get(REQUEST_PARAMETER_USER_ID).flatMap(_.headOption)
    val appKeyOpt = queryString.get(REQUEST_PARAMETER_APP_KEY).flatMap(_.headOption)
    val result =
    for (userId <- userIdOpt; appKey <- appKeyOpt if checkSig) yield {
      val externalId = OdnoklassnikiController.PARTNER_ODNOKLASSNIKI + userId
      userDAO.getUserByExternalId(externalId) match {
        case Some(user) =>
          logger.debug(s"user#${user.id} is logged in")
          user.altPartnerParams(Some(ctx.uri))
          GamePage.createGameSession(user.id)
        case None =>
          val user = userDAO.createUser().provideContextInfo().externalId(Some(externalId))
          logger.debug(s"user#${user.id} created")
          user.altPartnerParams(Some(ctx.uri))
          Redirect(routes.TaskController.taskList()).createGameSession(user.id)
      }
    }
    result.getOrElse {
      logger.error(s"Invalid params or sig ${ctx.rawQueryString}")
      Redirect(ODKL_MAIN)
    }
  }

  def billing = FreeOdklAction {implicit ctx =>
    val parameters = ctx.queryString
    val userId = parameters.get(REQUEST_PARAMETER_UID).flatMap(_.headOption).getOrElse("")
    val sig = parameters.get(REQUEST_PARAMETER_SIG).flatMap(_.headOption).getOrElse("")
    val transactionId = parameters.get(REQUEST_PARAMETER_TRANSACTION_ID).flatMap(_.headOption).getOrElse("")
    val amount = parameters.get(REQUEST_PARAMETER_AMOUNT).flatMap(_.headOption).map(_.toInt).getOrElse(0)
    val method = parameters.get(REQUEST_PARAMETER_METHOD).flatMap(_.headOption).getOrElse("")
    val remoteIp = ctx.remoteAddress

    var headers: List[(String,String)] = "Pragma" -> "no-cache" :: "Cache-Control" -> "no-cache, no-store" :: Nil
    val result: Elem =
    if (amount <= 0 || !"callbacks.payment".equals(method)) {
      logger.error(s"!!! INCORRECT AMOUNT OR METHOD !!! Billing request. [ip: $remoteIp] [amount: $amount] [method: $method] [odnoklassniki_login: $userId]")
      <ns2:error_response xmlns:ns2='http://api.forticom.com/1.0/'>
        <error_code>1001</error_code>
        <error_msg>CALLBACK_INVALID_PAYMENT : Payment is invalid and can not be processed</error_msg>
      </ns2:error_response>

    } else {
      val payment = billingDAO.getPayment(PARTNER_ODNOKLASSNIKI + transactionId)
      if (payment.nonEmpty) {
        logger.error(s"!!! TRANSACTION EXISTS !!! Billing request. [ip: $remoteIp] [amount: $amount] [transactionId: $transactionId] [odnoklassniki_login: $userId]")
        <callbacks_payment_response xmlns='http://api.forticom.com/1.0/'>true</callbacks_payment_response>

      } else {
        if (!checkSigCorrect(parameters)) {
          logger.error(s"!!! INVALID SIG !!! Billing request. [ip: $remoteIp] [amount: $amount] [odnoklassniki_login: $userId]")
          headers = "invocation-error" -> "104" :: headers
          <ns2:error_response xmlns:ns2='http://api.forticom.com/1.0/'>
            <error_code>104</error_code>
            <error_msg>PARAM_SIGNATURE : Invalid signature</error_msg>
          </ns2:error_response>

        } else {
          val user = userDAO.getUserByExternalId(PARTNER_ODNOKLASSNIKI + userId)
          logger.info(s"Billing request. [ip: $remoteIp] [amount: $amount] [odnoklassniki_login: $userId] [user: ${user.map(_.id.toString).getOrElse("not found")}]")
          user.map { user =>
            val rubies = rubiesBy(amount)
            billingDAO.successPayment(user.gameUser, rubies, "odkl", Some(PARTNER_ODNOKLASSNIKI + transactionId), None, rubies * RUBIES_REAL_AMOUNT)
            <callbacks_payment_response xmlns='http://api.forticom.com/1.0/'>true</callbacks_payment_response>
          } getOrElse {
            logger.info("!!! USER NOT FOUND !!!")
            headers = "invocation-error" -> "1001" :: headers
            <ns2:error_response xmlns:ns2='http://api.forticom.com/1.0/'>
              <error_code>1001</error_code>
              <error_msg>CALLBACK_INVALID_PAYMENT : Payment is invalid and can not be processed (user not found)</error_msg>
            </ns2:error_response>

          }

        }

      }
    }
    Ok(result).withDateHeaders("Date" -> new DateTime(), "Expires" -> new DateTime(0L)).withHeaders(headers: _*)
  }
}

object OdnoklassnikiController {

  val logger = Logger(classOf[OdnoklassnikiController])
  val SECRET_KEY: String = "EAF5A4D2759B25EDA0A693D8"
  val PUBLIC_KEY: String = "CBABEOGMEBABABABA"
  val APP_ID: String = "1265451008"
  val PARTNER_ODNOKLASSNIKI: String = "od:"
  val ODKL_DOMAIN: String = "luko.mobi"
  val ODKL_APP_URL: String = s"https://m.ok.ru/game/$APP_ID"
  val PAYMENT_LINK: String = "https://m.ok.ru/api/show_payment?"
  val ODKL_MAIN: String = "https://m.ok.ru/"

  val REQUEST_PARAMETER_USER_ID = "logged_user_id"
  val REQUEST_PARAMETER_API_SERVER = "api_server"
  val REQUEST_PARAMETER_APP_KEY = "application_key"
  val REQUEST_PARAMETER_SESSION_KEY = "session_key"
  val REQUEST_PARAMETER_SESSION_SECRET_KEY = "session_secret_key"
  val REQUEST_PARAMETER_AUTHORIZED = "authorized"
  val REQUEST_PARAMETER_APICONNECTION = "apiconnection"
  val REQUEST_PARAMETER_AUTH_SIG = "auth_sig"
  val REQUEST_PARAMETER_SIG = "sig"
  val REQUEST_PARAMETER_METHOD = "method"
  val REQUEST_PARAMETER_WIDS = "wids"
  val REQUEST_PARAMETER_NAME = "name"
  val REQUEST_PARAMETER_CODE = "code"
  val REQUEST_PARAMETER_UID = "uid"
  val REQUEST_PARAMETER_PRICE = "price"
  val REQUEST_PARAMETER_CLIENT_LOG = "clientLog"
  val REQUEST_PARAMETER_REFPLACE = "refplace"
  val REQUEST_PARAMETER_WEB_SERVER = "web_server"
  val REQUEST_PARAMETER_CALL_ID = "call_id"
  val REQUEST_PARAMETER_TRANSACTION_ID = "transaction_id"
  val REQUEST_PARAMETER_TRANSACTION_TIME = "transaction_time"
  val REQUEST_PARAMETER_PRODUCT_CODE = "product_code"
  val REQUEST_PARAMETER_AMOUNT = "amount"
  val REQUEST_PARAMETER_FORMAT = "format"
  val REQUEST_PARAMETER_ACCESS_TOKEN = "access_token"
  val REQUEST_PARAMETER_CONTAINER = "container"

  def getRequestParametersSortedAsString(queryString: Map[String, Seq[String]]): String = {
    for {
      (parName, v) <- queryString
      parValue <- v.headOption
      if !"sig".equalsIgnoreCase(parName)
    } yield s"$parName=$parValue"
  }.toSeq.sorted.mkString


  def checkSigCorrect(queryString: Map[String, Seq[String]]): Boolean = {
    queryString.get("sig").flatMap(_.headOption).exists {sig =>
      val reqStr = getRequestParametersSortedAsString(queryString)
      val strForSig = reqStr + SECRET_KEY
      sig.equals(AuthorizationUtil.md5(strForSig))
    }
  }

  private def generateExternalPaymentLink(params: Map[String, Seq[String]], oks: Int): Call = {
    {
      for {
        sessionKey <- params.get(REQUEST_PARAMETER_SESSION_KEY).flatMap(_.headOption)
        sessionSecretKey <- params.get(REQUEST_PARAMETER_SESSION_SECRET_KEY).flatMap(_.headOption)

      } yield {

        val price = oks
        var name = s"Покупка ${rubiesBy(oks)} рубинов в игре"
        val sig = AuthorizationUtil.md5(s"application_key=${PUBLIC_KEY}code=1name=${name}price=${price}session_key=${sessionKey}${sessionSecretKey}")
        try {
          name = URLEncoder.encode(name, "UTF-8")
        } catch { case _: Throwable => name = s"Pokupka${rubiesBy(oks)}Rubinov"}
        val params = s"name=${name}&price=${price}&code=1"
        val url = s"https://m.ok.ru/api/show_payment?application_key=$PUBLIC_KEY&session_key=$sessionKey&sig=${sig}&${params}"

        Call("GET", url)
      }

    } getOrElse Call("GET", ODKL_MAIN)
  }

  private def rubiesBy(oks: Int): Int = math.ceil(oks * 8.3333).asInstanceOf[Int]

  def menuItems(implicit ctx: GameContext[_]): Seq[OdnoklassnikiPaymentMenuItem] = {

    val userAndParams =
    for {
      user <- ctx.userOptional if user.isOdnoklassnikiUser
      params <- user.altPartnerParams.map { uri =>
        new play.mvc.Http.RequestBuilder().uri(uri).
        build().
        _underlyingRequest().queryString
      }
    } yield params

    userAndParams match {
      case Some(params) =>
        for (oks <- 3 :: 9 :: 24 :: 60 :: 120 :: 600 :: Nil) yield {
          val rub = rubiesBy(oks)
          val call = generateExternalPaymentLink(params, oks)
          OdnoklassnikiPaymentMenuItem(call, rub, oks)
        }
      case _ =>
        logger.error(s"Cannot generate odnoklassniki payment menu, user#${ctx.userId}")
        Seq.empty
    }
  }



}
