package controllers

import bean.ProductionSlot
import components.PagingModel
import components.actions.Actions._
import components.context.GameContext
import dao.paging.CollectionPagingDAO
import model.TransactionGenerator._
import model.item.{Ingredient, Seed}
import model.TransactionGenerator
import model.price.RubyPrice
import model.price.CoinPrice
import play.twirl.api.Html

import scala.concurrent.Future

/**
  * @author Sergey Lebedev (salewan@gmail.com) 09.03.2017.
  */
class OgorodController extends BaseController {

  /**
    * Грядки
    */
  def ogorodPage = GameAction {implicit ctx =>
    val gameUser = getGameUser
    val ogorod = gameUser.ogorod()
    ogorod.checkReady()
    val ogorodView = views.html.gameplay.farm.FarmView2(ogorod)
    Ok(views.html.gameplay.production.BuildingListPage("farm", ogorodView, gameUser.buildings))
  }

  /**
    * Посадить count штук, выбранного растения
    */
  def plant(count: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser

    if (gameUser.currentSeed.isEmpty) Redirect(routes.OgorodController.selectSeedPage())
    else {
      val ogorod = gameUser.ogorod()
      val currentSeed = gameUser.currentSeed.get
      if (!ogorod.canMakeMore(count)) addFeedback(Html("Не хватает свободных грядок."))
      else if (currentSeed.level > gameUser.level) addFeedback(Html(s"Посадка этого растения доступна с ${currentSeed.level} уровня."))
      else ogorod.enqueue(currentSeed, count)
      Redirect(routes.OgorodController.ogorodPage())
    }
  }

  /**
    * Посадить кучкой
    */
  def plantBulk = GameAction.async {implicit ctx =>
    val gameUser = getGameUser

    if (gameUser.currentSeed.isEmpty) Future.successful(Redirect(routes.OgorodController.selectSeedPage()))
    else {
      val ogorod = gameUser.ogorod()
      val available = ogorod.howManyCanMake
      if (available >= 1) plant(available).apply(ctx)
      else Future.successful(Redirect(routes.OgorodController.ogorodPage()))
    }
  }

  /**
    * Купить землю
    */
  def buyGround(h: Option[String]) = GameAction.async {implicit ctx =>
    val user = getGameUser
    if (!user.canBuyGround) Future.successful(Redirect(routes.OgorodController.ogorodPage()))
    else user.groundPrice match {
      case x: CoinPrice => buyGroundForMoney(x, h).apply(ctx)
      case x: RubyPrice => buyGroundForRuby(x, h).apply(ctx)
      case _ => throw new Error
    }
  }

  private def buyGroundForRuby(price: RubyPrice, h: Option[String]) =
  (GameAction andThen BuyForRubyAction(h, _ => price, TransactionGenerator.GROUND_TX.get))
  {implicit ctx =>
      val basicPage = routes.OgorodController.ogorodPage()
      val accept = (x: Option[String]) => routes.OgorodController.buyGround(x)

      ctx.fold({
        getGameUser.addGround()
      }, confirm => {
        addFeedback(confirm, accept(confirm.hash), decline = basicPage)
      }, deficit => {
        addFeedback(deficit)
      })

      Redirect(basicPage)
    }

  private def buyGroundForMoney(price: CoinPrice, h: Option[String]) = (GameAction andThen
    BuyForCoinAction(h, _ => price, GROUND_TX.get, _ => Some(REST_GROUND_TX.get)) )
  {implicit ctx =>
    val basicPage = routes.OgorodController.ogorodPage()
    val accept = (x: Option[String]) => routes.OgorodController.buyGround(x)

    ctx.fold({
      getGameUser.addGround()
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = basicPage)
    }, coinDeficit => {
      addFeedback(coinDeficit)
    }, restConfirm => {
      addFeedback(restConfirm, accept(restConfirm.hash), decline = basicPage)
    }, rubyDeficit => {
      addFeedback(rubyDeficit)
    })

    Redirect(basicPage)
  }

  /**
    * Собрать урожай
    */
  def harvest(typeId: Int, count: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    Ingredient.find(typeId).foreach {ingredient =>
      if (count > user.barnFreeSpace) addFeedback(views.html.misc.FullBarn())
      else addFeedback(user.ogorod().takeProduct(ingredient, count))
    }
    Redirect(routes.OgorodController.ogorodPage())
  }

  /**
    * Собрать кучкой. Показывает фидбэк с выбором.
    */
  def harvestBulk(typeId: Int) = GameAction.async {implicit ctx =>
    val gameUser = getGameUser
    val ogorod = gameUser.ogorod()
    val availableCount = ogorod.basket.get(typeId).getOrElse(0)

    if (availableCount > 0) harvest(typeId, availableCount).apply(ctx)
    else Future.successful(Redirect(routes.OgorodController.ogorodPage()))
  }

  private def speedupGrounds(slotId: Int)(implicit ctx: GameContext[_]): Seq[ProductionSlot] = {
    val gameUser = getGameUser
    session.get(classOf[ProductionSlot], slotId).
      filter(slot => slot.productType.nonEmpty && slot.production.owner == gameUser).map {likeSlot =>
      val ogorod = gameUser.ogorod()
      ogorod.slots.
        filter(slot => slot.isInProgress && slot.productType.contains(likeSlot.productType.get)).
        sortBy(_.started.get)
    } getOrElse Seq.empty
  }

  private def timeToReduce(slotId: Int, count: Int) = (ctx: GameContext[_]) => take(count, speedupGrounds(slotId)(ctx)).
    flatMap(_.timeLeft).sum

  private def take(count: Int, seq: Seq[ProductionSlot]): Seq[ProductionSlot] = seq.take(seq.size min count)

  /**
    * Ускорить созревание
    *
    * @param slotId слот для навигации. Какое растение хотим ускорить? Такое как в данном слоте.
    * @param count сколько растений хотим ускорить
    */
  def speedup(slotId: Int, count: Int, h: Option[String]) =
    (GameAction andThen TimeReduceAction(h, ENFORCE_GROUND.get, timeToReduce(slotId, count))) {implicit ctx =>
      val basicPage = routes.OgorodController.ogorodPage()
      val grounds = take(count, speedupGrounds(slotId))
      if (grounds.isEmpty) {
        session.getTransaction.setRollbackOnly()
        Redirect(basicPage)
      } else {
        ctx.fold({
          grounds.foreach(_.interval(Some(0)))
        }, confirm => {
          grounds.headOption.flatMap(_.productType.map(Ingredient(_))).foreach {ingredient =>
            val timesLeft = speedupGrounds(slotId).flatMap(_.timeLeft)
            addFeedback(views.html.gameplay.farm.BulkSpeedupFeedback(ingredient, timesLeft, confirm.hash, slotId, count))
          }
        }, deficit => {
          addFeedback(deficit)
        })
        Redirect(basicPage)
      }
  }

  /**
    * Отправляет на страницу выбора растения для посадки
    */
  def selectSeedPage(page: Int) = GameAction {implicit ctx =>
    val pg = PagingModel(page, new CollectionPagingDAO[Seed](Seed.allSeeds) {
      override protected val pageSize: Int = 20
    }, routes.OgorodController.selectSeedPage)
    Ok(views.html.gameplay.farm.FarmSeedSelection(pg))
  }

  /**
    * Выбрать растение, посадить
    */
  def selectSeed(seedType: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    user.currentSeed(Seed.allSeedsMap.get(seedType).filter(user.level >= _.level))
    Redirect(routes.OgorodController.ogorodPage())
  }

  /**
    * Меняет вид представления страницы SelectSeed краткий/подробный.
    */
  def toggleSeedSelectionView(p: Int) = GameAction {implicit ctx =>
    getSettings.toggleSeedSelectView()
    Redirect(routes.OgorodController.selectSeedPage(p))
  }
}
