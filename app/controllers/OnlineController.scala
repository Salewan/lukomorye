package controllers

import bean.Avatar
import components.PagingModel
import dao.paging.CollectionPagingDAOProxy
import util.OnlineUtil
import util.javas.OnlineUtil.UserEntry

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.01.2017.
  */
class OnlineController extends BaseController {

  def onlinePage(page: Int) = GameAction {implicit ctx =>
    val dao = new CollectionPagingDAOProxy[UserEntry, Avatar](OnlineUtil.getOnline, e => session.get(classOf[Avatar], e.uid).get, 15)
    val pg = PagingModel(page, dao, routes.OnlineController.onlinePage)
    Ok(views.html.user.OnlinePage(pg))
  }

  def nocollective(page: Int) = GameAction {implicit ctx =>
    val dao = new CollectionPagingDAOProxy[UserEntry, Avatar](OnlineUtil.getOnlineNoCollective, e => session.get(classOf[Avatar], e.uid).get, 15)
    val pg = PagingModel(page, dao, routes.OnlineController.nocollective)
    Ok(views.html.user.NoCollectivePage(pg))
  }
}
