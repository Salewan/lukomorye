package controllers

import bean.{Pet, PetProduction}
import components.Results.NotFoundPage
import components.actions.Actions._
import components.context.GameContext
import model.item.Product
import model.price.CoinPrice
import model.price.RubyPrice
import model.{Building, PetHouse, Reward}
import model.TransactionGenerator._
import play.twirl.api.Html

import scala.concurrent.Future

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.01.2017.
  */
class PetFarmController extends BaseController {

  private def getBuilding(buildingType: Int): Option[Building with PetHouse] = Building.findPetHouse(buildingType)

  /**
    * Страница "загона"
    */
  def petfarmPage(buildingType: Int) = GameAction {implicit ctx =>
    Building.findPetHouse(buildingType).map { building =>

      val user = getGameUser

      val production = getGameUser.petProductions.find(_.buildingType == building.typeId)
      val productionView = views.html.gameplay.petfarm.PetFarmView(building, production)

      Ok(views.html.gameplay.production.BuildingListPage(building.name, productionView, getGameUser.buildings))
    } getOrElse NotFoundPage
  }

  private def getPrice(buildingType: Int) = (_: GameContext[_]) => getBuilding(buildingType).map(_.getPrice).getOrElse(CoinPrice(0))

  /**
    * Построить загон
    */
  def petfarmBuild(buildingType: Int, h: Option[String]) = (GameAction andThen
    BuyForCoinAction(h, getPrice(buildingType), MAKE_BUILDING.get(buildingType),
      _ => Some(REST_BUILDING.get(buildingType))))
  { implicit ctx =>
    val user = getGameUser
    val petfarm = routes.PetFarmController.petfarmPage(buildingType)
    getBuilding(buildingType) match {
      case Some(building) if building.level > user.level =>
        addFeedback(Html(s"Доступно с ${building.level} уровня."))
        session.getTransaction.setRollbackOnly()
        Redirect(petfarm)
      case Some(building) if user.petProductions.exists(_.buildingType == building.typeId) =>
        addFeedback(Html("У вас уже есть это производство."))
        session.getTransaction.setRollbackOnly()
        Redirect(petfarm)
      case Some(building) =>
        ctx.fold({
            user.petProductions += new PetProduction().owner(user).buildingType(buildingType)
            Redirect(petfarm)
          },
          confirm => {
            val accept = routes.PetFarmController.petfarmBuild(buildingType, confirm.hash)
            addFeedback(confirm, accept, decline = petfarm)
            Redirect(petfarm)
          },
          coinDeficit => {
            addFeedback(coinDeficit)
            Redirect(petfarm)
          },
          restConfirm => {
            val accept = routes.PetFarmController.petfarmBuild(buildingType, restConfirm.hash)
            addFeedback(restConfirm, accept, decline = petfarm)
            Redirect(petfarm)
          },
          rubyDeficit => {
            addFeedback(rubyDeficit)
            Redirect(petfarm)
          })
      case _ => NotFoundPage
    }
  }

  /**
    * Купить животное
    */
  def petfarmBuyPet(buildingType: Int, h: Option[String]) = GameAction.async { implicit ctx =>
    val user = getGameUser

    {
      for {
        building <- Building.findPetHouse(buildingType)
        production <- user.petProductions.find(_.buildingType == building.typeId) if production.canBuyNewPet
        price <- production.newPetPrice
        if building.level <= user.level
      } yield (building, production, price)

    }.map { case (building, production, price) =>
      price match {
        case p: CoinPrice => buyPetForMoney(h, p, production).apply(ctx)
        case p: RubyPrice => buyPetForRuby(h, p, production).apply(ctx)
      }
    } getOrElse {Future successful Redirect(routes.PetFarmController.petfarmPage(buildingType))}
  }

  def buyPetForMoney(h: Option[String], price: CoinPrice, production: PetProduction) = (GameAction andThen
    BuyForCoinAction(h, _ => price, BUY_PET.get(production.buildingType),
      _ => Some(REST_PET.get(production.buildingType))))
  {implicit ctx =>
    val typeId = production.buildingType
    val back = routes.PetFarmController.petfarmPage(typeId)
    val accept = (x: Option[String]) => routes.PetFarmController.petfarmBuyPet(typeId, x)
    ctx.fold({
      production.addPet()
      Redirect(back)
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
      Redirect(back)
    }, coinDeficit => {
      addFeedback(coinDeficit)
      Redirect(back)
    }, restConfirm => {
      addFeedback(restConfirm, accept(restConfirm.hash), decline = back)
      Redirect(back)
    }, rubyDeficit => {
      addFeedback(rubyDeficit)
      Redirect(back)
    })
  }

  def buyPetForRuby(h: Option[String], price: RubyPrice, production: PetProduction) =
  (GameAction andThen BuyForRubyAction(h, _ => price, BUY_PET.get(production.buildingType)))
  {implicit ctx =>
    val typeId = production.buildingType
    val back = routes.PetFarmController.petfarmPage(typeId)
    val accept = (x: Option[String]) => routes.PetFarmController.petfarmBuyPet(typeId, x)
    ctx.fold({
      production.addPet()
      Redirect(back)
    }, confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
      Redirect(back)
    }, deficit => {
      addFeedback(deficit)
      Redirect(back)
    })
  }

  private def params(buildingType: Int, petId: Int)(implicit ctx: GameContext[_]) = for {
    user <- ctx.gameUserOptional
    building <- Building.findPetHouse(buildingType) if building.level <= user.level
    production <- user.petProductions.find(_.buildingType == buildingType)
    pet <- session.get(classOf[Pet], petId) if pet.petProduction == production
  } yield (building, production, pet)

  private def reducingTime(buildingType: Int, petId: Int): (GameContext[_]) => Long = context => {
    implicit val ctx = context
    params(buildingType, petId) match {
      case Some((building, production, pet)) if pet.isWalk => pet.fullness.get - now
      case _ => 0L
    }
  }

  /**
    * Ускорить животину
    */
  def petfarmPetEnforce(buildingType: Int, petId: Int, hash: Option[String]) =
  (GameAction andThen TimeReduceAction(hash, ENFORCE_PET.get(buildingType),reducingTime(buildingType, petId))) { implicit ctx =>
    params(buildingType, petId) match {
      case Some((building, production, pet)) if pet.isWalk =>
        val back = routes.PetFarmController.petfarmPage(buildingType)
        ctx.fold({
          pet.fullness(Some(now))
          if (getGameUser.barnFreeSpace >= 1)
            addFeedback(pet.collect())

          Redirect(back)
        }, conf => {
          addFeedback(conf, accept = routes.PetFarmController.petfarmPetEnforce(buildingType, petId, conf.hash), decline = back)
          Redirect(back)
        }, deficit => {
          addFeedback(deficit)
          Redirect(back)
        })
      case _ =>
        session.getTransaction.setRollbackOnly()
        Redirect(routes.PetFarmController.petfarmPage(buildingType))
    }
  }

  // цена в рубинах за корм
  private def foodPrice(food: Product)(implicit ctx: GameContext[_]): RubyPrice = {
    getGameUser.deficitPrice(food.composition)
  }

  // цена в рубинах за корм
  private def foodRubyPrice(petId: Int): GameContext[_] => RubyPrice = implicit context => {
    val user = getGameUser
    session.get(classOf[Pet], petId).filter(_.petProduction.owner == user).
      map(pet => foodPrice(pet.food)).getOrElse(throw new RuntimeException)
  }

  /**
    * Покупка корма за рубины.
    */
  def notEnoughFoodCase(buildingType: Int, petId: Int, h: Option[String]) = (GameAction andThen
    BuyForRubyAction(h, foodRubyPrice(petId), FOOD_FOR_PET.get(buildingType), Some(_ => true)))
  {implicit ctx =>
    val user = getGameUser
    ctx.fold({
      params(buildingType, petId).foreach { case (_, _, pet) =>
        if (pet.isHungry) pet.doFeed(true)
        else session.getTransaction.setRollbackOnly()
      }

    }, confirm => {
      params(buildingType, petId).foreach { case (_, _, pet) =>
        addFeedback(views.html.gameplay.petfarm.DeficitFeedback(pet.food, foodPrice(pet.food), buildingType, petId,
          confirm.hash, true))
      }

    }, deficit => {
      addFeedback(deficit)

    })

    Redirect(routes.PetFarmController.petfarmPage(buildingType))
  }

  /**
    * Клик по животному. Собрать ускорить или покормить.
    */
  def petfarmPetClick(buildingType: Int, petId: Int) = GameAction.async {implicit ctx =>
    val user = getGameUser
    val back = Future.successful(Redirect(routes.PetFarmController.petfarmPage(buildingType)))

    params(buildingType, petId) map { case (building, production, pet) =>
      if (pet.isFull) {
        if (user.barnFreeSpace >= 1) addFeedback(pet.collect())
        else addFeedback(views.html.misc.FullBarn())
        back
      } else if (pet.isHungry) {
        if (pet.doFeed()) back
        else notEnoughFoodCase(buildingType, petId, None).apply(ctx)

      } else {
        petfarmPetEnforce(buildingType, petId, None).apply(ctx)
      }
    } getOrElse Future.successful(NotFoundPage)
  }

  // цена на корм для всех голодных
  private def priceForFeedAll(prod: PetProduction): GameContext[_] => RubyPrice = implicit ctx => {
    val amount = prod.pets.filter(_.isHungry).map(pet => foodPrice(pet.food)).foldLeft[Long](0L)(_ + _.raw)
    RubyPrice(amount)
  }

  // покормить всех
  private def buyFoodForAll(prod: PetProduction, h: Option[String]) = (GameAction andThen
    BuyForRubyAction(h, priceForFeedAll(prod), FEED_ALL.get(prod.buildingType), Some(_ => true)))
  {implicit ctx =>
    val back = routes.PetFarmController.petfarmPage(prod.buildingType)
    ctx.fold({
      val hungryPets = prod.pets.filter(_.isHungry)
      if (hungryPets.isEmpty) session.getTransaction.setRollbackOnly()
      else hungryPets.foreach(pet => pet.doFeed(true))
    }, confirm => {
      val confirmView = views.html.gameplay.petfarm.FeedAllFeedback(confirm)
      val accept = routes.PetFarmController.feedAll(prod.buildingType, confirm.hash)
      addFeedback(confirmView, accept, decline = back)
    }, deficit => {
      addFeedback(deficit)
    })
    Redirect(back)
  }

  /**
    * Покормить всех
    */
  def feedAll(buildingType: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val user = getGameUser
    val back = Future.successful(Redirect(routes.PetFarmController.petfarmPage(buildingType)))
    user.getPetProduction(buildingType).map {prod =>
      val pets = prod.pets.toSeq
      if (!pets.exists(_.isHungry)) {
        addFeedback(Html("Некого кормить."))
        back
      }
      else if (user.barn.getOrElse(prod.building.food.typeId, 0) == 0) buyFoodForAll(prod, h).apply(ctx)
      else {
        prod.pets.toSeq.foreach(_.doFeed())
        back
      }
    } getOrElse back
  }

  /**
    * Собрать всё
    */
  def claimAll(buildingType: Int, h: Option[String]) = GameAction.async { implicit ctx =>
    val user = getGameUser
    val reward = Reward()
    val back = routes.PetFarmController.petfarmPage(buildingType)
    val FutureBack = Future.successful(Redirect(back))


    user.getPetProduction(buildingType).map { prod =>
      val ready = prod.pets.toSeq.filter(_.isFull)
      val running = getRunningPets(buildingType)

      if (ready.nonEmpty) {
        if (ready.size > user.barnFreeSpace) addFeedback(views.html.misc.FullBarn())
        else  {
          ready.foreach(reward += _.collect())
          addFeedback(reward)
        }
        FutureBack
      } else if (running.nonEmpty) {
        enforceAll(buildingType, h).apply(ctx)
      } else {
        addFeedback(Html("Сначала нужно покормить."))
        FutureBack
      }


    } getOrElse FutureBack
  }

  private def getRunningPets(buildingType: Int)(implicit ctx: GameContext[_]): Iterable[Pet] = {
    getGameUser.getPetProduction(buildingType).map(petFarm => petFarm.pets.toSeq.filter(_.isWalk)).getOrElse(Iterable.empty)
  }

  private def allPetsTimeSum(buildingType: Int): GameContext[_] => Long = implicit ctx => {
    getRunningPets(buildingType).foldLeft[Long](0L)(_ + _.remainTime.getOrElse(0L) )
  }

  /**
    * Ускорить всех
    */
  private def enforceAll(buildingType: Int, h: Option[String]) =
    (GameAction andThen TimeReduceAction(h, ENFORCE_PETS.get(buildingType), allPetsTimeSum(buildingType))) {implicit ctx =>
      val back = routes.PetFarmController.petfarmPage(buildingType)
      val user = getGameUser

      ctx.fold({
        val running = getRunningPets(buildingType)
        if (running.isEmpty) {
          session.getTransaction.setRollbackOnly()
        } else {
          val reward = Reward()
          running.foreach {pet =>
            pet.fullness(Some(now))
            if (user.barnFreeSpace > 0) reward += pet.collect()
          }
          addFeedback(reward)
        }

      }, confirm => {
        addFeedback(views.html.gameplay.petfarm.EnforceAllFeedback(confirm),
          accept = routes.PetFarmController.claimAll(buildingType, confirm.hash), decline = back)

      }, deficit => {
        addFeedback(deficit)

      })
      Redirect(back)
    }

  /**
    * admin mode
    */
  def makeFullAdminAction(prodId: Int) = GameAction {implicit ctx =>
    if (getUser.isAdmin) {
      session.get(classOf[PetProduction], prodId).foreach {prod =>
        prod.pets.foreach(pet => if (pet.isWalk) pet.fullness(Some(now)))
      }
    }
    Refresh
  }
}
