package controllers

import bean.{GameUser, Production, ProductionSlot}
import components.Results._
import components.actions.Actions._
import components.context.GameContext
import model.item.Product
import play.api.mvc.AnyContent
import model.{Building, ProdFactory, Reward}
import model.price.RubyPrice
import model.TransactionGenerator._
import play.twirl.api.Html

import scala.concurrent.Future

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.01.2017.
  */
class ProductionController extends BaseController {

  private def getBuilding(buildingType: Int): Option[Building with ProdFactory] = Building.findProdFactory(buildingType)

  /**
    * Страница производства
    */
  def productionPage(buildingType: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    getBuilding(buildingType).map { building =>
      val production = getGameUser.getProduction(building.typeId)
      production.foreach {p =>
        p.checkReady()
        p.visit()
      }
      val productionView = views.html.gameplay.production.ProductionView(building, production)



      Ok(views.html.gameplay.production.BuildingListPage(building.name, productionView, user.buildings))
    } getOrElse NotFoundPage
  }

  private def getPrice(buildingType: Int) = (_: GameContext[_]) =>
    getBuilding(buildingType).map(_.getPrice).get


  /**
    * Построить производство
    */
  def buyProduction(buildingType: Int, h: Option[String]) = (GameAction andThen
    BuyForCoinAction(h, getPrice(buildingType), MAKE_BUILDING.get(buildingType),
      _ => Some(REST_BUILDING.get(buildingType))))
  {implicit ctx =>
    val user = getUser
    val gameUser = getGameUser
    val page = routes.ProductionController.productionPage(buildingType)

    getBuilding(buildingType) match {
      case Some(building) if gameUser.productions.exists(_.buildingType == building.typeId) =>
        addFeedback(Html("У вас уже есть это производство."))
        session.getTransaction.setRollbackOnly()
        Redirect(page)
      case Some(building) if building.level > gameUser.level =>
        addFeedback(Html(s"Доступно с ${building.level} уровня."))
        session.getTransaction.setRollbackOnly()
        Redirect(page)
      case Some(building) =>
        ctx.fold({
          gameUser.createProduction(building)
          Redirect(page)
        }, confirm => {
          val accept = routes.ProductionController.buyProduction(buildingType, confirm.hash)
          addFeedback(confirm, accept, decline = page)
          Redirect(page)
        },
          coinDeficit => {
            addFeedback(coinDeficit)
            Redirect(page)
          },
          restConfirm => {
            val accept = routes.ProductionController.buyProduction(buildingType, restConfirm.hash)
            addFeedback(restConfirm, accept, decline = page)
            Redirect(page)
          },
          rubyDeficit => {
            addFeedback(rubyDeficit)
            Redirect(page)
          })
      case _ => NotFoundPage
    }
  }

  private def _yield(buildingType: Int, productType: Int)
                    (implicit ctx: GameContext[_]): Option[(Building with ProdFactory, Product, Production)] = {
    for (
      building <- Building.findProdFactory(buildingType);
      product <- building.products.find(_.typeId == productType);
      production <- getGameUser.productions.find(_.buildingType == building.typeId) if production.buildTime < now
    ) yield (building,product, production)
  }

  private def deficitAsRubies(buildingType: Int, productType: Int): GameContext[_] => RubyPrice = implicit context => {

    _yield(buildingType, productType).map { case (_, product, _) =>
        getGameUser.deficitPrice(product)
    } getOrElse RubyPrice(0L)
  }


  /**
    * Докупить ингредиенты.
    */
  private def enqueueWithDeficit(buildingType: Int, productType: Int, h: Option[String]) = (GameAction andThen
    BuyForRubyAction(h, deficitAsRubies(buildingType, productType),
      INGREDIENTS_TX.get(productType), Some(_ => true))) { implicit ctx =>

    val back = routes.ProductionController.productionPage(buildingType)
    val user = getGameUser

    ctx.fold({
      _yield(buildingType, productType).foreach {case (_, product, production) =>
          _enqueue(user, production, product, true)
      }
      Redirect(back)
    }, confirm => {
      _yield(buildingType, productType).foreach {case (_, product, _) =>
        addFeedback(
          views.html.gameplay.production.DeficitFeedback(buildingType, productType, user.deficit(product),
            user.deficitPrice(product), confirm.hash, true)
        )
      }
      Redirect(back)
    }, deficit => {
      addFeedback(deficit)
      Redirect(back)
    })


  }

  private def _enqueue(
                       gameUser: GameUser, production: Production,

                       product: Product, force: Boolean)(implicit ctx: GameContext[AnyContent]): Unit = {
    gameUser.subtractIngredients(product, force)
    production.enqueue(product)
  }


  /**
    * Добавить продукт в очередь на производство
    */
  def enqueue(buildingType: Int, productType: Int, h: Option[String]) = GameAction.async {implicit ctx =>
    val gameUser = getGameUser

    _yield(buildingType, productType) map { case (building, product, production) =>
      production.selection(productType)
      val back = Future successful Redirect(routes.ProductionController.productionPage(buildingType))

      if (gameUser.level < product.level) {
        addFeedback(Html(s"Доступно с ${product.level} уровня."))
        back
      } else if (production.isQueueFull) {
        addFeedback(Html("Очередь заполнена."))
        back
      } else if (!production.canMakeMore) {
        addFeedback(Html("Сначала нужно забрать готовую продукцию."))
        back
      } else if (gameUser.deficit(product).nonEmpty) {
        enqueueWithDeficit(buildingType, productType, h).apply(ctx)
      } else {
        _enqueue(gameUser, production, product, false)
        back
      }


    } getOrElse Future.successful(NotFoundPage)
  }

  /**
    * Выделить продукт
    */
  def selectProduct(buildingType: Int, productType: Int) = GameAction {implicit ctx =>
    _yield(buildingType, productType) foreach { case (_, product, production) =>
      val gameUser = getGameUser
      if (product.level <= gameUser.level) {
        production.selection(product.typeId)
      }
    }
    Redirect(routes.ProductionController.productionPage(buildingType))
  }


  /**
    * Забрать готовый продукт и положить в амбар
    */
  def takeProduct(buildingType: Int, productType: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    _yield(buildingType, productType)foreach {case (_, product, production) =>
      val reward = Reward()
      if (user.barnFreeSpace >= {if (production.isFeedMill) 3 else 1}) reward += production.takeProduct(product)
      else addFeedback(views.html.misc.FullBarn())
      addFeedback(reward)
    }
    Redirect(routes.ProductionController.productionPage(buildingType))
  }


  private def time(id: Int) = (context: GameContext[_]) => {
    implicit val ctx = context
    val gameUser = getGameUser

    for {
      production <- session.get(classOf[Production], id) if gameUser == production.owner
    } yield production.buildTime - now
  } getOrElse 0L


  /**
    * Ускорить строительство производства
    */
  def enforceBuilding(buildingType: Int, id: Int, h: Option[String]) =
    (GameAction andThen TimeReduceAction(h, ENFORCE_BUILDING.get(buildingType), time(id))) {implicit ctx =>
    val gameUser = getGameUser
    val page = routes.ProductionController.productionPage(buildingType)

    session.get(classOf[Production], id).
      filter(production => production.owner == gameUser && production.buildTime > now).map {production =>
      ctx.fold({
        production.buildTime(now)
        Redirect(page)
      },
        confirm => {
          val accept = routes.ProductionController.enforceBuilding(buildingType, id, confirm.hash)
          addFeedback(confirm, accept, decline = page)
          Redirect(page)
        },
        deficit => {
          addFeedback(deficit)
          Redirect(page)
        })
    } getOrElse {
      session.getTransaction.setRollbackOnly()
      Redirect(page)
    }
  }

  private def queueTime(id: Int) = (context: GameContext[_]) => {
    implicit val ctx = context
    val gameUser = getGameUser

    session.get(classOf[Production], id).filter(_.owner == gameUser).flatMap {production =>
      production.checkReady()
      production.timeLeft
    }
  } getOrElse 0L

  /**
    * Ускорить приготовление
    */
  def enforceQueue(buildingType: Int, id: Int, h: Option[String]) =
    (GameAction andThen TimeReduceAction(h, ENFORCE_QUEUE.get(buildingType), queueTime(id))) {implicit ctx =>
      val gameUser = getGameUser
      val page = routes.ProductionController.productionPage(buildingType)

      session.get(classOf[Production], id).
        filter(p => p.owner == gameUser && p.timeLeft.exists(_ > 0L)).map {production =>
        ctx.fold({
          production.getQueue.head.becomeReady()
          Redirect(page)
        },
          confirm => {
            val accept = routes.ProductionController.enforceQueue(buildingType, id, confirm.hash)
            addFeedback(confirm, accept, decline = page)
            Redirect(page)
          },
          deficit => {
            addFeedback(deficit)
            Redirect(page)
          })
      } getOrElse {
        session.getTransaction.setRollbackOnly()
        Redirect(page)
      }
  }

  def toggleBriefView(buildingType: Int) = GameAction {implicit ctx =>
    for (building <- getBuilding(buildingType); production <- getGameUser.getProduction(building.typeId)) {
      production.toggleBriefView()
    }
    Redirect(routes.ProductionController.productionPage(buildingType))
  }
}
