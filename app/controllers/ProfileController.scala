package controllers

import bean.{GameUser, Offer, User, UserBanRecord}
import components.PagingModel
import components.Results._
import components.context.GameContext
import controllers.helpers.BanHelper
import dao.paging.CollectionPagingDAO
import model.ReputationModel
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.AnyContent
import play.twirl.api.Html

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.11.2016.
  */
class ProfileController extends BaseController with BanHelper {

  private val statusForm = Form(single("status" -> text) verifying Constraint[String]("") { newStatus =>
    if (newStatus.trim.length > 140) Invalid("error.profile.about.isTooLong")
    else Valid
  })

  /**
    * Профиль игрока
    */
  def profilePage(id: Option[Int], statusForm: Boolean) = GameAction { implicit ctx =>
    id.flatMap(session.get(classOf[User], _)) map { foreigner =>
      val optionalFormAbout = Option(statusForm).find(_ && foreigner.isMe).
        map(_ => getProfile.about.map(this.statusForm.fill).getOrElse(this.statusForm))
      Ok(views.html.user.ProfilePage(foreigner, optionalFormAbout))
    } getOrElse NotFoundPage
  }

  /**
    * Сохранить статус
    */
  def saveAbout(id: Int) = GameAction {implicit ctx =>
    val back = routes.ProfileController.profilePage(Some(id))
    session.get(classOf[User], id).filter(_.isMe).map { me =>
      statusForm.bindFromRequest().fold(formWithErrors => Ok(views.html.user.ProfilePage(me, Some(formWithErrors))),
        newStatus => {
          me.profile.about(Some(newStatus))
          Redirect(back)
        })
    } getOrElse Redirect(back)
  }

  /**
    * Заявки и приглашения
    */
  def profileOffers(userId: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], userId).map { pageUser =>
      if (pageUser.isMe || getUser.isAdmin)
        Ok(views.html.user.OffersPage(pageUser.offers))
      else NotFoundPage
    } getOrElse NotFoundPage
  }

  /**
    * Принять приглашение вступить в княжество
    */
  def profileAcceptOffer(offerId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Offer], offerId).map { offer =>
      val clan = offer.clan

      if (clan.isFull) {
        addFeedback(Html("В княжестве уже максимальное количество участников."))
        Redirect(routes.ProfileController.profileOffers(offer.candidate.id))
      } else {
        val me = getGameUser
        if (offer.candidate == me) {
          me.clan.foreach(_.leave(me))
          clan.join(me, offer.master)
          me.clearOffers()

          val youJoined = me.avatar.ifFemale("Ты принята", "Ты принят")
          addFeedback(Html(s"Поздравляем $youJoined в княжество ${clan.visualName}"))
        }
        Redirect(routes.ClanController.clan(clan.id))
      }
    } getOrElse NotFoundPage
  }

  /**
    * Удалить свою заявку или отклонить приглашение
    */
  def profileDeclineOffer(offerId: Int) = GameAction {implicit ctx =>
    session.get(classOf[Offer], offerId).map { offer =>
      val me = getGameUser
      if (me == offer.candidate) {
        offer.clan.offers -= offer
        me.offers -= offer
        session.delete(offer)
      }
      Redirect(routes.ProfileController.profileOffers(offer.candidate.id))
    } getOrElse NotFoundPage
  }

  private def getBanTarget(targetId: Int)(implicit ctx: GameContext[AnyContent]): Option[User] = {
    val user = getUser
    session.get(classOf[User], targetId).filter(target => target != user && user.canBan)
  }

  private def renderBanPage(target: User, banForm: Form[(Int, Int)], timeKey: Int)
                           (implicit ctx: GameContext[AnyContent]) = {
    val opts = orderedReasons.toMap.map(f => f._1.toString -> f._2).toSeq
    views.html.user.ProfileBanPage(target, banForm, timeKey, opts)
  }

  /**
    * Страница "забанить" юзера
    */
  def profileBanPage(targetId: Int, timeKey: Int) = GameAction { implicit ctx =>
    val user = getUser

    getBanTarget(targetId) map { target =>
      Ok(renderBanPage(target, makeBanForm, timeKey))
    } getOrElse NotFoundPage
  }

  /**
    * POST. Обработка забанивания
    */
  def profileBan(targetId: Int, timeKey: Int) = GameAction {implicit ctx =>
    getBanTarget(targetId) map {target =>

      makeBanForm.bindFromRequest().fold(formWithErrors => {
        Ok(renderBanPage(target, formWithErrors, timeKey))
      },

      { case (validTimeKey, reasonKey) =>
        val user = getUser
        val time = getBanTimes(user).toMap.apply(validTimeKey)
        val reason = orderedReasons.toMap.apply(reasonKey)
        target.ban(time, user, reason, "")
        Redirect(routes.ProfileController.profilePage(Some(targetId)))
      })
    } getOrElse NotFoundPage
  }

  /**
    * Разбанить
    */
  def profileUnban(targetId: Int) = GameAction {implicit ctx =>
    val user = getUser
    session.get(classOf[User], targetId) foreach {target =>
      if (user.isAdminOrSupporter || (user.isModerator && !target.isUserBlocked))
        target.unban(user)
    }
    Redirect(routes.ProfileController.profilePage(Some(targetId)))
  }

  def profileBanHistory(targetId: Option[Int], p: Int) = GameAction {implicit ctx =>
    val user = getUser
    if (!user.isAdminOrSupporterOrModerator) AccessDeniedPage
    else {
      val target = targetId.flatMap(session.get(classOf[User], _))
      val banRecords = if (target.isEmpty) userDAO.getAllBanRecords else target.get.profile.banLog.toSeq
      val dao = new CollectionPagingDAO[UserBanRecord](banRecords)
      val pg = PagingModel(p, dao, routes.ProfileController.profileBanHistory(targetId, _))
      Ok(views.html.user.BanHistoryPage(target, pg))
    }
  }

  /**
    * Описание репутаций
    */
  def reputations(typeId: Int) = GameAction {implicit ctx =>
    val rep = ReputationModel.withTypeOption(typeId).getOrElse(ReputationModel.SALTAN)
    Ok(views.html.user.ReputationOverview(rep))
  }
}
