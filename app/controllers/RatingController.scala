package controllers

import bean.Clan
import components.PagingModel
import components.Results.NotFoundPage
import dao.paging.{CollectionPagingDAO, CollectionPagingDAOProxy}
import model.rating.{RatingEntity, RatingType}
import model.ReputationModel
import model.ReputationModel._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.12.2016.
  */
class RatingController extends BaseController {

  /**
    * Персональные
    */
  def ratingPage(kind: Int, param: Int, page: Int) = GameAction {implicit ctx =>
    RatingType.findPersonal(kind).map { ratingType =>

      val reputationType = ReputationModel.withTypeOption(param).getOrElse(SALTAN)
      val rating = ratingType.getRating(reputationType.typeId)
      val pg = PagingModel(page,
        new CollectionPagingDAO[RatingEntity](rating.entries), routes.RatingController.ratingPage(kind, param, _))

      Ok(views.html.rating.RatingPage(ratingType, reputationType, rating, pg))
    } getOrElse NotFoundPage
  }

  /**
    * Внутренние клановские
    */
  def clanRatingPage(ratingId: Int, clanId: Int, page: Int) = GameAction {implicit ctx =>
    {for {
      ratingType <- RatingType.findClanInner(ratingId)
      clan <- session.get(classOf[Clan], clanId)

    } yield (ratingType, clan)}.map { case (ratingType, clan) =>

      val rating = ratingType.getRating(clan)
      val pg = PagingModel(page, new CollectionPagingDAO(rating.entries),
        routes.RatingController.clanRatingPage(ratingId, clanId, _))

      Ok(views.html.rating.ClanRatingPage(clan, rating, ratingType, pg))
    } getOrElse NotFoundPage
  }

  /**
    * Внешние клановские
    */
  def clanList(ratingTypeOpt: Option[Int], reputation: Option[Int], p: Int) = GameAction {implicit ctx =>
    val ratingType = ratingTypeOpt.flatMap(RatingType.findClanOuter).getOrElse(RatingType.CLAN_LEVEL_EXP)
    val alliance = reputation
      .flatMap(i => ReputationModel.withTypeOption(i))
      .filter(_ => ratingType == RatingType.CLAN_ALLIANCE)
      .orElse {
        if (ratingType == RatingType.CLAN_ALLIANCE) Some(ReputationModel.SALTAN)
        else None
      }
    ratingType.getRating(alliance).map {rating =>
      val mapper: RatingEntity => (Clan, Long) = re => session.get(classOf[Clan], re.id).get -> re.value
      val dao = new CollectionPagingDAOProxy[RatingEntity, (Clan, Long)](rating.entries, mapper, 10)
      val pg = PagingModel(p, dao, routes.RatingController.clanList(ratingTypeOpt, reputation, _))

      Ok(views.html.gameplay.clan.ClanListPage(pg, ratingType, rating, alliance))
    } getOrElse NotFoundPage
  }
}
