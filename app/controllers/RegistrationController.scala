package controllers

import components.HibernateSessionAware
import components.actions.Actions._
import components.actions.OdnoklassnikiProhibitedFilter
import components.context.{GameContext, PlayContext}
import dao.UserDAO
import org.apache.commons.lang3.StringUtils
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.twirl.api.Html
import util.{AuthorizationUtil, SingleLanguageValidator}

import scala.collection.mutable.ArrayBuffer
import util.WebFormsUtil.nickOf
import RegistrationController.{nicknameCheckConstraint, nicknameOccupiedCheckConstraint}

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
class RegistrationController extends BaseController {

  def RegForm(implicit hs: HibernateSessionAware) = Form(mapping(
  "nick" -> text.verifying(nicknameCheckConstraint, nicknameOccupiedCheckConstraint),
  "sex" -> nonEmptyText,
  "pass1" -> text.verifying(passRequired("error.PasswordRequired")),
  "pass2" -> text.verifying(passRequired("error.ConfirmPassRequired")),
  "email" -> optional(email)
  )(RegistrationData.apply)(RegistrationData.unapply)
  verifying Constraint[RegistrationData]("") { data =>
      if (!data.pass1.trim.equalsIgnoreCase(data.pass2)) Invalid("error.PasswordsNoMatch")
      else Valid
  })



  def passRequired(errorKey: String): Constraint[String] = Constraint("") {
    pass => if (StringUtils.isNoneBlank(pass)) Valid else Invalid(errorKey)
  }

  private def filledForm(implicit ctx: PlayContext[_]) = {
    ctx.avatarOptional
      .map(avatar => RegForm.fill(RegistrationData("", avatar.sex.toString, "", "", ctx.profileOptional.flatMap(_.email))))
      .getOrElse(RegForm)
  }

  def registrationPage = AnonymousAction { implicit ctx =>
    Ok(views.html.auth.registration(filledForm))
  }

  def registrationPageMandatory = GameAction { implicit ctx =>
    Ok(views.html.auth.registration(filledForm, true))
  }

  /**
    * Do registration
    */
  def registration = AnonymousAction.andThen(new OdnoklassnikiProhibitedFilter) { implicit ctx =>

    RegForm.bindFromRequest().fold(formWithErrors => {
      Ok(views.html.auth.registration(formWithErrors))
    }, registrationData => {
      val nickDanger = registrationData.nick
      val pass = AuthorizationUtil.md5WithSalt(registrationData.pass1)
      var id = 0
      if (isLoggedIn) {
        ctx.userOptional.foreach(_.registerWith(nickDanger, pass, registrationData.email))
        id = ctx.userIdOptional.get
        addFeedback(id, Html("Спасибо за регистрацию!"))
        Redirect(routes.Application.city()).createGameSession(id)
      } else {
        val user = userDAO.createUser().provideContextInfo()
        user.registerWith(nickDanger, pass, registrationData.email)
        id = user.id
        Redirect(routes.TutorialController.tutorial(1)).createGameSession(id)
      }
    })
  }
}

object RegistrationController {
  lazy val nicknameCheckConstraint: Constraint[String] = Constraint("") { plainText: String =>

    val errors = ArrayBuffer[ValidationError]()
    val nick = nickOf(plainText)
    if (nick.isEmpty || nick.length < config.NICK_SIZE_MIN || nick.length > config.NICK_SIZE_MAX) {
      errors += ValidationError("error.NickLength")
    } else if (!nick.matches(config.NICK_PATTERN) || !SingleLanguageValidator.isValid(nick))
      errors += ValidationError("error.NickPattern")

    if (errors.isEmpty) Valid
    else Invalid(errors)
  }

  def nicknameOccupiedCheckConstraint(implicit hs: HibernateSessionAware): Constraint[String] = Constraint("") { plainText =>
    val nick = nickOf(plainText)
    if (UserDAO().findByNick(nick).isDefined) Invalid("error.NickOccupied")
    else Valid
  }
}

case class RegistrationData(nick: String, sex: String, pass1: String, pass2: String, email: Option[String] )
