package controllers

import components.actions.Actions.PlayAction
import components.context.PlayContext
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.mvc.AnyContent
import util.AuthorizationUtil

import scala.collection.mutable.ArrayBuffer

class RestoreController extends BaseController {

  // FORM
  def restoreForm(implicit ctx: PlayContext[AnyContent]): Form[(String, String)] = Form(
    tuple("pass1" -> text, "pass2" -> text)
    verifying Constraint[(String, String)]("") {data =>
      val errors = ArrayBuffer[String]()
      val pass1 = data._1.trim
      val pass2 = data._2.trim
      if (pass1.isEmpty) errors += "error.restore.pass1.required"
      if (pass2.isEmpty) errors += "error.restore.pass2.required"
      if (errors.isEmpty && !pass1.equalsIgnoreCase(pass2)) errors += "error.restore.passwords_unequal"
      if (errors.isEmpty) Valid
      else Invalid(errors.map(err => ValidationError(err)))
    }
  )

  def restore(key: String, expireTime: Long, check: String) = PlayAction {implicit ctx =>
    val user = userDAO.getUserByRestorePasswordKey(key)
    val error =
      if (user.isEmpty) Some("Неверный ключ.")
      else if (!check.equals(ForgotController.genCheck(key, expireTime))) Some("Неверные параметры.")
      else if (expireTime <= now) Some("Срок действия ключа истёк.")
      else None

    val through = (key, expireTime, check)
    (user, error) match {
      case (Some(usr), None) =>
        Redirect(routes.RestoreController.restorePage(key, expireTime, check)).createGameSession(usr.id)
      case _ =>
        Ok(views.html.forgot.RestorePage(restoreForm, error, through))
    }

  }

  def restorePage(key: String, expireTime: Long, check: String) = GameAction {implicit ctx =>
    Ok(views.html.forgot.RestorePage(restoreForm, None, (key, expireTime, check)))
  }

  def restorePost(key: String, expireTime: Long, check: String) = PlayAction {implicit ctx =>
    val optionalUser = userDAO.getUserByRestorePasswordKey(key)
    if (
      optionalUser.isEmpty || expireTime <= now || !check.equals(ForgotController.genCheck(key, expireTime))
    ) Redirect(routes.RestoreController.restore(key, expireTime, check))
    else restoreForm.bindFromRequest().fold(formWithErrors => {
      val through = (key, expireTime, check)
      Ok(views.html.forgot.RestorePage(formWithErrors, None, through))
    }, success => {
      val pass = AuthorizationUtil.md5WithSalt(success._1)
      val user = optionalUser.get
      user.profile.password(pass)
      user.secret(None)
      user.restorePasswordKey(None)
      Redirect(routes.ProfileController.profilePage(Some(user.id))).createGameSession(user.id)
    })
  }
}
