package controllers

import components.actions.Actions.{BuyForRubyAction, LevelRequiredAction}
import controllers.payment.PaymentController
import model.Reward
import model.TransactionGenerator.{BANK_CLAIM_TX, BANK_UPGRADE_TX}
import play.twirl.api.Html
import views.html.rubies._

class RubiesController extends BaseController {

  override val GameAction = LevelRequiredAction(4)

  private val index = routes.RubiesController.rubiesPage()

  def rubiesPage = GameAction {implicit ctx =>
    val actionPanel = PaymentController.getActionPanel()
    val view =
    if (getUser.isOdnoklassnikiUser) OdnoklassnikiPaymentView(actionPanel, OdnoklassnikiController.menuItems)
    else LukomorPaymentView(actionPanel, PaymentController.menuItems)

    Ok(views.html.rubies.RubiesView(view))
  }

  def claim = GameAction {implicit ctx =>
    val user = getGameUser
    val allowed = user.getAllowedRubies
    if (allowed > 0) {
      user.money.addRubies(allowed, BANK_CLAIM_TX.get)
      user.lastRubiesCollectTime(now)
      addFeedback(Reward().addRuby(allowed))
    }
    Redirect(index)
  }

  def upgradeBank(h: Option[String]) = (GameAction andThen BuyForRubyAction(
    h, _.gameUser.priceForBankUpgrade.get, BANK_UPGRADE_TX.get
  ))
  { implicit ctx =>
    val user = getGameUser
    ctx.fold({
      if (!user.canUpgradeBank) session.getTransaction.setRollbackOnly()
      else user.incBankLevel()
    }, confirm => {
      addFeedback(Html("Хотите улучшить банк?"), accept = routes.RubiesController.upgradeBank(confirm.hash), decline = index)
    }, deficit => {
      addFeedback(deficit)
    })
    Redirect(index)
  }

}
