package controllers

import bean.state.ClanRole
import play.twirl.api.Html
import components.Results.{GamePage, NotFoundPage}
import model.TransactionGenerator
import util.RuGrammar.rubiesBought
import components.actions.Actions.ConfirmationAction

class RubyMineController extends BaseController {

  def rubyMine = GameAction {implicit ctx =>
    getGameUser.clan.map(clan => Ok(views.html.gameplay.clan.RubyMinePage(clan))).getOrElse(NotFoundPage)
  }

  def claim = GameAction {implicit ctx =>
    val user = getGameUser
    user.clan.map {clan =>
      if (!user.allowClaimRubyMine) {
        addFeedback(Html(s"Только ${ClanRole.LEADER.name} может забирать рубины из шахты в пользу княжества."))
        Redirect(routes.RubyMineController.rubyMine())
      } else {
        val allowed = clan.getAllowedRubiesInMine
        if (allowed > 0) {
          clan.rubyMineCollectTime(now)
          clan.clanMoney().changeRuby(allowed, TransactionGenerator.CLAN_RUBY_MINE_CLAIM.get)
          addFeedback(Html(s"В кассу княжества поступило $allowed ${rubiesBought(allowed)}."))
        }
        Redirect(routes.RubyMineController.rubyMine())
      }
    } getOrElse {
      addFeedback(Html("Вы не состоите в княжестве."))
      GamePage
    }
  }

  def upgradeRubyMine(h: Option[String]) = (GameAction andThen ConfirmationAction(h)) {implicit ctx =>
    val user = getGameUser
    user.clan.map {clan =>
      ctx.fold({
        for(price <- clan.rubyMineUpgradePrice if user.allowUpgradeRubyMine) {
          val money = clan.clanMoney()
          val deficit = price.value - money.rubies
          if (deficit > 0) addFeedback(views.html.gameplay.clan.NotEnoughClanRubies(deficit))
          else {
            money.changeRuby(-price.value, TransactionGenerator.CLAN_RUBY_MINE_UPGRADE.get)
            clan.rubyMineLevel(clan.rubyMineLevel + 1)
          }
        }
        Redirect(routes.RubyMineController.rubyMine())
      }, newHash => {
        clan.rubyMineUpgradePrice.map(price =>
          addFeedback(
            views.html.gameplay.clan.UpgradeRubyMineAsk(price.value),
            accept = routes.RubyMineController.upgradeRubyMine(newHash),
            decline = routes.RubyMineController.rubyMine() ))
        Redirect(routes.RubyMineController.rubyMine())
      })
    } getOrElse GamePage
  }
}
