package controllers

import components.context.GameContext
import play.api.data.Forms._
import play.api.data._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.mvc.AnyContent


class SearchController extends BaseController {

  def searchForm(implicit ctx: GameContext[AnyContent]) = Form(
    single("query" -> text) verifying Constraint[String]("") {query =>
      if (query.trim.isEmpty) Invalid(ValidationError("error.search.empty_query"))
      else Valid
    }
  )

  def search = GameAction {implicit ctx =>
    Ok(views.html.search.SearchPage(searchForm, false, None, None))
  }

  def searchPost = GameAction {implicit ctx =>
    searchForm.bindFromRequest().fold(formWithErrors => {
      Ok(views.html.search.SearchPage(formWithErrors, false, None, None))
    }, success => {
      val user = userDAO.findByNick(success.trim.toLowerCase)
      val clan = clanDAO.getClanByName(success)
      if ((user.nonEmpty && clan.nonEmpty) || (user.isEmpty && clan.isEmpty))
        Ok(views.html.search.SearchPage(searchForm, true, user.map(_.avatar), clan))
      else if (user.nonEmpty) Redirect(routes.ProfileController.profilePage(Some(user.get.id)))
      else Redirect(routes.ClanController.clan(clan.get.id))
    })
  }

}
