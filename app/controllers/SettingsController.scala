package controllers

import components.Results.AccessDeniedPage
import components.actions.Actions.PlayAction
import components.context.GameContext
import org.apache.commons.lang3.StringUtils
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Constraints, Invalid, Valid, ValidationError}
import play.twirl.api.Html
import util.AuthorizationUtil

import scala.collection.mutable.ArrayBuffer

/**
  * @author Sergey Lebedev (salewan@gmail.com) 17.02.2017.
  */
class SettingsController extends BaseController {

  private val SettingsForm: Form[SettingsData] = Form (
    mapping(
      "onlyFriendsMessages" -> boolean,
      "smallSpendConfirm" -> boolean,
      "noReceiveGuildInvites" -> boolean,
      "showNewMailNotifications" -> boolean,
      "missionRemoves" -> boolean
    )(SettingsData.apply)(SettingsData.unapply)
  )

  def settings = GameAction {implicit ctx =>
    val settingsForm = SettingsForm.fill(getUser.settings.getSettingsData)
    Ok(views.html.user.SettingsPage(settingsForm))
  }

  def changeSettings = GameAction {implicit ctx =>
    SettingsForm.bindFromRequest().fold(
      formWithErrors => Ok(views.html.user.SettingsPage(formWithErrors)),
      goodSettings => {
        getUser.settings.applySettings(goodSettings)
        addFeedback(Html("Настройки сохранены."))
        Redirect(routes.SettingsController.settings())
      }
    )
  }

  def autologin(param: String) = PlayAction {implicit ctx =>
    userDAO.getUserBySecret(param).map {user =>
      val profile = user.profile
      val domain = ctx.getDomain
      Logger.debug(s"AUTOLOGIN: user#${user.id}, profile domain = ${profile.domain}, actual domain = ${domain}")
      if (domain.equals(profile.domain) || StringUtils.isBlank(profile.domain))
        Ok(views.html.user.AutologinPage(None)).withSession(createSession(user.id))
      else Ok(views.html.user.AutologinPage(Some("Неверный домен."))).withNewSession

    } getOrElse AccessDeniedPage

  }

  private def PassForm()(implicit ctx: GameContext[_]) = Form(tuple("pass1" -> text, "pass2" -> text, "pass3" -> text)
    verifying Constraint[(String,String,String)]("") {data =>
    val errors = ArrayBuffer[String]()
    val pass1 = data._1.trim
    val pass2 = data._2.trim
    val pass3 = data._3.trim
    if (pass1.isEmpty) errors += "error.pass1.required"
    if (pass2.isEmpty) errors += "error.pass2.required"
    if (pass3.isEmpty) errors += "error.pass3.required"
    if (pass1.nonEmpty && !AuthorizationUtil.md5WithSalt(pass1).equals(ctx.profile.password))
      errors += "error.pass1.incorrect"
    if (pass2.nonEmpty && pass3.nonEmpty && !pass2.toLowerCase.equals(pass3.toLowerCase))
      errors += "error.pass23NoMatch"

    if (errors.nonEmpty) Invalid(errors.map(ValidationError.apply(_)))
    else Valid
  })

  private def SexForm()(implicit ctx: GameContext[_]) = Form(tuple("sex" -> text, "email" -> optional(text), "nick" -> optional(text))
    verifying Constraint[(String, Option[String], Option[String])]("") {data =>
    val sex = data._1.trim.toLowerCase
    val email = data._2.map(_.trim)
    val errors = ArrayBuffer[ValidationError]()

    if (sex.isEmpty || !Array("m", "f").contains(sex)) errors += ValidationError("error.sex")

    if (getProfile.email.isEmpty && email.nonEmpty) Constraints.emailAddress.apply(email.get) match {
      case Invalid(errSeq) => errors ++= errSeq
      case _ =>
    }

    data._3.map(_.trim).foreach {nick =>
      val user = ctx.user
      if (user.isOdnoklassnikiUser && user.avatar.isAnonymous) {
        errors ++= (
          RegistrationController.nicknameCheckConstraint.apply(nick) ::
          RegistrationController.nicknameOccupiedCheckConstraint.apply(nick) :: Nil
          ).filter(_.isInstanceOf[Invalid]).flatMap(_.asInstanceOf[Invalid].errors)
      }
    }

    if (errors.nonEmpty) Invalid(errors)
    else Valid
  })

  private def SexFormFilled()(implicit ctx: GameContext[_]) = {
    val avatar = getAvatar
    SexForm().fill(
      (avatar.sex.toString, getProfile.email.map(AuthorizationUtil.cryptEmail), avatar.nick)
    )
  }

  def changeRegData(formNum: Int) = GameAction {implicit ctx =>
    Ok(views.html.user.ChangeRegDataPageView(PassForm(), SexFormFilled()))
  }

  def changeRegDataPost(formNum: Int) = GameAction {implicit ctx =>
    val index = routes.SettingsController.changeRegData()
    formNum match {
      case 2 =>
        SexForm().bindFromRequest().fold(formWithErrors => {
          Ok(views.html.user.ChangeRegDataPageView(PassForm(), formWithErrors))
        }, {
          case (newSex, email, nick) =>
            val sex = newSex.trim.toLowerCase
            val profile = getProfile
            val avatar = getAvatar
            avatar.sex(sex.charAt(0))
            profile.email(email.map(_.trim))
            if (getUser.isOdnoklassnikiUser && avatar.isAnonymous && nick.nonEmpty) {
              avatar.updateNick(nick.get)
            }
            addFeedback(Html("Настройки обновлены."))
            Redirect(index)
        })
      case 3 => Ok
      case _ =>
        PassForm().bindFromRequest().fold(formWithErrors => {
          Ok(views.html.user.ChangeRegDataPageView(formWithErrors, SexFormFilled()))
        }, {
          case (_, _, newPass) =>
            getProfile.password(AuthorizationUtil.md5WithSalt(newPass))
            getUser.secret(None)
            addFeedback(Html("Пароль успешно изменён."))
            Redirect(index)
        })
    }
  }
}

case class SettingsData(
                         onlyFriendsMessages: Boolean, smallSpendConfirm: Boolean,
                         noReceiveGuildInvites: Boolean, showNewMailNotifications: Boolean,
                         missionRemoves: Boolean
                       )
