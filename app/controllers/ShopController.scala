package controllers

import bean.{BuySlot, SellSlot}
import com.google.common.collect.ComparisonChain
import components.actions.Actions
import components.actions.Actions.BuyForRubyAction
import model.TransactionGenerator._
import play.twirl.api.Html

import scala.concurrent.Future


/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.10.2016.
  */
class ShopController extends BaseController {

  /**
    * Страница покупки
    */
  def shopPage = GameAction { implicit ctx =>
    val user = getGameUser

    user.updateBuyList(config.SHOP_GEN_INTERVAL)

    Ok(views.html.gameplay.shop.BuyListPage(user.buyList.toSeq.filter(!_.sold)))
  }

  /**
    * Страница продажи
    */
  def sellPage = GameAction { implicit ctx =>
    val user = getGameUser

    user.updateSellList(config.SHOP_GEN_INTERVAL)

    val f = (o1: SellSlot, o2: SellSlot) => ComparisonChain.start().compareTrueFirst(
      user.barn.get(o1.item.typeId).getOrElse(0) > 0, user.barn.get(o2.item.typeId).getOrElse(0) > 0
    ).compare(o1.id, o2.id).result()

    val sellSlots = user.sellList.toSeq.filter(!_.sold).sortWith((o1, o2) => f(o1,o2) < 0)

    Ok(views.html.gameplay.shop.SellListPage(sellSlots))
  }

  /**
    * Краткий/подробный вид
    */
  def toggleView(view: Int) = GameAction { implicit ctx =>
    getSettings.toggleShopView()
    if (view == 0) Redirect(routes.ShopController.shopPage()) else Redirect(routes.ShopController.sellPage())
  }

  private def doBuy(slot: BuySlot, unique: Long, h: Option[String]) = (GameAction andThen
    Actions.BuyForCoinAction(h, _ => slot.price, SHOP_PURCHASE_TX.get, _ => Some(REST_SHOP_PURCHASE_TX.get)) ) {
    implicit ctx =>
      val back = routes.ShopController.shopPage()
      val accept = (hh: Option[String]) => routes.ShopController.buy(slot.id, unique, hh)
      ctx.fold({
        addFeedback(getGameUser.addIngredient(slot.item, 1))
        slot.buyOrSell(1)
      }, confirm => {
        addFeedback(confirm, accept = accept(confirm.hash), decline = back)
      }, coinDeficit => {
        addFeedback(coinDeficit)
      }, restConfirm => {
        addFeedback(restConfirm, accept = accept(restConfirm.hash), decline = back)
      }, rubyDeficit => {
        addFeedback(rubyDeficit)
      })
      Redirect(back)
  }

  /**
    * Покупка
    */
  def buy(slotId: Int, unique: Long, h: Option[String]) = GameAction.async { implicit ctx =>
    val user = getGameUser
    user.updateBuyList(config.SHOP_GEN_INTERVAL)

    session.get(classOf[BuySlot], slotId).filter(_.owner == user).flatMap { slot =>
      if (unique != slot.unique) {
        addFeedback(Html("Предложение устарело. Предложения обновлены."))
        None
      } else if (user.barnFreeSpace <= 0) {
        addFeedback(views.html.misc.FullBarn())
        None
      } else {
        Some(slot)
      }
    }.map {slot =>
      doBuy(slot, unique, h).apply(ctx)
    }.getOrElse(Future.successful(Redirect(routes.ShopController.shopPage())))
  }

  /**
    * Продажа
    */
  def sell(slotId: Int, h: Long) = GameAction { implicit ctx =>
    val user = getGameUser
    user.updateSellList(config.SHOP_GEN_INTERVAL)

    session.get(classOf[SellSlot], slotId).filter(_.owner == user).foreach { slot =>
      if (h != slot.unique) addFeedback(Html("Предложение устарело. Предложения обновлены."))
      else if (user.barn.getOrElse(slot.item.typeId, 0) <= 0) addFeedback(Html("Недостаочно продукции для продажи."))
      else {
        user.barn.update(slot.item.typeId)(have => have.get - 1)
        addFeedback(user.money.addCoins(slot.value, STORE_SELL_TX.get))
        slot.buyOrSell(1)
      }
    }
    Redirect(routes.ShopController.sellPage())
  }

  def refreshBuyList(h: Option[String]) = (GameAction andThen
    BuyForRubyAction(h, _.gameUser.shopRefreshPrice, REFRESH_BUY_LIST_TX.get))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.ShopController.shopPage()

    ctx.fold({
      user.updateBuyList(1L)
      user.shiftForwardShopRefreshTimer()
      addFeedback(Html("Список для покупки обновлён."))
    }, confirm => {
      addFeedback(confirm, accept = routes.ShopController.refreshBuyList(confirm.hash), decline = back)
    }, deficit => {
      addFeedback(deficit)
    })

    Redirect(back)
  }

  def refreshSellList(h: Option[String]) =
  (GameAction andThen BuyForRubyAction(h, _.gameUser.shopRefreshPrice, REFRESH_SELL_LIST_TX.get))
  {implicit ctx =>
    val user = getGameUser
    val back = routes.ShopController.sellPage()

    ctx.fold({
      user.updateSellList(1L)
      user.shiftForwardShopRefreshTimer()
      addFeedback(Html("Список для продажи обновлён."))
    }, confirm => {
      addFeedback(confirm, accept = routes.ShopController.refreshSellList(confirm.hash), decline = back)
    }, deficit => {
      addFeedback(deficit)
    })

    Redirect(back)
  }
}
