package controllers

import javax.inject.Inject

import bean.Avatar
import components.PlayComponentsHolder
import components.actions.Actions.LevelRequiredAction
import components.actions.SupporterFilter
import components.context.GameContext
import play.api.data.Forms._
import play.api.data._
import play.api.mvc.ActionBuilder
import play.twirl.api.Html

class SupportController extends BaseController {

  override val GameAction: ActionBuilder[GameContext] = LevelRequiredAction() andThen SupporterFilter

  def changeNick(id: Option[Int]) = GameAction {implicit ctx =>
    id.flatMap(uid => session.get(classOf[Avatar], uid)).foreach {avatar =>
      val form = Form(
        single(
          "nick" -> optional(
            text.verifying(
              RegistrationController.nicknameOccupiedCheckConstraint,
              RegistrationController.nicknameOccupiedCheckConstraint)
          )))

      form.bindFromRequest().fold(formWithErrors => {
        val err = "Не удалось сохранить ник.\n" +
          formWithErrors.errors.toSet[FormError].map(fe => PlayComponentsHolder.messagesApi(fe.message)).mkString("\n")
        addFeedback(Html(err))
      }, success => {
        avatar.updateNick(success.get.trim)
      })
    }
    Redirect(routes.ProfileController.profilePage(id))
  }
}
