package controllers

import bean.{GameUser, Task}
import components.PagingModel
import components.actions.Actions.{BuyForCoinAction, BuyForRubyAction, ConfirmationAction}
import components.context.GameContext
import dao.paging.CollectionPagingDAO
import model.TransactionGenerator._
import model.price.{CoinPrice, RubyPrice}
import play.api.mvc.{AnyContent, Call}
import play.twirl.api.Html

import scala.concurrent.Future

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.11.2016.
  */
class TaskController extends BaseController {

  val REFRESH_PRICE = RubyPrice(1)

  /**
    * Список заданий.
    */
  def taskList(p: Int) = GameAction {implicit ctx =>
    val gameUser = getGameUser

    val sort_func = (task: Task) => {
      val canComplete = task.canComplete
      val asEvent = task.generator.asEvent

      if (asEvent && canComplete) Long.MinValue + task.id
      else if (task.canComplete) Long.MinValue + Int.MaxValue + task.id
      else if (asEvent) Long.MinValue + Int.MaxValue + Int.MaxValue + task.id
      else task.id
    }

    val tasks = gameUser.tasks.toSeq.sortBy(sort_func)
    val pg = PagingModel(p, new CollectionPagingDAO[Task](tasks) {
      override protected val pageSize: Int = 5
    }, routes.TaskController.taskList)

    Ok(views.html.gameplay.task.TaskListPage(pg, REFRESH_PRICE))
  }

  private def getTask(taskId: Int)(implicit ctx: GameContext[_]): Option[Task] = for {
    task <- session.get(classOf[Task], taskId)
    if task.owner == getGameUser && !task.isExpired
  } yield task

  /**
    * Выполнить задание
    */
  def taskFinish(taskId: Int, p: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    getTask(taskId).filter(_.canComplete).foreach { task =>
      addFeedback(task.complete)
      session.flush()
    }
    Redirect(routes.TaskController.taskList(p))
  }

  /**
    * Удалить задание
    */
  def taskDrop(taskId: Int, p: Int, h: Option[String]) =
  (GameAction andThen ConfirmationAction(h))
  {implicit ctx =>
    val user = getGameUser
    val home = routes.TaskController.taskList(p)

    {for (task <- getTask(taskId))
      yield {
        ctx.fold({
          task.delete()
        }, confirm => {
          addFeedback(Html("Удалить задание?"), accept = routes.TaskController.taskDrop(taskId, p, confirm), decline = home)
        })
        Redirect(home)
      }
    }.getOrElse {
      addFeedback(Html("Задание не найдено."))
      Redirect(home)
    }
  }

  /**
    * Режим краткий/подробный.
    */
  def toggleView(p: Int) = GameAction {implicit ctx =>
    getSettings.toggleTaskView()
    Redirect(routes.TaskController.taskList(p))
  }

  // 1
  def upgradeTaskLevel(h: Option[String], p: Int) = GameAction.async { implicit ctx =>
    getGameUser.priceForTasksCountUpgrade.map {
      case x: RubyPrice => _rubyUpgrade(h, x, p).apply(ctx)
      case x: CoinPrice => _coinUpgrade(h, x, p).apply(ctx)
    } getOrElse Future.successful(Redirect(routes.TaskController.taskList(p)))
  }

  // 2
  private def _upgradeTaskLevel(user: GameUser)(implicit ctx: GameContext[AnyContent]): Unit = {
    user.taskLevelInc()
    addFeedback(Html(s"Поздравляем! Максимальное количество заданий увеличено до ${user.tasksCountMax}!"))
  }

  // 3
  private def _rubyUpgrade(h: Option[String], price: RubyPrice, page: Int) =
  (GameAction andThen BuyForRubyAction(h, _ => price, TASKS_MAX_TX.get))
  {implicit ctx =>

    val me = getGameUser
    val accept: Option[String] => Call = routes.TaskController.upgradeTaskLevel(_, page)
    val back = routes.TaskController.taskList(page)

    ctx.fold({
      _upgradeTaskLevel(me)
      Redirect(back)
    },
    confirm => {
      addFeedback(confirm, accept(confirm.hash), decline = back)
      Redirect(back)
    },
    deficit => {
      addFeedback(deficit)
      Redirect(routes.TaskController.taskList(page))
    })
  }

  // 4
  private def _coinUpgrade(h: Option[String], price: CoinPrice, page: Int) = (GameAction andThen
    BuyForCoinAction(h, _ => price, TASKS_MAX_TX.get, _ => Some(REST_TASKS_MAX_TX.get)))
  {implicit ctx =>

      val me = getGameUser
      val accept: Option[String] => Call = routes.TaskController.upgradeTaskLevel(_, page)
      val back = routes.TaskController.taskList(page)

      ctx.fold({
        _upgradeTaskLevel(me)
        Redirect(back)
      },
        confirm => {
          addFeedback(confirm, accept(confirm.hash), decline = back)
          Redirect(back)
        },
        coinDeficit => {
          addFeedback(coinDeficit)
          Redirect(back)
        },
        payRestConfirm => {
          addFeedback(payRestConfirm, accept(payRestConfirm.hash), decline = back)
          Redirect(back)
        },
        rubyDeficit => {
          addFeedback(rubyDeficit)
          Redirect(back)
        })
    }
}
