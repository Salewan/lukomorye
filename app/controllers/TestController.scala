package controllers

import javax.inject.Inject

import play.api.cache.CacheApi

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
class TestController extends BaseController {

  @Inject
  var cache: CacheApi = _
}
