package controllers

import bean.Ticket
import bean.messages.personal.PrivateMessage
import components.PagingModel
import components.Results.{AccessDeniedPage, GamePage}
import components.context.GameContext
import dao.paging.CollectionPagingDAOProxy
import model.TicketState
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.AnyContent
import util.MessagesUtil

/**
  * Created by Salewan on 11.04.2017.
  */
class TicketsController extends BaseController {

  val PAGE_SIZE = 5

  /**
    * Саппортерская страница списка тикетов
    */
  def ticketsPage(uid: Option[Int], filter: Option[Char], p: Int) = GameAction {implicit ctx =>
    val user = getUser

    if (user.isAdminOrSupporter) {
      val filterState = filter.map(TicketState.withType).getOrElse(TicketState.UNKNOWN)
      val tickets = uid match {
        case Some(userId) => ticketDAO.getTicketIds(userId, filterState)
        case None => ticketDAO.getTicketIds(filterState, filterState == TicketState.NEW || filterState == TicketState.ADMIN)
      }
      val dao = new CollectionPagingDAOProxy[Int, Ticket](tickets, session.get(classOf[Ticket], _).get, PAGE_SIZE)

      val pg = PagingModel(p, dao, routes.TicketsController.ticketsPage(uid, filter, _))

      Ok(views.html.ticket.TicketListView(pg, uid, filterState))

    } else AccessDeniedPage
  }

  /**
    * Изменить состояние тикета
    */
  def changeState(tid: Int, state: Char, uid: Option[Int], p: Int) = GameAction {implicit ctx =>
    {
      for {
        user <- ctx.userOptional if user.isAdminOrSupporter
        ticket <- session.get(classOf[Ticket], tid)
      } yield (user, ticket)

    } foreach {case (user, ticket) =>
        ticket.stateId(state).resolver(Some(user)).resolveTime(Some(now))
    }
    Redirect(routes.TicketsController.ticketsPage(uid, Some(state), p))
  }

  /**
    * История переписки с игроком
    */
  def supportCorrespondencePage(senderId: Int, p: Int) = GameAction {implicit ctx =>
    val user = getUser
    if (user.isAdminOrSupporter) {
      val dao = new CollectionPagingDAOProxy[Int, PrivateMessage](
        ticketDAO.getSupportCorrespondenceMessagesIds(senderId),
        session.get(classOf[PrivateMessage], _).get, 5)

      val pg = PagingModel(p, dao, routes.TicketsController.supportCorrespondencePage(senderId, _))
      Ok(views.html.ticket.SupportCorrespondenceView(pg))
    } else AccessDeniedPage
  }

  /**
    * Удалить тикет
    */
  def delete(tid: Int, state: Char, uid: Option[Int], p: Int) = GameAction {implicit ctx =>
    (for {
      user <- ctx.userOptional if user.isAdmin
      ticket <- session.get(classOf[Ticket], tid)
    } yield ticket) foreach session.delete
    Redirect(routes.TicketsController.ticketsPage(uid, Some(state), p))
  }

  private val ticketTypes = Seq("1" -> "Сообщение об ошибке", "2" -> "Проблемы с восстановлением персонажа", "3" -> "Проблемы с платежами")

  private def makeHelpForm(implicit ctx: GameContext[AnyContent]) = Form(
    tuple(
      "type" -> number,
      "text" -> text
    ) verifying Constraint[(Int, String)]("") {case (ticketType, ticketText) =>
      val text = ticketText.trim
      if (text.isEmpty) Invalid("error.ticket.required")
      else if (!ticketTypes.map(_._1).contains(ticketType.toString)) Invalid("error.ticket.key")
      else if (getGameUser.level < config.MESSAGES_ALLOWED_LEVEL) Invalid("error.ticket.level", config.MESSAGES_ALLOWED_LEVEL)
      else Valid
    }
  )

  /**
    * Форма для игрока для ввода заявки
    */
  def helpPage = GameAction {implicit ctx =>
    Ok(views.html.ticket.HelpView(makeHelpForm, ticketTypes))
  }

  /**
    * POST отправить тикет
    */
  def submitTicket = GameAction {implicit ctx =>
    makeHelpForm.bindFromRequest().fold(formWithErrors => Ok(views.html.ticket.HelpView(formWithErrors, ticketTypes)),
    {case (ticketType, message) =>
        val ticket = Ticket()
        ticket.typeId(ticketTypes.toMap.apply(ticketType.toString))
        ticket.sender(getUser)
        ticket.text(message)
        session.persist(ticket)
        MessagesUtil.sendNotificationMessage(getUser, "Ваш запрос отправлен в Службу тех. поддержки.", config.ADMINISTRATION_LOGIN, false)
        GamePage
    })
  }
}
