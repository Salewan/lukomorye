package controllers

import bean.{GameUser, MoneyTransaction}
import components.PagingModel
import components.Results.NotFoundPage
import dao.paging.DatabasePagingDAO
import model.price.Currency

class TransactionController extends BaseController {

  def transactions(userId: Int, p: Int) = GameAction {implicit ctx =>
    session.get(classOf[GameUser], userId).filter(_ => getUser.isAdminOrSupporter).map {user =>

      val dao = new DatabasePagingDAO[MoneyTransaction] {

        override protected val pageSize: Int = 10

        override def loadPage(p: Int): Iterable[MoneyTransaction] =
          moneyTransactionDAO.getPage(user, Currency.Ruby.typeId, this.pageSize, p)

        override protected def allSize: Int =
          moneyTransactionDAO.getSize(user, Currency.Ruby.typeId)
      }

      val pg = PagingModel(p, dao, routes.TransactionController.transactions(userId, _))
      Ok(views.html.user.RubyTransactions(pg, user))
    } getOrElse NotFoundPage
  }
}
