package controllers

import bean.state.TransformState
import components.PagingModel
import components.actions.Actions._
import dao.paging.TransformIngredientsPagingDAO
import play.api.data.Form
import play.api.data.Forms._
import play.twirl.api.Html
import util.PlusUtil
import views.html.tutorial.IvanSpeech

/**
  * @author Sergey Lebedev (salewan@gmail.com) 07.11.2016.
  */
class TransformController extends BaseController {

  /**
    * Форма для ввода количества превращений
    */
  def form = Form(single("amount", number(1, 100))).fill(1)


  /**
    * Главная страница превращений.
    */
  def transformPage(page: Int) = GameAction {implicit ctx =>
    val user = getGameUser
    val transform = user.transform
    val countable = transform.getCountableIngredients
    val pg = PagingModel(page, TransformIngredientsPagingDAO(countable), routes.TransformController.transformPage)
    val uncountable = transform.getUncountableIngredients
    Ok(views.html.gameplay.transform.TransformPage(form, transform, pg, uncountable))
  }

  /**
    * Выбрать слот. Если там что-то очистить. Если был выбран до этого поставить туда
    * первый итем из списка ингредиентов.
    */
  def transformSelectSlot(slotNum: Int, page: Int) = GameAction {implicit ctx =>
    val transform = getGameUser.transform
    val selOpt = transform.ing(slotNum)

    if (selOpt.isDefined) {
      transform.ing(slotNum, None)
      transform.activeSlot(slotNum)
    } else if (transform.activeSlot() == slotNum) {
      transform.ing(slotNum, transform.getFirstAvailableIngredient map (_.typeId))
      transform.makeNextSlotActive()
    } else transform.activeSlot(slotNum)

    Redirect(routes.TransformController.transformPage(page))
  }

  /**
    * Обработка клика по итему в списке ингредиентов.
    * Ставит ингредиент в активный слот.
    */
  def transformSelectIngredient(ing: Int, page: Int) = GameAction {implicit ctx =>
    val transform = getGameUser.transform
    transform.ing(transform.activeSlot(), Some(ing))
    transform.makeNextSlotActive()
    Redirect(routes.TransformController.transformPage(page))
  }

  /**
    * Делает превращение ингредиентов
    */
  def transformSubmit(page: Int) = GameAction {implicit ctx =>
    form.bindFromRequest().fold(formWithErrors => {
      addFeedback(Html("Поддерживается количество превращений от 1 до 100."))
      Redirect(routes.TransformController.transformPage(page))
    }, amount => {

      val user = getGameUser
      val transform = user.transform

      transform.state match {
        case TransformState.NotAvailable =>
          addFeedback(Html("Не хватает знаний для превращения!"))
        case TransformState.Available =>
          transform.futureRecipe.foreach { recipe =>
            if (recipe.canTransform(user, amount)) {
              addFeedback(recipe.doTransform(user, amount))
            } else addFeedback(Html("Не хватает ингредиентов."))
          }
        case _ => ()
      }
      Redirect(routes.TransformController.transformPage(page))


    })
  }

  /**
    * Очистка слотов от ингредиентов
    */
  def transformClearSlots(page: Int) = GameAction {implicit ctx =>
    getGameUser.transform.clear()
    Redirect(routes.TransformController.transformPage(page))
  }
}
