package controllers

import components.context.GameContext
import model.{ReputationModel, Reward, item}
import model.hero.HeroTemplate
import model.item.Thing.{MOLOD_YABLOCHKI}
import play.api.mvc.{ActionFilter, AnyContent, Result, Results}
import model.hero.HeroTemplate._
import model.item.{Product, Seed, Thing, ThingCase}
import model.item.Product.{EGG, KOLOBOK}
import model.item.Seed.Solnechnik
import play.twirl.api.Html
import views.html.tutorial.IvanSpeech
import components.Results.GamePage
import TutorialController._

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 24.03.2016.
 */
class TutorialController extends BaseController {

  def tutorial(step: Int) = GameAction { implicit ctx =>
    if (getGameUser.isTutorialCompleted) Redirect(routes.Application.city())
    else step match {
      case 0 => sexScreen()
      case 1 => firstScreen(false)
      case 2 => acquaintance()
      case 3 => missions(current = 0)
      case 4 => mission(idx = 0)
      case 5 => pickedMission(mission = 0)
      case 6 => missions(current = 1)
      case 7 => mission(idx = 1)
      case 8 => pickedMission(mission = 1)
      case 9 => missions(current = 2)
      case 10 => mission(idx = 2)
      case 11 => pickedMission(mission = 2)
      case 12 => completedList(missionToComplete = 0)
      case 13 => completedList(missionToComplete = 1)
      case 14 => completedList(missionToComplete = 2)
      case 15 => firstScreen(true)
      case 16 => secondTask(false)
      case 17 => village(false)
      case 18 => village(true)
      case 19 => pechka(false, false)
      case 20 => pechka(true, false)
      case 21 => pechka(true, true)
      case 22 => pechka(true, false, true)
      case 23 => secondTask(true)
      case 24 => thirdTask(false)
      case 25 => village(true, true)
      case 26 => nasest(false)
      case 27 => nasest(true)
      case 28 => nasest(true)
      case 29 => thirdTask(true)
      case _ => Redirect(routes.Application.city())
    }
  }

  private def sexScreen()(implicit ctx: GameContext[AnyContent]) = {
    if (ctx.avatar.nick.nonEmpty) {
      getGameUser.incrementTutorial()
      Redirect(routes.TutorialController.tutorial(getGameUser.tutorialStep))
    } else Ok(views.html.tutorial.SexChoice())
  }

  private def firstScreen(completed: Boolean)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.FirstScreen(completed)(ctx))
  }

  import TutorialController.StepFilter

  def sexAction(step: Int, sex: Char) = (GameAction andThen StepFilter(0)) {implicit ctx =>
    val s = Array('m', 'f').find(_ == sex).getOrElse('m')
    getAvatar.sex(s)
    getGameUser.incrementTutorial()
    Redirect(routes.TutorialController.tutorial(getGameUser.tutorialStep))
  }

  // Стрелка "поручения героям"
  def firstAction(step: Int) = (GameAction andThen StepFilter(1)) {implicit ctx =>
    getGameUser.incrementTutorial()
    acquaintance()
  }

  private def acquaintance()(implicit ctx: GameContext[AnyContent]) = {
    addFeedback(views.html.tutorial.AcquaintanceFeedback())
    Ok(views.html.tutorial.Acquaintance())
  }

  def acquaintanceAction(step: Int) = (GameAction andThen StepFilter(2)) {implicit ctx =>
    getGameUser.incrementTutorial()
    missions(0)
  }

  private def missions(current: Int)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.Missions(current))
  }

  // Стрелка "выполнять поручение"
  def goMission(step: Int, idx: Int) = (GameAction andThen StepFilter(3, 6, 9)) {implicit ctx =>
    getGameUser.incrementTutorial()
    mission(idx)
  }

  private def mission(idx: Int)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.Mission(idx))
  }

  // Стрелка "вставить героя"
  def pickHero(step: Int, mission: Int) = (GameAction andThen StepFilter(4, 7, 10)) {implicit ctx =>
    getGameUser.incrementTutorial()
    pickedMission(mission)
  }

  private def pickedMission(mission: Int)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.PickedMission(mission))
  }

  // Стрелка "начать выполнение"
  def startMission(step: Int) = (GameAction andThen StepFilter(5, 8, 11)) {implicit ctx =>
    val user = getGameUser
    user.incrementTutorial()
    Redirect(routes.TutorialController.tutorial(user.tutorialStep))
  }

  private def completedList(missionToComplete: Int)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.CompletedList(missionToComplete))
  }

  // Стрелка "завершить"
  def finishMission(step: Int, mission: Int) = (GameAction andThen StepFilter(12, 13, 14)) {implicit ctx =>
    import TutorialController.expForMission
    val user = getGameUser
    val tutorial = user.tutorial
    user.incrementTutorial()
    addFeedback(Html("Успех в поручении!"))
    addFeedback(Reward().addExp(expForMission).addIngredient(TutorialController.goods(mission), 1))
    tutorial.exp(tutorial.exp + 30)
    Redirect(routes.TutorialController.tutorial(user.tutorialStep))
  }

  // стрелка "помочь царю"
  def finishTask(step: Int) = (GameAction andThen StepFilter(15)) {implicit ctx =>
    val user = getGameUser
    user.incrementTutorial()
    val tutorial = user.tutorial
    val reward = Reward().addExp(firstTaskExp)
    reward += tutorial.addRuby(firstTaskRuby)
    reward += tutorial.addCoin(firstTaskMoney)
    reward += tutorial.addDust(firstTaskDust)
    reward += tutorial.incLevel()
    addFeedback(reward)
    tutorial.exp(30)
    secondTask(false)
  }

  private def secondTask(finished: Boolean)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.SecondTask(finished))
  }

  // стрелка "мои владения 1"
  def goDomain(step: Int) = (GameAction andThen StepFilter(16)) {implicit ctx =>
    getGameUser.incrementTutorial()
    village(false)
  }

  private def village(bareGround: Boolean, showNasest: Boolean = false)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.Village(bareGround, showNasest))
  }

  // собрать солнечник
  def harvestSeed(step: Int) = (GameAction andThen StepFilter(17)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    user.incrementTutorial()
    tutorial.exp(50)
    import TutorialController.expForSolnechnik
    addFeedback(Reward().addIngredient(TutorialController.startingSeed, 3).addExp(expForSolnechnik))
    village(true)
  }

  // стрелка "волшебная печка"
  def goPechka(step: Int) = (GameAction andThen StepFilter(18)) {implicit ctx =>
    getGameUser.incrementTutorial()
    pechka(false, false)
  }

  private def pechka(fixed: Boolean, enqueued: Boolean, postProduction: Boolean = false)(implicit ctx: GameContext[AnyContent]) = {
    import TutorialController.good2
    val tutorial = ctx.user.tutorial
    tutorial.kolobokEnqueueTime.filter(_ + TutorialController.good2.duration < now).map { _ =>
      getGameUser.incrementTutorial()
      getGameUser.tutorial.kolobokEnqueueTime(None)
      addFeedback(Reward().addIngredient(good2, 1).addExp(good2.farmingExp))
      tutorial.exp(80)
      Ok(views.html.tutorial.Pechka(fixed, enqueued, true))
    } getOrElse Ok(views.html.tutorial.Pechka(fixed, enqueued, postProduction))
  }

  // отремонтировать печку
  def fixPechka(step: Int) = (GameAction andThen StepFilter(19)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    getGameUser.incrementTutorial()
    tutorial.addCoin(-TutorialController.repairPechkaPrice)
    pechka(true, false)
  }

  // колобка в очередь
  def enqueue(step: Int) = (GameAction andThen StepFilter(20)) {implicit ctx =>
    getGameUser.incrementTutorial()
    ctx.user.tutorial.kolobokEnqueueTime(Some(now))
    pechka(true, true)
  }

  // ускорить колобка
  def enforce(step: Int) = (GameAction andThen StepFilter(21)) {implicit ctx =>
    val tutorial = ctx.user.tutorial
    tutorial.kolobokEnqueueTime.filter(_ + TutorialController.good2.duration > now).map { _ =>
      addFeedback(views.html.tutorial.KolobokEnforceFeedback())
      Ok(views.html.tutorial.Pechka(true, true, false))
    } getOrElse pechka(true, true)
  }

  // подтвердить ускорение колобка
  def enforceAgree(step: Int) = (GameAction andThen StepFilter(21)) {implicit ctx =>
    import controllers.TutorialController.good2
    val user = getGameUser
    val tutorial = user.tutorial
    user.incrementTutorial()
    tutorial.exp(80)
    tutorial.addDust(-5)
    addFeedback(Reward().addIngredient(TutorialController.good2, 1).addExp(good2.farmingExp))
    pechka(true, false, true)
  }

  // стрелка в задания (отдать колобка)
  def goBringKolobok(step: Int) = (GameAction andThen StepFilter(22)) {implicit ctx =>
    getGameUser.incrementTutorial()
    secondTask(true)
  }

  // отдать колобка
  def finishSecondTask(step: Int) = (GameAction andThen StepFilter(23)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    user.incrementTutorial()
    tutorial.exp(60)
    val reward = Reward().setLevel(3).addExp(secondTaskExp)
    reward += tutorial.addRuby(secondTaskRuby)
    reward += tutorial.addCoin(secondTaskMoney)
    reward += tutorial.addDust(secondTaskDust)
    reward += tutorial.incLevel()
    addFeedback(reward)
    thirdTask(false)
  }

  private def thirdTask(finished: Boolean)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.ThirdTask(finished))
  }

  // стрелка "мои владения 2"
  def goDomain2(step: Int) = (GameAction andThen StepFilter(24)) {implicit ctx =>
    getGameUser.incrementTutorial()
    village(true, true)
  }

  // стрелка "волшебный насест"
  def goNasest(step: Int) = (GameAction andThen StepFilter(25)) {implicit ctx =>
    getGameUser.incrementTutorial()
    nasest(false)
  }

  private def nasest(fixed: Boolean)(implicit ctx: GameContext[AnyContent]) = {
    Ok(views.html.tutorial.Nasest(fixed))
  }

  // отремонтировать насест
  def fixNasest(step: Int) = (GameAction andThen StepFilter(26)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    user.incrementTutorial()
    tutorial.addCoin(-repairNasestPrice)
    nasest(true)
  }

  // купить курицу
  def buyHen(step: Int) = (GameAction andThen StepFilter(27)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    if (tutorial.hens < nasestLength) {
      tutorial.hens(tutorial.hens + 1)
      tutorial.addCoin(-10)
    }
    Redirect(routes.TutorialController.tutorial(user.tutorialStep))
  }

  // собрать яйку
  def farmHen(step: Int) = (GameAction andThen StepFilter(27)) {implicit ctx =>
    val user = getGameUser
    val tutorial = user.tutorial
    import TutorialController.nasestLength
    if (tutorial.eggs < nasestLength) {
      tutorial.eggs(tutorial.eggs + 1)
      addFeedback(Reward().addIngredient(TutorialController.good3, 1))
      if (tutorial.eggs == nasestLength) user.incrementTutorial()
    }
    Redirect(routes.TutorialController.tutorial(user.tutorialStep))
  }

  // идти отдавать яйца
  def goBringEggs(step: Int) = (GameAction andThen StepFilter(28)) {implicit ctx =>
    getGameUser.incrementTutorial()
    thirdTask(true)
  }

  // отдать яйца - закончить туториал
  def finishThirdTask(step: Int) = (GameAction andThen StepFilter(29)) {implicit ctx =>
    val user = getGameUser

    val reward = Reward().setLevel(4).addExp(thirdTaskExp).
      addRuby(thirdTaskRuby).addCoin(thirdTaskMoney).addDreamDust(thirdTaskDust)
    addFeedback(reward)

    user.finishTutorial()
    Redirect(routes.MissionController.missionList(0, true))
  }
}

object TutorialController {

  case object ZELIE_MOLODECKOY_SILY extends ThingCase(8, "зелье молодецкой силы", "misc3/t_8_128.png", 100)
  case object STIHI_O_LUBVI extends ThingCase(9, "стихи о любви", "misc3/t_9_128.png", 100)

  val goods: List[Thing] = STIHI_O_LUBVI :: ZELIE_MOLODECKOY_SILY :: MOLOD_YABLOCHKI :: Nil

  val heroes: List[HeroTemplate] = Kot_uchenyj :: Rusalka :: Leshij :: Nil

  val titles: List[String] =
    """
      |О как много разных стихов о любви хранятся в свитках библиотеки!
      | Только истинный Мудрец способен отыскать в библиотеке самые искренние и нежные стихи о любви.
    """.stripMargin ::
    """
      |Ходят слухи, что зелье силы молодецкой изготавливает суровый пират.
      | Суровый пират никогда и ни с кем не делился зельем, однако каменное сердце пирата тает при виде красоты
      | неземной! И суровый пират готов исполнить просьбу настоящей Красавицы.
    """.stripMargin ::
    """
      |Яблочки молодильные растут в саду Кощея, охраняют же тот сад злые псы да черны вороны.
      | Выкрасть яблочко молодильное может только обладатель Незаметности!
    """.stripMargin :: Nil

  val good2: Product = KOLOBOK

  val good3: Product = EGG

  val startingSeed: Seed = Solnechnik

  val nasestLength = 3

  val firstTaskRuby = 25
  val firstTaskMoney = 200
  val firstTaskDust = 10
  val firstTaskExp = 150

  val firstTaskReputation = ReputationModel.SALTAN
  val firstTaskReputationValue = 150

  val expForMission = 20
  val expForSolnechnik = 9
  val repairPechkaPrice = 200
  val repairNasestPrice = 100

  val secondTaskRuby = 25
  val secondTaskMoney = 170
  val secondTaskDust = 20
  val secondTaskExp = 170

  val thirdTaskRuby = 25
  val thirdTaskMoney = 100
  val thirdTaskDust = 25
  val thirdTaskExp = 150

  def StepFilter(allowed: Int*) = new ActionFilter[GameContext] {
    override protected def filter[A](request: GameContext[A]): Future[Option[Result]] = Future.successful {
      val user = request.gameUser
      if (allowed.contains(user.tutorialStep)) None
      else Some(Results.Redirect(routes.TutorialController.tutorial(user.tutorialStep)))
    }
  }
}
