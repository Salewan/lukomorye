package controllers

import bean.{GameUser, Notification, User}
import model.{NotificationType, Reward, TransactionGenerator}
import components.Results.NotFoundPage

/**
  * Created by Salewan on 25.05.2017.
  */
class UserController extends BaseController {

  def closeNotification(id: Int) = GameAction {implicit ctx =>
    getGameUser.notifications.toSeq.find(_.id == id).foreach {notification =>
      getGameUser.notifications -= notification
    }
    Refresh
  }

  def sendTestNotification(userId: Int) = GameAction {implicit ctx =>

    session.get(classOf[GameUser], userId).map {foreigner =>
      if (getUser.isAdmin) {
        foreigner.notifications += Notification(foreigner, NotificationType.SimpleInfo, "Тестовая нотификация")
      }
      Redirect(routes.ProfileController.profilePage(Some(userId)))
    } getOrElse NotFoundPage
  }

  def getVictoryDayGift = GameAction {implicit ctx =>
    if (ctx.victoryFeedbackShown) {
      getUser.victoryGiftGot(Some(now))
      val reward = Reward()
      reward += getUser.money.addDreamDust(1000, TransactionGenerator.VIC_DAY_GIFT_TX.get)
      reward += getUser.money.addRubies(50, TransactionGenerator.VIC_DAY_GIFT_TX.get)
      addFeedback(reward)
    }
    Refresh
  }

  def clones(userId: Int) = GameAction {implicit ctx =>
    session.get(classOf[User], userId).filter(_ => getUser.isAdminOrSupporter).map {user =>
      val clones = userDAO.getClones(user)
      Ok(views.html.user.MultisPage(userId, clones))
    } getOrElse NotFoundPage
  }
}
