package controllers

import components.{DefaultWidth, WindowWidth}
import play.api.data.Form
import play.api.data.Forms._
import play.twirl.api.Html

/**
  * @author Sergey Lebedev (salewan@gmail.com) 21.10.2016.
  */
class WidthController extends BaseController {

  /**
    * Страница с настройками
    */
  def widthPage() = GameAction {implicit ctx =>
    Ok(views.html.user.WidthPage(widthForm))
  }

  /**
    * Задать значение для ширины эрана по умолчанию
    */
  def widthDefault() = GameAction {implicit ctx =>
    ctx.settings.windowWidth(DefaultWidth)
    Redirect(routes.SettingsController.settings())
  }

  /**
    * Задать ширину 320px
    */
  def width320() = GameAction {implicit ctx =>
    ctx.settings.windowWidth(WindowWidth(320))
    Redirect(routes.SettingsController.settings())
  }

  /**
    * Задать ширину 240px
    */
  def width240() = GameAction {implicit ctx =>
    ctx.settings.windowWidth(WindowWidth(240))
    Redirect(routes.SettingsController.settings())
  }

  // Form
  val widthForm = Form(single("width", number))

  /**
    * Произвольное значение для ширины экрана
    */
  def widthCustom() = GameAction {implicit ctx =>
    widthForm.bindFromRequest().fold(

      formWithErrors => {
        addFeedback(Html("Недопустимый аргумент"))
        Redirect(routes.WidthController.widthPage())
      },

      width => {
        ctx.settings.windowWidth(WindowWidth(width))
        Redirect(routes.SettingsController.settings())
      }
    )
  }


}
