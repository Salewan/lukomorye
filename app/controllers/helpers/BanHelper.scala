package controllers.helpers

import bean.User
import components.context.GameContext
import controllers.ContextMethods
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.AnyContent

import scala.collection.mutable
import util.Implicits._

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.02.2017.
  */
trait BanHelper {
  this: ContextMethods =>

  def makeBanForm(implicit ctx: GameContext[AnyContent]) = Form (
    tuple("time" -> number, "reason" -> number) verifying Constraint[(Int, Int)]("") { case (time, reason) =>
      if (getBanTimes(getUser).toMap.get(time).isEmpty) Invalid("error.ban.BlockingTime")
      else if (reason < 0 || reason >= reasons.size) Invalid("error.ban.Reason")
      else Valid
    }
  )

  def getBanTimes(user: User): Seq[(Int, Long)] = {
    val seq = mutable.Buffer[(Int, Long)]()
    seq += 0 -> (1 hour)
    seq += 1 -> (1 day)

    if (user.isAdminOrSupporter) {
      seq += 2 -> (7 days)
      seq += 3 -> (3650 days)
    }
    seq
  }

  val banTimesName = BanHelper.banTimesName

  val reasonsOrder = BanHelper.reasonsOrder

  val reasons = BanHelper.reasons

  val orderedReasons = BanHelper.orderedReasons
}

object BanHelper {

  val banTimesName = Seq("1 час", "1 день", "7 дней", "Навсегда")

  val reasonsOrder = Seq(23, 22, 5, 9, 18, 16, 12, 21, 4, 0, 11, 1, 14, 2, 3, 6, 7, 8, 10, 13, 15, 17, 19, 20)

  val reasons = Seq("Мошенник",
    "Реклама сторонних ресурсов",
    "Ругань на форуме",
    "Ругань в чате",
    "Оскорбление игроков",
    "Попрошайничество",
    "Злоупотребление рассылкой",
    "Запрещенный ник",
    "Много мультов",
    "Мат",
    "Спамер",
    "Здесь дети играют",
    "Прилетело НЛО и забанило",
    "До выяснения",
    "Нарушение правил форума",
    "Обсуждение администрации",
    "По просьбе",
    "Флуд",
    """Продажа\покупка аккаунта""",
    "Нарушение Соглашения",
    "Провокация",
    "Оффтоп",
    """Продажа\обмен игровых объектов""",
    "Нарушение правил сайта").zipWithIndex.map{case (a, b) => b -> a}.toMap

  val orderedReasons = reasonsOrder.map(i => i -> reasons(i))
}
