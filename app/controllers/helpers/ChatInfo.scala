package controllers.helpers

import bean.{Avatar, ChatMessage, Clan, GameUser}
import components.PagingModel
import components.context.GameContext
import controllers.ContextMethods
import play.api.mvc.AnyContent
import util.{MessagesUtil, SmilesUtil, TimeUtil}

class ChatInfo(val clan: Clan, val pg: PagingModel[ChatMessage])(implicit ctx: GameContext[AnyContent]) extends ContextMethods{

  def clanId: Int = clan.id

  private def messageText(message: ChatMessage): String = {
    var messageText = message.text
    messageText = MessagesUtil.processForumMessage(messageText, true, message.author.isAdminOrSupporterOrModerator)
    SmilesUtil.getInstance().processSmiles(messageText)
  }

  def messages: Iterable[MessageInfo] = pg.page.map {chatMessage =>
    val author = chatMessage.author.avatar
    val time = TimeUtil.formatTime(now - chatMessage.creationTime)
    val text = messageText(chatMessage)
    MessageInfo(author, time, text, chatMessage.canDelete(ctx.gameUser)(ctx), chatMessage.id)
  }

  def canUseBBCodes(gameUser: GameUser): Boolean = true
}

case class MessageInfo(author: Avatar, time: String, text: String, canDelete: Boolean, id: Int)
