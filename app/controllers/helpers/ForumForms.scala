package controllers.helpers

import bean.Clan
import bean.messages.forum.{Folder, FolderType, Topic}
import components.context.GameContext
import controllers.ContextMethods
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid}
import play.api.mvc.AnyContent

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 08.02.2017.
  */
trait ForumForms extends BanHelper {
  this: ContextMethods =>

  /**
    * Форма при создании/редактировании Folder
    */
  protected def makeFolderForm(folder: Folder, clan: Option[Clan])(implicit ctx: GameContext[AnyContent]) = Form (
    tuple(
      "name" -> text,
      "option" -> text
    ) verifying Constraint[(String, String)]("") { case (name, option) =>
      val folderName = name.trim
      if (folderName.isEmpty) Invalid("error.forum.FolderNameRequired")
      else if (name.trim.length > 32) Invalid("error.forum.FolderNameTooLong")
      else if (clan.nonEmpty && !FolderType.clanCodes.contains(option)) Invalid("error.forum.ClanFolderTypeOnly")
      else if (clan.isEmpty && !FolderType.commonCodes.contains(option)) Invalid("error.forum.CommonFolderTypeOnly")
      else Valid
    }
  )

  /**
    * Форма при создании топика
    */
  protected def makeTopicForm(implicit ctx: GameContext[AnyContent]) = Form (
    tuple(
      "subject" -> text, "body" -> text
    ) verifying Constraint[(String, String)]("") { case (s, b) =>
      val subject = s.trim
      val body = b.trim
      if (subject.isEmpty) Invalid("error.forum.SubjectRequired")
      else if (subject.length > 50) Invalid("error.forum.SubjectTooLong")
      else if (body.isEmpty) Invalid("error.forum.BodyRequired")
      else if (ctx.user.isUser && body.length > 10000) Invalid("error.forum.BodyTooLong")
      else Valid
    }
  )

  /**
    * Форма "оставить комментарий" в топике
    */
  protected def makeTopicWriteForm(topic: Topic)(implicit ctx: GameContext[AnyContent]) = Form {
    single("text" -> text) verifying Constraint[String]("") { text =>
      val user = getUser
      val gameUser = getGameUser

      if (user.isBanned) Invalid("forum.error.YouBanned")
      else if (!gameUser.isWriteAllowed) Invalid("error.forum.WriteNotAllowed")
      else if (topic.folder.clan.exists(_.isIgnored(getAvatar))) Invalid("error.form.YouInClanBlackList")
      else if (topic.closed) Invalid("error.forum.TopicClosed")
      else if (text.trim.isEmpty) Invalid("error.forum.EmptyComment")
      else Valid
    }
  }

  /**
    * Форма при перемещении топика
    */
  protected def moveForm(topic: Topic)(implicit ctx: GameContext[AnyContent]) = Form (
    single("forum" -> number) verifying Constraint[Int]("") { newFolderId =>
      session.get(classOf[Folder], newFolderId) map {testFolder =>
        val oldFolder = topic.folder

        if (oldFolder.clan.isEmpty && testFolder.clan.nonEmpty)
          Invalid("error.forum.move.WrongFolderType")
        else if (oldFolder.clan.nonEmpty && !oldFolder.clan.get.folders.contains(testFolder))
          Invalid("error.forum.move.WrongGuildFolder")
        else Valid
      } getOrElse Invalid("error.forum.move.UnknownForum")
    }
  )
}
