package controllers.helpers

import bean.Clan
import bean.messages.forum.{Comment, Folder, FolderType, Topic}
import components.Results._
import components.context.GameContext
import controllers.{ForumController, routes}
import play.api.mvc.{AnyContent, Result}

/**
  * @author Sergey Lebedev (salewan@gmail.com) 08.02.2017.
  */
trait ForumRestrictions {
  this: ForumController =>

  /**
    * Проверка прав к разделу форума (Folder)
    */
  protected def checkFolderPageAccess(folderId: Int)
                                     (implicit ctx: GameContext[AnyContent]): (Option[Folder], Option[Result]) = {

    session.get(classOf[Folder], folderId) map { folder =>
      val errorResult =
        if (folder.deleted) Some(Redirect(routes.ForumController.forumMain()))
        else if (folder.isAdmin && !getUser.isAdminOrSupporterOrModerator) Some(NotFoundPage)
        else if (folder.clan.nonEmpty && !getGameUser.canViewClanFolder(folder)) Some(AccessDeniedPage)
        else None
      (Some(folder), errorResult)
    } getOrElse (None, None)

  }


  private def getFolder(folderId: Option[Int], clanId: Option[Int])
                       (implicit ctx: GameContext[AnyContent]): Folder = {

    folderId flatMap (session.get(classOf[Folder], _)) getOrElse {
      val folder = new Folder
      clanId flatMap (session.get(classOf[Clan], _)) foreach (clan => folder.clan(clan).folderType(FolderType.MEMBERS))
      folder
    }
  }

  /**
    * Проверка прав на создание/редактирование раздела (Folder)
    */
  protected def checkFolderFormAccess(folderId: Option[Int], clanId: Option[Int])
                                     (implicit ctx: GameContext[AnyContent]): (Folder, Option[Clan], Option[Result]) = {

    val gameUser = getGameUser
    val folder = getFolder(folderId, clanId)
    val clan = clanId flatMap (session.get(classOf[Clan], _))
    val result = if (folder.isPersistent && !gameUser.canEditFolder(folder)) Some(AccessDeniedPage)
    else if (!gameUser.canCreateFolder(folder, clan)) Some(AccessDeniedPage)
    else None
    (folder, clan, result)
  }

  /**
    * Проверка прав на странице топика
    */
  protected def checkTopicAccess(topicId: Int)
                                (implicit ctx: GameContext[AnyContent]): (Option[Topic], Option[Result]) = {

    val forumMainRedirect = (None, Some(Redirect(routes.ForumController.forumMain())))

    session.get(classOf[Topic], topicId) map { topic =>
      if (topic.deleted) forumMainRedirect
      else {
        val folder = topic.folder
        if (folder.isAdmin && !getUser.isAdminOrSupporterOrModerator) (None, Some(NotFoundPage))
        else if (folder.clan.nonEmpty && !getGameUser.canViewClanFolder(folder)) (None, Some(AccessDeniedPage))
        else (Some(topic), None)
      }
    } getOrElse forumMainRedirect
  }

  /**
    * Проверка прав на перемещение топика
    */
  protected def checkMoveAccess(topicId: Int)
                               (implicit ctx: GameContext[AnyContent]): (Option[Topic], Option[Result]) = {

    session.get(classOf[Topic], topicId) map { topic =>
      if ((topic.folder.clan.isEmpty && !getUser.isAdminOrSupporterOrModerator) ||
        (topic.folder.clan.nonEmpty && !getGameUser.canMove(topic))) (None, Some(AccessDeniedPage))
      else (Some(topic), None)
    } getOrElse (None, Some(Redirect(routes.ForumController.forumMain())))
  }

  /**
    * Проверка прав на создание топика в разделе форума
    */
  protected def checkTopicCreation(folderId: Int)
                                  (implicit ctx: GameContext[AnyContent]): (Option[Folder], Option[Result]) = {

    session.get(classOf[Folder], folderId).map { folder =>
      if (!getGameUser.isTopicCreationAllowed(folder)) (None, Some(Redirect(routes.ForumController.forumMain())))
      else (Some(folder), None)
    } getOrElse (None, Some(NotFoundPage))
  }

  /**
    * Проверка прав на редактирование топика
    */
  protected def checkTopicEditAccess(topicId: Int)
                                    (implicit ctx: GameContext[AnyContent]): (Option[Topic], Option[Result]) = {

    session.get(classOf[Topic], topicId) map { topic =>
      if (!getGameUser.canEdit(topic)) (None, Some(AccessDeniedPage))
      else (Some(topic), None)
    } getOrElse (None, Some(NotFoundPage))
  }

  /**
    * Проверка прав на бан коментария
    */
  protected def checkBanCommentAccess(commentId: Int)
                                     (implicit ctx: GameContext[AnyContent]): (Option[Comment], Option[Result]) = {
    if (!getUser.canBan) (None, Some(AccessDeniedPage))
    else session.get(classOf[Comment], commentId).map(c => (Some(c), None)) getOrElse (None, Some(NotFoundPage))
  }
}
