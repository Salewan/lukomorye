package controllers.helpers

import bean.{Avatar, User}
import bean.messages.forum.{Comment, Topic}
import bean.state.ClanRole
import com.google.common.collect.Lists
import components.PagingModel
import components.context.GameContext
import controllers.ContextMethods
import dao.MessageDAO
import play.api.mvc.AnyContent
import util.{MessagesUtil, SmilesUtil, TimeUtil}

/**
  * @author Sergey Lebedev (salewan@gmail.com) 08.02.2017.
  */
class TopicInfo(val topic: Topic, val pg: PagingModel[Int])
               (implicit ctx: GameContext[AnyContent]) extends ContextMethods {

  def author: Avatar = topic.author.avatar

  def authorClass: String = {
    val a = topic.author
    if (a.isBanned) "banned" else if (a.isModerator) "moderator" else if (a.isAdminOrSupporter) "admin" else ""
  }

  def time: String = TimeUtil.formatTime(now - topic.creationTime)

  def body: String = {
    var text = topic.body
    text = MessagesUtil.processForumMessage(text, canUseBBCodes(topic.author, topic), canUseExternalLinks(topic.author))
    text = SmilesUtil.getInstance().processSmiles(text, Lists.newArrayList())
    text
  }

  def bodyClass: String = author.textCssClass

  private def commentPopulated(comment: Comment): Unit = {
    val mark = new MessageDAO(session).getTopicMark(getUser, comment.topic, persist = true)
    if (comment.creationTime > mark.lastViewedCommentTime)
      mark.lastViewedCommentTime(comment.creationTime)
  }

  private def canUseExternalLinks(author: User): Boolean = {
    author.isAdminOrSupporterOrModerator
  }

  def canUseBBCodes(author: User, topic: Topic): Boolean = {
    val user = author
    val gameUser = user.gameUser
    topic.folder.clan.map {messageClan =>
      val clanRole = gameUser.clanRole
      user.isAdminOrSupporterOrModerator ||
        gameUser.clan.exists(c => c.id == messageClan.id &&
          (clanRole == ClanRole.LEADER || clanRole == ClanRole.preLeader))
    }.getOrElse(user.isAdminOrSupporterOrModerator)
  }

  private def commentText(comment: Comment): String = {
    if (comment.deleted) "Комментарий удалён."
    else {
      var text = comment.text
      text = MessagesUtil.processForumMessage(text, canUseBBCodes(comment.author, comment.topic), canUseExternalLinks(comment.author))
      text = SmilesUtil.getInstance().processSmiles(text, Lists.newArrayList())
      text
    }
  }

  private def populateComment(commentId: Int): CommentInfo = {
    val comment = session.get(classOf[Comment], commentId).get
    val text = commentText(comment)
    val author = comment.author.avatar
    val time = TimeUtil.formatTimeSince(comment.creationTime)
    val info = CommentInfo(text, author, time, comment)
    commentPopulated(comment)
    info
  }

  def comments: Iterable[CommentInfo] = for(id <- pg.page) yield populateComment(id)

  def currentPage: Int = pg.currentPage
}

case class CommentInfo(text: String, author: Avatar, time: String, comment: Comment)
