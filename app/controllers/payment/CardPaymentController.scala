package controllers.payment

import javax.inject.Inject

import controllers.BaseController
import play.api.mvc.Call

import scala.concurrent.ExecutionContext

class CardPaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

  def cardPaymentPage = GameAction.async {implicit ctx =>
    xsolla.fetchToken(userId, Some(26)).map {response =>
      val token = (response.json \ "token").as[String]
      val redirectUrl = XsollaKey(ctx.domain).shopLink + token
      Redirect(Call("GET", redirectUrl))
    }
  }
}
