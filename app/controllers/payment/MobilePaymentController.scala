package controllers.payment

import javax.inject.Inject

import controllers.BaseController
import play.api.mvc.Call

import scala.concurrent.ExecutionContext

class MobilePaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

  def mobilePaymentPage(method: Int) = GameAction.async {implicit ctx =>
    val m = MobilePaymentController.methods.find(t => t._1 == method).map(_._1).getOrElse(MobilePaymentController.defaultMethod)
    xsolla.fetchToken(userId, Some(m), getProfile.email, getUser.phoneNumber).map {response =>
      val token = (response.json \ "token").as[String]
      val redirectUrl = XsollaKey(ctx.domain).shopLink + token
      Redirect(Call("GET", redirectUrl))
    }
  }
}

object MobilePaymentController {
  val defaultMethod = 1738
  val methods: List[(Int, String)] = 255 -> "МТС" :: 253 -> "Билайн" :: 254 -> "Мегафон" :: 1738 -> "Мобильные платежи" :: Nil
}
