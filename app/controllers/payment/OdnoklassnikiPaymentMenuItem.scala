package controllers.payment

import play.api.mvc.Call

case class OdnoklassnikiPaymentMenuItem(call: Call, rub: Int, oks: Int)
