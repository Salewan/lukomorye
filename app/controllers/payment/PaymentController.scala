package controllers.payment

import javax.inject.Inject

import components.HibernateSessionAware
import components.actions.Actions.LevelRequiredAction
import controllers.BaseController
import dao.ActionDAO
import play.api.mvc.Call
import views.html.action._
import model.action.ActionType._
import play.api.Logger
import play.twirl.api.Html

import scala.concurrent.ExecutionContext

class PaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

}

object PaymentController {
  val ePaymentOptions: List[PaymentMenuItem] =
    PaymentMenuItem(controllers.payment.routes.QiwiPaymentController.qiwiWallet(), "QIWI кошелёк") ::
    PaymentMenuItem(controllers.payment.routes.YandexPaymentController.yandexPaymentPage(), "Яндекс деньги") ::
    PaymentMenuItem(controllers.payment.routes.WebmoneyPaymentController.webmoneyPaymentPage(), "Webmoney") :: Nil

  val termPaymentOptions: List[PaymentMenuItem] =
    PaymentMenuItem(controllers.payment.routes.CardPaymentController.cardPaymentPage(), "Visa / Mastercard") ::
    Nil

  val menuItems: List[PaymentMenuItem] = MobilePaymentController.methods.map{ case (method, name) =>
    PaymentMenuItem(controllers.payment.routes.MobilePaymentController.mobilePaymentPage(method), name)
  } :::
    PaymentController.ePaymentOptions :::
    PaymentController.termPaymentOptions :::
    PaymentMenuItem(controllers.payment.routes.XSollaPaymentController.xsollaPayment, "Оплата другими способами") ::
    Nil

  def getActionPanel()(implicit h: HibernateSessionAware): Html = {
    ActionDAO(h.hibernateSession).getCurrentAction.map { action =>
      action.model match {
        case ACTION_MORE_30 => ActionPanel2()
        case ACTION_CLUB_HELP_50 => ActionPanel3()
        case ACTION_BUFF_DISCOUNT_50 => ActionPanel1()
        case ACTION_ADDITIONAL_COINS => ActionPanel4()
        case ACTION_EXPERIENCE_BUFFS => ActionPanel5()
        case ACTION_BANK_UPGRADE_DISCOUNT_50 => ActionPanel6()
        case ACTION_BUILDINGS_UPGRADE_DISCOUNT_50 => ActionPanel7()
        case _ => ActionPanel0()
      }
    } getOrElse ActionPanel0()
  }
}
