package controllers.payment

import play.api.mvc.Call

case class PaymentMenuItem(call: Call, name: String)
