package controllers.payment

import components.context.GameContext
import controllers.BaseController
import org.apache.commons.lang3.StringUtils
import play.api.data.Form
import play.api.data.Forms.{optional, single, text}
import play.api.data.validation.{Constraint, Invalid, Valid}

class PhoneController extends BaseController {

  def phoneForm()(implicit ctx: GameContext[_]) =
    Form(single("phone" -> optional(text)) verifying Constraint[Option[String]]("") {data =>
      if (data.isEmpty) Invalid("error.phone.empty")
      else if (!data.exists(p => p.length >= 10 && p.length <= 14)) Invalid("error.phone.length")
      else if (!data.exists(p => StringUtils.isNumeric(p))) Invalid("error.phone.number_only")
      else Valid
    } )

  def phonePage = GameAction {implicit ctx =>
    val user = getUser
    val myForm = phoneForm().fill(user.phoneNumber)
    Ok(views.html.payment.PhonePageView(phoneForm))
  }

  def updatePhone = GameAction {implicit ctx =>
    phoneForm().bindFromRequest().fold(
      formWithErrors => {
        Ok(views.html.payment.PhonePageView(formWithErrors))
      }, success => {
        getUser.phoneNumber(success)
        Redirect(controllers.payment.routes.MobilePaymentController.mobilePaymentPage(MobilePaymentController.defaultMethod)) // TODO fix method
      }
    )
  }
}
