package controllers.payment

import bean.GameUser
import components.HibernateSessionAware
import components.actions.Actions.FreeOnlyAction
import controllers.BaseController
import controllers.payment.TwoPayProcessor.{INCORRECT_INVOICE, INVALID_PARAMETER, INVALID_SIGNATURE, INVALID_USER, logger}
import model.TransactionGenerator
import org.apache.commons.codec.digest.DigestUtils
import org.slf4j
import play.api.Logger
import play.api.libs.json.{JsValue, Json}

class TwoPayProcessor extends BaseController {

  // not a part of web hook api
  private def findUser(userId: String)(implicit hsa: HibernateSessionAware): Option[GameUser] = {
    try {
      logger.info(s"#findUser seeking $userId")
      hsa.hibernateSession.get(classOf[GameUser], userId.toInt)
    } catch {
      case t: Throwable =>
        logger.warn(s"#findUser - cannot parse Int `$userId`", t)
        None
    }
  }

  private def userSearch[T](json: JsValue, onError: JsValue => T, onSuccess: JsValue => T)(implicit hsa: HibernateSessionAware): T = {
    val publicUserId = (json \ "user" \ "public_id").as[String]
    findUser(publicUserId) match {
      case Some(user) =>
        onSuccess(Json.parse(
          s"""{
             | "user": {
             |   "public_id": "$publicUserId",
             |   "id": "${user.id}",
             |   "name": "${user.avatar.visualNick}"
             | }
             |}""".stripMargin))
      case _ =>
        onError(INVALID_USER(s"Invalid user $publicUserId"))
    }
  }

  private def refund(json: JsValue)(implicit hsa: HibernateSessionAware): Option[JsValue] = {
    // val userId = (json \ "user" \ "id").as[String]
    val externalId = (json \ "transaction" \ "id").as[Int]
    billingDAO.getPayment(externalId.toString) match {
      case Some(pay) =>
        if (pay.canceled) {
          logger.warn(s"#refund - payment id=${pay.id} externalId=${pay.externalId} is already cancelled")
          Some(INCORRECT_INVOICE(s"Transaction ${pay.externalId} is already cancelled"))
        } else if (pay.owner.money.rubies < pay.amount) {
          logger.warn(s"#refund - user=${pay.owner.id} payment=${pay.id} user has not enough money")
          Some(INCORRECT_INVOICE(s"Cannot refund transaction ${pay.externalId} user has not enough rubies"))
        } else {
          billingDAO.cancelPayment(externalId.toString)
          pay.owner.money.addRubies(-pay.amount, TransactionGenerator.BILLING_ROLLBACK_TX.get)
          None
        }
      case _ =>
        logger.error(s"#refund Cannot find transaction#$externalId")
        Some(INCORRECT_INVOICE(s"Cannot find transaction#$externalId"))
    }
  }

  private def payment(json: JsValue)(implicit hsa: HibernateSessionAware): Option[JsValue] = {
    val userId = (json \ "user" \ "id").as[String]
    val quantity = (json \ "purchase" \ "virtual_currency" \ "quantity").as[Double]
    val amount = (json \ "purchase" \ "virtual_currency" \ "amount").as[Double]
    val externalId = (json \ "transaction" \ "id").as[Int]
    val date = (json \ "transaction" \ "payment_date").as[String]
    logger.info(s"#payment userId=$userId externalId=$externalId $quantity=$quantity date=$date")
    findUser(userId) match {
      case Some(user) =>
        val rubies = math.round(quantity).asInstanceOf[Int]
        billingDAO.successPayment(user, rubies, "2pay", Some(externalId.toString), None, amount)
        None
      case _ => Some(INVALID_PARAMETER(s"User $userId not found"))
    }
  }

  private def userValidation(json: JsValue)(implicit hsa: HibernateSessionAware): Option[JsValue] = {
    val userId = (json \ "user" \ "id").as[String]
    val optionalUser = findUser(userId)
    if (optionalUser.isEmpty) Some(INVALID_USER(s"Invalid user $userId"))
    else None
  }

  private def checkSignature(body: Option[String], sig: Option[String], secretKey: String): Boolean = {

    (for (b <- body; s <- sig) yield {
      val sha1 = DigestUtils.sha1Hex(b + secretKey)
      logger.info("SHA-1 = " + sha1 + " SIG = " + s)

      s contains sha1
    }).contains(true)
  }

  private def notified(key: XsollaKey) = FreeOnlyAction { implicit ctx =>
    val optionalJson = ctx.body.asJson
    val optionalSignature = ctx.headers.get(AUTHORIZATION)

    val authorized = checkSignature(optionalJson.map(_.toString()), optionalSignature, key.secretKey)

    if (!authorized) BadRequest(INVALID_SIGNATURE("Signature is invalid"))
    else {

      for (json <- optionalJson) yield {
        val command = (json \ "notification_type").as[String]
        logger.info(s"Notified with command=$command json=${json.toString()}")

        command match {
          case "user_validation" => userValidation(json).map(err => BadRequest(err)).getOrElse(NoContent)
          case "payment" => payment(json).map(err => BadRequest(err)).getOrElse(NoContent)
          case "refund" => refund(json).map(err => BadRequest(err)).getOrElse(NoContent)
          case "user_search" => userSearch(json, err => BadRequest(err), usr => Ok(usr))
          case _ =>
            logger.error(s"Not implemented command=$command")
            NotImplemented
        }
      }
    } getOrElse BadRequest
  }

  def gdcNotified = FreeOnlyAction.async {implicit ctx =>
    notified(XsollaKey.gdc).apply(ctx)
  }

  def lukNotified = FreeOnlyAction.async {implicit ctx =>
    notified(XsollaKey.luk).apply(ctx)
  }
}

object TwoPayProcessor {
  class XSollaError(code: String) {
    def apply(message: String): JsValue = Json.parse(s"""{"error":{"code":"$code","message":"$message"}}""")
  }
  case object INVALID_USER extends XSollaError("INVALID_USER") // неверный пользователь
  case object INVALID_PARAMETER extends XSollaError("INVALID_PARAMETER") // неверный параметр
  case object INVALID_SIGNATURE extends XSollaError("INVALID_SIGNATURE") // невалидная подпись
  case object INCORRECT_AMOUNT extends XSollaError("INCORRECT_AMOUNT") // некорректная сумма
  case object INCORRECT_INVOICE extends XSollaError("INCORRECT_INVOICE") // неверный заказ

  val logger: slf4j.Logger = Logger(classOf[TwoPayProcessor]).underlyingLogger
}
