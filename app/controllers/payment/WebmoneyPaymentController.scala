package controllers.payment

import javax.inject.Inject

import controllers.BaseController
import play.api.mvc.Call

import scala.concurrent.ExecutionContext

class WebmoneyPaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

  def webmoneyPaymentPage = GameAction.async {implicit ctx =>
    xsolla.fetchToken(userId, Some(6)).map {response =>
      val token = (response.json \ "token").as[String]
      val redirectUrl = XsollaKey(ctx.domain).shopLink + token
      Redirect(Call("GET", redirectUrl))
    }
  }
}
