package controllers.payment

import javax.inject.{Inject, Singleton}

import components.context.PlayContext
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.{WSAuthScheme, WSClient, WSRequest, WSResponse}

import scala.concurrent.{ExecutionContext, Future}

object Impl {
  case class Data(user: User, settings: Settings)
  case class Value(value: String)
  case class User(id: Value, email: Option[Value], phone: Option[Value] = None)
  case class Settings(project_id: Int, mode: Option[String], ui: Option[Ui] = None, payment_method: Option[Int] = None)
  case class Ui(version: String = "mobile", theme: String = "dark", components: Option[Components] = None, mobile: Mobile = Mobile(Header()))
  case class Mobile(header: Header)
  case class Header(close_button: Boolean = true)
  case class Components(virtual_currency: Option[VirtualCurrency])
  case class VirtualCurrency(custom_amount: Boolean)

  implicit val valueWrites = Json.writes[Value]
  implicit val userWrites = Json.writes[User]
  implicit val virtualCurrencyWrites = Json.writes[VirtualCurrency]
  implicit val componentsWrites = Json.writes[Components]
  implicit val headerWrites = Json.writes[Header]
  implicit val mobileWrites = Json.writes[Mobile]
  implicit val uiWrites = Json.writes[Ui]
  implicit val settingsWrites = Json.writes[Settings]
  implicit val dataWrites = Json.writes[Data]
}

@Singleton
class XSollaApi @Inject() (ws: WSClient)(implicit context: ExecutionContext) {
  import Impl._

  private def getRequest(implicit ctx: PlayContext[_]): WSRequest = {
    val key = XsollaKey(ctx.domain)
    ws.url(key.url).withHeaders(key.headers : _*).withAuth(key.username, key.password, WSAuthScheme.BASIC)
  }

  private def tokenRequestJson(
                        userId: Int,
                        paymentSystem: Option[Int] = None,
                        email: Option[String] = None,
                        phone: Option[String] = None)(implicit ctx: PlayContext[_]): JsValue = {
    val key = XsollaKey(ctx.domain)
    Json.toJson(Data(
      User(
        id = Value(userId.toString),
        email = email.map(Value.apply),
        phone = phone.map(Value.apply)
      ),
      Settings(
        key.projectId,
        key.mode,
        ui = Some(Ui(
          components = Some(Components(
            virtual_currency = Some(VirtualCurrency(
              custom_amount = true
            ))
          ))
        )),
        payment_method = paymentSystem
      )
    ))
  }

  def fetchToken(
                  userId: Int,
                  method: Option[Int] = None,
                  email: Option[String] = None,
                  phone: Option[String] = None)(implicit ctx: PlayContext[_]): Future[WSResponse] = {


    val data = tokenRequestJson(userId, method, email, phone)
    getRequest.post(data)
  }
}
