package controllers.payment

import javax.inject.Inject

import controllers.BaseController
import play.api.mvc.Call

import scala.concurrent.ExecutionContext

class XSollaPaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

  def xsollaPayment = GameAction.async {implicit ctx =>
    xsolla.fetchToken(userId, email = getProfile.email).map {response =>
      val token = (response.json \ "token").as[String]
      val redirectUrl = XsollaKey(ctx.domain).shopLink + token
      Redirect(Call("GET", redirectUrl))
    }
  }
}
