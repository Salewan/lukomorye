package controllers.payment

import org.slf4j
import play.api.Logger

trait XsollaKey {

  val SANDBOX: Boolean = false

  val merchantId: Int
  val projectId: Int
  val apiKey: String
  val secretKey: String

  lazy val url: String = s"https://api.xsolla.com/merchant/merchants/$merchantId/token"
  val shopLink: String =
    if (SANDBOX) "https://sandbox-secure.xsolla.com/paystation2/?access_token="
    else "https://secure.xsolla.com/paystation2/?access_token="

  lazy val username: String = merchantId.toString
  lazy val password: String = apiKey
  val mode: Option[String] = if (SANDBOX) Some("sandbox") else None
  val headers = Array("Accept" -> "application/json", "Content-Type" -> "application/json")
}

case object X_GDC extends XsollaKey {
  override val merchantId = 8286
  override val projectId = 26047
  override val apiKey = "A3SBc07n8eqxQe9o"
  override val secretKey = "YtXBLrXAvMDR5DVi"
}

case object X_LUK extends XsollaKey {
  override val merchantId = 13806
  override val projectId = 24507
  override val apiKey = "Th8OlYkMqpN2oI82"
  override val secretKey = "dvxWQMQHNKcbuWyK"
}

object XsollaKey {

  val logger: slf4j.Logger = Logger.underlyingLogger

  def apply(domain: String): XsollaKey = {
    if (domain.contains("lukomor.mobi")) X_LUK
    else if (domain.contains("skazka.mobi")) X_GDC
    else {
      logger.error(s"didn't find credentials for domain#$domain, returning by default")
      X_LUK
    }
  }

  def luk: X_LUK.type = X_LUK
  def gdc: X_GDC.type = X_GDC
}
