package controllers.payment

import javax.inject.Inject

import controllers.BaseController
import play.api.mvc.Call

import scala.concurrent.ExecutionContext

class YandexPaymentController @Inject() (xsolla: XSollaApi)(implicit context: ExecutionContext) extends BaseController {

  def yandexPaymentPage = GameAction.async {implicit ctx =>
    //or 2887
    xsolla.fetchToken(userId, Some(27)).map {response =>
      val token = (response.json \ "token").as[String]
      val redirectUrl = XsollaKey(ctx.domain).shopLink + token
      Redirect(Call("GET", redirectUrl))
    }
  }
}
