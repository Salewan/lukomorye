package dao

import bean.{Action, User}
import hibernate.HibernateSession
import model.action.ActionType

import scala.collection.JavaConverters.asScalaBufferConverter

/**
  * Created by Salewan on 30.06.2017.
  */
class ActionDAO(session: HibernateSession) {

  def getCurrentAction: Option[Action] = Option(session.
    createQuery("from Action where Queued = false order by CreationTime desc").
    setCacheable(true).
    setCacheRegion("ActionDAO.getCurrentAction").
    setMaxResults(1).
    uniqueResult).map(_.asInstanceOf[Action])

  def getQueuedActions: Seq[Action] = session.
    createQuery("from Action where Queued = true order by id").
    setCacheable(true).
    setCacheRegion("ActionDAO.getQueuedActions").
    list.asScala.map(_.asInstanceOf[Action])

  def getActionFromQueue: Option[Action] = getQueuedActions.headOption

  def cleanQueue(): Unit = session.createQuery("delete from Action where Queued = true").executeUpdate()

  def createAction(user: User, actionType: ActionType, queued: Boolean): Action = {
    val action = Action()
    action.author(user)
    action.creationTime(System.currentTimeMillis())
    action.typeId(actionType.typeId)
    action.name(actionType.name)
    action.queued(queued)
    session.persist(action)
    session.flush()
    action
  }
}

object ActionDAO {
  def apply(session: HibernateSession): ActionDAO = new ActionDAO(session)
}
