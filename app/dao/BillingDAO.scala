package dao

import bean.{GameUser, Payment}
import components.HibernateSessionAware
import hibernate.HibernateSession
import model.TransactionGenerator
import model.action.ActionType
import org.apache.commons.lang3.StringUtils
import BillingDAO.logger
import model.TransactionGenerator.CLAN_FUND_TX
import model.buff.BuffType
import play.api.Logger
import util.{MessagesUtil, RuGrammar}

import scala.collection.JavaConverters._

/**
  * Created by Salewan on 13.07.2017.
  */
class BillingDAO(session: HibernateSession) {

  implicit val hib: HibernateSessionAware = new HibernateSessionAware {
    override val hibernateSession: HibernateSession = session
  }

  def successPayment(user: GameUser, rubies: Int, typeStr: String, externalId: Option[String], phone: Option[String],
                     realAmount: Double): Payment =
    (for(extId <- externalId; paym <- getPayment(extId)) yield (extId, paym)).map {case (extId, existingPayment) =>
      logger.info(s"--- PAYMENT IGNORED rubies=$rubies user=${user.id} type=$typeStr externalId=$extId")
      existingPayment
    } getOrElse {
      var bonus = 0
      if (user.user.firstPayment) {
        bonus = math.ceil(rubies / 2.0).asInstanceOf[Int]
      }
      logger.info("***************************")
      logger.info(s"** PAYMENT SUCCESS rubies=$rubies user=${user.id} type=$typeStr externalId=$externalId phone=$phone realAmount=$realAmount")
      logger.info("***************************")


      val text = StringBuilder.newBuilder.append(s"Получен платёж на Ваш аккаунт: $rubies ${RuGrammar.rubiesBought(rubies)}.")

      if (bonus > 0 && user.user.firstPayment) {
        text.append("\nБонус за первую покупку рубинов +50%.")
        user.user.firstPayment(false)
      }

      ActionDAO(session).getCurrentAction.foreach {action =>
        if (action.model == ActionType.ACTION_BUFF_DISCOUNT_50) {
          if (rubies >= 100) {
            user.lastDiscountActionTime(Some(System.currentTimeMillis()))
            logger.info(s"Enabled 50% discount on buffs to user #${user.id}")
            text.append("\nДля Вас работают 50% скидки в магазине на время действия акции.")
          }

        } else if (action.model == ActionType.ACTION_MORE_30) {
          val x = math.max(1, math.ceil(rubies * 0.3).asInstanceOf[Int])
          bonus = bonus + x
          logger.info(s"30% more ($x) rubies to user ${user.id}")
          text.append(s"\nБонус за акцию +30%: $x ${RuGrammar.rubiesBought(x)}.")

        } else if (action.model == ActionType.ACTION_CLUB_HELP_50) {
          user.clan.foreach {clan =>
            val x = math.max(1, math.ceil(rubies / 2.0).asInstanceOf[Int])
            logger.info(s"50% ($x) as a help for clan#${clan.id} from user#${user.id}")
            text.append(s"\nБонус в княжество по акции 50%: $x ${RuGrammar.rubiesBought(x)}.")
            clan.addRubies(user, x, CLAN_FUND_TX.get)
          }

        } else if (action.model == ActionType.ACTION_ADDITIONAL_COINS) {
          val x = rubies * 50
          logger.info(s"additional coins ($x) for user#$user.id")
          text.append(s"\nМонеты по акции: $x ${RuGrammar.moneyPayment(x)}.")
          user.money.addCoins(x, TransactionGenerator.ACTION_COINS_TX.get)

        } else if (action.model == ActionType.ACTION_EXPERIENCE_BUFFS) {
          if (user.level >= config.BUFFS_LEVEL) {
            user.getBuff(BuffType.Exp50.typeId).prolong()
            user.getBuff(BuffType.HeroExp50.typeId).prolong()
            logger.info(s"Experience action for user#${user.id}")
            text.append(s"\nЗаговор мудрости и Просветление ворона по акции на сутки.")
          }
        } else if (action.model == ActionType.ACTION_BANK_UPGRADE_DISCOUNT_50) {
          if (rubies >= 100) {
            user.lastDiscountActionTime(Some(System.currentTimeMillis()))
            if (user.canUpgradeBank) {
              text.append(s"\nДля Вас работает 50% скидка на улучшение рубинового банка на время дейтсвия акции.")
            }
          }
        } else if (action.model == ActionType.ACTION_BUILDINGS_UPGRADE_DISCOUNT_50) {
          if (rubies >= 100) {
            user.lastDiscountActionTime(Some(System.currentTimeMillis()))
            if (user.canUpgradeTasks || user.canUpgradeMissions || user.canUpgradeBarn) {
              text.append(s"\nДля Вас работает 50% скидка на улучшение заданий, поручений, амбара, банка на время действия акции.")
            }
          }
        }
      }

      user.money.addRubies(rubies.toLong + bonus, TransactionGenerator.BILLING_INCOME_TX.get)
      // ############## PERSIST PAYMENT ##############
      val now = System.currentTimeMillis()
      val payment = Payment().owner(user).creationTime(now).typeStr(typeStr).amount(rubies).externalId(externalId)
      payment.realAmount(realAmount).level(user.level).phone(phone.map(phone => StringUtils.left(phone, 16)))
      session.persist(payment)
      session.flush()
      user.user.lastPaymentTime(Some(now))
      MessagesUtil.sendNotificationMessage(user.user, text.toString(), config.ADMINISTRATION_LOGIN)
      payment
    }

  def getPayments(userId: Int): Seq[Payment] = {
    session.createQuery("from Payment where Owner.Id = :user", classOf[Payment]).setParameter("user", userId).list().asScala
  }

  def getPayment(externalId: String): Option[Payment] = Option {
    session.
      createQuery("from Payment where ExternalId = :extId", classOf[Payment]).setParameter("extId", externalId).
      uniqueResult()
  }

  def getPaymentSum(userId: Int): Int = {
    getPayments(userId).filter(p => !p.canceled).map(p => p.amount).sum
  }

  def cancelPayment(externalId: String): Unit = {
    getPayment(externalId).foreach(_.canceled(true))
  }
}

object BillingDAO {

  val logger = Logger("billing")

  def apply()(implicit hsa: HibernateSessionAware): BillingDAO = new BillingDAO(hsa.hibernateSession)
}
