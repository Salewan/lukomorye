package dao

import java.util
import java.util.Date

import bean.state.{ClanHistoryRecordType, ClanRole}
import bean.{Clan, ClanHistoryRecord, ClanMoney, GameUser}
import components.HibernateSessionAware
import hibernate.HibernateSession

import scala.collection.JavaConverters.collectionAsScalaIterableConverter

/**
  * @author Sergey Lebedev (salewan@gmail.com) 09.11.2016.
  */
class ClanDAO(session: HibernateSession) {

  implicit val hib: HibernateSessionAware = new HibernateSessionAware {
    override val hibernateSession: HibernateSession = session
  }

  def getClanByName(name: String): Option[Clan] = Option {
    session.createQuery("from Clan where lower(name)=:name", classOf[Clan]).
      setParameter("name", name.trim.toLowerCase).
      setMaxResults(1).uniqueResult
  }

  def createClan(name: String, currentUser: GameUser): Clan = {
    val clan = new Clan
    clan.name(name)
    clan.creationTime(new Date())
    clan.creator(currentUser)
    session.persist(clan)

    clan.join(currentUser)
    currentUser.clanRole(ClanRole.LEADER)

    clan.history += new ClanHistoryRecord(clan, ClanHistoryRecordType.CREATION, currentUser)
    session.flush()
    clan
  }

  def getAllClans: Iterable[Clan] = session.createQuery("from Clan order by creationTime", classOf[Clan]).
    list().asScala

  def isCandidateOfferedByClan(clan: Clan, candidate: GameUser): Boolean = {
    Option(session.
      createQuery("from Offer where TheClan = :clan and Candidate = :candidate").
      setParameter("clan", clan).
      setParameter("candidate", candidate).
      list) exists (_.size() > 0)
  }

  def getClanMoney(clanId: Int): ClanMoney = {
    session.get(classOf[ClanMoney], clanId).getOrElse {
      val clanMoney = new ClanMoney
      clanMoney.id(clanId)
      clanMoney.rubies(0L)
      session.persist(clanMoney)
      clanMoney
    }
  }

  def getClanHistory(clan: Clan, page: Int, pageSize: Int): Seq[ClanHistoryRecord] = {
    val results = session.createQuery("from ClanHistoryRecord where TheClan = :clan order by id desc", classOf[ClanHistoryRecord])
      .setCacheable(true)
      .setCacheRegion("ClanDAO.getClanHistory")
      .setParameter("clan", clan)
      .scroll()
    results.scroll(page * pageSize)
    val resultList = new util.ArrayList[ClanHistoryRecord](pageSize)
    while (resultList.size() < pageSize && results.next()) {
      resultList.add(results.get(0).asInstanceOf[ClanHistoryRecord])
    }
    resultList.asScala.toSeq
  }

  def calcPagesCount(collectionSize: Long, pageSize: Int): Int = {
    var pageCount = collectionSize / pageSize
    if (collectionSize % pageSize > 0) {
      pageCount += 1; pageCount - 1
    }
    pageCount.toInt
  }

  def getClanHistoryPageCount(clan: Clan, pageSize: Int): Int = {
    session.createQuery("select count(id) from ClanHistoryRecord where TheClan = :clan", classOf[java.lang.Long])
      .setParameter("clan", clan)
      .setCacheable(true)
      .setCacheRegion("ClanDAO.getClanHistoryCount")
      .uniqueResult().toInt
  }
}

object ClanDAO {
  def apply(session: HibernateSession): ClanDAO = new ClanDAO(session)
}
