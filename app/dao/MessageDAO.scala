package dao

import java.util.Date

import bean.{Clan, User}
import bean.messages.forum._
import bean.messages.personal.PrivateMessage
import com.google.common.collect.ImmutableList
import hibernate.HibernateSession

import scala.collection.JavaConverters._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 26.01.2017.
  */
class MessageDAO(session: HibernateSession) {

  def getTopicMark(user: User, topic: Topic): TopicMark = getTopicMark(user, topic, persist = false)

  def getTopicMark(user: User, topic: Topic, persist: Boolean): TopicMark = {
    val id = new TopicMark.ID(user, topic)
    session.get(classOf[TopicMark], id).getOrElse {
      val folderMark = getFolderMark(user, topic.folder)
      val date = folderMark.lastMarkAllReadTime.getOrElse(0L)
      val mark = new TopicMark().id(id).lastViewedCommentTime(date).folderId(topic.folder.id)
      if (persist) {
        session.persist(mark)
        session.flush()
      }
      mark
    }
  }

  def getRootFolders(withAdmin: Boolean): Seq[Folder] = Option {
    if (!withAdmin) {
      session.createQuery("from Folder where Deleted=false and TypeId in (:types) and Clan is null order by id", classOf[Folder]).
        setParameterList("types", ImmutableList.of(FolderType.NORMAL.id, FolderType.NEWS.id)).
        list()
    } else {
      session.createQuery("from Folder where Deleted=false and Clan is null order by id", classOf[Folder])
        .list()
    }
  } map (_.asScala) getOrElse Seq.empty

  def getRootFolders(clan: Clan): Seq[Folder] = {
    val list =
    session.createQuery("from Folder where Deleted=false and Clan = :cln", classOf[Folder])
      .setParameter("cln", clan)
      .setCacheable(true)
      .setCacheRegion("MessageDAO.getRootFolders")
      .list()

    list.asScala
  }


  def getLastMessageId(dialogId: Int): Option[Int] = {
    val optRes =
    session.createQuery(
      "select m.id from PrivateDialog d join d.Messages m " +
      "where d.id = :dialogId order by m.id desc", classOf[java.lang.Integer]).
      setParameter("dialogId", dialogId).
      setCacheable(true).
      setCacheRegion("MessageDAO.getLastMessageId").
      setMaxResults(1).
      uniqueResultOptional()

    if (optRes.isPresent) Some(Int.unbox(optRes.get()))
    else None
  }

  def getMessagesIds(dialogId: Int): Seq[Int] = {
    session.createQuery(
      "select m.id from PrivateDialog d join d.Messages m " +
      "where d.id=:dialogId and m.Deleted=false order by m.id desc", classOf[java.lang.Integer])
      .setParameter("dialogId", dialogId)
      .list().asScala.map(Int.unbox)
  }

  def getMessagesIds(beforeDate: Date, max: Int): Seq[Int] = {
    session.createQuery("select id from PrivateMessage where CreationTime < :beforeDate ORDER BY creationTime ASC", classOf[Integer]).
      setParameter("beforeDate", beforeDate).setMaxResults(max).list().asScala.map(Int.unbox)
  }

  def getEmptyMessagesDialogIds: Seq[Int] = {
    session.createQuery("select id from PrivateDialog where size(Messages) = 0", classOf[Integer]).list().asScala.map(Int.unbox)
  }

  def getFolderMark(user: User, folder: Folder): FolderMark = {
    val id = new FolderMark.ID(user, folder)
    session.get(classOf[FolderMark], id).getOrElse {
      val mark = new FolderMark().id(id).lastViewTime(0)
      session.persist(mark)
      session.flush()
      mark
    }
  }

  def getTopicCommentsIds(topicId: Int): Seq[Int] = {
    session.
      createQuery("select c.id from Topic t join t.Comments c where t.id=:topicId and c.Deleted=false order by c.id", classOf[java.lang.Integer]).
      setParameter("topicId", topicId).getResultList.asScala.map(Int.unbox)
  }

  def getTopicCommentsPage(topicId: Int, lastViewedCommentTime: Long, pageSize: Int): Int = {
    val query =
      """select count(c.id)
        | from Topic t join t.Comments c
        | where t.id=:topicId
        | and c.Deleted=false and c.CreationTime <= :lastViewedCommentTime""".stripMargin
    val count = session.createQuery(query, classOf[java.lang.Long]).
      setParameter("topicId", topicId).
      setParameter("lastViewedCommentTime", new Date(lastViewedCommentTime)).
      setMaxResults(1).
      uniqueResult()
    (count / pageSize).asInstanceOf[Int]
  }

  def getLastComment(topic: Topic): Option[Comment] = Option {
    session.
      createQuery("from Comment c where c.Topic.id=:tid and c.Deleted=false order by CreationTime desc", classOf[Comment]).
      setParameter("tid", topic.id).
      setMaxResults(1).
      uniqueResult()
  }

  def getNewsForum: Option[Folder] = Option {
    session.
      createQuery("from Folder where TypeId = 1 and Name='Новости'", classOf[Folder]).
      setCacheable(true).
      setCacheRegion("MessageDAO.getNewsForum").
      uniqueResult()
  }

  def findPrivateMessageByParam(param: Int): Option[PrivateMessage] = Option {
    session.
      createQuery("from PrivateMessage where Param = :param", classOf[PrivateMessage]).
      setParameter("param", param).
      getSingleResult
  }
}
