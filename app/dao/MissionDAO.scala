package dao

import bean.GameUser
import bean.state.MissionState
import hibernate.HibernateSession
import model.mission.MissionGenerator
import play.api.Logger
import util.RandomUtil

import scala.collection.mutable
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 17.11.2016.
  */
class MissionDAO(session: HibernateSession) {

  def generateMissions(gameUser: GameUser, n: Int): Unit = {
    if (n > 0) {
      val onLevelMissions = MissionGenerator.byLevel(gameUser.level)
      val available = mutable.LinkedHashSet[MissionGenerator]() ++= filterNotCurrent(gameUser, onLevelMissions)

      for (_ <- 0 until n) {

        val optionalGenerator = RandomUtil.random[MissionGenerator](
          available.toList,
          (g: MissionGenerator) => g.weight
        )

        optionalGenerator match {
          case Some(generator) =>
            available -= generator
            gameUser.missions += generator.generate(gameUser)
          case None => Logger.error(s"User#${gameUser.id} ${gameUser.level} level cannot retrieve a mission generator.")
        }
      }
    }
  }

  def generateMissions(gameUser: GameUser): Unit = {
    val missMax = gameUser.missionsCountMax
    val curr = gameUser.newMissionListSize
    val genCount = math.min(math.max(0, missMax - curr), missMax)
    generateMissions(gameUser, genCount)
  }

  def filterNotCurrent(gameUser: GameUser, allPossible: Seq[MissionGenerator]): Seq[MissionGenerator] = {
    val current = gameUser.missions.toSeq.filter(_.state == MissionState.NEW).map(_.generator)
    allPossible.filterNot(current.contains)
  }
}

object MissionDAO {
  val MISSIONS_MAX = 10

  def apply(session: HibernateSession): MissionDAO = new MissionDAO(session)
}
