package dao

import bean.{GameUser, MoneyTransaction}
import hibernate.HibernateSession
import scala.collection.JavaConverters._

class MoneyTransactionDAO(session: HibernateSession) {

  def getSize(user: GameUser, currency: Int): Int = {
    val c: Long = session
      .createQuery("select count(*) from MoneyTransaction where Actor = :actor and Currency = :curr", classOf[java.lang.Long])
      .setParameter("actor", user)
      .setParameter("curr", Int.box(currency))
      .uniqueResult()
    c.asInstanceOf[Int]
  }

  def getPage(user: GameUser, currency: Int, pageSize: Int, page: Int): Seq[MoneyTransaction] = {
    session.createQuery("from MoneyTransaction where Actor = :actor and Currency = :curr order by CreationTime desc", classOf[MoneyTransaction])
      .setParameter("actor", user)
      .setParameter("curr", Int.box(currency))
      .setMaxResults(pageSize)
      .setFirstResult(page * pageSize)
      .list().asScala
  }
}

object MoneyTransactionDAO {
  def apply(session: HibernateSession): MoneyTransactionDAO = new MoneyTransactionDAO(session)
}
