package dao

import java.util.Date

import scala.collection.JavaConverters._
import bean.GameUser
import hibernate.HibernateSession
import util.{JavaClassUtil, TimeUtil}

import scala.collection.mutable
/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.11.2016.
  */
class RatingDAO(session: HibernateSession) {

  def getUserLevelRating: Seq[GameUser] = Option {
    session.createQuery(
      """
        | SELECT g FROM GameUser g, Avatar a, User u
        | WHERE u.Id = g.Id AND g.Id = a.Id AND g.TotalExperience > 0
        | AND a.Nick != null
        | AND g.Id not in (select u1.Id from User u1 where u1.Role = 'a')
        | AND (g.LastActionTime = null or g.LastActionTime >= :activeTime)
        | ORDER BY g.Level desc, g.TotalExperience desc
      """.stripMargin).
      setMaxResults(config.MAX_RATING_USERS).
      setParameter("activeTime", new Date(System.currentTimeMillis() - TimeUtil.WEEK_MILLIS)).
      setCacheable(false).
      list()
  } map (_.asScala map (_.asInstanceOf[GameUser])) getOrElse mutable.Buffer.empty

  def getUserReputationLevelRating(reputation: Int): Seq[GameUser] = Option {
    session.createQuery(
      """
        | SELECT o FROM Avatar a, Reputation r JOIN r.Owner o
        | WHERE a.Id = o.Id AND o.TotalExperience > 0
        | AND a.Nick != null AND r.ReputationType = :reputation
        | AND o.Id not in (select u1.Id from User u1 where u1.Role = 'a')
        | AND (o.LastActionTime = null or o.LastActionTime >= :activeTime)
        | ORDER BY r.RepForRating desc, o.TotalExperience desc
      """.stripMargin
    ).setMaxResults(config.MAX_RATING_USERS).
      setParameter("activeTime", new Date(System.currentTimeMillis() - TimeUtil.WEEK_MILLIS)).
      setParameter("reputation", reputation).
      setCacheable(false).
      list()
  } map (_.asScala map (_.asInstanceOf[GameUser])) getOrElse mutable.Buffer.empty

  val arrToTuple: Array[AnyRef] => (Int, Long) =
    arr => arr(0).asInstanceOf[Int] -> Option(arr(1)).map(_.asInstanceOf[Long]).getOrElse(0L)

  def getClanLevelRating: Seq[(Int,Long)] = Option {
    session.createQuery(
      """
        | SELECT c.id, cast(c.Level as long), sum(value(rv)) as summa FROM Clan c
        | JOIN c.ReputationValues rv
        | GROUP BY c.id
        | ORDER BY c.Level desc, summa desc
      """.stripMargin, JavaClassUtil.objectArray
    ).setCacheable(false).getResultList
  } map (_.asScala map arrToTuple) getOrElse Seq.empty

  def getClanAllianceRating(alliance: Int): Seq[(Int,Long)] = Option {
    session.createQuery(
      """
        | SELECT c.id, value(rv) as repval from Clan c
        | JOIN c.ReputationValues rv
        | WHERE key(rv) = :alliance and c.AllianceType = :alliance
        | ORDER BY repval desc, c.id
      """.stripMargin, JavaClassUtil.objectArray
    ).setParameter("alliance", alliance).setCacheable(false).getResultList
  } map (_.asScala map arrToTuple) getOrElse Seq.empty
}
