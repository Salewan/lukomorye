package dao

import bean.User
import hibernate.HibernateSession
import scala.collection.JavaConverters._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.03.2017.
  */
class SpamDAO(session: HibernateSession) {

  def getSpamIds(sender: Option[User]): Seq[Int] = {
    if (sender.isEmpty) session.
      createQuery("select id from Spam order by id desc", classOf[java.lang.Integer]).
      setCacheable(true).
      setCacheRegion("SpamDAO.getSpamIdsAll").
      getResultList
    else session.
      createQuery("select id from Spam where Sender.id=:senderId order by id desc", classOf[java.lang.Integer]).
      setParameter("senderId", sender.get.id).
      setCacheable(true).
      setCacheRegion("SpamDAO.getSpamIds").
      getResultList
  }.asScala.map(Int.unbox)

  def deleteSpam(spamIds: Seq[Int]): Unit = {
    session.createQuery("delete from Spam where id in (:spamIds)").
      setParameterList("spamIds", spamIds.asJava).executeUpdate()
  }

}
