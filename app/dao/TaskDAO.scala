package dao

import bean.GameUser
import hibernate.HibernateSession
import model.task.TaskGenerator
import play.api.Logger
import util.RandomUtil

import scala.collection.JavaConverters._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.11.2016.
  */
class TaskDAO(session: HibernateSession) {

  def generateTasks(gameUser: GameUser, n: Int): Unit = {
    if (n > 0) {
      val onLevelEvents = TaskGenerator.questEvents(gameUser.level)
      val happenedEvents = filterHappenedEvents(gameUser, onLevelEvents)

      for (_ <- 0 until n) {

        val optionalGenerator =
          filterNotHappenedEvents(gameUser, happenedEvents, onLevelEvents).headOption.orElse {
            RandomUtil.random[TaskGenerator](
              filterNotCurrent(gameUser, TaskGenerator.onlyTasks(gameUser.level)),
              (g: TaskGenerator) => g.weight)
          }

        optionalGenerator match {
          case Some(generator) =>
            gameUser.tasks += generator.generate(gameUser)
            if (generator.asEvent) gameUser.obtainedEvents += generator.typeId
          case None => Logger.error(s"User#${gameUser.id} ${gameUser.level} level cannot retrieve a task generator.")
        }
      }
    }
  }

  def generateTasks(gameUser: GameUser): Unit = {
    val tasksMax = gameUser.tasksCountMax
    val until = ((tasksMax - gameUser.tasks.size) max 0) min tasksMax //количество заданий
    generateTasks(gameUser, until)
  }

  def filterHappenedEvents(owner: GameUser, checkCol: Seq[TaskGenerator]): Seq[Int] = {
    if (checkCol.isEmpty) Seq.empty
    else {
      val checks = checkCol.map(_.typeId).toSet
      owner.obtainedEvents.filter(checks.contains)
    }
  }

  def filterNotHappenedEvents(user: GameUser, happened1: Seq[Int], all: Seq[TaskGenerator]): Seq[TaskGenerator] = {
    val happened2 = user.tasks.toSeq.map(_.generator).filter(_.asEvent).map(_.typeId)
    val happened = happened1.toSet ++ happened2.toSet
    all.filterNot(happened contains _.typeId)
  }

  def filterNotCurrent(user: GameUser, variants: Seq[TaskGenerator]): Seq[TaskGenerator] = {
    val current = user.tasks.toSeq.map(_.generator)
    variants.filterNot(current.contains)
  }
}

object TaskDAO {
  def apply(session: HibernateSession): TaskDAO = new TaskDAO(session)
}
