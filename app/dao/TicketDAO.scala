package dao

import components.HibernateSessionAware
import hibernate.HibernateSession
import model.TicketState

import scala.collection.JavaConverters._

/**
  * Created by Salewan on 12.04.2017.
  */
class TicketDAO(session: HibernateSession) {

  implicit val hib: HibernateSessionAware = new HibernateSessionAware {
    override val hibernateSession: HibernateSession = session
  }

  def getTicketIds(sortMode: Boolean): Seq[Int] = {
    val sort = if (sortMode) "asc" else "desc"
    session.createQuery(s"select id from Ticket order by id $sort", classOf[java.lang.Integer]).
      setMaxResults(1000).
      getResultList.
      asScala.map(Int.unbox)
  }

  def getTicketIds(state: TicketState, sortMode: Boolean): Seq[Int] = {
    if (state == TicketState.UNKNOWN) getTicketIds(sortMode)
    else session.createQuery(s"select id from Ticket where StateId = :state order by id ${if (sortMode) "asc" else "desc"}", classOf[java.lang.Integer]).
      setParameter("state", state.typeId).
      setMaxResults(1000).
      getResultList.
      asScala.map(Int.unbox)
  }

  def getTicketIds(senderId: Int): Seq[Int] = {
    session.createQuery("select id from Ticket where Sender.id = :senderId order by id", classOf[java.lang.Integer]).
      setParameter("senderId", senderId).
      setMaxResults(1000).
      getResultList.
      asScala.map(Int.unbox)
  }

  def getTicketIds(senderId: Int, state: TicketState): Seq[Int] = {
    if (state == TicketState.UNKNOWN) getTicketIds(senderId)
    else session.createQuery("select id from Ticket where StateId = :state and Sender.id = :senderId order by id", classOf[java.lang.Integer]).
      setParameter("state", state.typeId).
      setParameter("senderId", senderId).
      setMaxResults(1000).
      getResultList.
      asScala.map(Int.unbox)
  }

  def getSupportCorrespondenceMessagesIds(userId: Int): Seq[Int] = {
    val query = """select m.id from PrivateMessage m
      | where (m.Dialog.Owner.Role = :roleSup or m.Dialog.Owner.Role = :roleAdmin)
      | and (m.Author.id = :userId or m.Target.id = :userId)
      | order by id desc""".stripMargin
    session.createQuery(query, classOf[java.lang.Integer]).
      setParameter("userId", userId).
      setParameter("roleSup", "s").
      setParameter("roleAdmin", "a").
      setCacheable(false).
      getResultList.
      asScala.map(Int.unbox)
  }
}
