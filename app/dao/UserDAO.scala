package dao

import java.util
import java.util.Date

import bean._
import components.HibernateSessionAware
import hibernate.HibernateSession
import model.hero.HeroTemplate._
import model.item.Seed
import model.transform.recipe.TransformRecipe
import play.api.Logger

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
class UserDAO(session: HibernateSession) {

  implicit val hib: HibernateSessionAware = new HibernateSessionAware {
    override val hibernateSession: HibernateSession = session
  }

  def createUser(): User = {
    val now = System.currentTimeMillis()

    val user = new User
    user.role('u')
    session.persist(user)

    val avatar = new Avatar
    avatar.id(user.id).sex('m')
    session.persist(avatar)

    val profile = new Profile
    profile.id(user.id).creationTime(now)
    session.persist(profile)

    val gameUser = new GameUser
    gameUser.id(user.id).currentSeed(Some(Seed.Solnechnik)).level(1)
    session.persist(gameUser)

    gameUser.checkNewIngredients()

    Array(Leshij, Rusalka, Kot_uchenyj) foreach { t =>
      val hero = Hero(gameUser, t)
      gameUser.heroes += (t.typeId -> hero)
    }

    TransformRecipe.recipes.take(2) foreach { r =>
      gameUser.transformRecipes += r.typeId
    }

    val userMoney = new UserMoney().id(user.id).dreamDust(250)
    session.persist(userMoney)

    val settings = new Settings
    settings.id(user.id)
    session.persist(settings)

    user
  }

  def findByNick(nick: String): Option[Avatar] = Option(
    session.createQuery("from Avatar where Nick != null and Nick = :nick", classOf[Avatar]).
      setParameter("nick", nick).
      setMaxResults(1).
      setCacheable(true).
      setCacheRegion("UserDAO.findByNick")
      uniqueResult()
  )

  def getAdministration: java.util.List[java.lang.Integer] = {
    session.
      createQuery("SELECT id FROM User WHERE Role IN (:roles) ORDER BY id", classOf[java.lang.Integer]).
      setParameterList("roles", util.Arrays.asList('a', 'm', 's')).list()
  }

  def getAdminNicks: java.util.List[String] = {
    session.
      createQuery("SELECT a.Nick FROM User u, Avatar a WHERE u.id=a.id and u.Role = :role ORDER BY a.id", classOf[String]).
      setParameter("role", 'a').
      list()
  }

  def ban(banned: User, actor: User, time: Long, comment: String, messageText: String): Unit = {
    val now = System.currentTimeMillis()
    banned.
      banEndTime(now + time).
      blocked(banned.isUserBlocked)
    banned.profile.
      banComment(comment).
      bannedBy(actor)
    banned.avatar.
      exileBan(banned.isUserBlocked)

    val record = new UserBanRecord().
      actor(actor).
      comment(comment).
      creationTime(now).
      endTime(banned.banEndTime.get).
      target(banned).
      messageText(messageText)

    session.persist(record)
    banned.profile.banLog += record

    Logger.info(s"User #${banned.id} banned by #${actor.id}")
  }

  def unban(unbanned: User, actor: User): Unit = {
    unbanned.clearBanEndTime()
    unbanned.clearBlocked()
    unbanned.avatar.exileBan(false)
    val record = new UserBanRecord
    record.actor(actor)
    record.creationTime(System.currentTimeMillis())
    record.target(unbanned)
    session.persist(record)
    unbanned.profile.banLog += record

    Logger.info(s"User #${unbanned.id} unbanned by user #${actor.id}")
  }

  def getAllBanRecords: Seq[UserBanRecord] = {
    session.createQuery(
      "from UserBanRecord where EndTime >= :time or EndTime is null order by CreationTime desc",
      classOf[UserBanRecord])
      .setParameter("time", new Date(System.currentTimeMillis()))
      .setMaxResults(1000)
      .getResultList.asScala
  }

  def getUserBySecret(secret: String): Option[User] = Option {
    session.createQuery("from User where secret=:secret", classOf[User]).setParameter("secret", secret).uniqueResult()
  }

  def getUserByNickAndEmail(login: String, email: String): Option[User] = {
    val foundId = Option apply
    session.createQuery(
      "select a.id from Avatar a, Profile p where a.Nick=:nick and p.id = a.id and lower(p.Email)=:email",
      classOf[java.lang.Integer]).
      setParameter("nick", login.trim.toLowerCase).
      setParameter("email", email.trim().toLowerCase).uniqueResult()

    foundId.map(Int.unbox).flatMap(id => session.get(classOf[User], id))
  }

  def getUserByRestorePasswordKey(key: String): Option[User] = Option {
    session.createQuery("from User where RestorePasswordKey=:key", classOf[User]).setParameter("key", key).uniqueResult()
  }

  def getUserByExternalId(externalId: String): Option[User] = Option {
    session.createQuery("from User where ExternalId=:externalId", classOf[User]).
      setParameter("externalId", externalId).uniqueResult()
  }

  def getUsersIdsForWeekClanRubiesProcessing: Seq[Int] = {
    session.createQuery("select u.id from GameUser u join u.ClanWeekRubies e group by u.id having sum(e) > 0", classOf[Integer]).
      list().asScala.map(Int.unbox)
  }

  def getUsersIdsForWeekClanExpProcessing: Seq[Int] = {
    session.createQuery("select u.id from GameUser u join u.ClanWeekExp e group by u.id having sum(e) > 0", classOf[Integer]).
      list().asScala.map(Int.unbox)
  }

  def getClones(user: User): Seq[User] = {
    session
      .createQuery("select u from Profile p, User u where u.id = p.id and p.VirusId = :id order by p.id", classOf[User])
      .setParameter("id", user.profile.virusId)
      .list().asScala
  }
}

object UserDAO {
  def apply()(implicit hs: HibernateSessionAware): UserDAO = new UserDAO(hs.hibernateSession)
}
