package dao.paging

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.10.2016.
  */
class CollectionPagingDAO[T](collection: Iterable[T]) extends PagingDAO[T] {

  override def allSize: Int = collection.size

  override def page(page: Int): Iterable[T] = {
    val from = fromIndex(page)
    val until = (from + pageSize) min allSize
    collection.slice(from, until)
  }
}
