package dao.paging

/**
  * @author Sergey Lebedev (salewan@gmail.com) 25.01.2017.
  */
class CollectionPagingDAOProxy[O, T](col: Iterable[O], f: O => T, psize: Int) extends PagingDAO[T] {

  val dao = new CollectionPagingDAO[O](col) {
    override protected val pageSize: Int = psize
  }

  override protected val pageSize: Int = psize

  override protected def allSize: Int = dao.allSize

  override def page(page: Int): Iterable[T] = dao.page(page).map(f)
}
