package dao.paging

/**
  * @author Sergey Lebedev (salewan@gmail.com) 27.02.2017.
  */
abstract class DatabasePagingDAO[T] extends PagingDAO[T] {

  def loadPage(p: Int): Iterable[T]

  override def page(page: Int): Iterable[T] = {
    val from = fromIndex(page)
    loadPage(from / pageSize)
  }
}
