package dao.paging

import bean.Hero

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.11.2016.
  */
class HeroesPagingDAO(heroes: Iterable[Hero]) extends CollectionPagingDAO[Hero](heroes) {
  override protected val pageSize: Int = 3
}
