package dao.paging

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.10.2016.
  */
trait PagingDAO[T] {

  protected val pageSize = 10

  protected def allSize: Int

  def pageCount: Int = Math.ceil(1.0 * allSize / pageSize).asInstanceOf[Int]

  def fromIndex(page: Int) = (Math.max(0, pageCount - 1) * pageSize) min (page * pageSize)

  def page(page: Int): Iterable[T]
}
