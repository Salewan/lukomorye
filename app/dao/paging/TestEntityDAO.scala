package dao.paging

import bean.TestEntity

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.10.2016.
  */
class TestEntityDAO extends CollectionPagingDAO[TestEntity](for (i <- 1 to 100) yield TestEntity(i))
