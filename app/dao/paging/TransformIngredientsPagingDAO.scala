package dao.paging

import model.item.Ingredient

/**
  * @author Sergey Lebedev (salewan@gmail.com) 07.11.2016.
  */
case class TransformIngredientsPagingDAO(map: Map[Ingredient, Int]) extends CollectionPagingDAO[(Ingredient, Int)](map)
