package hibernate

/**
  * @author Sergey Lebedev (salewan@gmail.com) 18.10.2016.
  */
trait DomainKeys[K,V,D] {

  this: HibernateMapAdapter[K,V] =>

  def convert(key: K): D

  def domainKeyMap: Map[D,V] = asImmutableMap.map { case (key, value) => (convert(key), value)}
}
