package hibernate

import scala.collection.{TraversableOnce, mutable}
import scala.collection.JavaConversions._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.02.2017.
  */
class HibernateBagAdapter[T](set: java.util.List[T]) {

  def iter(): Iterable[T] = {
    val xs = mutable.LinkedHashSet[T]()
    for (t <- set) {
      xs += t
    }
    xs
  }

  def toSeq: Seq[T] = {
    val iterator = set.iterator()
    val buffer = mutable.ArrayBuffer[T]()
    while (iterator.hasNext) {
      buffer += iterator.next()
    }
    buffer
  }

  def addAll(xs: TraversableOnce[T]) = set.addAll(xs.toSeq)

  def isEmpty: Boolean = set.isEmpty

  def nonEmpty: Boolean = set.size() > 0

  def +=(t: T): Unit = set.add(t)

  def -=(t: T): Boolean = set.remove(t)

  def contains(t: T): Boolean = set.contains(t)

  def clear(): Unit = set.clear()

  def filter(p: T => Boolean): Seq[T] = {
    val iterator = set.iterator()
    val buffer = mutable.ArrayBuffer[T]()
    while (iterator.hasNext) {
      val item = iterator.next()
      if (p(item)) buffer += item
    }
    buffer
  }

  def size = set.size()
}
