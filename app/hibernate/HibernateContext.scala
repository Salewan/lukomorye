package hibernate

import javax.inject.{Inject, Singleton}

import modules.GameInit
import org.hibernate.boot.registry.StandardServiceRegistryBuilder
import org.hibernate.boot.{Metadata, MetadataSources}
import org.hibernate.service.ServiceRegistry
import org.hibernate.{CacheMode, FlushMode, SessionBuilder}
import play.api.Logger
import play.api.inject.ApplicationLifecycle
import schedulers.Schedulers

import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 09.03.2016.
 */
@Singleton
class HibernateContext @Inject() (lifecycle: ApplicationLifecycle, schedulers: Schedulers, gameInit: GameInit) {

  Logger.info("Initializing Hibernate SessionFactory...")

  private val standardRegistry: ServiceRegistry = new StandardServiceRegistryBuilder().
    configure("hibernate.cfg.xml").
    build()

  private val sources: Metadata = new MetadataSources(standardRegistry).
    addResource("Beans.hbm.xml").
    getMetadataBuilder.
    build()

  val sessionFactory = sources.buildSessionFactory()

  Logger.info("Initializing Hibernate SessionFactory DONE")

  runDependable()

  lifecycle.addStopHook( () =>
    Future.successful {
      for(s <- schedulers.allSchedulers) s.stop()
      Logger.info("Closing Hibernate SessionFactory...")
      sessionFactory.close()
      Logger.info("Closing Hibernate SessionFactory DONE")
    }
  )

  def openSession(): HibernateSession = {
    val session = sessionFactory.withOptions().asInstanceOf[SessionBuilder[SessionBuilder[_]]].
      interceptor(new LongSessionInterceptor).openSession()
    session.setHibernateFlushMode(FlushMode.COMMIT)
    session.setCacheMode(CacheMode.NORMAL)
    session.beginTransaction()
    new HibernateSession(session)
  }

  def runDependable(): Unit = {
    gameInit.run(this)
    for(s <- schedulers.allSchedulers) s.run(this)
  }
}
