package hibernate

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.11.2016.
  */
trait HibernateDependable {

  def run(hibernateContext: HibernateContext): Unit

  def stop(): Boolean
}
