package hibernate

import javax.inject.Inject

import akka.stream.Materializer
import play.api.cache.CacheApi
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.03.2016.
 */
class HibernateFilter @Inject() (hibernateContext: HibernateContext, cache: CacheApi, implicit val mat: Materializer) extends Filter {

  override def apply(f: (RequestHeader) => Future[Result])(rh: RequestHeader): Future[Result] = {
    if (rh.uri.startsWith("/assets/")) f(rh)
    else {
      val requestId = rh.id.toString
      val session = hibernateContext.openSession()
      cache.set(requestId, session)

      f(rh).map { result =>
        session.close(commit = true, cache.remove(requestId))
        result
      }
    }
  }
}
