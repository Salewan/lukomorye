package hibernate

import scala.collection.{GenTraversableOnce, mutable}
import scala.collection.JavaConversions._
import scala.language.implicitConversions

/**
  * @author Sergey Lebedev (salewan@gmail.com) 18.10.2016.
  */
class HibernateMapAdapter[K,V](map: java.util.Map[K,V]) {

  def asImmutableMap: Map[K,V] = map.toMap

  def putAll(xs: GenTraversableOnce[(K, V)]): Unit = xs.foreach { case (k,v) => map.put(k, v) }

  def update(k: K, v: V): Unit = map.put(k, v)

  def update(k: K)(f: Option[V] => V): Unit = map.put(k, f(Option(map.get(k))))

  def +=(t: (K,V)): Unit = update(t._1, t._2)

  def -=(k: K): V = map.remove(k)

  def containsKey(k: K): Boolean = map.containsKey(k)

  def get(k: K): Option[V] = Option(map.get(k))

  def clear(): Unit = map.clear()

  def size: Int = map.size()

  def toSeq: Seq[(K, V)] = {
    val iterator = map.entrySet().iterator()
    val buffer = mutable.ArrayBuffer[(K,V)]()
    while (iterator.hasNext) {
      val entry = iterator.next()
      buffer += entry.getKey -> entry.getValue
    }
    buffer
  }

  // TODO optimize
  def values: Seq[V] = HibernateMapAdapter.unwrap(this).values.toSeq
}

object HibernateMapAdapter {
  implicit def unwrap[K,V](a: HibernateMapAdapter[K,V]): Map[K,V] = a.asImmutableMap
}
