package hibernate

import play.api.Logger

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.11.2016.
  */
trait HibernateRunnable {
  self =>

  var hibernateContext: HibernateContext = _
  var session: HibernateSession = _

  def runnable(hibernateContext: HibernateContext): Runnable = {
    this.hibernateContext = hibernateContext

    new Runnable {
      override def run(): Unit = {
        session = hibernateContext.openSession()
        try {
          self.run()
          session.close(true)
        } catch { case t: Throwable =>
          Logger.error("", t)
          session.close(false)
        }
      }
    }
  }

  def run(): Unit
}
