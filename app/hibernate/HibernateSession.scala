package hibernate

import org.hibernate.Session
import play.api.Logger

import scala.language.implicitConversions

/**
 * @author Sergey Lebedev (salewan@gmail.com) 12.03.2016.
 */
class HibernateSession(session: Session) {
  def unwrap = session

  def get[T](clazz: Class[T], id: Int): Option[T] = Option(session.get(clazz, id.asInstanceOf[java.io.Serializable]))

  def get[T](clazz: Class[T], id: java.io.Serializable) = Option(session.get(clazz, id))

  def close(commit: Boolean, end: => Unit = ()): Unit = {
    val s = this.unwrap
    try {
      val transaction = s.getTransaction
      if (transaction != null && transaction.isActive) {
        if (commit && !transaction.getRollbackOnly) {
          transaction.commit()
        }
        else {
          transaction.rollback()
        }
      }
    } catch { case t: Throwable =>
      Logger.error("Transaction " + (if (commit) "commit" else "rollback") + " failed", t)
    } finally {
      try {
        s.close()
      } finally {
        end
      }
    }
  }

  def resetCache[T](clazz: Class[T], id: Int): Unit = {
    resetCache(clazz, id.asInstanceOf[java.io.Serializable])
  }

  def resetCache[T](clazz: Class[T], id: java.io.Serializable): Unit = {
    session.getSessionFactory.getCache.evictEntity(clazz, id)
  }

  def resetCollectionCache[T](clazz: Class[T], id: Int, collectionName: String): Unit = {
    resetCollectionCache(clazz, id.asInstanceOf[java.io.Serializable], collectionName)
  }

  def resetCollectionCache[T](clazz: Class[T], id: java.io.Serializable, collectionName: String): Unit = {
    session.getSessionFactory.getCache.evictCollection(clazz.getName + "." + collectionName, id)
  }
}

object HibernateSession {
  implicit def unwrap(s: HibernateSession): Session = s.unwrap
}
