package hibernate

import java.util

import scala.collection.JavaConversions._
import scala.collection.{TraversableOnce, mutable}
import scala.language.implicitConversions

/**
  * @author Sergey Lebedev (salewan@gmail.com) 18.10.2016.
  */
class HibernateSetAdapter[T](set: java.util.Set[T]) {

  def iter(): Iterable[T] = {
    val xs = mutable.LinkedHashSet[T]()
    for (t <- set) {
      xs += t
    }
    xs
  }

  def toSeq: Seq[T] = {
    val iterator = set.iterator()
    val buffer = mutable.ArrayBuffer[T]()
    while (iterator.hasNext) {
      buffer += iterator.next()
    }
    buffer
  }

  def addAll(xs: TraversableOnce[T]) = set.addAll(xs.toSeq)

  def isEmpty: Boolean = set.isEmpty

  def nonEmpty: Boolean = set.size() > 0

  def +=(t: T): Unit = set.add(t)

  def -=(t: T): Boolean = set.remove(t)

  def contains(t: T): Boolean = set.contains(t)

  def exists(p: T => Boolean): Boolean = {
    var found = false
    iterate(t => found = p(t), _ => !found)
    found
  }

  def clear(): Unit = set.clear()

  def filter(p: T => Boolean): Seq[T] = {
    val iterator = set.iterator()
    val buffer = mutable.ArrayBuffer[T]()
    while (iterator.hasNext) {
      val item = iterator.next()
      if (p(item)) buffer += item
    }
    buffer
  }

  def size = set.size()

  def iterate(f: T => Unit, cont: Unit => Boolean): Unit = {
    val iterator = set.iterator()
    while (iterator.hasNext && cont(())) {
      f(iterator.next())
    }
  }

  def underlyingCollection: util.Set[T] = set
}

object HibernateSetAdapter {
  implicit def unwrap[T](a: HibernateSetAdapter[T]): Iterable[T] = a.iter()
}
