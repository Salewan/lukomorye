package hibernate;

import com.google.common.base.Joiner;
import com.google.common.collect.LinkedHashMultiset;
import com.google.common.collect.Multiset;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import util.TimeUtil;

import java.util.stream.Collectors;

public class LongSessionInterceptor extends EmptyInterceptor {


    private static final Logger logger = play.Logger.of(LongSessionInterceptor.class).underlying();

    private long time = System.currentTimeMillis();
    private Multiset<String> queries = LinkedHashMultiset.create();
    private int counter = 0;

    @Override
    public void afterTransactionCompletion(Transaction tx) {
        long now = System.currentTimeMillis();
        if (now - time >= TimeUtil.SECOND_MILLIS) {

            if (logger.isInfoEnabled()) {
                Iterable<String> qs = queries.elementSet().stream()
                        .map(input -> queries.count(input) + ": " + input).collect(Collectors.toList());
                logger.info("Total {} queries time {}ms, first queries are\n{}", counter, now - time,
                        Joiner.on("\n").join(qs));
            }
        }
        queries.clear();
        counter = 0;
        time = System.currentTimeMillis();
    }

    @Override
    public String onPrepareStatement(String sql) {
        if (queries.elementSet().size() < 20) {
            queries.add(sql);
        }
        ++counter;

        return super.onPrepareStatement(sql);
    }
}
