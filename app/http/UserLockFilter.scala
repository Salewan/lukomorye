package http

import javax.inject.Inject

import akka.stream.Materializer
import play.api.cache.CacheApi
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration

/**
  * В начале реквеста кладёт в кэш пару userId -> requestId, в конце реквеста очищает сохранённое.
  * Используется для предотвращения юзером двойных реквестов. @see UserBusyFilter
  * Created by Salewan on 07.06.2017.
  */
class UserLockFilter @Inject()(cache: CacheApi, implicit val mat: Materializer) extends Filter {
  override def apply(f: (RequestHeader) => Future[Result])(rh: RequestHeader): Future[Result] = {
    if (rh.uri.startsWith("/assets/")) f(rh)
    else {
      val cacheKey = for (uid <- rh.session.get(config.COOKIE_ID_NAME).map(_.toInt)) yield {
        val key = s"lock.user.${uid.toString}"

        if (cache.get(key).isEmpty) cache.set(key, rh.id, Duration.fromNanos(10000))

        key
      }

      f(rh).map { result =>
        cacheKey.foreach(key => cache.remove(key))
        result
      }
    }
  }
}
