package model;

import bean.Avatar;
import com.google.common.collect.ImmutableList;
import dao.UserDAO;
import hibernate.HibernateSession;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import scala.runtime.AbstractFunction1;
import util.OnlineUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BBCode {

    public static final List<BBCode> BB_CODES = ImmutableList.of(
            new BBCode("<b>b</b>", "[b][/b]", "\\[b\\](.*?)\\[/b\\]", "\\<b\\>$1\\</b\\>"),
            new BBCode("<u>u</u>", "[u][/u]", "\\[u\\](.*?)\\[/u\\]", "\\<u\\>$1\\</u\\>"),
            new BBCode("<i>i</i>", "[i][/i]", "\\[i\\](.*?)\\[/i\\]", "\\<i\\>$1\\</i\\>"),
            new BBCode("<c>c</c>", "[c][/c]", "\\[c\\](.*?)\\[/c\\]", "\\<span style=\"text-align:center;display:block;\"\\>$1\\</span\\>"),
            new BBCode("<r>r</r>", "[r][/r]", "\\[r\\](.*?)\\[/r\\]", "\\<span style=\"text-align:right;display:block;\"\\>$1\\</span\\>"),
            new BBCode("br", "[br]", "\\[br\\]", "\\<br/\\>") {
                @Override
                public boolean isHidden() {
                    return true;
                }
            },
            new BBCode("<span style=\"color: red\">c</span><span style=\"color: yellow\">o</span><span style=\"color: green\">l</span><span style=\"color: blue\">o</span><span style=\"color: violet\">r</span>", "[color=][/color]", "\\[color=(.*?)\\](.*?)\\[/color\\]", "\\<span style=\"color: $1;\"\\>$2\\</span\\>"),
            new BBCode("img", "img=", "img(\\d+?),(\\d+?)\\=(.*?)(\\s+?|$)|img(\\d+?)=(.*?)(\\s+?|$)|img=(.*?)(\\s+?|$)|\\[img=(.*?)\\]") {
                @Override
                public String process(String text, Session session, boolean canUseExternalLinks) {
                    StringBuffer sb = new StringBuffer();
                    Matcher matcher = getPattern().matcher(text);
                    while (matcher.find()) {
                        String width = matcher.group(1);
                        if (width == null) {
                            width = matcher.group(5);
                        }

                        String height = matcher.group(2);
                        if (height == null) {
                            height = width;
                        }

                        String imgLink = matcher.group(3);
                        boolean oldStyle = false;
                        if (imgLink == null) {
                            imgLink = matcher.group(6);
                        }
                        if (imgLink == null) {
                            imgLink = matcher.group(8);
                        }
                        if (imgLink == null) {
                            imgLink = matcher.group(10);
                            if (imgLink != null) {
                                oldStyle = true;
                            }
                        }

                        if (imgLink != null && !canUseExternalLinks && (!imgLink.startsWith("/") || imgLink.startsWith("//"))) {
                            matcher.appendReplacement(sb, "");
                            continue;
                        }

                        if (!oldStyle) {
                            if (width == null) {
                                width = "48";
                            }
                            if (height == null) {
                                height = "48";
                            }
                            matcher.appendReplacement(sb, "\\<img style=\"max-width:100%;\" alt=\"\" border=\"0\" src=\"" + imgLink + "\" width=\"" + width + "\" height=\"" + height + "\"/\\>");
                        } else {
                            matcher.appendReplacement(sb, "\\<img style=\"max-width:100%;\" alt=\"\" border=\"0\" src=\"" + imgLink + "\"/\\>");
                        }
                    }
                    matcher.appendTail(sb);
                    return sb.toString();
                }
            },
            new BBCode("url", "[url=][/url]", "\\[url=(.*?)\\](.*?)\\[/url\\]") {
                @Override
                public String process(String text, Session session, boolean canUseExternalLinks) {
                    Matcher matcher = getPattern().matcher(text);
                    StringBuffer sb = new StringBuffer();
                    while (matcher.find()) {
                        String urlLink = matcher.group(1);
                        String urlText = matcher.group(2);
                        if (urlLink != null && !canUseExternalLinks && (!urlLink.startsWith("/") || urlLink.startsWith("//"))) {
                            urlLink = null;
                        }
                        matcher.appendReplacement(sb, urlLink != null ? "\\<a style=\"text-decoration: underline;\" href=\"" + urlLink + "\"\\>" + urlText + "\\</a\\>" : urlText);
                    }
                    matcher.appendTail(sb);
                    return sb.toString();
                }
            },
            new BBCode("@ник@", "@@", "@(([a-zA-Zа-яА-ЯёЁ]+\\s{0,1}){1,3})@") {
                @Override
                public String process(String text, Session session, boolean canUseExternalLinks) {
                    String result = text;
                    if (!StringUtils.isEmpty(text)) {
                        Matcher matcher = getPattern().matcher(text);
                        StringBuffer sb = new StringBuffer();
                        while (matcher.find()) {
                            String userName = matcher.group(1);
                            if (!StringUtils.isEmpty(userName)) {
                                new UserDAO(new HibernateSession(session)).findByNick(userName).foreach(new AbstractFunction1<Avatar, Object>() {
                                    @Override
                                    public Object apply(Avatar avatar) {
                                        String replacement = "<img src=\"/assets/images/" + avatar.image() +
                                                "\" alt=\"image\" width=\"24\" height=\"24\"/> " +
                                                "<a href=\"/profile/" + avatar.getId() + "\">" +
                                                "<span class=\"" + (OnlineUtil.isOnline(avatar.getId()) ? "online" : "offline") + "\">"
                                                + avatar.getVisualNick() + "</span></a>";

                                        matcher.appendReplacement(sb, replacement);
                                        return null;
                                    }
                                });
                            }
                        }
                        matcher.appendTail(sb);
                        result = sb.toString();
                    }
                    return result;
                }
            }
    );

    public static final List<BBCode> AVAILABLE_BB_CODES = BB_CODES.stream().filter(code -> !code.isHidden()).collect(Collectors.toList());

    private String name;
    private String template;
    private Pattern pattern;
    private String replaceWith;

    private BBCode(String name, String template, String sPattern, String replaceWith) {
        this.name = name;
        this.template = template;
        this.pattern = Pattern.compile(sPattern, Pattern.CASE_INSENSITIVE);
        this.replaceWith = replaceWith;
    }

    private BBCode(String name, String template, String sPattern) {
        this(name, template, sPattern, null);
    }

    public String getName() {
        return name;
    }

    public String getTemplate() {
        return template;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public boolean isHidden() {
        return false;
    }

    public String process(String text, Session session, boolean canUseExternalLinks) {
        if(replaceWith == null) {
            return text;
        }
        text = StringUtils.replaceEach(text, new String[]{"$", "\\"}, new String[]{"", ""});
        return pattern.matcher(text).replaceAll(replaceWith);
    }
}
