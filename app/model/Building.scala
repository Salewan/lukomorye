package model

import bean.GameUser
import controllers.routes
import model.item.Product._
import model.item.{Ingredient, Product}
import play.api.mvc.Call
import util.Implicits._
import model.price.{CoinPrice => Coins}

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.01.2017.
  */
abstract class Building(
                         val typeId: Int,
                         val name: String,
                         val level: Int,
                         price: Long
                       ) {

  def now = System.currentTimeMillis()

  val router: Call

  def getPrice = Coins(price)

  def timeToReward(user: GameUser): Option[Long]

  val icon: String

  def visitWarn(user: GameUser): Boolean = false
}

trait PetHouse {
  this: Building =>

  val petFull: String
  val petWalk: String
  val petHungry: String
  val food: Product
  val product: Ingredient
  val interval: Long
  val blackImage: String

  override val icon: String = "shelter.png"

  override def timeToReward(user: GameUser): Option[Long] = {
    val now = System.currentTimeMillis()
    user.getPetProduction(typeId).flatMap(_.pets.flatMap(_.fullness).toSeq.sorted.headOption).filter(_ > now).map(_ - now)
  }
}

trait ProdFactory {
  this: Building =>

  override val icon: String = "building.png"

  override val router: Call = routes.ProductionController.productionPage(typeId)
  val products: Array[Product]
  val buildingTime: Long

  override def timeToReward(user: GameUser): Option[Long] = {
    user.getProduction(typeId).flatMap(_.timeLeft)
  }

  override def visitWarn(user: GameUser): Boolean = user.getProduction(typeId).exists(_.visitWarn)
}

object Building {

  case object FARM extends Building(1, "Огород", 1, 0) {
    override val router: Call = routes.OgorodController.ogorodPage()

    override val icon: String = "farm.png"

    override def timeToReward(user: GameUser): Option[Long] = {
      user.getProduction(typeId).flatMap(_.timeLeft)
    }
  }

  case object VOLSHEBNYJ_NASEST extends Building(2, "Волшебный насест", config.NASEST_LEVEL, 100) with PetHouse {
    override val router: Call = routes.PetFarmController.petfarmPage(typeId)
    override val petFull: String = "misc3/pet_1_128.png"
    override val petWalk: String = "misc3/pet_1_128.png"
    override val petHungry: String = "misc3/pet_1_128.png"
    override val food: Product = Product.IZBUSHKA_FOOD
    override val product: Ingredient = Product.EGG
    override val interval: Long = 18 minutes
    override val blackImage: String = "black/2_1.png"
  }

  case object OGNEUPORNYJ_HLEV extends Building(3, "Огненный хлев", config.HLEV_LEVEL, 3000) with PetHouse {
    override val router: Call = routes.PetFarmController.petfarmPage(typeId)
    override val petFull: String = "misc3/pet_2_128.png"
    override val petWalk: String = "misc3/pet_2_128.png"
    override val petHungry: String = "misc3/pet_2_128.png"
    override val food: Product = Product.BUIVOL_FOOD
    override val product: Ingredient = Product.MILK
    override val interval: Long = 60 minutes
    override val blackImage: String = "black/3_1.png"
  }

  case object PESHHERA_VASILISKOV extends Building(4, "Пещера василисков", config.PESHHERA_LEVEL, 25000) with PetHouse {
    override val router: Call = routes.PetFarmController.petfarmPage(typeId)
    override val petFull: String = "misc3/pet_3_128.png"
    override val petWalk: String = "misc3/pet_3_128.png"
    override val petHungry: String = "misc3/pet_3_128.png"
    override val food: Product = Product.VASILISK_FOOD
    override val product: Ingredient = Product.TAIL
    override val interval: Long = 240 minutes
    override val blackImage: String = "black/4_1.png"
  }

  case object ZAGON_ZOLOTYH_ARHAROV extends Building(5, "Загон золотых архаров", config.ZAGON_LEVEL, 425000) with PetHouse {
    override val router: Call = routes.PetFarmController.petfarmPage(typeId)
    override val petFull: String = "misc3/pet_4_128.png"
    override val petWalk: String = "misc3/pet_4_128.png"
    override val petHungry: String = "misc3/pet_4_128.png"
    override val food: Product = Product.ARHAR_FOOD
    override val product: Ingredient = Product.WOOL
    override val interval: Long = 360 minutes
    override val blackImage: String = "black/5_1.png"
  }

  case object FEED_MILL extends Building(6, "Мельница", 4, 120) with ProdFactory {
    override val products: Array[Product] = Array(IZBUSHKA_FOOD, BUIVOL_FOOD, VASILISK_FOOD, ARHAR_FOOD)
    override val buildingTime: Long = 30 seconds
  }

  case object VOLSHEBNAYA_PECHKA extends Building(7, "Волшебная печка", 2, 200) with ProdFactory {
    override val products: Array[Product] = Array(KOLOBOK, OLADUSHKI_POHIDYASHKI, PECHENKA, VESELIY_KEKSIK, KEKSIK_KOSHEISKI_ZLODEISKI)
    override val buildingTime: Long = 0 seconds
  }

  case object DOMOVOY_S_MASLOBOYKOY extends Building(8, "Домовой с маслобойкой", 12, 4000) with ProdFactory {
    override val products: Array[Product] = Array(RYAZHENKA_PRIVOROTNAYA, MASLO, SNADOBIE_VASILISKA)
    override val buildingTime: Long = 2 hours
  }

  case object SUMERECHNAYA_MELNITCA extends Building(9, "Сумеречная мельница", 14, 6000) with ProdFactory {
    override val products: Array[Product] = Array(LUNNY_BISER, SUMERECHNAYA_PIL, ZVEZDNAYA_ESENCIYA)
    override val buildingTime: Long = 4 hours
  }

  case object VOLSHEBNIY_STANOK extends Building(10, "Волшебный станок", 12, 4000) with ProdFactory {
    override val products: Array[Product] = Array(AMULET_UDACHI, BLUDCE, ZOLOTAYA_SHAL_NEOTRAZIMOSTI, OJERELIE_PROTIVOVAMPIRSKOE)
    override val buildingTime: Long = 2 hours
  }

  case object DRAKONYA_ZHAROVNYA extends Building(11, "Драконья жаровня", 19, 20000) with ProdFactory {
    override val products: Array[Product] = Array(PIROZHKI_BODROSTI, JARKOE_ZMEINOI_GIBKOSTI, SHASHLYK_ZHELEZNOY_SILY, KANOPE_PROTIVOKIKIMORNOYE)
    override val buildingTime: Long = 8 hours
  }

  case object VEDMINA_ZELIEVARILKA extends Building(12, "Ведьмина зельеварилка", 26, 150000) with ProdFactory {
    override val products: Array[Product] = Array(ZELIE_UVERENNOSTI, ZELIE_OZARENIYA, ELEKSIR_DIAVOLSKOY_HITROSTI, SNADOBIE_VECHNOI_ZHIZNI, SNADOBIE_USPEHA)
    override val buildingTime: Long = 12 hours
  }

  case object TKACKII_STANOK extends Building(13, "Магический ткацкий станок", 34, 800000) with ProdFactory {
    override val products: Array[Product] = Array(KOVER_SAMOLET, TKAN_NOCHNOI_SVEHZESTI, OGNENNIE_RUKOVICI, TCARSKAYA_DUBLENKA)
    override val buildingTime: Long = 24 hours
  }

  case object FEYA_SHVEYA extends Building(14, "Фея швея", 41, 3500000) with ProdFactory {
    override val products: Array[Product] = Array(PLATIE_VESENNEY_SVEZHESTI, BOYARSKII_KAMZOL, KRUJEVNAYA_SOROCHKA)
    override val buildingTime: Long = 20 hours
  }

  case object LEKARNYA extends Building(15, "Лекарня", 43, 6000000) with ProdFactory {
    override val products: Array[Product] = Array(VAFLI_NOCHNIH_KOSHMAROV, PIROJENKA_NOCHNOI_STRASTI, PRIMANKA_OBOROTNEY, SNADOBIE_ASTRALNIH_PUTESHESTVII)
    override val buildingTime: Long = 24 hours
  }

  case object KUZNICA_GNOMOV extends Building(16, "Кузница гномов", 46, 10000000) with ProdFactory {
    override val products: Array[Product] = Array(OBEREG_OT_SGLAZA, KOLCHUJKA_MIFRILNAYA)
    override val buildingTime: Long = 18 hours
  }

  case object ZMEEVIK extends Building(17, "Волшебный змеевик", 51, 30000000) with ProdFactory {
    override val products: Array[Product] = Array(NASTOYKA_HRABROSTI, NASTOYKA_VECHNOY_UNOSTI)
    override val buildingTime: Long = 31 hours
  }

  case object FUGANOK extends Building(18, "Волшебный фуганок", 53, 50000000) with ProdFactory {
    override val products: Array[Product] = Array(VOLSHEBNAYA_PALOCHKA_IZLECHENIYA)
    override val buildingTime: Long = 31 hours
  }

  private val petBuildings: Array[Building with PetHouse] = Array(VOLSHEBNYJ_NASEST, OGNEUPORNYJ_HLEV, PESHHERA_VASILISKOV, ZAGON_ZOLOTYH_ARHAROV)
  private val prodFactories: Array[Building with ProdFactory] = Array(
    FEED_MILL, VOLSHEBNAYA_PECHKA, DOMOVOY_S_MASLOBOYKOY, SUMERECHNAYA_MELNITCA,
    VOLSHEBNIY_STANOK, DRAKONYA_ZHAROVNYA, VEDMINA_ZELIEVARILKA, TKACKII_STANOK,
    FEYA_SHVEYA, LEKARNYA, KUZNICA_GNOMOV, ZMEEVIK,FUGANOK
  )
  private val buildings = Array(FARM) ++ petBuildings ++ prodFactories
  private val petBuildingsMap: Map[Int, Building with PetHouse] = petBuildings.map(b => b.typeId -> b).toMap
  private val prodFactoriesMap: Map[Int, Building with ProdFactory] = prodFactories.map(b => b.typeId -> b).toMap
  private val buildingsMap: Map[Int, Building] = buildings.map(b => b.typeId -> b).toMap

  def all = buildings
  def petHouses = petBuildings
  def prodBuildings = prodFactories

  def petHouse(id: Int): Building with PetHouse = petBuildingsMap(id)
  def findPetHouse(id: Int): Option[Building with PetHouse] = petBuildingsMap.get(id)

  def prodFactory(id: Int): Building with ProdFactory = prodFactoriesMap(id)
  def findProdFactory(id: Int): Option[Building with ProdFactory] = prodFactoriesMap.get(id)
}
