package model

/**
 * @author Sergey Lebedev (salewan@gmail.com) 18.03.2016.
 */
trait Deficit {
  val amount: Long
}

case class CoinDeficit(amount: Long) extends Deficit
case class RubyDeficit(amount: Long) extends Deficit
case class DreamDustDeficit(amount: Long) extends Deficit
case object EmptyDeficit extends Deficit {
  override val amount: Long = 0L
}
