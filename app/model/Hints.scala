package model

import components.context.PlayContext
import util.RandomUtil

/**
  * Created by Salewan on 01.06.2017.
  */
object Hints {
  case class Hint(minLevel: Int = 1, maxLevel: Int = config.MAX_USER_LEVEL, text: String)

  val mission = Seq(
    Hint(text = "В сложное поручение можно специально отправлять начинающего героя даже без шансов на успех поручения для того, чтобы герой получил много опыта.")
  )

  val farm = Seq(
    Hint(text = "Если кликнуть на иконку земли или посаженного растения - действие выполнится для всей земли и всех растений, если кликнуть на кнопку под иконкой - действие выполнится для одного растения")
    //Hint(text = "На ночь можно сажать долго растущие растения, а утром готовить из них много настойки времени для ускорения.")
  )

  val tasks = Seq(
    Hint(text = "Если долгое время не удается выполнить задание - задание можно удалить и вместо него через небольшое время появится новое.")
  )

  val missions = Seq(
    Hint(text = "Если награда поручения Вам не нужна и выполнять поручение Вы не собираетесь - поручение можно удалить и вместо него появится новое.")
  )

  val ranMissions = Seq(
    Hint(text = "Выполнение поручений можно ускорить зельем времени.")
  )

  val filter = (hint: Hint, userLevel: Int) => userLevel >= hint.minLevel && userLevel <= hint.maxLevel

  private def findHint(seq: Seq[Hint], level: Int): Option[String] = {
    val hints = seq.filter(filter(_, level))
    if (hints.isEmpty) None
    else Some(RandomUtil.random(hints).text)
  }

  def nextHint(pageName: String, level: Int)(implicit ctx: PlayContext[_]): Option[String] = pageName match {
    case "mission" => findHint(mission, level)
    case "farm" => findHint(farm, level)
    case "tasks" => findHint(tasks, level)
    case "missions" => findHint(missions, level)
    case "ranMissions" => findHint(ranMissions, level)
    case _ => None
  }
}
