package model

import bean.Notification
import components.context.PlayContext
import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}
import play.api.mvc.AnyContent
import play.twirl.api.Html
import play.api.libs.json._

/**
  * Created by Salewan on 25.05.2017.
  */
sealed trait NotificationType extends EnumEntry with Enumerable[Int] {

  def render(notification: Notification)(implicit ctx: PlayContext[AnyContent]): Html
}

object NotificationType extends Enum[NotificationType] with EnumerationBoilerplate[Int, NotificationType] {

  def values = findValues

  override def entries: IndexedSeq[NotificationType] = values

  case object SimpleInfo extends NotificationType {
    override val typeId: Int = 1

    override def render(notification: Notification)(implicit ctx: PlayContext[AnyContent]): Html =
      views.html.notification.SimpleInfoView(notification)
  }

  case object IGotItAboutOneMoreHero extends NotificationType {
    override def render(notification: Notification)(implicit ctx: PlayContext[AnyContent]): Html =
    views.html.notification.IGotItAboutOneMoreHeroView(notification)

    override val typeId: Int = 1
  }

  case object YouBecomeOfficer extends NotificationType {
    override def render(notification: Notification)(implicit ctx: PlayContext[AnyContent]): Html = {
      (for (avatar <- ctx.avatarOptional) yield {
        views.html.notification.YouBecomeOfficerView(notification.id, avatar.sex, notification.content)
      }).getOrElse(Html(""))
    }

    override val typeId: Int = 2
  }

  case object ClanNotice extends NotificationType {
    override def render(notification: Notification)(implicit ctx: PlayContext[AnyContent]): Html = {
      val json = Json.parse(notification.content)
      val title = (json \ "title").as[String]
      val text = (json \ "text").as[String]

      views.html.notification.ClanNoticeView(notification.id, title, text)
    }

    override val typeId: Int = 3
  }
}
