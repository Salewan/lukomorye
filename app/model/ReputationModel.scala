package model

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

/**
  * Created by Salewan on 02.06.2017.
  */
sealed trait ReputationModel extends EnumEntry with Enumerable[Int] {
  val name: String
  val name_tv: String
  val image: String
}

object ReputationModel extends Enum[ReputationModel] with EnumerationBoilerplate[Int, ReputationModel] {

  def values = findValues

  override def entries: IndexedSeq[ReputationModel] = values

  case object SALTAN extends ReputationModel {
    override val name: String = "Царь Салтан"
    override val name_tv: String = "Царём Салтаном"
    override val image: String = "reputation/tsar.png"
    override val typeId: Int = 0
  }
  case object KOSHEY extends ReputationModel {
    override val name: String = "Кощей Бессмертный"
    override val name_tv: String = "Кощеем Бессмертным"
    override val image: String = "reputation/koshey.png"
    override val typeId: Int = 1
  }
  case object PADISHAH extends ReputationModel {
    override val name: String = "Падишах"
    override val name_tv: String = "Падишахом"
    override val image: String = "reputation/padishah.png"
    override val typeId: Int = 2
  }
  //case object SREDIZEMIE extends ReputationModel {
  //  override val name: String = "Средиземье"
  //  override val image: String = "reputation/sredizemie.png"
  //  override val typeId: Int = 3
  //}
}

