package model

import bean.Hero
import model.hero.{Ability, HeroTemplate}
import model.item.Ingredient
import model.renderer.TemplateRenderer

import scala.collection.mutable

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.04.2016.
  */
class Reward {

  var level: Option[Int] = None
  var exp: Option[Long] = None
  var coin: Option[Long] = None
  var ruby: Option[Long] = None
  var ingredients: mutable.Map[Ingredient, Int] = mutable.Map()
  var dreamDust: Option[Long] = None
  var heroLevels: mutable.Map[HeroTemplate, Int] = mutable.Map()
  var reputation: mutable.Map[Int, Int] = mutable.Map()
  var heroAbilities: mutable.Map[HeroTemplate, Ability] = mutable.Map()
  var heroLevel: mutable.Map[Int, Int] = mutable.Map()
  var keys: Option[Int] = None
  var chests: Option[Int] = None
  var levelUpBonus: Option[(Int,Int)] = None
  var reputationLevel: mutable.Map[ReputationModel, Int] = mutable.Map()
  var templateRenderer: Option[TemplateRenderer] = None
  var heroExp: mutable.Map[Int, Long] = mutable.Map()

  def setLevel(level: Int) = {
    this.level = Some(level)
    this
  }

  def setHeroLevel(heroType: Int, level: Int) = {
    heroLevels update (HeroTemplate.withType(heroType), level)
    this
  }

  def addExp(exp: Long) = {
    if (exp > 0)
      this.exp = this.exp.orElse(Some(0L)).map(_ + exp)
    this
  }

  def addCoin(arg: Long) = {
    if (arg > 0)
      coin = coin.orElse(Some(0L)).map(_ + arg)
    this
  }

  def addRuby(arg: Long) = {
    if (arg > 0)
      ruby = ruby.orElse(Some(0L)).map(_ + arg)
    this
  }

  def addIngredient(gameItem: Ingredient, amount: Int) = {
    ingredients update (gameItem, ingredients.getOrElse(gameItem, 0) + amount)
    this
  }

  def addDreamDust(amount: Long) = {
    if (amount > 0) dreamDust = dreamDust orElse Some(0L) map (_ + amount)
    this
  }

  def addReputation(rep: Int, amount: Int) = {
    if (amount > 0)
      reputation update (rep, reputation.getOrElse(rep, 0) + amount)
    this
  }

  def addReputationLevel(rep: ReputationModel, level: Int): Reward = {
    reputationLevel.update(rep, level)
    this
  }

  def addHeroAbility(hero: HeroTemplate, ability: Ability) = {
    heroAbilities update (hero, ability)
    this
  }

  def addHeroLevel(hero: Hero) = {
    heroLevel update (hero.heroType, hero.level + hero.pendingLevel)
    this
  }

  def addHeroExp(hero: Int, exp: Long): Reward = {
    val cur = heroExp.getOrElse(hero, 0L)
    heroExp.update(hero, cur + exp)
    this
  }

  def addHeroExp(hero: Hero, exp: Long): Reward = {
    addHeroExp(hero.heroType, exp)
  }

  def addKeys(k: Int) = {
    if (k > 0) {
      keys = Some(k + keys.getOrElse(0))
    }
    this
  }

  def addChests(c: Int) = {
    if (c > 0) {
      chests = Some(c + chests.getOrElse(0))
    }
    this
  }

  def addLevelUpBonus(levelToRubies: (Int, Int)) = {
    this.levelUpBonus = Some(levelToRubies)
    this
  }

  def addTemplateRenderer(tr: TemplateRenderer): Reward = {
    this.templateRenderer = Some(tr)
    this
  }

  def +=(anotherReward: Reward): Reward = {
    anotherReward.keys.foreach(addKeys)
    anotherReward.chests.foreach(addChests)
    anotherReward.level.foreach(setLevel)
    anotherReward.exp.foreach(addExp)
    anotherReward.coin.foreach(addCoin)
    anotherReward.ruby.foreach(addRuby)
    anotherReward.dreamDust.foreach(addDreamDust)
    anotherReward.templateRenderer.foreach(addTemplateRenderer)
    anotherReward.ingredients foreach {case (seed, amount) => addIngredient(seed, amount)}
    anotherReward.heroLevels.foreach {case (hero, lvl) => setHeroLevel(hero.typeId, lvl)}
    anotherReward.reputation.foreach {case (rep, value) => addReputation(rep, value)}
    anotherReward.reputationLevel foreach {case (rep, lvl) => addReputationLevel(rep, lvl)}
    anotherReward.heroAbilities.foreach {case (hero, ability) => addHeroAbility(hero, ability)}
    anotherReward.heroExp.foreach {case (hero, e) => addHeroExp(hero, e)}
    anotherReward.levelUpBonus.foreach(addLevelUpBonus)
    anotherReward.heroLevel.foreach {case (heroType, lvl) =>
      this.heroLevel update (heroType, lvl max this.heroLevel.getOrElse(heroType, 0))
    }
    this
  }

  def isEmpty: Boolean = {
    level.isEmpty &&
      exp.isEmpty &&
      coin.isEmpty &&
      ruby.isEmpty &&
      dreamDust.isEmpty &&
      ingredients.isEmpty &&
      heroLevels.isEmpty &&
      reputation.isEmpty &&
      heroAbilities.isEmpty &&
      heroLevel.isEmpty &&
      levelUpBonus.isEmpty &&
      keys.isEmpty &&
      chests.isEmpty &&
      reputationLevel.isEmpty &&
      templateRenderer.isEmpty
  }

  def nonEmpty: Boolean = !isEmpty
}

object Reward {
  def apply() = new Reward
}
