package model

import bean.Task

/**
  * @author Sergey Lebedev (salewan@gmail.com) 02.12.2016.
  */
trait TaskTemplate {

  val typeId: Int

  def getTemplate(task: Task): Template
}

trait Template {

  def header: Option[String] = None

  def reputationImageShown: Boolean = true

  def footer: Option[String] = None

  def startLabel: String = "Получить награду"
}

object TaskTemplate {

  case object DEFAULT_TEMPLATE extends TaskTemplate {

    override val typeId: Int = 0

    override def getTemplate(task: Task): Template = new Template {}
  }

  case object KING_MARRIAGE extends TaskTemplate {
    override val typeId: Int = 1

    override def getTemplate(task: Task): Template = new Template {
      override def header: Option[String] = Some(
        """<div class="center row">
          |<img src="assets/images/tutorial/tsar1_1.png" width="110" height="110" alt="" title=""/>
          |<img src="assets/images/tutorial/tsarevna_1.png" width="80" height="80" alt="" title=""/>
          |<div class="center">Царь желает жениться. Вот только стар он стал и немощен. Помоги ему омолодиться - достань предметы.</div>
          |</div>""".stripMargin)

      override def reputationImageShown: Boolean = false

      override def footer: Option[String] = Some(
        """<div class="center">
          |<div>Царь обещает дать имение в награду и взять себе в советники того, кто поможет ему.</div>
          |<img src="assets/images/tutorial/tsar2_1.png" width="110" height="110" alt="" title=""/>
          |</div>""".stripMargin)

      override def startLabel: String = "Помочь царю"
    }

  }

  case object MAKE_KOLOBOK extends TaskTemplate {
    override val typeId: Int = 2

    override def getTemplate(task: Task): Template = new Template {
      override def header: Option[String] = Some(
        """<div class="center row">
          |Оправляйся во владения которые царь дал, собери урожай да печь почини, испеки колобка там, принеси сюда.
          |</div>
        """.stripMargin)

      override def startLabel: String = "Отдать колобка"
    }
  }

  case object MAKE_EGGS extends TaskTemplate {
    override val typeId: Int = 3

    override def getTemplate(task: Task): Template = new Template {
      override def header: Option[String] = Some(
        """<div class="center row">
          |Избушки на курьих ножках яйца несут. Построй насест, собери 3 яйца и неси их сюда.
          |</div>
        """.stripMargin)

      override def startLabel: String = "Сдать яйца"
    }
  }

  def find(typeId: Int): Option[TaskTemplate] = typeId match {
    case 1 => Some(KING_MARRIAGE)
    case 2 => Some(MAKE_KOLOBOK)
    case 3 => Some(MAKE_EGGS)
    case _ => None
  }
}
