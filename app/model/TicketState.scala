package model

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

/**
  * Created by Salewan on 12.04.2017.
  */
sealed class TicketState(val typeId: Char, val name: String) extends EnumEntry with Enumerable[Char] {
  val deskKey: String = "TicketState." + typeId
}

object TicketState extends Enum[TicketState] with EnumerationBoilerplate[Char, TicketState] {
  def values = findValues

  override def entries: IndexedSeq[TicketState] = values

  override def withTypeOption(tid: Char): Option[TicketState] = Some(withType(tid))

  override def withType(tid: Char): TicketState = super.withTypeOption(tid).getOrElse(UNKNOWN)

  case object NEW extends TicketState('n', "новые")
  case object ADMIN extends TicketState('a', "админу")
  case object DECLINED extends TicketState('d', "отклонённые")
  case object RESOLVED extends TicketState('r', "обработанные")
  case object UNKNOWN extends TicketState('u', "все")
}
