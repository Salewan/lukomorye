package model

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

/**
  * Created by Salewan on 23.06.2017.
  */
sealed trait TransactionGenerator extends EnumEntry with Enumerable[Int] {
  def get = TransactionType(typeId, None)
  def get(param: Int) = TransactionType(typeId, Some(param))
}

class TG(val typeId: Int, val name: String) extends TransactionGenerator

object TransactionGenerator extends Enum[TransactionGenerator] with EnumerationBoilerplate[Int, TransactionGenerator] {
  override def values = findValues

  override def entries: IndexedSeq[TransactionGenerator] = values

  case object LEVEL_UP_TX extends TG(1, "взятие уровня") //
  case object TASK_COMPLETE_TX extends TG(2, "выполненное задание") //
  case object STORE_SELL_TX extends TG(3, "продажа на рынке") //
  case object MISSION_COMPLETE_TX extends TG(4, "выполнение поручения") //
  case object BURN_INGREDIENT_TX extends TG(5, "сжигание") //
  case object ALCHEMY_TX extends TG(6, "алхимия") //
  case object ADMIN_WISHES_TX extends TG(7, "админ изменил") //
  case object UPGRADE_MISSIONS_LEVEL_TX extends TG(8, "колич. заданий")
  case object GROUND_TX extends TG(9, "покупка земли") //
  case object BARN_UPGRADE_TX extends TG(10, "прокачка амбара") //
  case object CLAN_TX extends TG(11, "покупка княжества") //
  case object ACTIVE_HEROES_TX extends TG(12, "увеличение числа активных героев") //
  case object MISSIONS_REFRESH_TX extends TG(13, "обновить список поручений") //
  case object MISSIONS_ADD_TX extends TG(14, "дополнить список поручений") //
  case object MISSIONS_MAX_TX extends TG(15, "увеличить максимальное количество поручений в списке") //
  case object INGREDIENTS_TX extends TG(16, "докупка ингредиентов за рубины") //
  case object SHOP_PURCHASE_TX extends TG(17, "покупка в магазине") //
  case object REFRESH_BUY_LIST_TX extends TG(18, "обновить список на рынке") //
  case object REFRESH_SELL_LIST_TX extends TG(19, "обновить список в магазине") //
  case object TASKS_ADD_TX extends TG(20, "дополнить список заданий") //
  case object TASKS_MAX_TX extends TG(21, "увеличить максимальное количество заданий в списке") //
  case object BUY_HERO_TX extends TG(22, "покупка героя") //
  case object MAKE_BUILDING extends TG(23, "постройка загона или производства") //
  case object BUY_PET extends TG(24, "покупка животного") //
  case object FOOD_FOR_PET extends TG(25, "покупка еды для животного за рубины") //
  case object FEED_ALL extends TG(26, "покормить всех животных") //
  case object BUFF_TX extends TG(27, "покупка в магазие (баффы)") //
  case object BANK_UPGRADE_TX extends TG(28, "апгрейд банка") //
  case object BANK_CLAIM_TX extends TG(29, "забрать рубины из банка") //
  case object BILLING_INCOME_TX extends TG(30, "платёж") //
  case object BUY_KEY_TX extends TG(31, "покупка ключа") //
  case object FROM_CHEST_TX extends TG(32, "выпало из сундука") //
  case object CLAN_FUND_TX extends TG(33, "взнос в кассу княжества") //
  case object LEVEL_UP_BONUS_TX extends TG(34, "бонус за каждый 10 взятый уровень") //
  case object UPGRADE_HERO_TX extends TG(35, "добавление абилки герою") //
  case object BILLING_ROLLBACK_TX extends TG(36, "Отмена платежа") //
  case object KOSHEI_DUST_CLAIM_TX extends TG(37, "Получить бесплатную пыль") //
  case object ACTION_COINS_TX extends TG(38, "Монеты по акции") //
  case object REPUTATION_COINS_TX extends TG(39, "Монеты за взятый уровень репутации") //
  case object REPUTATION_DUST_TX extends TG(40, "Пыль за взятый уровень репутации") //
  case object VIC_DAY_GIFT_TX extends TG(41, "+50руб +1000пыли в честь дня победы") //

  // доплата за разное
  case object REST_UPGRADE_MISSIONS_LEVEL_TX extends TG(201, "доплата")
  case object REST_GROUND_TX extends TG(202, "доплата за землю")
  case object REST_TASKS_MAX_TX extends TG(203, "доплата за макс. кол. заданий")
  case object REST_HERO extends TG(204, "доплата за героя")
  case object REST_BUILDING extends TG(205, "доплата за здание")
  case object REST_PET extends TG(206, "доплата за покупку животного") //
  case object REST_SHOP_PURCHASE_TX extends TG(207, "доплата за покупку ингредиентов") //

  // ускорения
  case object ENFORCE_MISSION extends TG(301, "ускорение поручения") //
  case object ENFORCE_GROUND extends TG(302, "ускорение созревания") //
  case object ENFORCE_PET extends TG(303, "ускорить животину") //
  case object ENFORCE_PETS extends TG(304, "ускорить всех животных") //
  case object ENFORCE_BUILDING extends TG(305, "ускорить строительство") //
  case object ENFORCE_QUEUE extends TG(306, "ускорить производство") //

  // для княжества
  case object CLAN_CASHDESK_REFILL extends TG(1001, "касса княжества пополнена") //
  case object CLAN_MEMBERS_UPGRADE extends TG(1002, "увеличение макс. количества членов") //
  case object CLAN_RUBY_MINE_CLAIM extends TG(1003, "забрать рубины из шахты") //
  case object CLAN_RUBY_MINE_UPGRADE extends TG(1004, "улучшение генератора рубинов в княжестве") //
  case object CLAN_BUY_STAMP extends TG(1005, "покупка печати") //
}
