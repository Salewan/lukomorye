package model

/**
  * Created by Salewan on 08.06.2017.
  */
case class TransactionType(typeId: Int, param: Option[Int])
