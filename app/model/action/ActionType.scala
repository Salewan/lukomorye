package model.action

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

/**
  * Created by Salewan on 30.06.2017.
  */
sealed trait ActionType extends EnumEntry with Enumerable[Int] {
  val name: String
  val image: String
}

class AT(val typeId: Int, val name: String, val image: String) extends ActionType

object ActionType extends Enum[ActionType] with EnumerationBoilerplate[Int, ActionType] {

  def values = findValues

  override def entries: IndexedSeq[ActionType] = values

  case object ACTION_OFF extends AT(1, "Выключена", "apple.png")
  case object ACTION_BUFF_DISCOUNT_50 extends AT(2, "Скидка в магазине 50% на всё.", "apple.png")
  case object ACTION_MORE_30 extends AT(3, "30% в подарок!", "apple.png")
  case object ACTION_CLUB_HELP_50 extends AT(4, "Помощь Княжествам!", "apple.png")
  case object ACTION_ADDITIONAL_COINS extends AT(5, "Монетная акция!", "apple.png")
  case object ACTION_EXPERIENCE_BUFFS extends AT(6, "+50% к опыту за покупку рубинов.", "apple.png")
  case object ACTION_BANK_UPGRADE_DISCOUNT_50 extends AT(7, "50% скидка на улучшение рубинового банка - DEPRECATED", "apple.png")
  case object ACTION_BUILDINGS_UPGRADE_DISCOUNT_50 extends AT(8, "Скидка на апгрейды Заданий, Поручений, Амбара, Банка - 50%", "apple.png")
}
