package model.buff

/**
  * Created by Salewan on 29.06.2017.
  */
sealed trait BuffFunction

object BuffFunction {

  case class ExpFunction(expMultiplier: Double) extends BuffFunction {
    def apply(baseExp: Long): Long = math.round(baseExp * expMultiplier)
  }

  case class HeroExpFunction(expMultiplier: Double) extends BuffFunction {
    def apply(baseExp: Long): Long = math.round(baseExp * expMultiplier)
  }
}
