package model.buff

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}
import model.buff.BuffFunction.{ExpFunction, HeroExpFunction}
import model.price.RubyPrice
import model.price.discount.impl.BuffHalfPriceByAction
import util.Implicits._

import scala.language.postfixOps

/**
  * Created by Salewan on 28.06.2017.
  */
sealed trait BuffType extends EnumEntry with Enumerable[Int] {
  val name: String
  val image: String
  val desc: String
  val price: RubyPrice
  val buffFunction: BuffFunction
  val maximumDuration: Long = 6 days
  val prolongStep: Long = 1 day
}

class BT(val typeId: Int, val name: String, val image: String, val desc: String, val price: RubyPrice,
         val buffFunction: BuffFunction) extends BuffType

object BuffType extends Enum[BuffType] with EnumerationBoilerplate[Int, BuffType] {
  def values = findValues

  override def entries: IndexedSeq[BuffType] = values

  case object Exp50 extends BT(1, "Заговор мудрости!", "buff_1.png", "Увеличивает опыт на 50% на сутки",
    RubyPrice(50, BuffHalfPriceByAction), ExpFunction(0.5))
  case object Exp100 extends BT(2, "Заклятье мудрости!", "buff_2.png", "Увеличивает опыт на 100% на сутки",
    RubyPrice(100, BuffHalfPriceByAction), ExpFunction(1.0))
  case object HeroExp50 extends BT(3, "Просветление ворона!", "buff_3.png", "Увеличивает опыт получаемый героями на 50% на сутки",
    RubyPrice(50, BuffHalfPriceByAction), HeroExpFunction(0.5))
  case object HeroExp100 extends BT(4, "Просветление филина!", "buff_4.png", "Увеличивает опыт получаемый героями на 100% на сутки",
    RubyPrice(100, BuffHalfPriceByAction), HeroExpFunction(1.0))
}
