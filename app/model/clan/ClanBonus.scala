package model.clan

import util.Implicits._
import util.TimeUtil

trait ClanBonus {

  def typeId: Int = ClanBonus.typeToId(this)

  def name: String = ClanBonus.idToName(typeId)

  def desc: String = ClanBonus.idToDesc(typeId)

  def maxLevel: Int
  def value(level: Int): Double
  def price(level: Int): Int
  def formatValue(d: Double): String
}

object ClanBonus {

  class ClanBonusExt(val maxLevel: Int, _value: Int => Double, _price: Int => Int, formatter: Double => String) extends ClanBonus {
    override def value(level: Int): Double = _value(level)

    override def price(level: Int): Int = _price(level)

    override def formatValue(d: Double): String = formatter(d)
  }

  val p1 = (x: Int) => math.round(2.5 + (x / 2.0)).asInstanceOf[Int]
  val fp = (d: Double) => s"+${(d * 100).asInstanceOf[Int]}%"
  val fn = (d: Double) => s"+${d.asInstanceOf[Int]}"
  val fs = (d: Double) => s"-${TimeUtil.formatTime(d.asInstanceOf[Long])}"


  case object MissionChance extends ClanBonusExt(10, x => 0.02 * x, p1, fp)
  case object ChestPerDay extends ClanBonusExt(10, x => 0.03 * x, p1, fp)
  case object KeyPerDay extends ClanBonusExt(5, x => 0.02 * x, x => x + 4, fp)
  case object Ambar extends ClanBonusExt(10, x => x * 2, p1, fn)
  case object TaskCount extends ClanBonusExt(5, x => x, x => x + 4, fn)
  case object MissionCount extends ClanBonusExt(10, x => x, p1, fn)
  case object ShopSlots extends ClanBonusExt(5, x => x, x => x * 5, fn)
  case object TaskGenSpeedup extends ClanBonusExt(3, x => 1.0 * 1.minute * x, x => x * 10, fs)
  case object MissionGenSpeedup extends ClanBonusExt(3, x => 1.0 * 1.minute * x, x => x * 10, fs)
  case object SaltanBonus extends ClanBonusExt(10, x => 0.05 * x, p1, fp)
  case object PadishahBonus extends ClanBonusExt(10, x => 0.05 * x, p1, fp)
  case object KosheyBonus extends ClanBonusExt(10, x => 0.05 * x, p1, fp)
  case object MissionSendLimit extends ClanBonusExt(5, x => x, x => x + 4, fn)
  case object TaskSendLimit extends ClanBonusExt(5, x => x, x => x + 4, fn)
  case object HeroExp extends ClanBonusExt(10, x => 0.05 * x, p1, fp)

  private def list: List[ClanBonus] = List(
    MissionChance,
    ChestPerDay,
    KeyPerDay,
    Ambar,
    TaskCount,
    MissionCount,
    ShopSlots,
    TaskGenSpeedup,
    MissionGenSpeedup,
    SaltanBonus,
    PadishahBonus,
    KosheyBonus,
    MissionSendLimit,
    TaskSendLimit,
    HeroExp
  )

  private def names: List[String] = List(
    "Успех в поручениях",
    "Шансы получить сундук",
    "Шансы получить ключ",
    "Место в амбаре",
    "Количество заданий",
    "Количество поручений",
    "Слоты на рынке",
    "Ускорение появления заданий",
    "Ускорение появления поручений",
    "Бонус к репутации у Салтана",
    "Бонус к репутации у Падишаха",
    "Бонус к репутации у Кощея",
    "Увеличение пересылки поручений",
    "Увеличение пересылки заданий",
    "Дополнительный опыт героям"
  )

  private def descs: List[String] = List(
    "Увеличивает шанс успеха в поручениях на 2% за каждый уровень. Всего 10 уровней.",
    "Увеличивает шанс найти сундук на 3% за каждый уровень. Всего 10 уровней.",
    "Увеличивает шансы найти ключ. Изначальный шанс найти ключ - 33%, шанс увеличивается на 2% за уровень, до 43% за 5 уровней.",
    "Увеличивает место в амбаре на 2 за каждый уровень. Всего 10 уровней.",
    "Увеличивает максимальное количество заданий на 1 за каждый уровень. Всего 5 уровней.",
    "Увеличивает максимальное количество поручений на 1 за каждый уровень. Всего 10 уровней.",
    "Увеличивает число слотов на рынке на 1 за каждый каждый уровень. Срабатывает после обновления рынка. Всего 5 уровней.",
    "Ускоряет появление новых заданий на 1 минуту за каждый уровень. Изначально одно задание появляется за 5 минут. Увеличивается за 3 уровня до одного задания за 2 минуты.",
    "Ускоряет появление новых поручений на 1 минуту за каждый уровень. Изначально одно поручение появляется за 5 минут. Увеличивается за 3 уровня до одного поручения за 2 минуты.",
    "Увеличивает получаемую репутацию у Салтана на 5% за каждый уровень. Всего 10 уровней. Бонус СУММИРУЕТСЯ с бонусом или штрафом от союза!",
    "Увеличивает получаемую репутацию у Падишаха на 5% за каждый уровень. Всего 10 уровней. Бонус СУММИРУЕТСЯ с бонусом или штрафом от союза!",
    "Увеличивает получаемую репутацию у Кощея на 5% за каждый уровень. Всего 10 уровней. Бонус СУММИРУЕТСЯ с бонусом или штрафом от союза!",
    "Увеличивает количество пересылаемых поручений на 1 за каждый уровень. Всего 5 уровней.",
    "Увеличивает количество пересылаемых заданий на 1 за каждый уровень. Всего 5 уровней.",
    "Увеличивает получаемый героями опыт на 5% за каждый уровень. Всего 10 уровней. Бонус СУММИРУЕТСЯ с бонусом от заклинаний просветления!"
  )

  private def ids = 1 to list.size
  private def typeToId = list.zip(ids).toMap
  private def idToType = ids.zip(list).toMap
  private def idToName = ids.zip(names).toMap
  private def idToDesc = ids.zip(descs).toMap

  def getAll: Seq[ClanBonus] = list
  def find(id: Int): Option[ClanBonus] = idToType.get(id)
}
