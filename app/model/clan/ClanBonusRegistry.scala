package model.clan

import java.util.concurrent.ConcurrentHashMap

import bean.Clan
import model.clan.ClanBonusRegistry.{clearedSet, registry}

class ClanBonusRegistry(val clan: Clan) {

  def getInitialState: Map[Int, Int] = ClanBonus.getAll.map(cb => cb.typeId -> 0).toMap
  def getState: Map[Int, Int] = {
    Option(registry.get(clan.id)) orElse
      Option(clan.bonuses.asImmutableMap).filter(_.nonEmpty) getOrElse
      getInitialState
  }

  def isEditing: Boolean = registry.containsKey(clan.id)

  def edit(): Unit = registry.put(clan.id, getState)

  def clear(): Unit = {
    registry.put(clan.id, getInitialState)
    clearedSet.add(clan.id)
  }

  def cancel(): Unit = {
    registry.remove(clan.id)
    clearedSet.remove(clan.id)
  }

  def submit(): Unit = {
    clan.bonuses.clear()
    clan.bonuses.putAll(getState)
    registry.remove(clan.id)
    if (clearedSet.contains(clan.id)) {
      clearedSet.remove(clan.id)
      clan.bonusRedistTime(Some(System.currentTimeMillis()))
    }
  }

  def valueOf(clanBonus: ClanBonus): Double = clanBonus.value(getState(clanBonus.typeId))
  def formattedString(clanBonus: ClanBonus): String = clanBonus.formatValue(valueOf(clanBonus))

  def pointsRest: Int = {
    val max = clan.stamps
    val state = getState
    max - ClanBonus.getAll.map {cb =>
      val curLvl = state(cb.typeId)
      (1 to curLvl).map(cb.price).sum
    }.sum
  }

  def currentLevel(clanBonus: ClanBonus): Int = getState(clanBonus.typeId)

  def increment(clanBonus: ClanBonus): Unit = {
    val curLvl = currentLevel(clanBonus)
    if (clanBonus.maxLevel <= curLvl) throw new RuntimeException("Exceeding the maximum bonus level")
    if (pointsRest < clanBonus.price(curLvl + 1)) throw new RuntimeException("Not enough points to upgrade bonus")
    registry.put(clan.id, getState.updated(clanBonus.typeId, curLvl + 1))
  }
}

object ClanBonusRegistry {

  val registry = new ConcurrentHashMap[Int, Map[Int, Int]]()
  val clearedSet: java.util.Set[Int] = ConcurrentHashMap.newKeySet()
}
