package model.hero

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

import scala.collection.immutable.IndexedSeq

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.10.2016.
 */
sealed trait Ability extends EnumEntry with Enumerable[Int] {
  val name: String
  val image: String
}

class AbilityTemplate(val typeId: Int, val name: String) extends Ability {
  override val image: String = "ability.png"
}

object Ability extends Enum[Ability] with EnumerationBoilerplate[Int, Ability] {

  def values = findValues

  override def entries: IndexedSeq[Ability] = values

  case object Mudrost extends AbilityTemplate(1, "Мудрость")
  case object Krasavitsa extends AbilityTemplate(2, "Красавица")
  case object Krasavets extends AbilityTemplate(3, "Красавец")
  case object Nezametnost extends AbilityTemplate(4, "Незаметность")
  case object Sila extends AbilityTemplate(5, "Сила")
  case object Zolotye_ruki extends AbilityTemplate(6, "Золотые руки")
  case object Lovkost extends AbilityTemplate(7, "Ловкость")
  case object Hitrost extends AbilityTemplate(8, "Хитрость")
  case object Ustrashenie extends AbilityTemplate(9, "Устрашение")
  case object Terpenie extends AbilityTemplate(10, "Терпение")
  case object Umelitsa extends AbilityTemplate(11, "Умелица")
}
