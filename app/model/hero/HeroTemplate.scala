package model.hero

import bean.GameUser
import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum._
import model.ReputationModel
import model.hero.Ability._
import model.price.{Price, RubyPrice}

/**
 * @author Sergey Lebedev (salewan@gmail.com) 11.10.2016.
 */

sealed trait HeroTemplate extends EnumEntry with Enumerable[Int] with CanBuy {
  val name: String
  val image: String = s"heroes/$typeId.png"
  val image48: String = s"heroes/${typeId}_48.png"
  val image160: String = s"heroes/$typeId.png"
  val level: Int
  val availableAbilities: Array[Ability]
  val initialAbilities: Array[Ability]
  val price: Price
}



trait CanBuy {
  def canBuy(user: GameUser): Boolean
}
trait ForLevel extends CanBuy {
  this: HeroTemplate =>
  override def canBuy(user: GameUser): Boolean = user.level >= level
}
trait SaltanRep1st extends CanBuy {
  override def canBuy(user: GameUser): Boolean = user.reputation.get(ReputationModel.SALTAN.typeId).exists(_.level > 3)
}
trait KosheiRep1st extends CanBuy {
  override def canBuy(user: GameUser): Boolean = user.reputation.get(ReputationModel.KOSHEY.typeId).exists(_.level > 3)
}
trait PadishahRep1st extends CanBuy {
  override def canBuy(user: GameUser): Boolean = user.reputation.get(ReputationModel.PADISHAH.typeId).exists(_.level > 3)
}




abstract class CaseHero(
                val typeId: Int,
                val name: String,
                val level: Int,
                val price: Price,
                initialAbility: Ability,
                moreAbilities: Ability*
              ) extends HeroTemplate {

  override val initialAbilities: Array[Ability] = Array(initialAbility)

  override val availableAbilities: Array[Ability] = initialAbilities ++ moreAbilities
}






object HeroTemplate extends Enum[HeroTemplate] with EnumerationBoilerplate[Int, HeroTemplate] {
  val values = findValues
  override def entries: IndexedSeq[HeroTemplate] = values


  case object Kot_uchenyj extends CaseHero(
    1, "Кот учёный", 1, RubyPrice(1),  Mudrost, Nezametnost, Lovkost, Hitrost) with ForLevel

  case object Rusalka extends CaseHero(
    2, "Русалка", 1, RubyPrice(1),  Krasavitsa, Umelitsa, Hitrost, Terpenie) with ForLevel

  case object Leshij extends CaseHero(
    3, "Леший", 1, RubyPrice(1),  Nezametnost, Mudrost, Zolotye_ruki, Hitrost) with ForLevel

  case object Zolushka extends CaseHero(
    4, "Золушка", 5, RubyPrice(50),  Umelitsa, Mudrost, Krasavitsa, Terpenie) with ForLevel

  case object Domovoy extends CaseHero(
    5, "Домовой", 5, RubyPrice(50),  Zolotye_ruki, Mudrost, Nezametnost, Hitrost) with ForLevel

  case object Seriy_volk extends CaseHero(
    7, "Серый волк", 10, RubyPrice(50),  Lovkost, Nezametnost, Ustrashenie, Hitrost) with ForLevel

  case object Velikan extends CaseHero(
    8, "Великан", 10, RubyPrice(50),  Sila, Ustrashenie, Zolotye_ruki, Krasavets) with ForLevel

  case object Gusar extends CaseHero(
    9, "Гусар", 12, RubyPrice(50),  Krasavets, Sila, Lovkost, Ustrashenie) with ForLevel

  case object Mishka extends CaseHero(
    6, "Мишка косолапый", 20, RubyPrice(50),  Terpenie, Sila, Mudrost, Ustrashenie) with ForLevel

  case object Troll extends CaseHero(
    10, "Лесной тролль", 20, RubyPrice(50), Ustrashenie, Sila, Lovkost, Nezametnost) with ForLevel

  case object Jar_prica extends CaseHero(
    11, "Жар Птица", 30, RubyPrice(75), Krasavitsa, Umelitsa, Hitrost, Terpenie) with ForLevel

  case object Djin extends CaseHero(
    13, "Джинн", 30, RubyPrice(75), Krasavets, Hitrost, Zolotye_ruki) with ForLevel

  case object Gnom extends CaseHero(
    15, "Гном", 30, RubyPrice(75), Zolotye_ruki, Sila, Mudrost) with ForLevel


  // Награда за репутацию

  case object Vityaz extends CaseHero(
    16, "Витязь", 60, RubyPrice(75), Sila, Terpenie, Lovkost, Ustrashenie) with PadishahRep1st

  case object Efreet extends CaseHero(
    17, "Ифрит", 60, RubyPrice(50), Hitrost, Sila, Krasavets, Zolotye_ruki) with PadishahRep1st

  case object Deiw extends CaseHero(
    12, "Дэв", 60, RubyPrice(75), Ustrashenie, Mudrost, Sila, Krasavets) with KosheiRep1st

  case object Prividenie extends CaseHero(
    18, "Привидение", 60, RubyPrice(50), Hitrost, Nezametnost, Lovkost, Ustrashenie) with KosheiRep1st

  case object Lisa extends CaseHero(
    19, "Лисичка-сестричка", 60, RubyPrice(50), Hitrost, Mudrost, Krasavitsa, Lovkost) with SaltanRep1st

  case object Feya extends CaseHero(
    14, "Фея", 60, RubyPrice(75), Umelitsa, Krasavitsa, Nezametnost, Terpenie) with SaltanRep1st

  def forSale(gameUser: GameUser): Seq[HeroTemplate] = values.filter(t =>
    !gameUser.heroes.containsKey(t.typeId) && t.canBuy(gameUser))
}
