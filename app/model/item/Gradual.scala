package model.item

/**
  * @author Sergey Lebedev (salewan@gmail.com) 10.03.2017.
  */
trait Gradual {

  val duration: Long
}
