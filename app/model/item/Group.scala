package model.item

import play.api.mvc.Call
import controllers.routes
import model.Building

/**
  * @author Sergey Lebedev (salewan@gmail.com) 16.11.2016.
  *         Для классификации игровых предметов
  */
class Group(n: Int) {

  val step = 1000

  val idStartsFrom: Int = n * step

  def contains(test: Int): Boolean = test > idStartsFrom && test < (idStartsFrom + step)

  def sourceLabel: Option[String] = None
  def sourceAnchor(arg: Ingredient): Option[Call] = None
}

object Group {

  case object Seeds extends Group(0) {
    override def sourceLabel: Option[String] = Some("выращивать")

    override def sourceAnchor(arg: Ingredient): Option[Call] = Some(routes.OgorodController.ogorodPage())
  }

  case object Potions extends Group(1)

  case object Things extends Group(2)

  case object UnlimitedIngs extends Group(3)


  case object LivestockGroup extends Group(5) {
    override def sourceLabel: Option[String] = Some("производить")

    override def sourceAnchor(arg: Ingredient): Option[Call] =
      Building.petHouses.find(_.product.typeId == arg.typeId).
        map(building => routes.PetFarmController.petfarmPage(building.typeId))
  }
  case object FactoryGroup extends Group(5) {
    override def sourceLabel: Option[String] = Some("производить")

    override def sourceAnchor(arg: Ingredient): Option[Call] =
      Building.prodBuildings.find(_.products.exists(_.typeId == arg.typeId)).
        map(building => routes.ProductionController.productionPage(building.typeId))
  }
}
