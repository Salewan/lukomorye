package model.item

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.10.2016.
  */
trait Ingredient {

  val typeId: Int

  val name: String

  val image: String

  val level: Int

  val group: Group

  val nominal: Long

  def farmingExp: Long = nominal / 10

  def taskExp: Long = nominal / 2

  val tutorialFlag: Boolean = false

  protected def id(arg: Int): Int = group.idStartsFrom + arg

  val unlimited: Boolean = false
}

object Ingredient {

  val ingredients: Array[Ingredient] = Seed.allSeeds ++ Thing.entries ++ UnlimitedIng.unlimitedIngs ++ Product.all

  val ingredientsMap: Map[Int, Ingredient] = ingredients map (i => i.typeId -> i) toMap

  def all = ingredients

  def find(typeId: Int): Option[Ingredient] = ingredientsMap.get(typeId)

  def apply(typeId: Int): Ingredient = ingredientsMap(typeId)
}
