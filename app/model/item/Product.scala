package model.item

import model.item.Seed._
import model.item.Thing._
import util.Implicits._

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 13.01.2017.
  */
trait Product extends Ingredient with Gradual {

  val composition: Seq[(Ingredient, Int)]
}

trait Livestock {
  this: Product =>
  override val group: Group = Group.LivestockGroup
}

trait Factory {
  this: Product =>
  override val group: Group = Group.FactoryGroup
}

object Product {

  // Pet food
  case object IZBUSHKA_FOOD extends Product with Factory {
    override val typeId: Int = id(1)
    override val level: Int = config.NASEST_LEVEL
    override val name: String = "корм избушек"
    override val image: String = "pets/food1_128.png"
    override val composition: Seq[(Ingredient, Int)] = Seq(Solnechnik -> 2, TravaMurova -> 1)
    override val duration: Long = 6 minutes
    override val nominal: Long = 60
  }
  case object BUIVOL_FOOD extends Product with Factory {
    override val typeId: Int = id(2)
    override val level: Int = config.HLEV_LEVEL
    override val name: String = "корм буйволиц"
    override val image: String = "pets/food2_128.png"
    override val composition: Seq[(Ingredient, Int)] = Seq(TravaMurova -> 3)
    override val duration: Long = 10 minutes
    override val nominal: Long = 90
  }
  case object VASILISK_FOOD extends Product with Factory {
    override val typeId: Int = id(3)
    override val level: Int = config.PESHHERA_LEVEL
    override val name: String = "корм василисков"
    override val image: String = "pets/food3_128.png"
    override val composition: Seq[(Ingredient, Int)] = Seq(Koshmarnik -> 2, TravaMurova -> 1)
    override val duration: Long = 20 minutes
    override val nominal: Long = 150
  }
  case object ARHAR_FOOD extends Product with Factory {
    override val typeId: Int = id(4)
    override val level: Int = config.ZAGON_LEVEL
    override val name: String = "корм архаров"
    override val image: String = "pets/food4_128.png"
    override val composition: Seq[(Ingredient, Int)] = Seq(Solnechnik -> 3, TravaMurova -> 1)
    override val duration: Long = 30 minutes
    override val nominal: Long = 150
  }


  // Pet products
  case object EGG extends Product with Livestock {
    override val typeId: Int = id(6)
    override val level: Int = 3
    override val name: String = "яйцо избушки"
    override val image: String = "misc3/p_6_128.png"
    override val nominal: Long = 260
    override val duration: Long = 20 minutes
    override val composition: Seq[(Ingredient, Int)] = Seq.empty
  }

  case object MILK extends Product with Livestock {
    override val typeId: Int = id(7)
    override val level: Int = 10
    override val name: String = "радужное молоко"
    override val image: String = "misc3/p_7_128.png"
    override val nominal: Long = 690
    override val duration: Long = 60 minutes
    override val composition: Seq[(Ingredient, Int)] = Seq.empty
  }

  case object TAIL extends Product with Livestock {
    override val typeId: Int = id(8)
    override val level: Int = 20
    override val name: String = "хвост василиска"
    override val image: String = "misc3/p_8_128.png"
    override val nominal: Long = 2550
    override val duration: Long = 240 minutes
    override val composition: Seq[(Ingredient, Int)] = Seq.empty
  }

  case object WOOL extends Product with Livestock {
    override val typeId: Int = id(9)
    override val level: Int = 31
    override val name: String = "золотое руно"
    override val image: String = "misc3/p_9_128.png"
    override val nominal: Long = 3750
    override val duration: Long = 360 minutes
    override val composition: Seq[(Ingredient, Int)] = Seq.empty
  }


  // Production
  case object KOLOBOK extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Solnechnik -> 3)
    override val level: Int = 2
    override val name: String = "колобок"
    override val image: String = "misc3/p_10_128.gif"
    override val typeId: Int = id(10)
    override val duration: Long = 5 minutes
    override val nominal: Long = 140
  }

  case object RYAZHENKA_PRIVOROTNAYA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MILK -> 1)
    override val level: Int = 12
    override val name: String = "ряженка приворотная"
    override val image: String = "misc3/p_11_128.png"
    override val typeId: Int = id(11)
    override val duration: Long = 20 minutes
    override val nominal: Long = 890
  }

  case object OLADUSHKI_POHIDYASHKI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TravaMurova -> 1, Solnechnik -> 1, EGG -> 2)
    override val level: Int = 13
    override val name: String = "оладушки похудяшки"
    override val image: String = "misc3/p_12_128.png"
    override val typeId: Int = id(12)
    override val duration: Long = 30 minutes
    override val nominal: Long = 910
  }

  case object LUNNY_BISER extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(LunnyLotos -> 1)
    override val level: Int = 14
    override val name: String = "лунный бисер"
    override val image: String = "misc3/p_13_128.png"
    override val typeId: Int = id(13)
    override val duration: Long = 20 minutes
    override val nominal: Long = 500
  }

  case object AMULET_UDACHI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(PERO_JAR_PTICI -> 1)
    override val level: Int = 12
    override val name: String = "амулет удачи"
    override val image: String = "misc3/p_14_128.png"
    override val typeId: Int = id(14)
    override val duration: Long = 90 minutes
    override val nominal: Long = 1200
  }

  case object MASLO extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MILK -> 2)
    override val level: Int = 18
    override val name: String = "волшебное масло"
    override val image: String = "misc3/p_15_128.png"
    override val typeId: Int = id(15)
    override val duration: Long = 30 minutes
    override val nominal: Long = 1680
  }

  case object PIROZHKI_BODROSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(EGG -> 2, MOLOD_YABLOCHKI -> 1, LUNNY_BISER -> 1)
    override val level: Int = 19
    override val name: String = "пирожок бодрости"
    override val image: String = "misc3/p_16_128.png"
    override val typeId: Int = id(16)
    override val duration: Long = 30 minutes
    override val nominal: Long = 1520
  }

  case object BLUDCE extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MOLOD_YABLOCHKI -> 1, LUNNY_BISER -> 1)
    override val level: Int = 15
    override val name: String = "блюдце с яблочком"
    override val image: String = "misc3/p_17_128.png"
    override val typeId: Int = id(17)
    override val duration: Long = 60 minutes
    override val nominal: Long = 1300
  }

  case object PECHENKA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Solnechnik -> 2, EGG -> 2, LUNNY_BISER -> 1)
    override val level: Int = 21
    override val name: String = "печенька сновидений"
    override val image: String = "misc3/p_18_128.gif"
    override val typeId: Int = id(18)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 178
  }

  case object JARKOE_ZMEINOI_GIBKOSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TAIL -> 2, EGG -> 2, MILK -> 1)
    override val level: Int = 22
    override val name: String = "жаркое гибкости"
    override val image: String = "misc3/p_19_128.png"
    override val typeId: Int = id(19)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 691
  }

  case object SNADOBIE_VASILISKA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MILK -> 2, TAIL -> 1)
    override val level: Int = 23
    override val name: String = "снадобье василиска"
    override val image: String = "misc3/p_20_128.png"
    override val typeId: Int = id(20)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 453
  }

  case object SUMERECHNAYA_PIL extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(LunnyLotos -> 2)
    override val level: Int = 25
    override val name: String = "сумеречная пыль"
    override val image: String = "misc3/p_21_128.png"
    override val typeId: Int = id(21)
    override val duration: Long = 40 minutes
    override val nominal: Long = 10 * 100
  }

  case object ZELIE_UVERENNOSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Koshmarnik -> 3, Solnechnik -> 2, EGG -> 1)
    override val level: Int = 26
    override val name: String = "зелье уверенности"
    override val image: String = "misc3/p_22_128.png"
    override val typeId: Int = id(22)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 122
  }

  case object ZELIE_OZARENIYA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Muhomor -> 3, Solnechnik -> 2, EGG -> 1)
    override val level: Int = 28
    override val name: String = "зелье озарения"
    override val image: String = "misc3/p_23_128.png"
    override val typeId: Int = id(23)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 692
  }

  case object ZOLOTAYA_SHAL_NEOTRAZIMOSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(WOOL -> 1, PERO_JAR_PTICI -> 1, Ognennaya_roza -> 1)
    override val level: Int = 32
    override val name: String = "золотая шаль"
    override val image: String = "misc3/p_24_128.png"
    override val typeId: Int = id(24)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 585
  }

  case object KOVER_SAMOLET extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(VOLSHEBNIY_KLUBOCHEK -> 1, WOOL -> 1)
    override val level: Int = 34
    override val name: String = "ковёр-самолёт"
    override val image: String = "misc3/p_25_128.png"
    override val typeId: Int = id(25)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 585
  }

  case object ZVEZDNAYA_ESENCIYA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(LunnyLotos -> 4)
    override val level: Int = 35
    override val name: String = "звёздная эссенция"
    override val image: String = "misc3/p_26_128.png"
    override val typeId: Int = id(26)
    override val duration: Long = 90 minutes
    override val nominal: Long = 10 * 210
  }

  case object TKAN_NOCHNOI_SVEHZESTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(ElfiskyLen -> 3)
    override val level: Int = 38
    override val name: String = "ткань ночной свежести"
    override val image: String = "misc3/p_27_128.png"
    override val typeId: Int = id(27)
    override val duration: Long = 30 minutes
    override val nominal: Long = 10 * 480
  }

  case object SHASHLYK_ZHELEZNOY_SILY extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(KOLOBOK -> 2, TAIL -> 2)
    override val level: Int = 37
    override val name: String = "шашлык железной силы"
    override val image: String = "misc3/p_28_128.png"
    override val typeId: Int = id(28)
    override val duration: Long = 180 minutes
    override val nominal: Long = 10 * 718
  }

  case object ELEKSIR_DIAVOLSKOY_HITROSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TAIL -> 2, LunnyLotos -> 1, EGG -> 1)
    override val level: Int = 39
    override val name: String = "эликсир хитрости"
    override val image: String = "misc3/p_49_128.png"
    override val typeId: Int = id(49)
    override val duration: Long = 180 minutes
    override val nominal: Long = 10 * 722
  }

  case object VESELIY_KEKSIK extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(EGG -> 1, Solnechnik -> 2, KalinkaMalinka -> 1)
    override val level: Int = 40
    override val name: String = "весёлый кексик"
    override val image: String = "misc3/p_29_128.png"
    override val typeId: Int = id(29)
    override val duration: Long = 45 minutes
    override val nominal: Long = 10 * 347
  }

  case object OGNENNIE_RUKOVICI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Ognennaya_roza -> 1, VOLSHEBNIY_KLUBOCHEK -> 1)
    override val level: Int = 41
    override val name: String = "огненные рукавицы"
    override val image: String = "misc3/p_30_128.png"
    override val typeId: Int = id(30)
    override val duration: Long = 90 minutes
    override val nominal: Long = 10 * 300
  }

  case object PLATIE_VESENNEY_SVEZHESTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TKAN_NOCHNOI_SVEHZESTI -> 2, ZVEZDNAYA_ESENCIYA -> 1)
    override val level: Int = 41
    override val name: String = "платье свежести"
    override val image: String = "misc3/p_31_128.png"
    override val typeId: Int = id(31)
    override val duration: Long = 45 minutes
    override val nominal: Long = 10 * 1215
  }

  case object TCARSKAYA_DUBLENKA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(WOOL -> 2, Ognennaya_roza -> 2)
    override val level: Int = 42
    override val name: String = "царская дублёнка"
    override val image: String = "misc3/p_32_128.png"
    override val typeId: Int = id(32)
    override val duration: Long = 180 minutes
    override val nominal: Long = 10 * 1170
  }

  case object VAFLI_NOCHNIH_KOSHMAROV extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Koshmarnik -> 2, LUNNY_BISER -> 1, Muhomor -> 1)
    override val level: Int = 43
    override val name: String = "вафельки кошмариков"
    override val image: String = "misc3/p_33_128.png"
    override val typeId: Int = id(33)
    override val duration: Long = 90 minutes
    override val nominal: Long = 10 * 340
  }

  case object BOYARSKII_KAMZOL extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TKAN_NOCHNOI_SVEHZESTI -> 1, WOOL -> 3)
    override val level: Int = 43
    override val name: String = "дивный камзол"
    override val image: String = "misc3/p_34_128.png"
    override val typeId: Int = id(34)
    override val duration: Long = 90 minutes
    override val nominal: Long = 10 * 1695
  }

  case object PIROJENKA_NOCHNOI_STRASTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(RYAZHENKA_PRIVOROTNAYA -> 1, SUMERECHNAYA_PIL -> 1, Solnechnik -> 3)
    override val level: Int = 45
    override val name: String = "пироженка страсти"
    override val image: String = "misc3/p_35_128.png"
    override val typeId: Int = id(35)
    override val duration: Long = 180 minutes
    override val nominal: Long = 10 * 378
  }

  case object PRIMANKA_OBOROTNEY extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(VolchyaYagodka -> 2, EGG -> 1, TAIL -> 1)
    override val level: Int = 44
    override val name: String = "приманка оборотней"
    override val image: String = "misc3/p_36_128.png"
    override val typeId: Int = id(36)
    override val duration: Long = 60 minutes
    override val nominal: Long = 10 * 1121
  }

  case object SNADOBIE_ASTRALNIH_PUTESHESTVII extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(SNADOBIE_VASILISKA -> 1, PECHENKA -> 1)
    override val level: Int = 46
    override val name: String = "астральное снадобье"
    override val image: String = "misc3/p_37_128.png"
    override val typeId: Int = id(37)
    override val duration: Long = 240 minutes
    override val nominal: Long = 10 * 861
  }

  case object OBEREG_OT_SGLAZA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(ZLATO_SEREBRO -> 1, Ognennaya_roza -> 1)
    override val level: Int = 46
    override val name: String = "оберег от сглаза"
    override val image: String = "misc3/p_38_128.png"
    override val typeId: Int = id(38)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 420
  }

  case object OJERELIE_PROTIVOVAMPIRSKOE extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(ChesnokYadrenisty -> 2, PERO_JAR_PTICI -> 1)
    override val level: Int = 48
    override val name: String = "ожерелье противовампирское"
    override val image: String = "misc3/p_39_128.png"
    override val typeId: Int = id(39)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 630
  }

  case object KOLCHUJKA_MIFRILNAYA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MIFRIL -> 2, SUMERECHNAYA_PIL -> 2)
    override val level: Int = 48
    override val name: String = "кольчужка мифрильная"
    override val image: String = "misc3/p_40_128.png"
    override val typeId: Int = id(40)
    override val duration: Long = 360 minutes
    override val nominal: Long = 10 * 1040
  }

  case object KRUJEVNAYA_SOROCHKA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(TKAN_NOCHNOI_SVEHZESTI -> 2, Ognennaya_roza -> 1, PERO_JAR_PTICI -> 2)
    override val level: Int = 49
    override val name: String = "платье феи"
    override val image: String = "misc3/p_41_128.png"
    override val typeId: Int = id(41)
    override val duration: Long = 135 minutes
    override val nominal: Long = 10 * 1275
  }

  case object KEKSIK_KOSHEISKI_ZLODEISKI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Chernoknizhnitsa -> 1, EGG -> 2, Koshmarnik -> 2)
    override val level: Int = 50
    override val name: String = "кексик злодейский"
    override val image: String = "misc3/p_42_128.png"
    override val typeId: Int = id(42)
    override val duration: Long = 48 minutes
    override val nominal: Long = 10 * 600
  }

  case object NASTOYKA_HRABROSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Koshmarnik -> 2, MILK -> 1)
    override val level: Int = 51
    override val name: String = "настройка храбрости"
    override val image: String = "misc3/p_43_128.png"
    override val typeId: Int = id(43)
    override val duration: Long = 30 minutes
    override val nominal: Long = 10 * 119
  }

  case object KANOPE_PROTIVOKIKIMORNOYE extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(Solnechnik -> 2, ChesnokYadrenisty -> 2, MAGIC_GEMCHUG -> 1)
    override val level: Int = 51
    override val name: String = "канопэ противокикиморное"
    override val image: String = "misc3/p_44_128.png"
    override val typeId: Int = id(44)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 726
  }

  case object SNADOBIE_VECHNOI_ZHIZNI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MOLOD_YABLOCHKI -> 2, ZVEZDNAYA_ESENCIYA -> 1, BessmertnikZolotisty -> 2)
    override val level: Int = 52
    override val name: String = "снадобье жизни"
    override val image: String = "misc3/p_45_128.png"
    override val typeId: Int = id(45)
    override val duration: Long = 150 minutes
    override val nominal: Long = 10 * 860
  }

  case object SNADOBIE_USPEHA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(EGG -> 2, MAGIC_GEMCHUG -> 2, AMULET_UDACHI -> 2)
    override val level: Int = 52
    override val name: String = "снадобье успеха"
    override val image: String = "misc3/p_46_128.png"
    override val typeId: Int = id(46)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 652
  }

  case object NASTOYKA_VECHNOY_UNOSTI extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(MOLOD_YABLOCHKI -> 2, Ognennaya_roza -> 2)
    override val level: Int = 53
    override val name: String = "настойка юности"
    override val image: String = "misc3/p_47_128.png"
    override val typeId: Int = id(47)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 380
  }

  case object VOLSHEBNAYA_PALOCHKA_IZLECHENIYA extends Product with Factory {
    override val composition: Seq[(Ingredient, Int)] = Seq(BessmertnikZolotisty -> 1, MILK -> 1, SUMERECHNAYA_PIL -> 1)
    override val level: Int = 53
    override val name: String = "палочка излечения"
    override val image: String = "misc3/p_48_128.png"
    override val typeId: Int = id(48)
    override val duration: Long = 120 minutes
    override val nominal: Long = 10 * 529
  }





  private val allProducts = Array(IZBUSHKA_FOOD, BUIVOL_FOOD, VASILISK_FOOD, ARHAR_FOOD,
    EGG, MILK, TAIL, WOOL,KOLOBOK,RYAZHENKA_PRIVOROTNAYA,OLADUSHKI_POHIDYASHKI,LUNNY_BISER,
    AMULET_UDACHI,MASLO,PIROZHKI_BODROSTI,BLUDCE,PECHENKA,JARKOE_ZMEINOI_GIBKOSTI,SNADOBIE_VASILISKA,
    SUMERECHNAYA_PIL, ZELIE_UVERENNOSTI, ZELIE_OZARENIYA, ZOLOTAYA_SHAL_NEOTRAZIMOSTI,
    KOVER_SAMOLET, ZVEZDNAYA_ESENCIYA, TKAN_NOCHNOI_SVEHZESTI, SHASHLYK_ZHELEZNOY_SILY, VESELIY_KEKSIK,
    OGNENNIE_RUKOVICI, PLATIE_VESENNEY_SVEZHESTI, TCARSKAYA_DUBLENKA, VAFLI_NOCHNIH_KOSHMAROV,
    BOYARSKII_KAMZOL, PIROJENKA_NOCHNOI_STRASTI, PRIMANKA_OBOROTNEY, SNADOBIE_ASTRALNIH_PUTESHESTVII,
    OBEREG_OT_SGLAZA, OJERELIE_PROTIVOVAMPIRSKOE, KOLCHUJKA_MIFRILNAYA, KRUJEVNAYA_SOROCHKA,
    KEKSIK_KOSHEISKI_ZLODEISKI, NASTOYKA_HRABROSTI, KANOPE_PROTIVOKIKIMORNOYE, SNADOBIE_VECHNOI_ZHIZNI,
    SNADOBIE_USPEHA, NASTOYKA_VECHNOY_UNOSTI, VOLSHEBNAYA_PALOCHKA_IZLECHENIYA, ELEKSIR_DIAVOLSKOY_HITROSTI)

  private val productsMap: Map[Int, Product] = allProducts.map(p => p.typeId -> p).toMap

  def all = allProducts
  def apply(id: Int): Product = productsMap(id)
  def find(id: Int): Option[Product] = productsMap.get(id)
}
