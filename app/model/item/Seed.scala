package model.item

import util.Implicits._

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 20.10.2016.
  */
trait Seed extends Ingredient with Gradual {
  val group = Group.Seeds
}

class HerbalSeed(val n: Int, val name: String, val nameV: String, val png: String, val level: Int, val duration: Long)
extends Seed {
  override val typeId: Int = id(n)
  override val nominal: Long = duration / (1 minutes) * 10
  val path = if (png.startsWith("misc3")) "" else "crops/round/"
  override val image = path + png
}

object Seed {

  case object Solnechnik extends HerbalSeed(1, "колосок", "колоска", "misc3/s_1_128.png", 4, 3 minutes)
  case object TravaMurova extends HerbalSeed(2, "трава-мурава", "травы-муравы", "misc3/s_2_128.png", 4, 6 minutes)
  case object LunnyLotos extends HerbalSeed(3, "лунный лотос", "лунного лотоса", "misc3/s_3_128.png", 11, 30 minutes)
  case object Koshmarnik extends HerbalSeed(4, "кошмарник", "кошмарника", "misc3/s_4_128.png", 17, 10 minutes)
  case object Ognennaya_roza extends HerbalSeed(5, "огненная роза", "огненной розы", "misc3/s_5_128.png", 24, 120 minutes)
  case object Muhomor extends HerbalSeed(6, "мухомор", "мухомора", "misc3/s_6_128.png", 27, 180 minutes)
  case object BessmertnikZolotisty extends HerbalSeed(7, "бессмертник золотистый", "бессмертника золотистого", "misc3/s_7_128.png", 30, 240 minutes)
  case object ElfiskyLen extends HerbalSeed(8, "эльфийский лён", "эльфийского льна", "misc3/s_8_128.png", 37, 150 minutes)
  case object KalinkaMalinka extends HerbalSeed(9, "калинка-малинка", "калинки-малинки", "misc3/s_9_128.png", 40, 270 minutes)
  case object VolchyaYagodka extends HerbalSeed(10, "волчья ягодка", "волчьей ягодки", "misc3/s_10_128.png", 44, 390 minutes)
  case object ChesnokYadrenisty extends HerbalSeed(11, "чеснок ядрёнистый", "чеснока ядрёнистого", "misc3/s_11_128.png", 47, 240 minutes)
  case object Chernoknizhnitsa extends HerbalSeed(12, "чернокнижница долгорастущая", "чернокнижницы долгорастущей", "misc3/s_12_128.png", 50, 480 minutes)


  val allSeeds = Array[HerbalSeed](
    Solnechnik, TravaMurova, LunnyLotos, Koshmarnik, Ognennaya_roza,
    Muhomor, BessmertnikZolotisty, ElfiskyLen, KalinkaMalinka, VolchyaYagodka, ChesnokYadrenisty,
    Chernoknizhnitsa
  )

  val allSeedsMap: Map[Int, Seed] = allSeeds map (seed => (seed.typeId, seed)) toMap
}
