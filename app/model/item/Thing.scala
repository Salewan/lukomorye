package model.item

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 12.10.2016.
  */
sealed trait Thing extends Ingredient with EnumEntry with Enumerable[Int] {
  override val group: Group = Group.Things

  override val level: Int = 1
}

class ThingCase(paramId: Int, val name: String, val image: String, val nominal: Long, override val level: Int = 1) extends Thing {
  override val typeId: Int = id(paramId)
}

object Thing extends Enum[Thing] with EnumerationBoilerplate[Int, Thing] {

  def values = findValues

  override def entries = values

  case object TEARS_DUST extends ThingCase(1, "пыль грёз", "powder2.png", 10 * 40, 210)
  case object MOLOD_YABLOCHKI extends ThingCase(2, "молодильные яблочки", "misc3/t_2_128.png", 100)
  case object PERO_JAR_PTICI extends ThingCase(3, "перо жар-птицы", "misc3/t_3_128.png", 300, 6)
  case object VOLSHEBNIY_KLUBOCHEK extends ThingCase(4, "волшебный клубочек", "misc3/t_4_128.png", 10 * 120, 33)
  case object ZLATO_SEREBRO extends ThingCase(5, "злато-серебро", "misc3/t_5_128.png", 10 * 180, 45)
  case object MIFRIL extends ThingCase(6, "мифрил", "misc3/t_6_128.png", 10 * 240, 47)
  case object MAGIC_GEMCHUG extends ThingCase(7, "магический жемчуг", "misc3/t_7_128.png", 10 * 120, 51)
}
