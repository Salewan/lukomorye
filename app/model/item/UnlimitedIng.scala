package model.item

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 16.11.2016.
  */
trait UnlimitedIng extends Ingredient {

  override val group: Group = Group.UnlimitedIngs

  override val unlimited: Boolean = true

  override val level: Int = 1

  override def farmingExp: Long = 0

  override def taskExp: Long = 0
}

object UnlimitedIng {

  case object HRONOSFERA extends UnlimitedIng {
    override val typeId: Int = id(1)
    override val name: String = "хроносфера"
    override val image: String = "bfire.png"
    override val nominal: Long = 0
  }

  val unlimitedIngs = Array(HRONOSFERA)

  val unlimitedIngMap: Map[Int, UnlimitedIng] = unlimitedIngs map (i => i.typeId -> i) toMap
}
