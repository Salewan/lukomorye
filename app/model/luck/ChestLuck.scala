package model.luck
import bean.Mission
import components.context.GameContext
import model.Reward

/**
  * Created by Salewan on 11.08.2017.
  */
case object ChestLuck extends Luck {
  override def weight: Double = 3d

  override def apply(mission: Mission)(implicit ctx: GameContext[_]): Reward = {
    mission.owner.addChests(1, mission.generator.typeId)
  }
}
