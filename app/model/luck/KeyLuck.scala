package model.luck
import bean.Mission
import components.context.GameContext
import model.Reward

/**
  * Created by Salewan on 11.08.2017.
  */
case object KeyLuck extends Luck {
  override def weight: Double = 1d

  override def apply(mission: Mission)(implicit ctx: GameContext[_]): Reward = {
    val user = mission.owner
    val reward = Reward()
    if (user.dailyKeys.available(user.getMaxKeysDrop()) > 0 && user.addKeys(1) > 0)  {
      user.dailyKeys.increment(user.getMaxKeysDrop())
      reward.addKeys(1)
    }
    reward
  }
}
