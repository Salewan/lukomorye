package model.luck

import bean.Mission
import components.context.GameContext
import model.Reward

/**
  * Created by Salewan on 11.08.2017.
  */
trait Luck {
  def weight: Double

  def apply(mission: Mission)(implicit ctx: GameContext[_]): Reward
}
