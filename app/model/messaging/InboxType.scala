package model.messaging

sealed trait InboxType {
  val typeId: Int
}

case object EnvelopeMission extends InboxType {
  override val typeId: Int = 1
}

case object EnvelopeMission_Expired extends InboxType {
  override val typeId: Int = 11
}

case object EnvelopeMission_Accepted extends InboxType {
  override val typeId: Int = 111
}

case object EnvelopeMission_Removed extends InboxType {
  override val typeId: Int = 1111
}

case object EnvelopeTask extends InboxType {
  override val typeId: Int = 2
}

case object EnvelopeTask_Expired extends InboxType {
  override val typeId: Int = 22
}

case object EnvelopeTask_Accepted extends InboxType {
  override val typeId: Int = 222
}

case object EnvelopeTask_Removed extends InboxType {
  override val typeId: Int = 2222
}

object Inbox {
  def findInboxType(typeId: Int): Option[InboxType] = typeId match {
    case 1 => Some(EnvelopeMission)
    case 11 => Some(EnvelopeMission_Expired)
    case 111 => Some(EnvelopeMission_Accepted)
    case 1111 => Some(EnvelopeMission_Removed)
    case 2 => Some(EnvelopeTask)
    case 22 => Some(EnvelopeTask_Expired)
    case 222 => Some(EnvelopeTask_Accepted)
    case 2222 => Some(EnvelopeTask_Removed)
    case _ => None
  }
}