package model.mission

import bean.{GameUser, Mission}
import components.context.GameContext
import model.Reward
import model.hero.Ability
import model.item.Ingredient
import model.renderer.ArbitraryBonusRewardRenderer
import play.api.mvc.AnyContent
import util.Implicits._
import util.RandomUtil

import scala.language.postfixOps

/**
  * Created by Salewan on 16.04.2017.
  */
class ItemMissionGenerator(
                                 val typeId: Int,

                                 val level: Int,

                                 val maxLevel: Int,

                                 h_num: Int,

                                 ing: Ingredient,

                                 abil: Map[Ability, Double],

                                 override val name: Option[String],

                                 override val text: Option[String]

                               ) extends MissionGenerator {


  override def generate(gameUser: GameUser): Mission = {
    val duration = ing.nominal * ingNum * (1 minutes) / 10 // время поручения
    val difficulty = h_num * (math.max(1, level - 3) + 10) // сложность
    val mission = Mission().
      questor(questor).
      owner(gameUser).
      generator(typeId).
      artefact(ing).
      duration(duration).
      heroesNum(h_num).
      level(level).
      difficulty(difficulty)
    mission.abilities.putAll(abil.toSeq.map({case (a, p) => a.typeId -> p}))
    mission.asInstanceOf[Mission]
  }


  private var heroExpMult: Double = _
  override def getHeroExpMultiplier: Double = heroExpMult

  override def reward(mission: Mission, win: Boolean, bonus: Boolean)(implicit ctx: GameContext[AnyContent]): Reward = {
    val arbitraryBonus = win && RandomUtil.throwTheDice(0.03) // arbitrary bonus LUK-304
    if (arbitraryBonus) heroExpMult = 3.0
    else heroExpMult = 1.0

    val reward = super.reward(mission, win, bonus)
    for (artifact <- mission.artefact if win) {
      reward += mission.owner.addExp(artifact.farmingExp * ingNum)
      reward += mission.owner.addIngredient(artifact, ingNum)

      if (arbitraryBonus) {
        reward += mission.owner.addIngredient(artifact, 2) // additional 2 items according to LUK-304
        reward.addTemplateRenderer(ArbitraryBonusRewardRenderer(artifact, mission.participatedHeroesCount))
      }
    }

    reward
  }
}
