package model.mission

import bean.{GameUser, Mission}
import components.context.GameContext
import model.ReputationModel.SALTAN
import model.hero.Ability._
import model.item.{Ingredient, Product, Seed, Thing}
import model.luck.{ChestLuck, KeyLuck, Luck}
import model.{ReputationModel, Reward}
import play.api.mvc.AnyContent
import util.Implicits._
import util.RandomUtil

import scala.language.postfixOps
import Questor._
import model.clan.ClanBonus

/**
  * Created by Salewan on 14.04.2017.
  */
trait MissionGenerator {

  type T = this.type
  val typeId: Int
  val level: Int
  val maxLevel: Int
  val reputationType: ReputationModel = SALTAN
  val name: Option[String] = None
  val text: Option[String] = None
  var image: Option[String] = None
  var bonus: Option[(Ingredient, Int)] = None
  var weight: Double = 0.1
  var expires: Long = 1440 minutes
  var ingNum: Int = 1
  var questor: Option[Questor] = None

  def generate(gameUser: GameUser): Mission

  def getHeroExpMultiplier: Double = 1.0

  def reward(mission: Mission, win: Boolean, bonus: Boolean)(implicit ctx: GameContext[AnyContent]): Reward = {
    var reward = Reward()
    reward += mission.giveExperienceToHeroes(win, getHeroExpMultiplier)
    for ((ing, x) <- this.bonus if bonus) {
      reward += mission.owner.addIngredient(ing, x)
    }

    val chestChance = math.max(0d, mission.chance - 1d) + ctx.gameUser.clan.map(_.valueOf(ClanBonus.ChestPerDay)).getOrElse(0.0)
    if (chestChance > 0d && RandomUtil.throwTheDice(chestChance)) {
      val keySeed = 0.33 + ctx.gameUser.clan.map(_.valueOf(ClanBonus.KeyPerDay)).getOrElse(0.0)
      if (RandomUtil.throwTheDice(keySeed)) reward += KeyLuck.apply(mission)
      else reward += ChestLuck.apply(mission)
    }
    reward
  }

  def bonus(ingredient: Ingredient, n: Int): MissionGenerator = {
    bonus = Some(ingredient -> n)
    this
  }

  def weight(arg: Double): T = {
    weight = arg
    this
  }

  def expires(arg: Long): MissionGenerator = {
    expires = arg
    this
  }

  def image(arg: String): MissionGenerator = {
    image = Some(arg)
    this
  }

  def ingNum(arg: Int): T = {
    ingNum = arg
    this
  }

  def questor(arg: Questor): T = {
    questor = Some(arg)
    this
  }
}

object MissionGenerator {

  private val gen_arr = Vector(
    // (Олег)
    new ItemMissionGenerator(401, 4, 1000, 1, Product.IZBUSHKA_FOOD, Map(Mudrost -> 1.0),
      Some("Мельник и чертежи"), Some("Решил мельник рядом с ветряной и водяную мельницу поставить. Ветер то есть, то нет, а вода в ручье течёт себе и течёт. Нужен мельнику грамотей, кто бы в чертежах разобрался."))
      .weight(0.1).ingNum(2).questor(MELNIK),
    new ItemMissionGenerator(402, 4, 1000, 1, Product.KOLOBOK, Map(Krasavitsa -> 1.0, Hitrost -> 1.0),
      Some("Мельника бросила жена"), Some("От мельника жена ушла, и с горя мельник уже неделю в запое. Мельница стоит, а в деревне мука заканчивается. Мельника нужно утешить и уговорить вернуться к жизни."))
      .weight(0.1).ingNum(2).questor(MELNIK),
    new ItemMissionGenerator(404, 4, 1000, 1, Product.EGG, Map(Nezametnost -> 1.0),
      Some("Баба Яга и секрет Кощея"), Some("Баба Яга прознала, что Кощей замыслил перепрятать свою смерть, потому что все уже знают, где её искать. Надо бы проследить за Кощеюшкой. Хоть Кощей и родственная душа и тоже сила нечистая, но на всякий случай. А случай бывает всякий..."))
      .weight(0.1).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(405, 4, 1000, 1, Product.EGG, Map(Mudrost -> 1.0),
      Some("Баба Яга и новая метла"), Some("Ивашка из дворца пионеров подарил Бабе Яге новую летающую метлу, с инструкцией. А разобраться она сама не может - рисунки какие-то, формулы... А на обновочке-то в лунную ночь полетать страсть как хочется!"))
      .weight(0.1).ingNum(1).questor(MELNIK),
    new ItemMissionGenerator(406, 4, 1000, 1, Seed.TravaMurova, Map(Krasavitsa -> 1.0),
      Some("Новый танец пастуха"), Some("Пастуху надоело играть на дудочке. Скоро в деревне праздник будет с танцами, и решил он придумать новый и самый красивый танец, чтоб первым парнем на деревне быть! А партнёрши для танца - нет."))
      .weight(0.1).ingNum(5).questor(PASTUH),
    new ItemMissionGenerator(407, 4, 1000, 1, Seed.TravaMurova, Map(Nezametnost -> 1.0, Ustrashenie -> 1.0),
      Some("Пастух и логово волков"), Some("Волки повадились нападать на овец. Пастух просит помочь ему проследить за волками и найти их логово, чтобы отвадить от набегов на стадо."))
      .weight(0.1).ingNum(5).questor(PASTUH),
    new HeroExpMissionGenerator(408, 4, 100, 1, 20, 40, Map(Krasavitsa -> 1.0, Umelitsa -> 1.0, Krasavets -> 1.0, Ustrashenie -> 1.0),
      Some("Высший колледж"), Some("Высший колледж для благородных девиц и юношей")).weight(0.2).questor(EXP_LVL1),
    new HeroExpMissionGenerator(409, 4, 100, 1, 20, 40, Map(Sila -> 1.0, Lovkost -> 1.0, Nezametnost -> 1.0),
      Some("Лагерь подготовки"), Some("Лагерь подготовки и развития тела и навыков")).weight(0.2).questor(EXP_LVL1),
    new HeroExpMissionGenerator(410, 4, 100, 1, 20, 40, Map(Mudrost -> 1.0, Terpenie -> 1.0, Zolotye_ruki -> 1.0, Hitrost -> 1.0),
      Some("Школа философии"), Some("Школа философии, наук и познания")).weight(0.2).questor(EXP_LVL1),
    new UserExpMissionGenerator(430, 4, 100, 1, 50, 100, Map(Krasavitsa -> 1.0, Krasavets -> 1.0),
      Some("Свита"), Some("'Красота спасет мир' считает Шамаханская царица и приглашает красавцев и красавиц в свой дворец на чаепитие!")).weight(0.1).questor(SHAMAHANSKAYA),
    new MoneyMissionGenerator(431, 4, 10, 1, 100, 200, Map(Nezametnost -> 1.0, Sila -> 1.0, Ustrashenie -> 1.0),
      Some("Доставить письмо"), Some("Боярину срочно надо письмо секретное отправить, письмо не должно в чужие руки попасть!")).weight(0.1).questor(BOYARIN_VSEVOLOD),

    // 4й уровень
    new ItemMissionGenerator(411, 4, 1000, 1, Product.KOLOBOK, Map(Mudrost -> 1.0),
      Some("Записать сказки"), Some("Прослышал Дед, что в граде Киеве боярин Бажов сказки записывает, да потом детишкам малым рассылает. А Дед сказок знает - не перечесть! Вот только грамоте Дед не обучен, нужен грамотей, кто мог бы сказки Дедкины записать да письмецо Бажову отправить."))
      .weight(0.1).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(412, 4, 1000, 1, Product.EGG, Map(Mudrost -> 1.0),
      Some("Вылечить избушку"), Some("У Бабы-Яги избушка захворала - ни к лесу передом повернуться не может, ни задом. Бабушка всё никак понять не может, что же за хворь на избушку напала. Нужен мудрец, кто мог бы разобраться! А за Бабой-Ягой не заржавеет."))
      .weight(0.1).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(413, 4, 1000, 1, Product.EGG, Map(Nezametnost -> 1.0, Hitrost -> 1.0, Krasavets -> 1.0),
      Some("Подсмотреть рецепт зелья"), Some("Собралась Баба-Яга зелье зловония готовить, да забыла рецепт! Сестрица её старшая - злая ведьма, рецепт знает, да спрашивать у неё Бабушке стыдно - засмеёт ведь. Вот и просит Баба-Яга проникнуть к злой ведьме и подсмотреть рецепт в магической книге."))
      .weight(0.1).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(414, 4, 1000, 1, Product.IZBUSHKA_FOOD, Map(Mudrost -> 1.0),
      Some("Корм избушек"), Some("Поступил мельнику крупный заказ на корм для избушек на курьих ножках. Отродясь мельник о таком не слышал, но звонкую монету упускать не хочется! Нужна подсказка умника, как корм для избушек изготавливать. А мельник за это готов кормом поделиться."))
      .weight(0.1).ingNum(2).questor(MELNIK),
    new ItemMissionGenerator(415, 4, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Krasavitsa -> 1.0),
      Some("Песню сочинить"), Some("Ох любит Садко на гуслях играть да песни о любви петь! Взялся он песню красивую о самой светлой любви писать, да на полпути застрял. Не идёт к нему вдохновение! Ищет Садко музу распрекрасную, что могла бы ему вдохновение вернуть."))
      .weight(0.1).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(416, 4, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Krasavitsa -> 1.0, Terpenie -> 1.0, Krasavets -> 1.0),
      Some("Зазывало"), Some("Собрался купец Садко на ярмарке большой поучаствовать. Товар у купца отменный, но нужен зазывало, чтобы люд честной заманивать. Работёнка зазывалы не каждому подойдёт, требуется раскрасавица, поглазеть на которую люд честной отовсюду сбежится."))
      .weight(0.1).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(417, 4, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Nezametnost -> 1.0, Lovkost -> 1.0, Hitrost -> 1.0),
      Some("Выкрасть яблочки"), Some("Распереживалась Баба Яга, что морщинок у ней прибавилось. Знает бабушка, где Кощей хранит яблочки молодильные. Вот бы кто выкрал яблочки и для бабушки и себе!"))
      .weight(0.2).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(418, 10, 1000, 1, Seed.Solnechnik, Map(Nezametnost -> 1.0, Sila -> 1.0, Ustrashenie -> 1.0),
      Some("Спасти стадо"), Some("Пас пастух баранов на высокогорном пастбище, да тут гроза разразилась. Спрятал пастух стадо в пещере, да пещера та оказалась горного Дэва! Еле пастух спастись успел, а стадо его Дэв запер в пещере. Просит пастух прокрасться и освободить стадо - бараны умные, сами домой дорогу найдут."))
      .weight(0.1).ingNum(5).questor(PASTUH),

    // 5й уровень +3 героя +3 абилки
    new ItemMissionGenerator(501, 5, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Zolotye_ruki -> 1.0, Lovkost -> 1.0),
      Some("Ладью подлатать"), Some("Вернулся Садко из путешествия нелёгкого, со штормами да грозами. Пошел проверять ладью Садко, да в трюме течь обнаружил. Надо срочно заделать течь, иначе больше никаких путешествий!"))
      .weight(0.15).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(502, 5, 1000, 1, Product.KOLOBOK, Map(Zolotye_ruki -> 1.0, Sila -> 1.0),
      Some("Постройка бани"), Some("Задумали Бабка и Дедка баню новую построить. Вот только Дедка старенький совсем, ему помощь нужна. Помоги Дедке, а Бабка за это колобок свежий испечет!"))
      .weight(0.1).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(503, 5, 1000, 1, Seed.Solnechnik, Map(Umelitsa -> 1.0),
      Some("Починить игрушки"), Some("К Деду и Бабке внучка вскоре приезжает на лето. Достали Дедка и Бабка с чердака игрушки внучкины - а они попортились. Тут у куклы платье порвалось, там у мишки лапа оторвалась. Вот бы кто подшил!"))
      .weight(0.1).ingNum(5).questor(BABKA_DEDKA),
    new ItemMissionGenerator(504, 5, 1000, 1, Product.KOLOBOK, Map(Umelitsa -> 1.0),
      Some("Постирать бельё"), Some("Пошла Бабка бельё стирать на реку. Да вот беда - поясницу прихватило! Бабка ни согнуться не может, ни разогнуться. Кто бы сходил на реку да белье постирал? Найдется ли умелица?"))
      .weight(0.1).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(505, 5, 1000, 1, Product.KOLOBOK, Map(Zolotye_ruki -> 1.0, Umelitsa -> 1.0),
      Some("Подшить сапоги"), Some("Сел Дед сапоги зимние чинить, но старость - не радость. Не может никак нитку в иголку вдеть - зрение слабовато. Да и Бабка не помощница - тоже старенькая. А без сапог теплых зимой никуда."))
      .weight(0.1).ingNum(2).questor(BABKA_DEDKA),
    new ItemMissionGenerator(506, 5, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Umelitsa -> 1.0, Terpenie -> 1.0),
      Some("Уборка у купца Садко"), Some("Купец Садко вернулся из путешествия, а терем его весь пылью покрылся. Жить в такой грязи просто невозможно. Срочно нужна уборка, и Садко готов расщедриться."))
      .weight(0.15).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(507, 5, 1000, 1, Product.KOLOBOK, Map(Zolotye_ruki -> 1.0, Sila -> 1.0),
      Some("Починить ворота деду с бабой"), Some("Прошёл ураган и у Деда с Бабкой ворота покосились, а самим починить уж сил не хватает. Ищут старики мастера, кто бы помог."))
      .weight(0.1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(508, 5, 1000, 1, Seed.Solnechnik, Map(Zolotye_ruki -> 1.0, Umelitsa -> 1.0, Lovkost -> 1.0),
      Some("Починить печь деду с бабой"), Some("У деда с бабой печь коптит, весь потолок уже черный! Видать дымоход надо прочистить. А как старенький Дед на крышу залезет? Без помощи никак не обойтись."))
      .weight(0.1).ingNum(5).questor(BABKA_DEDKA),

    //6й уровень - +перья жар птицы
    new ItemMissionGenerator(601, 6, 1000, 1, Thing.PERO_JAR_PTICI, Map(Zolotye_ruki -> 1.0, Terpenie -> 1.0),
      Some("Купец и заморские животные"), Some("Привёз купец Садко заморских животных царю Салтану в подарок. Просит смастерить клетки для них, чтоб звери диковинные не разбежались и люд честной не пугали."))
      .weight(0.2).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(602, 6, 1000, 1, Thing.PERO_JAR_PTICI, Map(Krasavitsa -> 1.0, Krasavets -> 1.0),
      Some("Танец гостям"), Some("Синдбад-мореход пригласил на праздник важных гостей и хочет им для развлечения красивый танец прекрасных танцоров показать."))
      .weight(0.2).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(603, 6, 1000, 1, Thing.PERO_JAR_PTICI, Map(Umelitsa -> 1.0, Terpenie -> 1.0),
      Some("Перебрать орехи"), Some("Вёз Синдбад-мореход груз орехов разных да в шторм попал - в трюме все орехи перемешались. Просит Синдбад перебрать орехи и разложить по разным мешкам."))
      .weight(0.2).ingNum(1).questor(SINDBAD),




    // 10й уровень
    new ItemMissionGenerator(1001, 10, 1000, 1, Product.MILK, Map(Nezametnost -> 1.0, Lovkost -> 1.0, Sila -> 0.3, Ustrashenie -> 0.3),
      Some("Вернуть клинок"), Some("На днях к пастуху сборщик податей приходил за налогами. А у пастуха как на зло ни монеты в кармане! Вот и забрал сборщик податей клинок отцовский. Если отец узнает - позор будет несмываемый! Просит пастух выкрасть клинок у сборщика податей."))
      .weight(0.2).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(1002, 10, 1000, 1, Product.MILK, Map(Sila -> 1.0, Ustrashenie -> 1.0, Zolotye_ruki -> 0.3, Lovkost -> 0.3),
      Some("Успокоить буйвола"), Some("Привели Пастуху в стадо нового буйвола. Буйвол огромный, как гора и злой, как чёрт, пастуха не слушается, копытами землю роет. Нужен настоящий силач, чтобы буйвола успокоить!"))
      .weight(0.2).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(1003, 10, 1000, 1, Product.BUIVOL_FOOD, Map(Mudrost -> 1.0, Zolotye_ruki -> 0.3, Hitrost -> 0.5),
      Some("Проверить бухгалтерию"), Some("Отправил мельник купцам на ярмарке для продажи сто пудов муки. Купцы муку распродали да с ним рассчитались, но гложат мельника сомнения - не приворовали ли? Вот бы кто бумаги о продаже муки проверил."))
      .weight(0.1).ingNum(3).questor(MELNIK),
    new ItemMissionGenerator(1004, 10, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Sila -> 1.0, Ustrashenie -> 1.0, Nezametnost -> 0.3, Lovkost -> 0.5),
      Some("Передать серёжки"), Some("Решил Боярин Всеволод своей возлюбленной в подарок сережки золотые отправить, чтоб Забавушка о нём не забывала. Да на дороге ушкуйнички шалят - грабят путников и гонцов. Чтоб серёжки передать нужен силач, коего разбойнички трогать бы побоялись!"))
      .weight(0.2).ingNum(3).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(1005, 10, 1000, 1, Thing.PERO_JAR_PTICI, Map(Lovkost -> 1.0, Terpenie -> 1.0, Nezametnost -> 0.3, Sila -> 0.5),
      Some("Собрать избушки по лесу"), Some("Ёлки-палки! Вывела Бабуся-Ягуся выводок избушек маленьких, да насест не закрыла! Разлетелись маленькие избушки по всему лесу, и старой бабушке их никак не поймать. Ищет Баба-Яга ловкача, кто бы переловил всех избушек."))
      .weight(0.2).ingNum(2).questor(BABA_YAGA),
    new ItemMissionGenerator(1101, 11, 1000, 1, Seed.LunnyLotos, Map(Umelitsa -> 1.0, Terpenie -> 1.0, Ustrashenie -> 0.8, Mudrost -> 0.3),
      Some("Сплести сети"), Some("Бедный водяной! Уже третью ночь ему лягушки своим кваканьем спать не дают. Вот бы кто изготовил сеть защитную, чтобы водяной заводь свою огородил и мог бы спать спокойно. А водяной за это лунный лотос подарит!"))
      .weight(0.2).ingNum(2).questor(VODYANOI),
    new ItemMissionGenerator(1201, 12, 1000, 1, Product.RYAZHENKA_PRIVOROTNAYA, Map(Krasavets -> 1.0, Krasavitsa -> 0.5, Hitrost -> 0.5),
      Some("Пир у Марьи-Моревны"), Some("Царица Марья-Моревна скучает. Войны никакой нет давно, сидит во дворце своём сиднем. Кутёж бы знатный закатить!  Вот и приглашает царица выдающихся кавалеров в гости на пир горой. Самые удачливые герои не останутся без награды."))
      .weight(0.2).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(1202, 12, 1000, 1, Product.RYAZHENKA_PRIVOROTNAYA, Map(Krasavets -> 1.0, Krasavitsa -> 0.5, Mudrost -> 0.5, Hitrost -> 1.0),
      Some("Поболтать с Шамаханской царицей"), Some("После того, как царь Салтан омолодился и бросил Шамаханскую царицу, царица загрустила - где же это видано, чтоб красавицу бросали? Можно наведаться в гости к царице и грусть её развеять. Цветы подарить да комплиментов отсыпать. Глядишь Шамаханская царица в ответ подарит что-нить."))
      .weight(0.2).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(1203, 12, 1000, 1, Product.AMULET_UDACHI, Map(Lovkost -> 1.0, Sila -> 1.0, Krasavets -> 0.3, Terpenie -> 0.3),
      Some("Поучаствовать в состязании"), Some("Договорился Синдбад-мореход с шахом Шахрияром устроить состязания среди силачей да ловкачей. Чьи спортсмены победят - тому и ценные призы. Вот и ищет Синдбад атлетов."))
      .weight(0.2).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(1301, 13, 1000, 1, Product.OLADUSHKI_POHIDYASHKI, Map(Krasavitsa -> 1.0, Umelitsa -> 0.3, Krasavets -> 0.3, Mudrost -> 0.3),
      Some("Фрейлина на прогулку"), Some("Любит царица Шамаханская восторг и трепет у мужчин вызывать. Вот и подбирает в свиту на прогулки самых красивых фрейлин - чтоб мужички штабелями к ногам падали!"))
      .weight(0.2).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(1401, 14, 1000, 1, Product.LUNNY_BISER, Map(Umelitsa -> 1.0, Zolotye_ruki -> 0.3, Terpenie -> 0.3),
      Some("Расшить кафтан"), Some("Всяк знает - по одёжке встречают, провожают по уму. Умом купец Садко не обделён, надо бы и одёжку под стать подобрать! Пожелал Садко расшить свой кафтан тесьмой золотой. Тесьма золотая уже есть, осталось найти швею-умелицу."))
      .weight(0.2).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(1402, 15, 1000, 1, Product.BLUDCE, Map(Zolotye_ruki -> 1.0, Sila -> 0.3, Umelitsa -> 0.3),
      Some("Починить карету"), Some("Вернулся боярин Всеволод с прогулки - катались с Забавой Путятичной в карете по бережку речки. И что-то задребезжала карета, видать подвеску разбило на ухабах. В такой карете ездить - только пятую точку отбивать."))
      .weight(0.2).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(1501, 15, 1000, 1, Product.BLUDCE, Map(Zolotye_ruki -> 1.0, Terpenie -> 1.0, Mudrost -> 1.0),
      Some("Водопровод"), Some("Пришла боярину Всеволоду в голову светлая мысль - водопровод в терем провести. На холмике рядом ключ с чистейшей водой есть, надобно трубу от ключа до терема проложить."))
      .weight(0.2).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(1701, 17, 1000, 1, Seed.Koshmarnik, Map(Nezametnost -> 1.0, Mudrost -> 1.0, Hitrost -> 0.5, Lovkost -> 0.3),
      Some("Спасти Ивана"), Some("Давеча был у Бабы-Яги в гостях Иван - до чего же славный молодец! Держит Иван путь через болота, где кикиморы живут, что путников в трясины заманивают. Хотела Баба-Яга подарить Ивану оберег от кикимор, да Иван скромный парень, отказался. Просит Бабушка или уговорить Ивана, или незаметно подложить ему оберег."))
      .weight(0.1).ingNum(5).questor(BABA_YAGA),
    new ItemMissionGenerator(1801, 18, 1000, 2, Product.MASLO, Map(Krasavets -> 1.0, Lovkost -> 1.0, Sila -> 1.0, Krasavitsa -> 0.3, Umelitsa -> 0.3, Ustrashenie -> 0.3),
      Some("Свита для Марьи Моревны"), Some("Собирается Марья Моревна в гости к Шамаханской царице и хочет чтобы у неё самая лучшая свита была. Чтобы Шамаханская царица потом ночами от зависти спать не могла!"))
      .weight(0.1).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(1901, 19, 1000, 2, Product.PIROZHKI_BODROSTI, Map(Krasavets -> 1.0, Krasavitsa -> 1.0),
      Some("Танцы у Шамаханской царицы"), Some("Шамаханская царица обожает танцы! Вот и устраивает царица бал и приглашает пары самых красивых танцоров. Лучшие танцоры не останутся без призов."))
      .weight(0.1).ingNum(1).questor(SHAMAHANSKAYA),

    new HeroExpMissionGenerator(1508, 15, 100, 2, 150, 250, Map(Krasavitsa -> 1.0, Umelitsa -> 1.0, Krasavets -> 1.0, Ustrashenie -> 1.0),
      Some("Высший колледж 2"), Some("Высший колледж для благородных девиц и юношей, второй курс")).weight(0.25).questor(EXP_LVL2),
    new HeroExpMissionGenerator(1509, 15, 100, 2, 150, 250, Map(Sila -> 1.0, Lovkost -> 1.0, Nezametnost -> 1.0),
      Some("Лагерь подготовки 2"), Some("Лагерь подготовки и развития тела и навыков, секция два")).weight(0.25).questor(EXP_LVL2),
    new HeroExpMissionGenerator(1510, 15, 100, 2, 150, 250, Map(Mudrost -> 1.0, Terpenie -> 1.0, Zolotye_ruki -> 1.0, Hitrost -> 1.0),
      Some("Школа философии 2"), Some("Школа философии, наук и познания, магистратура")).weight(0.25).questor(EXP_LVL2),


    // 20й-30й уровни
    new ItemMissionGenerator(2001, 20, 1000, 1, Product.TAIL, Map(Krasavitsa -> 1.0, Umelitsa -> 0.6),
      Some("Порадовать дедушку"), Some("Собирает дед пастуха всех внуков своих с женами и подругами на празднование. Да вот только у пастуха нет ни жены ни подруги. Чтобы дедушку не расстраивать просит пастух красавицу с ним на день рождения дедушки сходить и его подругой представиться."))
      .weight(0.1).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(2002, 20, 1000, 1, Product.VASILISK_FOOD, Map(Mudrost -> 1.0, Zolotye_ruki -> 1.0, Hitrost -> 0.5),
      Some("Наладить производство"), Some("Мельник затеял модернизацию жерновов и требуются помощники. Корма для василисков по большей части из-за бугра везут и денег они немалых стоят. А мельник наладит производство и импортозаместит корма забугорные. С помощниками рассчитается готовым кормом."))
      .weight(0.1).ingNum(3).questor(MELNIK),
    new ItemMissionGenerator(2003, 20, 1000, 1, Thing.PERO_JAR_PTICI, Map(Lovkost -> 1.0, Terpenie -> 1.0, Umelitsa -> 0.5),
      Some("Разогнать комаров"), Some("Синдбад-мореход в гневе - комары проклятые его донимают, ни одной ночи поспать нормально не получается! А скоро в путешествие дальнее, отдохнуть надо хорошенько. Уж помог бы кто разогнать этих комаров, чтоб хотя бы ночку отоспаться."))
      .weight(0.3).ingNum(2).questor(SINDBAD),
    new ItemMissionGenerator(2004, 20, 1000, 1, Thing.MOLOD_YABLOCHKI, Map(Sila -> 1.0, Ustrashenie -> 1.0),
      Some("Навести порядок"), Some("Купцы донесли до Садко, что ушкуйнички на пути в Чернигов град безобразничают, торговый люд грабят. Просят купцы ушкуйничков образумить! Садко хочет помочь братьям купцам и ищет, кто бы разогнал этих бандитов."))
      .weight(0.3).ingNum(3).questor(SADKO),
    new ItemMissionGenerator(2101, 21, 1000, 2, Product.PECHENKA, Map(Krasavitsa -> 1.0, Krasavets -> 1.0, Umelitsa -> 1.0),
      Some("Бал-маскарад"), Some("Царица Шамаханская собирается бал-маскарад устроить. А к балу столько всего нужно приготовить - подобрать красивых фрейлин, найти симпатичных кавалеров, костюмы для маскарада пошить."))
      .weight(0.2).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(2201, 22, 1000, 2, Product.JARKOE_ZMEINOI_GIBKOSTI, Map(Nezametnost -> 1.0, Hitrost -> 1.0, Lovkost -> 0.8, Ustrashenie -> 0.6),
      Some("Передать весточку"), Some("Боярин Всеволод хочет весточку своей возлюбленной Забаве Путятичне передать, да ответную весточку получить. Да вот гостит у Забавушки её вредная и чопорная тётушка, которая Всеволода на дух не переносит! Иссох уж Всеволод совсем от любви неземной и готов на всё ради весточки от зазнобы своей. Нужны гонцы, что бы смогли миновать тётушку и письмецо Забаве передать."))
      .weight(0.2).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(2301, 23, 1000, 2, Product.SNADOBIE_VASILISKA, Map(Mudrost -> 0.8, Krasavets -> 1.0, Hitrost -> 1.0, Terpenie -> 0.6),
      Some("Провести переговоры"), Some("Синдбаду-мореходу надо с Шамаханской царицей переговоры провести, чтобы ларёк на базарной площади открыть и диковинными сувенирами торговать. С женщиной такой не просто переговоры вести, требуется мудрость и терпение. Да хорошо бы и понравиться ей сразу."))
      .weight(0.2).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(2401, 24, 1000, 1, Seed.Ognennaya_roza, Map(Nezametnost -> 1.0, Sila -> 0.8, Ustrashenie -> 1.0, Hitrost -> 0.5),
      Some("Пойти на дело"), Some("Зависть бабу Ягу гложет! Сестрица её злая ведьма такие красивые цветочки выращивает, а бабушке Ягушке подарить отказывается - хочет, чтоб только у неё такие красивые цветочки были. Цветочки те злой старый пёс охраняет. Баба Яга подскажет, когда сестрица улетит по делам ведьминским и надо или прокрасться мимо пса, либо запугать его и добыть огненных роз."))
      .weight(0.2).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(2501, 25, 1000, 1, Product.SUMERECHNAYA_PIL, Map(Umelitsa -> 1.0, Zolotye_ruki -> 0.6),
      Some("Кашеварить"), Some("У боярина Всеволода повар взял отпуск на две недели и на моря рванул отдыхать. Сам-то боярин ни чего готовить не умеет, а в трактирах да забегаловках питаться надоело. Хочется домашнего чего-то, супчику да блинчиков!"))
      .weight(0.2).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(2601, 26, 1000, 2, Product.ZELIE_UVERENNOSTI, Map(Umelitsa -> 0.6, Zolotye_ruki -> 1.0, Lovkost -> 0.8, Terpenie -> 1.0),
      Some("Спасти кладовую"), Some("У Марьи Моревны катастрофа! Крысы в кладовой завелись - все запасы перепортили. Жрут всё: от продуктов, до тканей и свечек. Надо крыс разогнать или переловить!"))
      .weight(0.2).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(2701, 27, 1000, 2, Seed.Muhomor, Map(Sila -> 0.7, Ustrashenie -> 1.0, Terpenie -> 1.0),
      Some("Охрана теплицы"), Some("Посадил Водяной в теплице у себя на дачке мухоморчики. Вот-вот уже урожай созреет, и чтобы шустрые поганцы мухоморчики не свистнули, просит Водяной посторожить теплицу. А за помощь готов поделиться мухомором самым спелым!"))
      .weight(0.2).ingNum(1).questor(VODYANOI),
    new ItemMissionGenerator(2801, 28, 1000, 2, Product.ZELIE_OZARENIYA, Map(Zolotye_ruki -> 0.5, Sila -> 0.7, Krasavets -> 1.0, Lovkost -> 0.6),
      Some("Вызвать зависть"), Some("Марья Моревна повздорила с царицей Шамаханской. И чтоб у Шамаханской царицы зависть вызвать, хочет Марья Моревна на вечерок кавалеров нанять, чтоб покружили вокруг неё. И чтоб царица Шамаханская увидела, что Марья Моревна успешная женщина, обласканная  вниманием галантных кавалеров!"))
      .weight(0.2).ingNum(1).questor(MARYA_MOREVNA),

    // 30й-40й уровни

    new ItemMissionGenerator(3001, 30, 1000, 2, Seed.BessmertnikZolotisty, Map(Krasavitsa -> 1.0, Nezametnost -> 1.0, Ustrashenie -> 1.0, Hitrost -> 0.7),
      Some("Отрава для гусениц"), Some("Водяной - большой любитель цветов. Жизнь в болоте скучная и неприглядная, а цветочки её украшают. Возится водяной с грядками да теплицами и радуется каждому новому росточку. Да вот что-то гусеницы напали на его делянку - все побеги пожрали! Просит достать ему чудное средство, что колдун Черномор разработал. Просто так Черномор не отдаст, надо чем-то на него подействовать!"))
      .weight(0.2).ingNum(1).questor(VODYANOI),
    new ItemMissionGenerator(3101, 31, 1000, 2, Product.WOOL, Map(Zolotye_ruki -> 1.0, Terpenie -> 1.0, Lovkost -> 0.7, Sila -> 0.7),
      Some("Подстричь архаров"), Some("Ох, хороши архары золотые: шерсть так и лоснится! Пришло время стричь. Да вот одному пастуху никак не управиться - архары своенравные да силушки у них много. Нужна помощь!"))
      .weight(0.2).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(3102, 31, 1000, 2, Product.ARHAR_FOOD, Map(Umelitsa -> 1.0, Krasavets -> 1.0, Sila -> 0.7),
      Some("Рекламная компания"), Some("Что-то у мельника дела не важно пошли. Требуется рекламную кампанию провернуть. Ищет мельник красивого актёра и мастерицу, что смогла бы пошить яркий костюм."))
      .weight(0.2).ingNum(3).questor(VODYANOI),
    new ItemMissionGenerator(3201, 32, 1000, 2, Product.ZOLOTAYA_SHAL_NEOTRAZIMOSTI, Map(Mudrost -> 1.0, Nezametnost -> 1.0, Lovkost -> 0.7, Hitrost -> 0.6),
      Some("Оракул"), Some("Долгие годы мечтает Синдбад-мореход достать карты морских течений, что шах Шахрияр хранит. Какие только драгоценности Синдбад не предлагал в обмен на карты, но шах ни на что менять не хочет. Знает Синдбад, что шах предсказаниям оракула доверяет. Вот и замыслил отправить команду, которая смогла бы проникнуть в святилище и под видом оракула намекнуть шаху отдать Синдбаду карты."))
      .weight(0.2).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(3301, 33, 1000, 1, Thing.VOLSHEBNIY_KLUBOCHEK, Map(Mudrost -> 1.0, Zolotye_ruki -> 1.0, Hitrost -> 0.6),
      Some("Инвестиции"), Some("Баба Яга за небольшую мзду лечила похмелье и скопила целый чулок с деньгами! Вот теперь бабушка ночами не спит - а вдруг инфляция? Что будет со сбережениями, ежели вон какая не простая ситуация экономическая в княжестве? Нужен Бабе Яге совет, как со сбережениями поступить: али в банк положить, али инвестировать во что-то. А может просто новую ступу купить пока ступы не подорожали?"))
      .weight(0.3).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(3302, 33, 1000, 1, Thing.VOLSHEBNIY_KLUBOCHEK, Map(Umelitsa -> 1.0, Zolotye_ruki -> 1.0, Terpenie -> 1.0),
      Some("Ремонт терема"), Some("Коли свадьба с Забавушкой на носу, надо бы и терем отремонтировать! А-то стыдно невесту в такой хлев приводить. Ищет боярин тех, кто помог бы ему терем отремонтировать!"))
      .weight(0.3).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(3401, 34, 1000, 2, Product.KOVER_SAMOLET, Map(Krasavitsa -> 1.0, Krasavets -> 1.0, Lovkost -> 0.7, Sila -> 0.7),
      Some("Ведущие на конкурс"), Some("Устраивает Шамаханская царица конкурс красоты, в котором она и должна победить (судьям указания уже сделаны). Да чтоб побольше зрителей привлечь на конкурс и сильнее прославиться, нужны просто неотразимые ведущие!"))
      .weight(0.2).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(3501, 35, 1000, 2, Product.ZVEZDNAYA_ESENCIYA, Map(Mudrost -> 1.0, Ustrashenie -> 1.0, Lovkost -> 0.7, Sila -> 0.7),
      Some("Трактат о бое"), Some("Синдбад-мореход письмо прислал: 'Слухи об умениях Ваших достигли края земли! Прошу помочь написать трактат учебный о бое рукопашном для эмира Хорезма.' За это Синдбад предлагает награду драгоценную и клянется передать сведения о Вас самому Падишаху!"))
      .weight(0.2).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(3701, 37, 1000, 1, Seed.ElfiskyLen, Map(Nezametnost -> 1.0, Terpenie -> 1.0, Hitrost -> 1.0),
      Some("Подслушать разговоры"), Some("Чует Водяной, что кикиморы гадости про него рассказывают, да ни как не поймает их. Как он к ним приближается - так они сразу замолкают и улыбаются противно. Надо вывести их на чистую воду, нужен водяному соглядатай, кто бы подежурил и подслушал разговоры кикимор."))
      .weight(0.2).ingNum(1).questor(VODYANOI),
    new ItemMissionGenerator(3702, 37, 1000, 3, Product.SHASHLYK_ZHELEZNOY_SILY, Map(Lovkost -> 1.0, Sila -> 1.0, Ustrashenie -> 1.0, Zolotye_ruki -> 0.5, Hitrost -> 0.5),
      Some("Прогнать горных духов"), Some("Пастухи в панике, злые горные духи не дают пасти скот на самых лучших высокогорных пастбищах. Самим пастухам никак не совладать с духами - смелости не хватает. Нужны крепкие смельчаки, что смогли бы прогнать духов."))
      .weight(0.2).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(3801, 38, 1000, 2, Product.TKAN_NOCHNOI_SVEHZESTI, Map(Krasavets -> 1.0, Ustrashenie -> 1.0, Hitrost -> 1.0),
      Some("Расколдовать ворота"), Some("Злая колдунья заколдовал ворота во дворце Марьи-Моревны и теперь они громко скрипят по ночам да будят спящих обитателей. Ходят ныне все во дворце злые да невыспавшиеся. Дать бы в лоб колдунье да заставить расколдовать ворота, но как её поймать - она чуть что сразу шапку невидимку одевает! Коли силой нельзя заставить, тут уговором действовать надобно!"))
      .weight(0.2).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(3901, 39, 1000, 3, Product.ELEKSIR_DIAVOLSKOY_HITROSTI, Map(Krasavitsa -> 1.0, Umelitsa -> 1.0, Mudrost -> 0.7, Ustrashenie -> 0.7, Hitrost -> 0.5),
      Some("Отвлечь сыночка"), Some("Купец Садко в шоке! Сынок его единственный Акакий повадился с домовыми в кости играть. Уж что он только хитрющим домовым не проиграл - и коня, и амбар и перстень золотой. Надо бы отучить детинушку от порока проклятущего, да не может Садко с единственным сынулечкой строгим быть. Вот нашелся бы кто и отвадил Акакия от игр в кости!"))
      .weight(0.2).ingNum(1).questor(SADKO),

    // 40-50 уровни


    new ItemMissionGenerator(4001, 40, 1000, 1, Seed.KalinkaMalinka, Map(Umelitsa -> 1.0, Terpenie -> 1.0),
      Some("Посидеть с внучкой"), Some("К Деду и Бабке внучка приехала, оторва и непоседа. Собрались Дедка и Бабка на ярмарку, да вот кто бы за внучкой приглядел? С такой внучкой сидеть недюжее терпение надо иметь!"))
      .weight(0.2).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(4002, 40, 1000, 2, Product.VESELIY_KEKSIK, Map(Mudrost -> 0.5, Krasavitsa -> 1.0, Krasavets -> 1.0, Hitrost -> 0.5),
      Some("Компания в путешествие"), Some("Собралась царица Шамаханская в путешествие к берегам дальним - с Шахом турецким обсудить дела государственные. Да только скучно в дальней дороге, вот бы взять за компанию симпатичных и интересных собеседников!"))
      .weight(0.3).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(4003, 40, 1000, 2, Thing.VOLSHEBNIY_KLUBOCHEK, Map(Ustrashenie -> 1.0, Hitrost -> 1.0, Terpenie -> 0.5, Sila -> 0.5),
      Some("Поговорить с Горынычем"), Some("Змей Горыныч совсем совесть потерял! Как пролетает над опушкой, где Баба Яга живет, так обязательно огнем пыхнет да рыкнет. Гуси-Лебеди в ужасе по всему лесу разлетаются, а Змей Горыныч смеется. Вот потом ищи-свищи и собирай гусей-лебедей по лесу. Надо бы вразумить Змея, что не хорошо так шутить над старенькой бабушкой."))
      .weight(0.45).ingNum(3).questor(BABA_YAGA),
    new ItemMissionGenerator(4101, 41, 1000, 2, Product.OGNENNIE_RUKOVICI, Map(Nezametnost -> 1.0, Lovkost -> 1.0, Hitrost -> 0.8),
      Some("Месть"), Some("Достались Бабе Яге рукавицы огненные, да только зачем они? Готова бабушка распрощаться с рукавичками, но за помощь в деле деликатном. Обидел бабушку злой колдун Черномор, аж расплакалась бабушка! Говорит 'Ты, старая, колдовать совсем не умеешь: даже по небу без ступы летать не можешь! Толи дело Я!'. Надо пробраться в башню к Черномору и вонявку, бабушкой приготовленную, ему под шкаф подложить!"))
      .weight(0.3).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(4102, 41, 1000, 3, Product.PLATIE_VESENNEY_SVEZHESTI, Map(Mudrost -> 1.0, Umelitsa -> 1.0, Zolotye_ruki -> 1.0, Terpenie -> 0.9),
      Some("Детская комната"), Some("Детки у Марьи-Моревны хулиганистые и любят пошалить, впрочем, как и  другие дети. Убытков от их шалостей не счесть - больше чем от вражьего войска! Только ремонт во дворце сделаешь - тут на стенах нарисуют, там светильник уронят или зеркало разобьют! Задумала Марья-Моревна детскую комнату сделать - да со всякими игрушками, лесенками и горками. Пусть детки только в этой комнате резвятся и отдыхают. А заодно и мама отдохнет!"))
      .weight(0.3).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(4201, 42, 1000, 3, Product.TCARSKAYA_DUBLENKA, Map(Nezametnost -> 1.0, Sila -> 0.9, Ustrashenie -> 1.0, Terpenie -> 0.9),
      Some("Воспитательная работа"), Some("Доложили боярину, что злодей какой-то по ночам в его владениях шастает, всё что плохо лежит прикарманивает. Надобно вычислить негодяя, да произвести воспитательную работу. До того боярин Всеволод разгневался, что обещает в награду дублёнку со своего плеча!"))
      .weight(0.3).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(4301, 43, 1000, 2, Product.VAFLI_NOCHNIH_KOSHMAROV, Map(Krasavitsa -> 1.0, Umelitsa -> 1.0, Mudrost -> 0.6, Hitrost -> 0.6),
      Some("Похвалить султана"), Some("Султан Мехмед очень гордится своими одеяниями модными. Надо бы на приём напроситься, да похвалить его! И под шумок испросить разрешение в его порту  Синдбаду останавливаться и провиантом запасаться."))
      .weight(0.3).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(4302, 43, 1000, 3, Product.BOYARSKII_KAMZOL, Map(Krasavitsa -> 1.0, Krasavets -> 1.0, Ustrashenie -> 1.0, Hitrost -> 0.5, Sila -> 0.6),
      Some("Гости к Царю Морскому"), Some("Отправляет купец Садко делегацию с подарками, да не к кому-то, к самому Царю Морскому! Делегация должна подарки ценный вручить, да настоятельно попросить царя купцу в просьбах посодействовать!"))
      .weight(0.3).ingNum(1).questor(SADKO),
    new ItemMissionGenerator(4401, 44, 1000, 2, Seed.VolchyaYagodka, Map(Nezametnost -> 1.0, Mudrost -> 0.6, Hitrost -> 0.8, Lovkost -> 0.6),
      Some("Раздобыть саженцы ягодки волчьей"), Some("Водяной - знатный мичуринец, обожает у себя на болоте растения красивые выращивать. Красота природы тоску болотную развеивает. Давно заглядывается он на ягодку волчью, да ягодку ту жадина Черномор зажал. Ни с кем саженцами делиться не хочет! Надо бы спереть или выманить у колдуна Черномора саженец. А водяной потом ягодкой поделится."))
      .weight(0.2).ingNum(1).questor(VODYANOI),
    new ItemMissionGenerator(4402, 44, 1000, 3, Product.PRIMANKA_OBOROTNEY, Map(Zolotye_ruki -> 1.0, Sila -> 0.8, Lovkost -> 0.8, Ustrashenie -> 1.0),
      Some("Дальний поход"), Some("У Марьи-Моревны спёрли брошь волшебную, бабушкой на счастье подаренную. Блюдце с яблочком наливным показывает, что брошь украдена и спрятана кикиморами в болоте гиблом, где кикиморы на путников нападают и в трясины затягивают. Собери отряд из храбрецов да удальцов, разгони кикимор и достань со дна болотного брошь Марьи-Моревны. За это будет награда ценная!"))
      .weight(0.3).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(4501, 45, 1000, 2, Product.PIROJENKA_NOCHNOI_STRASTI, Map(Krasavets -> 1.0, Zolotye_ruki -> 1.0, Lovkost -> 0.5, Sila -> 0.5),
      Some("Подстрич газон"), Some("Приглашает царица Шамаханская мастеров, что могли бы художественно газон подстричь. Да чтоб глаз царский радовался работники должны быть фактурные и харизматичные! Потому, что во дворце у царицы всё должно быть прекрасно!"))
      .weight(0.3).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(4502, 45, 1000, 2, Thing.ZLATO_SEREBRO, Map(Nezametnost -> 1.0, Umelitsa -> 1.0, Mudrost -> 0.6, Hitrost -> 0.6, Ustrashenie -> 0.6),
      Some("Избавить боярина от шума"), Some("Ох, невесел боярин. Соседи купили гусли-самоплясы, и день-деньской слушают музыку да пляшут не жалея ног. Шум стоит как от грозы, у боярина аж голова разболелась. Что же делать, если сосед - это сам царский министр? Может быть обить терем тканью, чтоб от шума избавиться? Или нашелся бы кто, да сломал гусли. Или поговорил уже наконец с министром!"))
      .weight(0.45).ingNum(2).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(4503, 45, 1000, 2, Thing.ZLATO_SEREBRO, Map(Zolotye_ruki -> 1.0, Sila -> 1.0, Lovkost -> 0.8, Mudrost -> 0.6, Terpenie -> 0.6),
      Some("Достать сокровища"), Some("Лежит на дне озера, прямо у дома Водяного корабль купеческий, весь тиной да водорослями заросший. Надоел он Водяному: все красоты дня морского собой закрывает. Помог бы кто Водяному избавиться от корабля, и пусть забирает все сокровища из трюма."))
      .weight(0.45).ingNum(4).questor(VODYANOI),

    //45-50
    new ItemMissionGenerator(4601, 46, 1000, 3, Product.SNADOBIE_ASTRALNIH_PUTESHESTVII, Map(Krasavets -> 1.0, Krasavitsa -> 1.0, Umelitsa -> 1.0, Sila -> 0.7, Lovkost -> 0.3),
      Some("Провести праздник"), Some("У Бабы и Деда праздник намечается - золотая свадьба! Гости съедутся со всей округи. Надо помочь Бабе и Деду к празднику подготовиться, да гостей понаехавших представлением красивым развлечь! А в награду Бабка приготовит снадобье волшебное по секретному рецепту."))
      .weight(0.3).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(4602, 46, 1000, 2, Product.OBEREG_OT_SGLAZA, Map(Krasavets -> 1.0, Krasavitsa -> 1.0, Terpenie -> 0.5, Hitrost -> 0.5),
      Some("Сотрудники во дворец"), Some("Царица Шамаханская приглашает на работу статных да смышленых помощников и помощниц. Нужен кто-то, кто гостей бы встречал во дворце."))
      .weight(0.3).ingNum(1).questor(SHAMAHANSKAYA),
    new ItemMissionGenerator(4701, 47, 1000, 2, Thing.MIFRIL, Map(Ustrashenie -> 1.0, Terpenie -> 1.0, Lovkost -> 0.6, Nezametnost -> 0.6),
      Some("Охрана груза"), Some("Отправляется Синдбад-мореход в Грецию, повезет груз драгоценный! Да вот дорога пройдет мимо острова гарпий, что любят на корабли путников нападать. Нужны Синдбаду охранники для груза, что разогнали бы жадных гарпий. Поручение опасное, но и награда будет под стать!"))
      .weight(0.45).ingNum(3).questor(SINDBAD),
    new ItemMissionGenerator(4702, 47, 1000, 2, Thing.MIFRIL, Map(Mudrost -> 1.0, Zolotye_ruki -> 1.0, Hitrost -> 0.6, Sila -> 0.6),
      Some("Таможня"), Some("Заказал купец Садко товару ценного в самом Китае! Да вот как его привезти, чтоб таможенники царские купца не раздели? Ведь всем известна сказочная жадность таможни! Нужны купцу помощники, что бы груз правильно подготовили к таможне."))
      .weight(0.45).ingNum(3).questor(SADKO),
    new ItemMissionGenerator(4703, 47, 1000, 1, Seed.ChesnokYadrenisty, Map(Lovkost -> 1.0, Ustrashenie -> 0.6, Sila -> 0.6),
      Some("Вылечить избушку"), Some("У Бабы Яги избушка на курьих ножках по лесу гуляла и ножку занозила. Надо бы занозу достать, да избушка не даётся - попробуй удержи её! Нужны Бабе Яге помощники, что избушку бы придержали и помогли бы занозу достать."))
      .weight(0.2).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(4801, 48, 1000, 2, Product.OJERELIE_PROTIVOVAMPIRSKOE, Map(Mudrost -> 1.0, Krasavitsa -> 1.0, Terpenie -> 0.5, Hitrost -> 0.5),
      Some("Подружка-советчица"), Some("Марья-Моревна скучает в походе. Хочется вечером с кем-нибудь посудачить о делах о женских. Да для пользы государственной интриги какие-нибудь замыслить!"))
      .weight(0.3).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(4802, 48, 1000, 3, Product.KOLCHUJKA_MIFRILNAYA, Map(Krasavets -> 1.0, Nezametnost -> 1.0, Ustrashenie -> 0.8, Terpenie -> 0.6, Hitrost -> 0.6),
      Some("Послы к Снежной Королеве"), Some("Решил Синдбад-мореход найти волшебство, что зимний холод производит. Живёт он в странах жарких: днем на солнцепёке так тяжко бывает, что любые сокровища за прохладу отдать готов! А вот бы еще таким волшебством корабль оборудовать, чтоб с комфортом по морям южным плавать - мечта просто! А где волшебство морозное взять, как не у Снежной Королевы? Вот и отправляет делегацию."))
      .weight(0.3).ingNum(1).questor(SINDBAD),
    new ItemMissionGenerator(4901, 49, 1000, 3, Product.KRUJEVNAYA_SOROCHKA, Map(Umelitsa -> 1.0, Zolotye_ruki -> 1.0, Lovkost -> 0.8, Sila -> 0.8, Nezametnost -> 0.4),
      Some("Стричь золотых архаров"), Some("Феи лесные заказали у пастуха пряжу золотую. А значит пора стричь золотых архаров, коих огромное стадо! Работы - непочатый край, никак пастуху одному к сроку не управиться. Нужны помощники. А в награду Феи обещали платье распрекрасное!"))
      .weight(0.3).ingNum(1).questor(PASTUH),


    // 50+ уровни

    new ItemMissionGenerator(5001, 50, 1000, 2, Seed.Chernoknizhnitsa, Map(Umelitsa -> 1.0, Hitrost -> 1.0, Nezametnost -> 0.5, Krasavitsa -> 0.5),
      Some("Вернуть кольцо"), Some("Дело есть у Водяного деликатное. Соловей Разбойник спер у Водяного кольцо волшебное. Да только как его вернуть? Соловей-то из разбойника стал поп-звездой! Гастролирует по всем царствам да концерты дает. Вот бы кто в гримерку бы Соловья попал да колечко бы забрал. А водяной за это чернокнижицей рассчитается!"))
      .weight(0.3).ingNum(1).questor(VODYANOI),
    new ItemMissionGenerator(5002, 50, 1000, 2, Product.KEKSIK_KOSHEISKI_ZLODEISKI, Map(Zolotye_ruki -> 1.0, Sila -> 1.0, Lovkost -> 0.5, Terpenie -> 0.5),
      Some("Помочь с сундуком"), Some("Ох тыж, нашла Баба Яга в лесу сундук тяжеленный. Наверняка там золото да драгоценности! Да вот ни открыть его бабушка не может, ни дотащить до избушки. Нужны помощники! В благодарность бабушка испечет кексик злодейский."))
      .weight(0.3).ingNum(1).questor(BABA_YAGA),
    new ItemMissionGenerator(5101, 51, 1000, 2, Product.NASTOYKA_HRABROSTI, Map(Krasavitsa -> 1.0, Mudrost -> 1.0, Umelitsa -> 0.5, Terpenie -> 0.5),
      Some("Пообщаться с пастухом"), Some("Скучно совсем пастуху. Не часто в горах людей встретишь! Иногда до того по общению соскучишься, что на всё готов. Приглашает пастух в гости приятных собеседников, а в награду приготовит настойки храбрости."))
      .weight(0.3).ingNum(1).questor(PASTUH),
    new ItemMissionGenerator(5102, 51, 1000, 2, Thing.MAGIC_GEMCHUG, Map(Krasavets -> 1.0, Ustrashenie -> 1.0, Hitrost -> 0.5, Mudrost -> 0.5),
      Some("Снять проклятие"), Some("Испортил Синдбад отношения с колдуньей, да так, что колдунья на него порчу да хвори наслала! Вот и ищет теперь Синдбад посланников, что смогли бы колдунью убедить снять проклятия."))
      .weight(0.45).ingNum(3).questor(SINDBAD),
    new ItemMissionGenerator(5103, 51, 1000, 2, Thing.MAGIC_GEMCHUG, Map(Lovkost -> 1.0, Nezametnost -> 1.0, Terpenie -> 0.5, Hitrost -> 0.5),
      Some("Разведать обстановку"), Some("Есть у Водяного подозрение, что Кощей на его болото глаз положил. Как бы выяснить так ли это? Надо отправить кого-то подслушать разговоры Кощеевы. А в награду Водяной отсыплет жемчуга магического, благо этого добра у Водяного навалом."))
      .weight(0.45).ingNum(4).questor(VODYANOI),
    new ItemMissionGenerator(5104, 51, 1000, 2, Product.KANOPE_PROTIVOKIKIMORNOYE, Map(Umelitsa -> 1.0, Zolotye_ruki -> 1.0, Sila -> 0.5, Mudrost -> 0.5),
      Some("Ремонт мельницы"), Some("Мельник срочно обращается: Поломалась его ветряная мельница! Купцы за мукой приехали, а муки нет. Нет муки - нет и денег! Просит мельник починить его мельницу, а в награду жена мельника приготовит угощение."))
      .weight(0.3).ingNum(1).questor(MELNIK),
    new ItemMissionGenerator(5201, 52, 1000, 2, Product.SNADOBIE_VECHNOI_ZHIZNI, Map(Krasavets -> 1.0, Ustrashenie -> 1.0, Sila -> 0.5, Terpenie -> 0.5),
      Some("Соратники"), Some("Марья-Моревна ищет спутников в поход военный. На кого положиться можно было бы, да не стыдно на переговорах показать. Обещает Марья-Моревна за это снадобье жизни вечной, что Кощей ей регулярно поставляет."))
      .weight(0.3).ingNum(1).questor(MARYA_MOREVNA),
    new ItemMissionGenerator(5202, 52, 1000, 2, Product.SNADOBIE_USPEHA, Map(Krasavitsa -> 1.0, Lovkost -> 1.0, Umelitsa -> 0.5, Nezametnost -> 0.5),
      Some("Закатить пир"), Some("У боярина дела идут отменно! Надобно это отпраздновать, пир горой закатить. Триста человек гостей приглашенных! Да только кто бы помог праздник провести, гостей хлебом да солью угощать. Чтоб и гостям не мешать, и всегда у них полные чаши были."))
      .weight(0.3).ingNum(1).questor(BOYARIN_VSEVOLOD),
    new ItemMissionGenerator(5301, 53, 1000, 2, Product.NASTOYKA_VECHNOY_UNOSTI, Map(Mudrost -> 1.0, Zolotye_ruki -> 1.0, Sila -> 0.5, Terpenie -> 0.5),
      Some("Огород"), Some("Бабе с Дедом опять нужна помощь: надо бы на огороде порядок навести. Распланировать грядки, проложить дорожки, где бадью с водой поставить, а где пугало. Задачка требует и руки приложить и голову. А Бабка за это настоечки приготовит. "))
      .weight(0.3).ingNum(1).questor(BABKA_DEDKA),
    new ItemMissionGenerator(5302, 53, 1000, 2, Product.VOLSHEBNAYA_PALOCHKA_IZLECHENIYA, Map(Krasavets -> 1.0, Hitrost -> 1.0, Nezametnost -> 0.5, Ustrashenie -> 0.5),
      Some("Интриги"), Some("Есть у Шамаханской царицы подозрение, что придворные интриги плетут. Ищет царица тайных агентов, что смогли бы под видом пажей всё разнюхать."))
      .weight(0.3).ingNum(1).questor(SHAMAHANSKAYA),







    // new ItemMissionGenerator(11103, 115, 1000, 1, Seed.Solnechnik, Map(Terpenie -> 1.0),
    //     Some("Посидеть с внучкой"), Some("К Деду и Бабке внучка приехала, оторва и непоседа. Собрались Дедка и Бабка на ярмарку, да вот кто бы за внучкой приглядел? С такой внучкой сидеть недюжее терпение надо иметь!"))
    //     .weight(0.1).ingNum(5),
    new ItemMissionGenerator(500001, 450, 1000, 2, Product.PIROJENKA_NOCHNOI_STRASTI, Map(Krasavets -> 1.0, Zolotye_ruki -> 1.0, Lovkost -> 0.5, Sila -> 0.5),
      Some(""), Some(""))
      .weight(0.3).ingNum(1).questor(SHAMAHANSKAYA)


  )

  private val gen_map = gen_arr.map(gen => gen.typeId -> gen).toMap

  def find(id: Int) = gen_map.get(id)

  def apply(id: Int) = gen_map(id)

  def all = gen_arr

  def byLevel(level: Int) = gen_arr.filter(gen => gen.level <= level && gen.maxLevel >= level)
}
