package model.mission

import bean.{GameUser, Mission}
import components.context.GameContext
import model.hero.Ability
import model.{Reward, TransactionGenerator}
import play.api.mvc.AnyContent
import util.Implicits._

import scala.language.postfixOps

/**
  * Created by Salewan on 17.04.2017.
  */
class MoneyMissionGenerator(
                             val typeId: Int,

                             val level: Int,

                             val maxLevel: Int,

                             h_num: Int,

                             moneyMin: Long,

                             moneyMax: Long,

                             abil: Map[Ability, Double],

                             override val name: Option[String],

                             override val text: Option[String]

                           ) extends MissionGenerator {


  override def generate(gameUser: GameUser): Mission = {
    val money = util.RandomUtil.randomLong(moneyMin, moneyMax) //деньги
    val duration = money * (1 minutes) / 10 // время поручения
    val difficulty = h_num * (math.max(1, level - 3) + 10) // сложность
    val mission = Mission().
      questor(questor).
      owner(gameUser).
      generator(typeId).
      money(money).
      duration(duration).
      heroesNum(h_num).
      level(level).
      difficulty(difficulty)
    mission.abilities.putAll(abil.toSeq.map({case (a, p) => a.typeId -> p}))
    mission
  }

  override def reward(mission: Mission, win: Boolean, bonus: Boolean)(implicit ctx: GameContext[AnyContent]): Reward = {
    val reward = super.reward(mission, win, bonus)
    for (money <- mission.money if win) {
      reward += mission.owner.money.addCoins(money, TransactionGenerator.MISSION_COMPLETE_TX.get)
    }
    reward
  }
}
