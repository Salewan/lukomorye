package model.mission

import components.enumeration.{Enumerable, EnumerationBoilerplate}
import enumeratum.{Enum, EnumEntry}

/**
  * Created by Salewan on 25.08.2017.
  */
sealed trait Questor extends EnumEntry with Enumerable[Int] {
  val image: String
  val name: String
}

class QuestorTemplate(val typeId: Int, val image: String, val name: String) extends Questor

object Questor extends Enum[Questor] with EnumerationBoilerplate[Int, Questor] {
  def values = findValues
  override def entries: IndexedSeq[Questor] = values

  case object SALTAN extends QuestorTemplate(0, "reputation/tsar.png", "Царь Салтан")
  case object BABKA_DEDKA extends QuestorTemplate(1, "questors/babka_dedka.png", "Дед и Баба")
  case object MELNIK extends QuestorTemplate(2, "questors/melnik.png", "Мельник")
  case object BABA_YAGA extends QuestorTemplate(3, "questors/baba_yaga.png", "Баба Яга")
  case object PASTUH extends QuestorTemplate(4, "questors/pastuh.png", "Пастух")
  case object SADKO extends QuestorTemplate(5, "questors/sadko.png", "Купец Садко")
  case object SINDBAD extends QuestorTemplate(6, "questors/sindbad.png", "Синдбад-мореход")
  case object VODYANOI extends QuestorTemplate(7, "questors/vodyanoi.png", "Водяной")
  case object MARYA_MOREVNA extends QuestorTemplate(8, "questors/marya_morevna.png", "Марья-Моревна")
  case object SHAMAHANSKAYA extends QuestorTemplate(9, "questors/shamahanskaya.png", "Шамаханская царица")
  case object BOYARIN_VSEVOLOD extends QuestorTemplate(10, "questors/boyarin_vsevolod.png", "Боярин Всеволод")
  case object EXP_LVL1 extends QuestorTemplate(11, "questors/exp_lvl1.png", "Начальное обучение")
  case object EXP_LVL2 extends QuestorTemplate(12, "questors/exp_lvl2.png", "Продвинутое обучение")
}
