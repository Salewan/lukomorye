package model.mission

import bean.{GameUser, Mission}
import components.context.GameContext
import model.Reward
import model.hero.Ability
import play.api.mvc.AnyContent
import util.Implicits._

import scala.language.postfixOps

/**
  * Created by Salewan on 17.04.2017.
  */
class UserExpMissionGenerator(
                               val typeId: Int,

                               val level: Int,

                               val maxLevel: Int,

                               h_num: Int,

                               heroExpMin: Long,

                               heroExpMax: Long,

                               abil: Map[Ability, Double],

                               override val name: Option[String],

                               override val text: Option[String]

                             ) extends MissionGenerator {

  override def generate(gameUser: GameUser): Mission = {
    val exp = util.RandomUtil.randomLong(heroExpMin, heroExpMax) //эксп юзеру
    val duration = exp * (1 minutes) / 10 // время поручения
    val difficulty = h_num * (math.max(1, level - 3) + 10) // сложность
    val mission = Mission().
      questor(questor).
      owner(gameUser).
      generator(typeId).
      userExp(exp).
      duration(duration).
      heroesNum(h_num).
      level(level).
      difficulty(difficulty)
    mission.abilities.putAll(abil.toSeq.map({case (a, p) => a.typeId -> p}))
    mission
  }

  override def reward(mission: Mission, win: Boolean, bonus: Boolean)(implicit ctx: GameContext[AnyContent]): Reward = {
    val reward = super.reward(mission, win, bonus)
    for (userExp <- mission.userExp if win) {
      reward += mission.owner.addExp(userExp)
    }
    reward
  }
}
