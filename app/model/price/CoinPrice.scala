package model.price

import model.price.discount.Discount
import model.price.discount.impl.NoDiscount

/**
  * Created by Salewan on 10.07.2017.
  */
case class CoinPrice(raw: Long, discount: Discount = NoDiscount) extends Price {
  override val currency: Currency = Currency.Coin
}
