package model.price

import components.enumeration.Enumerable

/**
  * Created by Salewan on 08.07.2017.
  */
sealed trait Currency extends Enumerable[Int]

object Currency {

  case object Ruby extends Currency {
    override val typeId: Int = 1
  }

  case object Coin extends Currency {
    override val typeId: Int = 2
  }

  case object Dust extends Currency {
    override val typeId: Int = 3
  }

}
