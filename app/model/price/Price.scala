package model.price

import model.price.discount.Discount

/**
  * Created by Salewan on 03.07.2017.
  */
trait Price extends PriceContext {

  val raw: Long

  val currency: Currency

  val discount: Discount

  def valueDouble(implicit ctx: Context): Double = discount.applyDiscount(raw)

  def valueCeil(implicit ctx: Context): Long = math.ceil(valueDouble).asInstanceOf[Long]

  def valueRound(implicit ctx: Context): Long = math.round(valueDouble)

  // get value by default manner
  def value(implicit ctx: Context): Long = valueRound
}
