package model.price

import components.{HibernateSessionAware, UserEvidence}

/**
  * Created by Salewan on 10.07.2017.
  */
trait PriceContext {
  type Context = HibernateSessionAware with UserEvidence
}
