package model.price.discount

/**
  * Created by Salewan on 08.07.2017.
  */
abstract class AbstractDiscount extends Discount {
  override def andThen(other: Discount): Discount = DiscountSeq(this :: other :: Nil)

  override def applyDiscount(price: Long)(implicit ctx: Context): Double = {
    innerApply(price, price)._2
  }
}
