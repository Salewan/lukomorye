package model.price.discount

import bean.Action
import dao.ActionDAO
import model.price.PriceContext

/**
  * Created by Salewan on 03.07.2017.
  */
trait Discount extends PriceContext {

  def andThen(other: Discount): Discount
  def applyDiscount(price: Long)(implicit ctx: Context): Double
  def innerApply(start: Double, after: Double)(implicit ctx: Context): (Double, Double)
  protected def currentAction(implicit ctx: Context): Option[Action] = new ActionDAO(ctx.hibernateSession).getCurrentAction
}

object Discount {

  def absoluteDiscount(absValue: Int, start: Double, after: Double): (Double, Double) = {
    start -> math.max(0.0, after - absValue.asInstanceOf[Double])
  }

  def percentDiscount(humanPercent: Double, start: Double, after: Double): (Double, Double) = {
    start -> math.max(0.0, after - (start / 100.0 * humanPercent))
  }
}
