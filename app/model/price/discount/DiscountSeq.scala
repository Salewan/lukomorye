package model.price.discount

/**
  * Created by Salewan on 08.07.2017.
  */
class DiscountSeq(all: List[Discount]) extends AbstractDiscount {
  override def andThen(other: Discount): Discount = DiscountSeq(other :: all)

  override def innerApply(start: Double, after: Double)(implicit ctx: Context): (Double, Double) = {
    all.foldLeft[(Double, Double)](start -> after)((tpl, discount) => discount.innerApply(tpl._1, tpl._2))
  }
}

object DiscountSeq {
  def apply(all: List[Discount]) = new DiscountSeq(all)
}
