package model.price.discount.impl

import model.action.ActionType
import model.price.discount.{AbstractDiscount, Discount}

/**
  * Created by Salewan on 10.07.2017.
  */
object BuffHalfPriceByAction extends AbstractDiscount {

  override def innerApply(start: Double, after: Double)(implicit ctx: Context): (Double, Double) = {
    for {
      action <- currentAction if action.model == ActionType.ACTION_BUFF_DISCOUNT_50
      time <- ctx.gameUser.lastDiscountActionTime if action.creationTime <= time
    } yield Discount.percentDiscount(50.0, start, after)
  } getOrElse(start -> after)

}
