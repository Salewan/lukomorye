package model.price.discount.impl

import model.price.discount.AbstractDiscount

/**
  * Created by Salewan on 10.07.2017.
  */
object NoDiscount extends AbstractDiscount {
  override def innerApply(start: Double, after: Double)(implicit ctx: Context): (Double, Double) = start -> after
}
