package model.rating

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.12.2016.
  */
class Rating(ratingType: RatingType, val entries: Seq[RatingEntity]) {

  private val positions: Map[Int, Int] = entries.map(_.id).zip(1 to entries.length).toMap

  def isEmpty = entries.isEmpty

  def getPosition(id: Int): Int = positions.getOrElse(id, -1)
}
