package model.rating

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.12.2016.
  */
case class RatingEntity(id: Int, value: Long)
