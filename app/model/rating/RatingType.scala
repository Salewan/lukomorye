package model.rating

import bean.Clan
import model.ReputationModel
import util.RatingUtil

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.12.2016.
  */
trait RatingType {
  val typeId: Int
  val name: String
  val image: String
}

trait Personal {
  def getRating(optional: Int = 0): Rating
}

trait ClanInner {
  def getRating(clan: Clan): Rating
}

trait ClanOuter {
  def getRating(alliance: Option[ReputationModel] = None): Option[Rating]
}

object RatingType {

  case object LEVEL extends RatingType with Personal {
    override val typeId: Int = 1
    override val name: String = "уровню"
    override val image: String = "exp.png"

    override def getRating(optional: Int = 0): Rating = RatingUtil.getUserLevelRating
  }

  case object REPUTATION extends RatingType with Personal {
    override val typeId: Int = 2
    override val name: String = "репутации"
    override val image: String = "reputation.png"

    override def getRating(reputation: Int): Rating = RatingUtil.getReputationRating(reputation)
  }

  case object CLAN_INNER_EXP extends RatingType with ClanInner {
    override val typeId: Int = 3
    override val name: String = "опыту"
    override val image: String = "exp.png"

    override def getRating(clan: Clan): Rating = RatingUtil.getClanInnerExpRating(clan)
  }

  case object CLAN_INNER_WEEK_EXP extends RatingType with ClanInner {
    override val typeId: Int = 4
    override val name: String = "опыту за неделю"
    override val image: String = "exp.png"

    override def getRating(clan: Clan): Rating = RatingUtil.getClanInnerWeekExpRating(clan)
  }

  case object CLAN_INNER_RUBIES extends RatingType with ClanInner {
    override val typeId: Int = 5
    override val name: String = "взносам"
    override val image: String = "ruby.png"

    override def getRating(clan: Clan): Rating = RatingUtil.getClanInnerRubiesRating(clan)
  }

  case object CLAN_INNER_WEEK_RUBIES extends RatingType with ClanInner {
    override val typeId: Int = 6
    override val name: String = "взносам за неделю"
    override val image: String = "ruby.png"

    override def getRating(clan: Clan): Rating = RatingUtil.getClanInnerWeekRubiesRating(clan)
  }

  case object CLAN_LEVEL_EXP extends RatingType with ClanOuter {
    override val typeId: Int = 7
    override val name: String = "опыту"
    override val image: String = "exp.png"

    override def getRating(alliance: Option[ReputationModel]): Option[Rating] = Some(RatingUtil.getClanLevelRating)
  }

  case object CLAN_ALLIANCE extends RatingType with ClanOuter {
    override val typeId: Int = 8
    override val name: String = "союзу"
    override val image: String = "exp.png"

    override def getRating(alliance: Option[ReputationModel]): Option[Rating] =
      alliance.map(m => RatingUtil.getAllianceRating(m.typeId))
  }

  val userRatings: Array[RatingType with Personal] = Array(LEVEL, REPUTATION)
  private val userRatingMap = userRatings.map(r => r.typeId -> r).toMap
  def findPersonal(rating: Int): Option[RatingType with Personal] = userRatingMap.get(rating)

  val clanRatings: Array[RatingType with ClanInner] = Array(CLAN_INNER_EXP, CLAN_INNER_WEEK_EXP)
  private val clanRatingMap = clanRatings.map(r => r.typeId -> r).toMap
  def findClanInner(rating: Int): Option[RatingType with ClanInner] = clanRatingMap.get(rating)

  val clanOuterRatings: Array[RatingType with ClanOuter] = Array(CLAN_ALLIANCE, CLAN_LEVEL_EXP)
  def findClanOuter(rating: Int): Option[RatingType with ClanOuter] = clanOuterRatings.find(_.typeId == rating)

  val clanBankRatings: Array[RatingType with ClanInner] = Array(CLAN_INNER_RUBIES, CLAN_INNER_WEEK_RUBIES)
  def findBankRatings(rating: Int): Option[RatingType with ClanInner] = clanBankRatings.find(_.typeId == rating)
}
