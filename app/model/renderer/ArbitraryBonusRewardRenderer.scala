package model.renderer
import model.item.Ingredient
import play.twirl.api.Html

case class ArbitraryBonusRewardRenderer(ing: Ingredient, hnum: Int) extends TemplateRenderer {
  override def render: Html = views.html.misc.reward_template.ArbitraryBonusRewardTemplate(ing, hnum == 1)
}
