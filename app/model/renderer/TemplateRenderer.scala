package model.renderer

import play.twirl.api.Html

trait TemplateRenderer {

  def render: Html
}
