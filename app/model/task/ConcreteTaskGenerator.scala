package model.task

import bean.{GameUser, Task, TaskSlot}
import model.ReputationModel
import model.item.Ingredient
import util.RandomUtil

/**
  * Created by Salewan on 16.04.2017.
  */
class ConcreteTaskGenerator(
                             val typeId: Int, val level: Int, val maxLevel: Int, val weight: Double,

                             rep: ReputationModel, items: Map[Ingredient, (Int,Int)],

                             _name: String, _text: String

                           ) extends TaskGenerator {

  override val reputation: ReputationModel = rep
  override val name: Option[String] = Some(_name)
  override val text: Option[String] = Some(_text)
  override val asEvent: Boolean = false

  override def generate(gameUser: GameUser): Task = {
    val task = Task().owner(gameUser).generator(typeId)

    val slots: Iterable[TaskSlot] = for (((ing, (min, max)), idx) <- items.zipWithIndex) yield {
      val cnt = RandomUtil.random(min, max)
      new TaskSlot().gameItem(ing).count(cnt).index(idx).task(task)
    }

    task.slots.addAll(slots)
    task
  }
}
