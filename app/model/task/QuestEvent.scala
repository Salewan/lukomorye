package model.task

import model.ReputationModel
import model.item.Ingredient

/**
  * Created by Salewan on 05.05.2017.
  */
class QuestEvent(
                  typeId: Int, level: Int, maxLevel: Int, weight: Double,

                  rep: ReputationModel, items: Map[Ingredient, (Int,Int)],

                  _name: String, _text: String

                ) extends ConcreteTaskGenerator(typeId, level, maxLevel, weight, rep, items, _name, _text) {

  override val asEvent = true
}
