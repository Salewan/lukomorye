package model.transform.gamble

import bean.GameUser
import components.HibernateSessionAware
import model.Reward

/**
  * @author Sergey Lebedev (salewan@gmail.com) 25.11.2016.
  */
trait GambleStrategy {

  def doGamble(user: GameUser)(implicit hib: HibernateSessionAware): Reward
}
