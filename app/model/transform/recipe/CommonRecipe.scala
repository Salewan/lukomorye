package model.transform.recipe

import bean.{GameUser, Transform}
import components.HibernateSessionAware
import model.Reward
import model.item.Ingredient

/**
  * Обычный рецепт который проверяет полный матчинг ингредиентов
  */
class CommonRecipe(override val typeId: Int, ings: Set[Ingredient], result: Ingredient) extends TransformRecipe {

  override val name: (Transform) => String = _ => result.name

  override val image: (Transform) => String = _ => result.image

  override def isResultOf(test: Set[Ingredient]): Boolean = test == ings

  override def canTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Boolean = ings.forall { ing =>
    gameUser.barn.get(ing.typeId).exists(_ >= num)
  }

  override def doTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Reward = {

    if (canTransform(gameUser, num)) {
      gameUser.addIngredient(ings.head, -num)
      gameUser.addIngredient(ings.tail.head, -num)
      gameUser.transform.clear()
      gameUser.addIngredient(result, num)
    } else Reward()
  }
}
