package model.transform.recipe

import bean.{GameUser, Transform}
import components.HibernateSessionAware
import model.item.{Ingredient, UnlimitedIng}
import model.{Reward, TransactionGenerator}

/**
  * Конкретный рецепт для получения "Зелья времени"
  */
case class GettingSandsOfTime(typeId: Int) extends TransformRecipe {

  override val name: Transform => String = _ => "зелье времени"

  override val image: Transform => String = _ => "misc3/41_1.png"

  override def isResultOf(test: Set[Ingredient]): Boolean =
    test.size == 2 && test.contains(UnlimitedIng.HRONOSFERA) && test.count(_.unlimited) == 1

  private def secondIngredient(tr: Transform): Option[Ingredient] = {
    for (i <- 0 to 1; setuped <- tr.ing(i) if !setuped.unlimited) yield setuped
  }.headOption

  override def canTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Boolean = {
    val tr = gameUser.transform
    val barn = gameUser.barn
    val firstIng = UnlimitedIng.HRONOSFERA.typeId
    val secondIng = secondIngredient(tr).map(_.typeId)

    barn.containsKey(firstIng) && secondIng.exists(second => barn.get(second).exists(_ >= num))
  }

  override def doTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Reward = {
    for (burn <- secondIngredient(gameUser.transform) if canTransform(gameUser, num)) yield burn
  }.map { burn =>
    gameUser.addIngredient(burn, -num)
    gameUser.transform.clear()
    val reward = gameUser.money.addDreamDust(burn.nominal * num, TransactionGenerator.ALCHEMY_TX.get)
    reward
  } getOrElse Reward()
}
