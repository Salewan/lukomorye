package model.transform.recipe

import bean.{GameUser, Transform}
import components.HibernateSessionAware
import model.Reward
import model.item.Ingredient

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.10.2016.
  *         Рецепт для "Преобразований" ("Алхимии")
  */
trait TransformRecipe {

  val typeId: Int

  val name: Transform => String

  val image: Transform => String

  def isResultOf(test: Set[Ingredient]): Boolean

  def doTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Reward

  def canTransform(gameUser: GameUser, num: Int)(implicit hib: HibernateSessionAware): Boolean
}

object TransformRecipe {

  val recipes: Array[TransformRecipe] = Array(
    GettingSandsOfTime(4)
  )

  val recipeMap: Map[Int, TransformRecipe] = recipes map (r => r.typeId -> r) toMap

  def findRecipe(test: Set[Ingredient]): Option[TransformRecipe] = recipes.find(_.isResultOf(test))
}
