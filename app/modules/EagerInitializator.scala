package modules

import com.google.inject.AbstractModule
import components.PlayComponentsHolder

/**
 * @author Sergey Lebedev (salewan@gmail.com) 14.03.2016.
 */
trait EagerInitee

class EagerInitializator extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[EagerInitee]).to(classOf[PlayComponentsHolder]).asEagerSingleton()
  }
}
