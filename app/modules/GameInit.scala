package modules

import javax.inject.Singleton

import dao.UserDAO
import hibernate.{HibernateContext, HibernateDependable, HibernateSession}
import components.HibernateSessionAware
import play.api.Logger
import util.AuthorizationUtil

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.01.2017.
  */
@Singleton
class GameInit extends HibernateDependable {

  override def run(hibernateContext: HibernateContext): Unit = {
    val session = hibernateContext.openSession()
    implicit val hsa = HibernateSessionAware(session)
    val dao = new UserDAO(session)

    def checkUser(nick: String, role: Char, sex: Char, pp: String = "GyaSl"): Unit = {
      if (dao.findByNick(nick).nonEmpty) Logger.info(s"Initializing admin users: $nick already exists.")
      else {
        val pass = AuthorizationUtil.md5WithSalt(pp)
        dao.createUser().role(role).avatar.sex(sex).user.registerWith(nick, pass, None)
        Logger.info(s"Initializing admin users: $nick created with password 1")
      }
    }

    checkUser("admin", 'a', 'm', "HngpovbSnKDCQPwegxXj")
    checkUser("salewan", 's', 'm')
    checkUser("henaro", 's', 'm')
    checkUser("ilyha", 's', 'm')
    checkUser(config.POST_LOGIN, 'u', 'm')
    checkUser(config.ADMINISTRATION_LOGIN, 'u', 'm')
    session.close(true)
  }

  override def stop(): Boolean = true
}
