import java.text.{DecimalFormat, SimpleDateFormat}
import java.time.ZoneId
import java.util.{Locale, TimeZone}
import java.util.concurrent.TimeUnit

import util.Implicits._
import util.TimeUtil
import util.javas.OnlineUtil

import scala.concurrent.duration.Duration
import scala.language.postfixOps

package object app {

  private def _chance(x: Int, values: Array[String]): String = {
    if (x < 30) values(0)
    else if (x < 50) values(1)
    else if (x < 80) values(2)
    else values(3)
  }

  def chanceColor(x: Int): String = _chance(x, Array("#FF0000", "#C8600A", "#FFFC7F", "#62ff0e"))
  def chanceColor(d: Double): String = chanceColor(chanceInt(d))

  def chanceWord(x: Int): String = _chance(x, Array("ничтожный", "низкий", "средний", "высокий"))
  def chanceWord(d: Double): String = chanceWord(chanceInt(d))

  def chanceInt(d: Double): Int = ((d * 100) round) toInt

  def formatDouble(d: Double): String = {
    val formatter = new DecimalFormat("###.##")
    formatter.format(d)
  }

  def timeColor(time: Long): String = {
    if (time < (2 hours)) "red"
    else if (time < (8 hours)) "major"
    else "green"
  }
}

package object config {

  val COOKIE_ID_NAME = "id"
  val VIRUS_COOKIE_NAME = "hdid"
  val TZ = TimeZone.getTimeZone("Europe/Moscow")

  val FOOTER_DATE_FORMAT = new SimpleDateFormat("dd MMM HH:mm:ss", new Locale("RU"))
  FOOTER_DATE_FORMAT.setTimeZone(TZ)
  val DATE_FORMAT_DETAILED2 = new SimpleDateFormat("HH:mm dd.MM.yyyy", new Locale("RU"))
  DATE_FORMAT_DETAILED2.setTimeZone(TZ)
  // maggard
  val DATE_FORMAT = new SimpleDateFormat("dd MMM HH:mm", new Locale("RU"))
  DATE_FORMAT.setTimeZone(TZ)
  var DATE_FORMAT_DETAILED = new SimpleDateFormat("dd MMM HH:mm:ss", new Locale("RU"))
  DATE_FORMAT_DETAILED.setTimeZone(TZ)
  var DATE_ONLY_FORMAT = new SimpleDateFormat("d MMM yyyy", new Locale("RU"))
  DATE_ONLY_FORMAT.setTimeZone(TZ)

  val WIZARD_STATE_CACHE_STORE_TIME = Duration(10.0, TimeUnit.MINUTES)

  val SMALL_SPENDING_UP_TO = 10 //todo: get back 10

  val NICK_SIZE_MIN = 2

  val NICK_SIZE_MAX = 16

  val NICK_PATTERN = """^([a-zA-Zа-яёА-ЯЁ]+)(\s([a-zA-Zа-яёА-ЯЁ]+))?(\s([a-zA-Zа-яёА-ЯЁ]+))?$"""

  val TUTORIAL_STEPS = 29

  val MAX_USER_LEVEL = 60

  val START_DIRT_COUNT = 3

  val START_PET_COUNT = 3

  val TRANSFORM_LEVEL = 1 // Превращения

  val FARM_LEVEL = 2 // Огород

  val SHOP_LEVEL = 3 // Рынок

  val CLAN_REQUIRED_LEVEL = OnlineUtil.CLAN_REQUIRED_LEVEL // уровень для княжества

  val MISSIONS_GENERATION_INTERVAL = 5 minutes

  val TASKS_GENERATION_INTERVAL = 5 minutes

  val TASKS_BASIC_VALUE = 10
  val TASKS_MAX_VALUE = 30
  val MISSIONS_MAX_UPGRADES = 20
  val MISSIONS_BASIC_COUNT = 10
  val MISSIONS_UPGRADE_LEVEL = 10 // с какого уровня доступно увеличение поручений
  val TASKS_UPGRADE_LEVEL = 10 // с какого уровня доступно увеличение заданий

  val MAX_RATING_USERS = 1000

  val NASEST_LEVEL = 3
  val HLEV_LEVEL = 10
  val PESHHERA_LEVEL = 20
  val ZAGON_LEVEL = 31

  val MESSAGES_ALLOWED_LEVEL: Int = 1 // форму + личка
  val REQUIRED_FOR_WRITE_LEVEL = MESSAGES_ALLOWED_LEVEL // forum

  val ADMINISTRATION_LOGIN = "администрация"
  val POST_LOGIN = "почтовая служба"
  val SPETIAL_USERS = Array(ADMINISTRATION_LOGIN, POST_LOGIN)
  val MAX_DIALOG_MESSAGE_SIZE = 1024

  val SHOP_GEN_INTERVAL: Long = 30 minutes
  val SHOP_SLOTS = 6

  val BARN_SIZE = 50 // размер амбара

  val PET_FOOD_MULTIPLIER = 3

  val LABORATORY_LEVEL = 4 // Лаборатория

  val ACTIVE_HEROES_COUNT = 10 // базовое количество активных героев

  val SCARE_INTERVAL = 20 seconds // защитный интервал для поручений

  val INITIAL_SLOT_NUMBER = 3 // сколько первоначально слотов в производстве

  val TAVERNA_LEVEL = 5 // Уровень с которого показывать таверну для найма героев

  val BUFFS_LEVEL = 8 // Уровень с которого доступен магазин (баффы)

  val CLAN_BANK_LIMIT = 50000 // Максимум рубинов в кассе княжества

  val REG_LEVEL = 10 // Уровень принудительной регистрации

  val DAILY_MISSIONS_MAX = 4 // принять в день поручений
  val DAILY_TASKS_MAX = 4 // принять в день заданий

  val MIN_STAMPS = 20 // начальное значение княжеских печатей

  val CLAN_BONUS_REDIST_PERIOD = 1.day // интервал перераспределения бонусов в княжестве
}

package object billing {
  //payments
  val TWOPAY_DEFAULT_SUM = 200
  val RUBIES_REAL_AMOUNT = 0.1
  val TWOPAY_DEFAULT_PROJECT_ID = 24507
  val TWOPAY_SECRET_KEY = "L/fHV*Ju)H4Q0i5U-WyY4khl5JOmAtZ?"
  val MAX_GET_HTML_TIMEOUT: Long = 5 * TimeUtil.SECOND_MILLIS
  val MIN_PAYMENT_AMOUNT_FOR_CONFIRMATION = 2000
  val MAX_SMS_EXPENSE_PER_DAY = 20000 //200 рублей (в копейках)
}
