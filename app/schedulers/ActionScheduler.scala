package schedulers

import javax.inject.{Inject, Singleton}

import akka.actor.{ActorSystem, Cancellable}
import bean.Action
import dao.ActionDAO
import hibernate.{HibernateContext, HibernateDependable, HibernateRunnable}
import org.joda.time.DateTime
import play.api.Logger.logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import util.TimeUtil

import scala.concurrent.duration._

/**
  * Created by Salewan on 13.07.2017.
  */
@Singleton
class ActionScheduler @Inject() (system: ActorSystem) extends HibernateDependable {

  private val INITIAL_DELAY = 1.second
  private val INTERVAL = 5.minutes
  private val ACTION_DURATION = 30 * TimeUtil.HOUR_MILLIS
  private var cancellable: Cancellable = _

  private val block = new HibernateRunnable {
    override def run(): Unit = {

      if (new DateTime().hourOfDay().get() == 0) {
        val now = System.currentTimeMillis()
        val dao = ActionDAO(session)
        if (dao.getCurrentAction.exists(action => now - action.creationTime > ACTION_DURATION)) {
          logger.info("Switching action...")
          dao.getActionFromQueue.map(next => {
            next.queued(false)
            next.creationTime(now)
            session.persist(Action(next))
            s"Action switched, name=${next.name}"
          }) orElse Some("No queued actions found.") foreach logger.info
        }
      }

    }
  }
  override def run(hibernateContext: HibernateContext): Unit = {
    cancellable = system.scheduler.schedule(INITIAL_DELAY, INTERVAL, block.runnable(hibernateContext))
  }

  override def stop(): Boolean = {
    cancellable.isCancelled || cancellable.cancel()
  }
}
