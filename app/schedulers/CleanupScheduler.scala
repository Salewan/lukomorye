package schedulers

import java.util.Date
import javax.inject.{Inject, Singleton}

import akka.actor.{ActorSystem, Cancellable}
import bean.messages.personal.{PrivateDialog, PrivateMessage}
import dao.MessageDAO
import hibernate.{HibernateContext, HibernateDependable, HibernateRunnable}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.duration._

@Singleton
class CleanupScheduler @Inject() (system: ActorSystem) extends HibernateDependable {

  private val logger = Logger(classOf[CleanupScheduler])
  private val INITIAL_DELAY = 10.seconds
  private val INTERVAL = 1.day
  private val PERSONAL_MESSAGES_STORAGE_TIME = 30.days
  private var cancellable: Cancellable = _

  private val block = new HibernateRunnable {
    override def run(): Unit = {

      if (!cancellable.isCancelled) {
        logger.info("Cleaning personal messages")
        val beforeDate = new Date(System.currentTimeMillis() - PERSONAL_MESSAGES_STORAGE_TIME.toMillis)
        var hib = hibernateContext.openSession()
        val ids = new MessageDAO(hib).getMessagesIds(beforeDate, 500000)
        logger.info(s"Processing ${ids.size} messages ...")

        var count = 0
        for (id <- ids) {
          if (!cancellable.isCancelled) {
            if (!hib.isOpen) {
              hib = hibernateContext.openSession()
            }
            val message = hib.get(classOf[PrivateMessage], id)
            message.foreach { message =>
              hib.delete(message)
              message.dialog.foreach(dialog => hib.resetCollectionCache(classOf[PrivateDialog], dialog.id, "Messages"))
              count += 1
              if (count % 100 == 0) {
                hib.close(true)
              }
            }
          }
        }
        if (count % 100 != 0) {
          logger.info(s"Private messages flushed: $count")
          hib.close(true)
        }
        logger.info("Cleaning personal messages finished")
      }

      if (!cancellable.isCancelled) {
        logger.info("Cleaning empty dialogs")
        var hib = hibernateContext.openSession()
        val ids = new MessageDAO(hib).getEmptyMessagesDialogIds
        logger.info(s"Processing ${ids.size} empty dialog ...")

        var count = 0
        for (id <- ids) {
          if (!cancellable.isCancelled) {
            if (!hib.isOpen) {
              hib = hibernateContext.openSession()
            }
            val dialog = hib.get(classOf[PrivateDialog], id)
            dialog.foreach { dialog =>
              val d = dialog.owner.dialogs -= dialog.target.id
              if (d != null) {
                d.messages.clear()
              }
              hib.delete(dialog)
              count += 1
              if (count % 100 == 0) {
                hib.close(true)
                logger.info(s"Empty dialogs flushed: $count")
              }
            }
          }
        }
        if (count % 100 != 0) {
          logger.info(s"Empty dialogs flushed: $count")
          hib.close(true)
        }
        logger.info("Cleaning empty dialogs finished")
      }
    }
  }

  override def run(hibernateContext: HibernateContext): Unit = {
    cancellable = system.scheduler.schedule(INITIAL_DELAY, INTERVAL, block.runnable(hibernateContext))
    logger.info("CleanupScheduler has started.")
  }

  override def stop(): Boolean = {
    val status = cancellable.isCancelled || cancellable.cancel()
    logger.info("CleanupScheduler stopped - " + status)
    status
  }
}
