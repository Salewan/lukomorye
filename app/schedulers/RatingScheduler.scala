package schedulers

import javax.inject.{Inject, Singleton}

import akka.actor.{ActorSystem, Cancellable}
import dao.RatingDAO
import hibernate.{HibernateContext, HibernateDependable, HibernateRunnable}
import model.ReputationModel
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import util.RatingUtil

import scala.concurrent.duration._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.11.2016.
  */

@Singleton
class RatingScheduler @Inject() (system: ActorSystem) extends HibernateDependable {

  private val INITIAL_DELAY = 1.second
  private val INTERVAL = 5.minutes
  private var cancellable: Cancellable = _

  private val block = new HibernateRunnable {
    override def run(): Unit = {
      Logger.debug("Starting rating calculation")

      val dao = new RatingDAO(session)

      if (!cancellable.isCancelled) {
        RatingUtil.setUserLevelRating(dao.getUserLevelRating)
      }

      for (reputationType <- ReputationModel.entries) {
        if (!cancellable.isCancelled) {
          val reputation = reputationType.typeId
          RatingUtil.setReputationRating(reputation, dao.getUserReputationLevelRating(reputation))
        }
      }

      // внешние клановские рейтинги по репутации (по союзам)
      for (reputationType <- ReputationModel.entries) {
        if (!cancellable.isCancelled) {
          val reputation = reputationType.typeId
          RatingUtil.setAllianceRating(reputation, dao.getClanAllianceRating(reputation))
        }
      }

      if (!cancellable.isCancelled) {
        RatingUtil.setClanLevelRating(dao.getClanLevelRating)
      }

      Logger.debug("Ended ratings calculation")
    }
  }

  override def run(hibernateContext: HibernateContext): Unit = {
    cancellable = system.scheduler.schedule(INITIAL_DELAY, INTERVAL, block.runnable(hibernateContext))
    Logger.info("RatingScheduler has started.")
  }

  override def stop(): Boolean = {
    val status = cancellable.isCancelled || cancellable.cancel()
    Logger.info("RatingScheduler stopped - " + status)
    status
  }
}
