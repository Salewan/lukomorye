package schedulers

import javax.inject.{Inject, Singleton}

import hibernate.HibernateDependable

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.11.2016.
  */

@Singleton
class Schedulers @Inject() (
                             ratingScheduler: RatingScheduler,
                             actionScheduler: ActionScheduler,
                             cleanupScheduler: CleanupScheduler,
                             weekCountersScheduler: WeekCountersScheduler
                           ) {

  def allSchedulers: Iterable[HibernateDependable] =
    ratingScheduler :: actionScheduler :: cleanupScheduler :: weekCountersScheduler :: Nil
}
