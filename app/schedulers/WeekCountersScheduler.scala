package schedulers

import javax.inject.{Inject, Singleton}

import akka.actor.{ActorSystem, Cancellable}
import bean.GameUser
import dao.UserDAO
import hibernate._
import play.api.Logger
import util.WeekCounters

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

@Singleton
class WeekCountersScheduler @Inject() (system: ActorSystem) extends HibernateDependable {

  private val INITIAL_DELAY = 30.second
  private val INTERVAL = 23.hours
  private var cancellable: Cancellable = _
  private val logger = Logger(classOf[WeekCountersScheduler])

  private val block = new HibernateRunnable {
    override def run(): Unit = {
      if (!cancellable.isCancelled) process(NamedCounters(
        "WeekClanRubies",
        _.getUsersIdsForWeekClanRubiesProcessing,
        _.clanWeekRubies
      ))

      if (!cancellable.isCancelled) process(NamedCounters(
        "WeekClanExp",
        _.getUsersIdsForWeekClanExpProcessing,
        _.clanWeekExp
      ))
    }

    private def process(namedCounters: NamedCounters): Unit = {
      var session = hibernateContext.openSession()
      val ids = namedCounters.uidsSupplier(new UserDAO(session))
      logger.info(s"Processing ${namedCounters.name} ${ids.size} users ...")
      var count = 0
      var updated = 0
      var cleaned = 0

      for (id <- ids if !cancellable.isCancelled) {
        if (!session.isOpen) { session = hibernateContext.openSession() }

        session.get(classOf[GameUser], id).foreach {user =>
          val counters = namedCounters.countersExtractor(user)
          if (user.isActive) {
            if (WeekCounters.weekCounter(counters) > 0) {
              WeekCounters.refreshWeekCounters(counters)
              updated += 1
            }
          } else {
            counters.clear()
            cleaned += 1
          }
          count += 1
          if (count % 1000 == 0) {
            session.close(true)
            logger.info(s"${namedCounters.name} flushed")
          }
        }


      }

      if (count % 1000 != 0) {
        session.close(true)
        logger.info(s"${namedCounters.name} flushed")
      }
      logger.info(s"Processing ${namedCounters.name} finished, count=$count updated=$updated cleaned=$cleaned")
    }
  }

  override def run(hibernateContext: HibernateContext): Unit = {
    cancellable = system.scheduler.schedule(INITIAL_DELAY, INTERVAL, block.runnable(hibernateContext))
    Logger.info("WeekCountersScheduler has started.")
  }

  override def stop(): Boolean = {
    val status = cancellable.isCancelled || cancellable.cancel()
    Logger.info("WeekCountersScheduler stopped - " + status)
    status
  }

  case class NamedCounters(name: String,
                           uidsSupplier: UserDAO => Seq[Int],
                           countersExtractor: GameUser => HibernateMapAdapter[Int, Long])
}
