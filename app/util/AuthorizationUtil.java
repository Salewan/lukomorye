package util;

import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;

/**
 * @author Sergey Lebedev (salewan@gmail.com) 22.03.2016.
 */
public class AuthorizationUtil {

    public static String md5WithSalt(String str) {
        return md5("uRW6YaVMI9rJhnY9SS1R" + StringUtils.defaultString(str, "").trim().toLowerCase() + "ssh");
    }

    public static String md5(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes("utf-8"));
            byte[] bb = md.digest();
            for (byte aBb : bb) {
                String b = Integer.toString(aBb & 0x000000FF, 16).toLowerCase();
                while (b.length() < 2) {
                    b = "0" + b;
                }
                sb.append(b);
            }
            return sb.toString();
        } catch (Throwable t) {
            throw new RuntimeException("", t);
        }
    }

    public static String cryptEmail(String email) {
        if(StringUtils.isBlank(email) || email.length() < 4) {
            return email;
        } else {
            int len = email.length();
            int begin = len / 4;
            int end = begin + len / 2;
            return email.substring(0, begin) + StringUtils.repeat("*", len / 2) + email.substring(end);
        }
    }
}

