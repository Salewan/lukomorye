package util

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.04.2016.
  */
object ExpMoneyUnits {

  def experienceToNextLevel(currentLevel: Int): Long = currentLevel match {
    case 1 => 175
    case 2 => 50
    case 3 => 240
    case 4 => 450
    case _ => 50 * math.round (4 * math.pow (1.25, currentLevel) )
  }

  def experienceToNextClanLevel(currentLevel: Int): Long = 5000 * math.round (4 * math.pow (1.25, currentLevel) )
}
