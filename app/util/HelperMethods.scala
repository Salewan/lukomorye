package util

/**
 * @author Sergey Lebedev (salewan@gmail.com) 16.03.2016.
 */
object HelperMethods {

  def compareUrls(u1: String, u2: String) = deuniqify(u1) equals deuniqify(u2)

  def deuniqify(url: String) = {
    def rec(acc: Array[Char]): String = {
      if (acc.isEmpty) ""
      else {
        val head = acc.head
        if (head == '=') url
        else if (head == '&' || head == '?') new String(acc.tail.reverse)
        else rec(acc.tail)
      }
    }
    if (!url.contains("?") && !url.contains("&")) url
    else rec(url.reverse.toCharArray)
  }

  def uniquify(url: String): String = url + (if (url.indexOf('?') == -1) "?" else "&") + RandomUtil.getRandom.nextLong
}
