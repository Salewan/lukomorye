package util

import java.io.StringWriter

import play.api.http.Writeable
import play.api.mvc.Codec

import scala.language.implicitConversions
import scala.xml.NodeSeq

/**
 * @author Sergey Lebedev (salewan@gmail.com) 17.12.2015.
 */
object Implicits {

  implicit def wrap(n: Int): TimeWrapper = new TimeWrapper(n)

  implicit def writeableOf_Node: Writeable[NodeSeq] = {
    val codec = Codec.utf_8
    Writeable { xml =>
      val stringWriter = new StringWriter(1024)
      val node = scala.xml.Utility.trim(xml.head)
      scala.xml.XML.write(stringWriter, node, codec.charset, xmlDecl = true, null)
      val out = codec.encode(stringWriter.toString)

      stringWriter.close()
      out
    }
  }
}

class TimeWrapper(n: Int) {

  def minutes = TimeUtil.MINUTE_MILLIS * n

  def minute = minutes

  def seconds = TimeUtil.SECOND_MILLIS * n

  def days = TimeUtil.DAY_MILLIS * n

  def day = days

  def hours = TimeUtil.HOUR_MILLIS * n

  def hour = hours

  def years = TimeUtil.YEAR_MILLIS * n

  def year = years

  def weeks = TimeUtil.WEEK_MILLIS * n

  def week = weeks
}
