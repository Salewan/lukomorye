package util
import model.Building.{OGNEUPORNYJ_HLEV, PESHHERA_VASILISKOV, VOLSHEBNYJ_NASEST, ZAGON_ZOLOTYH_ARHAROV}
import model.hero.HeroTemplate
import model.{Building, PetHouse}

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 30.03.2017.
  */
object LevelUtil {

  def groundByLevel(level: Int): Int =
    (0 to 1 has 0) ++ (2 to 3 has 3) ++ (4 to 5 has 5) ++ (6 to 8 has 6) ++ (9 to 10 has 7) ++ (11 to 14 has 8) ++ (15 to 16 has 9) ++ (17 to 19 has 10) ++ (20 to 29 has 12) ++ (30 to 32 has 15) ++ (33 to 36 has 16) ++ (37 to 40 has 18) ++ (41 to 43 has 20) ++ (44 to 46 has 22) ++ (47 to 49 has 24) getOrElse(level, 25)

  def petsByLevel(petHouse: Building with PetHouse, level: Int): Int = petHouse match {
    case VOLSHEBNYJ_NASEST => (0 to 2 has 0) ++ (3 to 6 has 3) ++ (7 to 15 has 4) ++ (16 to 20 has 5) ++ (21 to 32 has 6) ++ (33 to 39 has 8) ++ (40 to 44 has 9) getOrElse(level, 10)
    case OGNEUPORNYJ_HLEV => (0 to 9 has 0) ++ (10 to 14 has 3) ++ (15 to 21 has 4) ++ (22 to 25 has 5) ++ (26 to 39 has 6) ++ (40 to 44 has 7) getOrElse(level, 8)
    case PESHHERA_VASILISKOV => (0 to 19 has 0) ++ (20 to 24 has 3) ++ (25 to 28 has 4) ++ (29 to 31 has 5) ++ (32 to 41 has 6) ++ (42 to 46 has 7)  getOrElse(level, 8)
    case ZAGON_ZOLOTYH_ARHAROV => (0 to 30 has 0) ++ (31 to 33 has 3) ++ (34 to 39 has 4) ++ (40 to 44 has 5) getOrElse(level, 6)
    case _ => throw new RuntimeException
  }

  implicit class RangeOps(range: Range) {
    def has(x: Int) = {
      if (range.isEmpty) throw new RuntimeException
      val noMatter = 0
      range.zipAll(Seq.empty, noMatter, x)
    } toMap
  }
}
