package util

import java.util.regex.Pattern

import bean.User
import bean.messages.personal.PrivateMessage
import components.HibernateSessionAware
import components.context.GameContext
import dao.UserDAO
import model.BBCode
import model.messaging.InboxType
import play.api.Logger
import play.api.mvc.AnyContent

import scala.collection.JavaConverters._

/**
  * @author Sergey Lebedev (salewan@gmail.com) 14.11.2016.
  */
object MessagesUtil {

  val logger = Logger("util.MessagesUtil")

  def sendNotificationMessage(
                               user: User,
                               text: String,
                               fromWhom: String,
                               markRead: Boolean = false,
                               inboxType: Option[InboxType] = None,
                               param: Option[Int] = None)
                             (
                               implicit hs: HibernateSessionAware): Option[PrivateMessage] = {
    val adminOpt = UserDAO().findByNick(fromWhom)
    if (adminOpt.isEmpty) logger.error(s"No $fromWhom user")
    adminOpt.map {a =>
      val admin = a.user
      val dialog = user.dialogWith(admin, create = true).get
      val message = new PrivateMessage
      message.dialog(dialog)
      message.text(text)
      message.author(admin)
      message.target(user)
      message.creationTime(System.currentTimeMillis())
      message.inboxType(inboxType)
      message.param(param)
      dialog.lastMessageDate(message.creationTime).deleted(false)
      dialog.messages += message
      if (!markRead) {
        user.newMessagesCount(1)
        user.lastMessageTime(message.creationTime)
      }
      message
    }
  }

  // TODO
  def filteredText(text: String, domain: String): String = {
    return text
  }

  def processForumMessage(t: String, canUseBBCodes: Boolean, canUseExternalLinks: Boolean)
                         (implicit ctx: GameContext[AnyContent]): String = {
    var text = t
    text = text.replace("<", "&lt;").replace(">", "&gt;")
    text = text.replaceAll("\n", " <br />").replaceAll("\r", "")

    ctx.avatar.nick.foreach(_ => {
      val nick = ctx.avatar.visualNick
      text =
        Pattern.compile(nick, Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE).
          matcher(text).
          replaceAll(s"<span class='bold'>$nick</span>")
    })

    if (canUseBBCodes) {
      for (bbCode <- BBCode.BB_CODES.asScala) {
        text = bbCode.process(text, ctx.hibernateSession.unwrap, canUseExternalLinks)
      }
    }
    text

  }
}
