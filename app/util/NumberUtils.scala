package util

/**
  * @author Sergey Lebedev (salewan@gmail.com) 19.10.2016.
  */
object NumberUtils {

  def ~=(x: Double, y: Double, precision: Double) = {
    if ((x - y).abs < precision) true else false
  }

  def min(a: Option[Long], b: Option[Long]): Option[Long] = List(a,b).flatten match {
    case Nil => None
    case list => Some(list.min)
  }

  def min(iter: Iterable[Long]): Option[Long] = if (iter.isEmpty) None else Some(iter.min)
}
