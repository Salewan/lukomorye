package util

import bean.User
import components.HibernateSessionAware
import dao.UserDAO
import util.javas.{OnlineUtil => JavaOnline}

import scala.collection.JavaConverters._
import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 24.01.2017.
  */
object OnlineUtil {

  def isOnline(id: Int): Boolean = JavaOnline.isOnline(id)

  def isAfk(id: Int): Boolean = JavaOnline.isAfk(id)

  def register(user: User)(implicit hib: HibernateSessionAware): Unit = {
    val gameUser = user.gameUser
    val entry = new JavaOnline.UserEntry(
      user.id,
      System.currentTimeMillis(),
      gameUser.level,
      gameUser.totalExperience,
      gameUser.isInClan,
      !user.settings.doNotReceiveClanInvitations()
    )
    JavaOnline.iamOnline(entry)
  }

  def initAdministration(implicit hib: HibernateSessionAware): Unit = {
    val dao = new UserDAO(hib.hibernateSession)
    JavaOnline.initAdministration(dao.getAdministration, dao.getAdminNicks)
  }

  def getOnline: Seq[JavaOnline.UserEntry] = JavaOnline.getOnlineUsers.asScala

  def getOnlineNoCollective: Seq[JavaOnline.UserEntry] = JavaOnline.getNoGuildOnlineUsers.asScala

  def getOnlineCount: Int = JavaOnline.getOnlineUsers.size()
}
