package util

import bean.GameUser
import bean.state.ClanRole
import components.HibernateSessionAware
import dao.MessageDAO
import model.Building

/**
  * @author Sergey Lebedev (salewan@gmail.com) 02.12.2016.
  */
object PlusUtil {

  def missionsPlus(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = user.hasCompletedMissions

  def tasksPlus(user: GameUser): Boolean = user.tasks.toSeq.exists(_.canComplete)

  def productionPlus(user: GameUser, building: Building): Boolean = {
    user.getProduction(building.typeId).exists(_.hasPlus) || user.getPetProduction(building.typeId).exists(_.hasFull)
  }

  def domainPlus(user: GameUser): Boolean =
    Building.all.filter(_.level <= user.level).exists(productionPlus(user, _))

  def heroesPlus(user: GameUser): Boolean = user.heroes.toSeq.exists {case (_, hero) =>
    hero.pendingLevel > 0
  }

  def chestsPlus(user: GameUser): Boolean = user.keys > 0 && user.chests.exists(chest => !chest.isExpired)

  def barnPlus(user: GameUser): Boolean = chestsPlus(user)

  def rubiesPlus(user: GameUser): Boolean = user.getAllowedRubies >= user.getMaxAllowedRubies

  def clanChatPlus(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = {
    user.clan.exists(clan => clan.chat().lastMessageTime.
      exists(lmt => user.lastClanChatVisitTime.isEmpty || user.lastClanChatVisitTime.exists(_ < lmt)))
  }

  def clanRequestPlus(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = {
    user.clan.exists(clan => !clan.requestsDisabled && (user.clanRole == ClanRole.LEADER || user.clanRole == ClanRole.OFFICER) && clan.hasNewRequests)
  }

  def clanNewForumMessages(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = {
    user.clan.exists {clan =>
      val dao = new MessageDAO(hib.hibernateSession)
      dao.getRootFolders(clan).exists { folder =>
        val mark = dao.getFolderMark(user.user, folder)
        user.canViewClanFolder(folder) && {
          val isNew = folder.lastCommentTime.nonEmpty && folder.lastCommentTime.get > mark.lastViewTime
          isNew
        }
      }
    }
  }

  def clanPlus(user: GameUser)(implicit hib: HibernateSessionAware): Boolean = {
    clanRequestPlus(user) || clanChatPlus(user) || clanNewForumMessages(user)
  }

  def labPlus(user: GameUser): Boolean = {
    val maxAllowed = user.getMaxAllowedFreeDust
    maxAllowed > 0 && user.getAllowedFreeDust >= maxAllowed
  }
}
