package util

import java.util.concurrent.ThreadLocalRandom

import scala.util.Random

/**
  * @author Sergey Lebedev (salewan@gmail.com) 25.04.2016.
  */
object RandomUtil {
  private val RANDOM: ThreadLocal[Random] = new ThreadLocal[Random]

  def getRandom: Random = {
    var r: Random = RANDOM.get
    if (r == null) {
      r = new Random
      RANDOM.set(r)
    }
    r
  }

  def randomOptional[T](ts: Seq[T]): Option[T] = if (ts.isEmpty) None else Some(ts(getRandom.nextInt(ts.length)))

  def random[T](ts: Seq[T]): T = ts(getRandom.nextInt(ts.length))

  def random[T](ts: Iterable[T]): Option[T] = random[T](ts, (p:T) => 1.0)

  def random[T](ts: Iterable[T], weigher: T => Double): Option[T] = {
    val totalWeight = ts.map(weigher).sum
    def throws(drop: Double, xt: Iterable[T], t: Option[T]): Option[T] = {
      if (xt.isEmpty) t
      else {
        val item = xt.head
        val itemChance = weigher(item) / totalWeight
        if (drop <= itemChance) Some(item)
        else throws(drop - itemChance, xt.tail, Some(item))
      }
    }
    throws(getRandom.nextDouble(), ts, None)
  }

  def random(min: Int, max: Int): Int = min + getRandom.nextInt(Math.max(1, max - min + 1))

  def randomLong(min: Long, max: Long): Long = ThreadLocalRandom.current().nextLong(min, max + 1)

  def throwTheDice(chance: Double): Boolean = getRandom.nextDouble() - chance < 0
}
