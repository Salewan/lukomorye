package util

import bean.state.ClanRole
import bean.{Clan, GameUser}
import model.rating.{Rating, RatingEntity, RatingType}

import scala.collection.mutable

/**
  * @author Sergey Lebedev (salewan@gmail.com) 01.12.2016.
  */
object RatingUtil {

  /**
    * Персональные рейтинги
    */

  // Рейтинг по уровню
  private var userLevelRating = new Rating(RatingType.LEVEL, Seq.empty)

  def setUserLevelRating(users: Seq[GameUser]): Unit = {
    userLevelRating = createLevelRating(RatingType.LEVEL, users)
  }

  def getUserLevelRating: Rating = userLevelRating

  // Рейтинг по репутации
  private var reputationRating = mutable.Map[Int, Rating]()

  private val emptyReputationRating = new Rating(RatingType.REPUTATION, Seq.empty)

  def setReputationRating(reputation: Int, users: Seq[GameUser]): Unit = {
    val valueOf: GameUser => Long = user => user.reputation.get(reputation).map(_.repForRating).getOrElse(0L)
    val entries = for (user <- users) yield RatingEntity(user.id, valueOf(user))
    val rating = new Rating(RatingType.REPUTATION, entries)
    reputationRating.update(reputation, rating)
  }

  def getReputationRating(reputation: Int): Rating = {
    reputationRating.getOrElse(reputation, emptyReputationRating)
  }

  /**
    * Княжеские рейтинги
    */
   private def ratingEntitiesOf(staff: Seq[(RatingEntity, (Int, ClanRole), (Int, Long))]): Seq[RatingEntity] = {
     val entities = staff.map(_._1)
     val roles = staff.map(_._2).toMap
     val times = staff.map(_._3).toMap
     val sorter = (o1: RatingEntity, o2: RatingEntity) =>
       if (o1.value == o2.value) {
         if (roles(o1.id).id == roles(o2.id).id) {
           if (times(o1.id) == times(o2.id)) {
             o1.id < o2.id
           } else times(o1.id) < times(o2.id)
         } else roles(o1.id).id > roles(o2.id).id
       } else o1.value > o2.value
     entities.sortWith(sorter)
   }

  // Внутриклановый рейтинг по опыту
  def getClanInnerExpRating(clan: Clan): Rating = {
    val staff =
      for (member <- clan.members.toSeq; clanJoinTime <- member.clanJoinTime)
        yield (
          RatingEntity(member.id, clan.experiences.get(member.id).getOrElse(0L)),
          member.id -> member.clanRole,
          member.id -> clanJoinTime)
    new Rating(RatingType.CLAN_INNER_EXP, ratingEntitiesOf(staff))
  }

  // Внутриклановый рейтинг по недельному опыту
  def getClanInnerWeekExpRating(clan: Clan): Rating = {
    val staff =
    for (member <- clan.members.toSeq; clanJoinTime <- member.clanJoinTime)
      yield (
        RatingEntity(member.id, member.clanWeekExp.values.sum),
        member.id -> member.clanRole,
        member.id -> clanJoinTime)
    new Rating(RatingType.CLAN_INNER_WEEK_EXP, ratingEntitiesOf(staff))
  }

  // Внутриклановый рейтинг по взносам рубинов за неделю
  def getClanInnerWeekRubiesRating(clan: Clan): Rating = {
    val staff =
      for (member <- clan.members.toSeq; clanJoinTime <- member.clanJoinTime)
        yield (
          RatingEntity(member.id, WeekCounters.weekCounter(member.clanWeekRubies)),
          member.id -> member.clanRole,
          member.id -> clanJoinTime)
    new Rating(RatingType.CLAN_INNER_WEEK_RUBIES, ratingEntitiesOf(staff))
  }

  // Внутриклановый рейтинг по взносам рубинов за всё время
  def getClanInnerRubiesRating(clan: Clan): Rating = {
    val staff =
      for (member <- clan.members.toSeq; clanJoinTime <- member.clanJoinTime)
        yield (
          RatingEntity(member.id, clan.memberRubies.get(member.id).getOrElse(0L)),
          member.id -> member.clanRole,
          member.id -> clanJoinTime)
    new Rating(RatingType.CLAN_INNER_RUBIES, ratingEntitiesOf(staff))
  }

  // Внешний клановский рейтинг по союзам
  private val allianceRating = mutable.Map[Int, Rating]()
  def setAllianceRating(alliance: Int, clans: Seq[(Int, Long)]): Unit = {
    allianceRating.update(alliance, createClanRating(RatingType.CLAN_ALLIANCE, clans))
  }
  def getAllianceRating(alliance: Int): Rating = {
    allianceRating.getOrElse(alliance, createEmptyRating(RatingType.CLAN_ALLIANCE))
  }


  // Внешний клановский рейтинг по опыту
  private var clanLevelRating = createEmptyRating(RatingType.CLAN_LEVEL_EXP)
  def setClanLevelRating(clans: Seq[(Int, Long)]): Unit = {
    clanLevelRating = createClanRating(RatingType.CLAN_LEVEL_EXP, clans)
  }
  def getClanLevelRating: Rating = clanLevelRating


  //
  private def createLevelRating(ratingType: RatingType, users: Seq[GameUser]): Rating = {
    new Rating(ratingType, for (user <- users) yield RatingEntity(user.id, user.level))
  }

  private def createClanRating(ratingType: RatingType, clans: Seq[(Int, Long)]): Rating = {
    val entries = for((id, value) <- clans) yield RatingEntity(id, value)
    new Rating(ratingType, entries)
  }

  private def createEmptyRating(ratingType: RatingType): Rating = new Rating(ratingType, Seq.empty)
}
