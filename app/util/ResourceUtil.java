package util;

import java.io.InputStream;
import java.net.URL;

/**
 * @author Sergey Lebedev (salewan@gmail.com) Date: 29.03.14
 */
public class ResourceUtil {

    public static InputStream getResourceAsStream(String resourceName)  {
        try {
            return getResourceURL(resourceName).openStream();
        } catch (Throwable t) {
            throw new RuntimeException("Resource name = " + resourceName, t);
        }
    }

    public static URL getResourceURL(String resourceName) {
        if (resourceName.startsWith("/")) {
            resourceName = resourceName.substring(1);
        }
        if (Thread.currentThread().getContextClassLoader() != null) {
            URL url = Thread.currentThread().getContextClassLoader().getResource(resourceName);
            if (url != null) return url;
            url = Thread.currentThread().getContextClassLoader().getResource("/" + resourceName);
            if (url != null) return url;
        }
        return null;
    }
}
