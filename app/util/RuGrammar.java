package util;

import com.google.common.base.Preconditions;

/**
 * @author Sergey Lebedev (salewan@gmail.com) 10.11.2015.
 */
public class RuGrammar {

    private static final String[] RUBIES_BOUGHT = {"рубинов", "рубин", "рубина"};
    private static final String[] RUBY_PAYMENT = {"рубинов", "рубин", "рубина"};
    private static final String[] RUBY_DEFICIT = {"рубинов", "рубина", "рубинов"};
    private static final String[] RUBY_REST = {"рубинами", "рубином", "рубинами"};

    public static String rubyRest(long n) {
        return plural(n, RUBY_REST);
    }

    public static String rubyPayment(long n) {
        return plural(n, RUBY_PAYMENT);
    }

    public static String rubyDeficit(long n) {
        return plural(n, RUBY_DEFICIT);
    }

    public static String rubiesBought(int n) {
        return plural(n, RUBIES_BOUGHT);
    }
    //
    private static final String[] MONEY_PAYMENT = {"монет", "монету", "монеты"};
    private static final String[] MONEY_DEFICIT = {"монет", "монеты", "монет"};
    private static final String[] MONEY_GOT = {"монет", "монета", "монеты"};

    public static String moneyDeficit(long n) {
        return plural(n, MONEY_DEFICIT);
    }

    public static String moneyPayment(long n) {
        return plural(n, MONEY_PAYMENT);
    }

    public static String moneyGot(long n) {
        return plural(n, MONEY_GOT);
    }

    //
    private static final String[] TIMEDRINK_PAYMENT = {"зелья времени", "зелья времени", "зелья времени"};
    private static final String[] TIMEDRINK_DEFICIT = {"зелья времени", "зелья времени", "зелья времени"};

    public static String timedrinkPayment(long n) {
        return plural(n, TIMEDRINK_PAYMENT);
    }

    public static String timedrinkDeficit(long n) {
        return plural(n, TIMEDRINK_DEFICIT);
    }

    //
    public static String plural(long n, String[] text) {

        Preconditions.checkNotNull(text);
        Preconditions.checkArgument(text.length == 3);

        n = Math.abs(n);

        long modulo100 = n % 100;
        if (modulo100 >= 11 && modulo100 <= 14) {
            return text[0];
        }

        long modulo10 = n % 10;
        if (modulo10 == 1) {
            return text[1];
        }
        if (modulo10 >= 2 && modulo10 <= 4) {
            return text[2];
        }
        return text[0];
    }
}
