package util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Sergey Lebedev (salewan@gmail.com) 12.10.2015.
 */
public class SingleLanguageValidator {

    public static boolean isValid(String input) {

        if (StringUtils.isEmpty(input)) {
            return true;
        }

        boolean hasASCII = false;
        boolean hasNonASCII = false;
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (c > 0x7F) {
                hasNonASCII = true;
            } else {
                hasASCII |= ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
            }
        }
        return (hasASCII && !hasNonASCII) || (!hasASCII && hasNonASCII) || !hasASCII;
    }
}
