package util;

import com.google.common.collect.Lists;
import play.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Lebedev (salewan@gmail.com) Date: 29.03.14
 */
public class SmilesUtil {

    private List<Smile> smiles = new ArrayList<Smile>();

    public static SmilesUtil getInstance() {
        return SmilesUtilHolder.instance;
    }

    private SmilesUtil() {
        List<Smile> smiles = new ArrayList<Smile>();
        try {
            InputStream is = ResourceUtil.getResourceAsStream("smiles.xml");
            try {
                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = documentBuilder.parse(is);
                List<Node> smileNodes = XmlUtil.getNamedNodes(document, "smiles/smile");
                for (Node smileNode : smileNodes) {
                    Smile smile = new Smile();
                    smile.patternList = XmlUtil.getNamedNodesValues(smileNode, "pattern");
                    smile.image = XmlUtil.getFirstNamedNodeValue(smileNode, "image");
                    smile.imagePink = smile.image;
                    smile.imageBlue = smile.image;
                    smile.width = Integer.valueOf(XmlUtil.getFirstNamedNodeValue(smileNode, "width"));
                    smile.height = Integer.valueOf(XmlUtil.getFirstNamedNodeValue(smileNode, "height"));
                    smiles.add(smile);
                }
                this.smiles = smiles;
            } finally {
                is.close();
            }
        } catch (Throwable t) {
            Logger.error("", t);
        }
    }

    public String processSmiles(String str) {
        return processSmiles(str, 10);
    }

    public String processSmiles(String str, List<Smile> moodSmiles) {
        return processSmiles(str, 10, moodSmiles);
    }

    private String processSmiles(String s, int limit) {
        return processSmiles(s, limit, null);
    }

    private String processSmiles(String s, int limit, List<Smile> moodSmiles) {
        String result = "";
        int lastStart = 0;
        int count = 0;
        boolean isUrl = false;
        for (int i = 0; i < s.length(); i++) {
            Smile smile = getSmile(s, i, moodSmiles);
            if(startsWith(s, "://", i)) {
                isUrl = true;
            } else if(isUrl && (startsWith(s, " ", i) || startsWith(s, ">", i))) {
                isUrl = false;
            }
            if (smile != null && !isUrl) {
                String smileImage = smile.imageBlue;
                String smilePattern = smile.patternList.get(0);
                result += s.substring(lastStart, i);
                result += "<img width=\"" +smile.width + "\" height=\"" + smile.height + "\" alt=\"" + smilePattern + "\" src=\"" + smileImage + "\"/>";
                lastStart = i + smilePattern.length();
                i += smilePattern.length() - 1;
                count++;
                if (count >= limit) {
                    break;
                }
            }
        }
        result += s.substring(lastStart);
        return result;
    }

    public List<Smile> getSmiles() {
        return smiles;
    }

    private Smile getSmile(String s, int idx, List<Smile> moodSmiles) {
        List<Smile> smileList = Lists.newArrayList();
        if (moodSmiles != null && !moodSmiles.isEmpty()) {
            smileList.addAll(moodSmiles);
        }
        smileList.addAll(smiles);
        for (Smile smile : smileList) {
            for (String pattern : smile.patternList) {
                if (startsWith(s, pattern, idx)) {
                    Smile resultSmile = new Smile();
                    resultSmile.image = smile.image;
                    resultSmile.imageBlue = smile.image;
                    resultSmile.imagePink = smile.image;
                    resultSmile.patternList = new ArrayList<String>();
                    resultSmile.patternList.add(pattern);
                    resultSmile.width = smile.width;
                    resultSmile.height = smile.height;
                    return resultSmile;
                }
            }
        }
        return null;
    }

    private static boolean startsWith(String s, String prefix, int idx) {
        if (s.length() - idx < prefix.length()) {
            return false;
        } else {
            for (int i = 0; i < prefix.length(); i++) {
                if (s.charAt(idx + i) != prefix.charAt(i)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static class Smile {

        private static final int DEFAULT_WIDTH = 30;
        private static final int DEFAULT_HEIGHT = 30;

        List<String> patternList;
        String image;
        String imagePink;
        String imageBlue;
        int width;
        int height;

        private Smile() {
        }

        public Smile(List<String> patternList, String image) {
            this(patternList, image, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        }

        public Smile(List<String> patternList, String imageBlue, String imagePink) {
            this(patternList, "", DEFAULT_WIDTH, DEFAULT_HEIGHT);
            this.image = imageBlue;
            this.imagePink = imagePink;
            this.imageBlue = imageBlue;
        }

        public Smile(List<String> patternList, String image, int width, int height) {
            this.patternList = patternList;
            this.image = image;
            this.width = width;
            this.height = height;
        }

        public List<String> getPatternList() {
            return patternList;
        }

        public void setPatternList(List<String> patternList) {
            this.patternList = patternList;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public String getImage() {
            return image;
        }

        public String getImagePink() {
            return imagePink;
        }

        public String getImageBlue() {
            return imageBlue;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class SmilesUtilHolder {

        static SmilesUtil instance = new SmilesUtil();

        private SmilesUtilHolder() {
        }
    }
}
