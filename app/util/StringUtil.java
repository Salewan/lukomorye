package util;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Author: Yaroslav Rudykh (slavan.it2me@gmail.com) Date: 29.08.13
 */
public class StringUtil {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd.MM.yyyy");

    private static long KILO = 1000;
    public static long KILO_100 = 1000 * 100;
    private static long MILI = 1000 * KILO;
    private static long GIGA = 1000 * MILI;
    private static long TERA = 1000 * GIGA;
    private static long PETA = 1000 * TERA;
    private static long EXA = 1000 * PETA;

    public static String toHumanFormat(long n) {
        if(n >= KILO_100) {
            if (n >= EXA) {
                return ((n / (10 * PETA)) / 100.0) + "e";
            } else if(n >= PETA) {
                return ((n / (10 * TERA)) / 100.0) + "p";
            } else if(n >= TERA) {
                return ((n / (10 * GIGA)) / 100.0) + "t";
            } else if(n >= GIGA) {
                return ((n / (10 * MILI)) / 100.0) + "g";
            } else if(n >= MILI) {
                return ((n / (10 * KILO)) / 100.0) + "m";
            } else {
                return ((n / 100) / 10.0) + "k";
            }
        } else {
            return String.valueOf(n);
        }
    }

    public static String formatBigInteger(long value) {
        if (value == 0) {
            return "0";
        }
        String result = "";
        while (value != 0) {
            result = expand(value % 1000) + (result.length() > 0 ? "'" : "") + result;
            value /= 1000;
        }
        return trimZeros(result);
    }

    private static String expand(long l) {
        String s = String.valueOf(l);
        while (s.length() < 3) {
            s = "0" + s;
        }
        return s;
    }

    private static String trimZeros(String s) {
        while (!StringUtils.isBlank(s) && s.startsWith("0")) {
            s = s.substring(1);
        }
        return s;
    }

    public static String formatDate(ReadableInstant instant) {
        return DATE_FORMATTER.print(instant);
    }
}
