package util

import model.price.DustPrice
import util.Implicits._

import scala.language.postfixOps

/**
  * @author Sergey Lebedev (salewan@gmail.com) 15.12.2016.
  */
object TimeReduction {

  def price(time: Long): DustPrice = DustPrice(Math.ceil(1.0 * time / (1 minutes)).asInstanceOf[Long] max 1L)
}
