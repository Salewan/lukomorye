package util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * @author Sergey Lebedev (salewan@gmail.com) 13.11.2015.
 */
public class TimeUtil {

    public static final SimpleDateFormat DATE_FORMAT_DETAILED2 = new SimpleDateFormat("HH:mm dd.MM.yyyy", new Locale("RU"));

    public static final long SECOND_MILLIS = 1000;
    public static final long MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final long HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final long DAY_MILLIS = 24 * HOUR_MILLIS;
    public static final long WEEK_MILLIS = 7 * DAY_MILLIS;
    public static final long MONTH_MILLIS = 30 * DAY_MILLIS;
    public static final long YEAR_MILLIS = 365 * DAY_MILLIS;

    public static String formatTime(long millis) {
        if (millis >= DAY_MILLIS) {
            return toStringDAYS(millis);
        } else if (millis >= HOUR_MILLIS) {
            return toStringHOURS(millis);
        } else if (millis >= MINUTE_MILLIS) {
            return toStringMINUTES(millis);
        } else {
            return toStringSECONDS(millis);
        }
    }

    public static String formatTimeShort(long millis) {
        if (millis >= DAY_MILLIS) {
            return toStringDAYSShort(millis);
        } else if (millis >= HOUR_MILLIS) {
            return toStringHOURSShort(millis);
        } else if (millis >= MINUTE_MILLIS) {
            return toStringMINUTESShort(millis);
        } else {
            return toStringSECONDSShort(millis);
        }
    }

    public static String formatTimeSince(Date dt) {
        return formatTimeSince(dt.getTime());
    }

    public static String formatTimeSince(long time) {
        return formatTime(System.currentTimeMillis() - time);
    }

    public static String formatTimeTill(Date dt) {
        return formatTime(Math.max(1, dt.getTime() - System.currentTimeMillis()));
    }

    public static String formatTimeTillShort(Date dt) {
        return formatTimeShort(Math.max(1, dt.getTime() - System.currentTimeMillis()));
    }

    public static String formatTimeTill(long dt) {
        return formatTime(Math.max(1, dt - System.currentTimeMillis()));
    }

    public static boolean passed(long from, long millis) {
        return System.currentTimeMillis() > from + millis;
    }

    public static boolean passed(Date from, long millis) {
        return passed(from == null ? 0 : from.getTime(), millis);
    }

    public static boolean after(long what, Date than) {
        return what > than.getTime();
    }

    public static boolean after(Date what, Date than) {
        return what != null && what.getTime() > than.getTime();
    }

    public static boolean after(Date what, long than) {
        return what != null && what.getTime() > than;
    }

    private static String toStringDAYS(double millis) {
        int days = (int) Math.floor(millis / DAY_MILLIS);
        int hours = (int) Math.round((millis - days * DAY_MILLIS) / HOUR_MILLIS);
        if (hours == 24) {
            days += 1;
            hours = 0;
        }
        return days + "д" + (hours == 0 ? "" : " " + hours + "ч");
    }

    private static String toStringDAYSShort(double millis) {
        int days = (int) Math.floor(millis / DAY_MILLIS);
        int hours = (int) Math.round((millis - days * DAY_MILLIS) / HOUR_MILLIS);
        if (hours == 24) {
            days += 1;
        }
        return days + "д";
    }

    private static String toStringHOURS(double millis) {
        int hours = (int) Math.floor(millis / HOUR_MILLIS);
        int minutes = (int) Math.round((millis - hours * HOUR_MILLIS) / MINUTE_MILLIS);
        if (minutes == 60) {
            hours += 1;
            minutes = 0;
        }
        if (hours == 24) {
            return toStringDAYS(millis);
        }
        return hours + "ч" + (minutes == 0 ? "" : " " + minutes + "м");
    }

    private static String toStringHOURSShort(double millis) {
        int hours = (int) Math.floor(millis / HOUR_MILLIS);
        int minutes = (int) Math.round((millis - hours * HOUR_MILLIS) / MINUTE_MILLIS);
        if (minutes == 60) {
            hours += 1;
            minutes = 0;
        }
        if (hours == 24) {
            return toStringDAYSShort(millis);
        }
        return hours + "ч";
    }

    private static String toStringMINUTES(double millis) {
        int minutes = (int) Math.floor(millis / MINUTE_MILLIS);
        int seconds = (int) Math.round((millis - minutes * MINUTE_MILLIS) / SECOND_MILLIS);
        if (seconds == 60) {
            minutes += 1;
            seconds = 0;
        }
        if (minutes == 60) {
            return toStringHOURS(millis);
        }
        return minutes + "м" + (seconds == 0 ? "" : " " + seconds + "с");
    }

    private static String toStringMINUTESShort(double millis) {
        int minutes = (int) Math.floor(millis / MINUTE_MILLIS);
        int seconds = (int) Math.round((millis - minutes * MINUTE_MILLIS) / SECOND_MILLIS);
        if (seconds == 60) {
            minutes += 1;
        }
        if (minutes == 60) {
            return toStringHOURSShort(millis);
        }
        return minutes + "м";
    }

    private static String toStringSECONDS(double millis) {
        int seconds = Math.max(1, (int) Math.round(millis / 1000));
        if (seconds == 60) {
            return toStringMINUTES(millis);
        }
        return seconds + "с";
    }

    private static String toStringSECONDSShort(double millis) {
        return toStringSECONDS(millis);
    }

    /**
     * Возвращает ближайший момент времени, кратный (относительно начала суток) указанному временному интервалу
     */
    public static long getNextTimeAdjusted(long from, long interval) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(from);
        long dayTime = getDayTime(gregorianCalendar);
        long time = from - dayTime;
        while (time < from) {
            time += interval;
        }
        return time;
    }

    public static long getNextTimeAdjusted(long interval) {
        return getNextTimeAdjusted(System.currentTimeMillis(), interval);
    }

    public static long getDayTime(GregorianCalendar gregorianCalendar) {
        return gregorianCalendar.get(GregorianCalendar.HOUR_OF_DAY) * 3600000
                + gregorianCalendar.get(GregorianCalendar.MINUTE) * 60000
                + gregorianCalendar.get(GregorianCalendar.SECOND) * 1000
                + gregorianCalendar.get(GregorianCalendar.MILLISECOND);
    }

    public static boolean isToday(Date time) {
        if(time == null) {
            return false;
        }
        GregorianCalendar calendar1 = new GregorianCalendar();
        GregorianCalendar calendar2 = new GregorianCalendar();
        calendar1.setTime(time);
        calendar2.setTimeInMillis(System.currentTimeMillis());
        return calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }

    public static Date today() {
        return DateUtils.truncate(new Date(), Calendar.DATE);
    }

    public static Date tomorrow() {
        return DateUtils.addDays(today(), 1);
    }
}