package util

object WebFormsUtil {

  def nickOf(text: String): String = visualNickOf(text).toLowerCase

  def visualNickOf(text: String): String = Option(text).map(_.trim).getOrElse("")
}
