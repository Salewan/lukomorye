package util

import hibernate.HibernateMapAdapter
import util.Implicits._
import scala.language.postfixOps

object WeekCounters {

  val DAYS_IN_WEEK = 8

  def getPseudoDayOfWeek(time: Long, daysInWeek: Int): Int = {
    (time / (1 day)) % daysInWeek
  }.asInstanceOf[Int]

  def addWeekCounter(counters: HibernateMapAdapter[Int, Long], amount: Long): Unit = {
    val now = System.currentTimeMillis()
    val currentWeekDay = getPseudoDayOfWeek(now, DAYS_IN_WEEK)
    val nextWeekDay = getPseudoDayOfWeek(now + (1 day), DAYS_IN_WEEK)
    counters.update(currentWeekDay)(opt => opt.map(_ + amount).getOrElse(amount))
    counters -= nextWeekDay
  }

  def todayCounter(counters: HibernateMapAdapter[Int, Long]): Long = {
    val now = System.currentTimeMillis()
    val currentWeekDay = getPseudoDayOfWeek(now, DAYS_IN_WEEK)
    counters.getOrElse(currentWeekDay, 0L)
  }

  def weekCounter(counters: HibernateMapAdapter[Int, Long]): Long = counters.values.sum

  def refreshWeekCounters(counters: HibernateMapAdapter[Int, Long]): Unit = {
    addWeekCounter(counters, 0)
  }
}
