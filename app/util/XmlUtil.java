package util;

import org.apache.commons.lang3.StringUtils;
import play.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sergey Lebedev (salewan@gmail.com) Date: 29.03.14
 */
public class XmlUtil {

    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

    public static Document getDocument(InputStream is) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(new ErrorHandler() {
            public void warning(SAXParseException exception) throws SAXException {
                Logger.warn("", exception);
            }
            public void error(SAXParseException exception) throws SAXException {
                Logger.error("", exception);
            }
            public void fatalError(SAXParseException exception) throws SAXException {
                Logger.error("", exception);
            }
        });
        return documentBuilder.parse(is);
    }

    public static Document getDocument(InputStream is, String schemaName) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setValidating(true);
        documentBuilderFactory.setNamespaceAware(true);
        documentBuilderFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
        InputStream schemaIS = ResourceUtil.getResourceAsStream(schemaName);
        documentBuilderFactory.setAttribute(JAXP_SCHEMA_SOURCE, schemaIS);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(new ErrorHandler() {
            public void warning(SAXParseException exception) throws SAXException {
                Logger.warn("", exception);
            }
            public void error(SAXParseException exception) throws SAXException {
                Logger.error("", exception);
            }
            public void fatalError(SAXParseException exception) throws SAXException {
                Logger.error("", exception);
            }
        });
        return documentBuilder.parse(is);
    }

    public static String getNodeValue(Node node) {
        if(node == null) {
            return "";
        }
        if (node.getTextContent() == null) {
            return "";
        } else {
            return node.getTextContent().trim();
        }
    }

    public static int getIntNodeValue(Node node) {
        try {
            return Integer.parseInt(getNodeValue(node));
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean hasAttribute(Node node, String attributeName) {
        return node.getAttributes().getNamedItem(attributeName) != null;
    }

    public static String getAttributeValue(Node node, String attributeName) {
        try {
            return node.getAttributes().getNamedItem(attributeName).getNodeValue();
        } catch(Exception e) {
            return "";
        }
    }

    public static int getIntAttributeValue(Node node, String attributeName) {
        try {
            return Integer.parseInt(getAttributeValue(node, attributeName));
        } catch (Exception e) {
            return 0;
        }
    }

    public static Integer getIntegerAttributeValue(Node node, String attributeName) {
        try {
            return Integer.valueOf(getAttributeValue(node, attributeName));
        } catch (Exception e) {
            return null;
        }
    }

    public static long getLongAttributeValue(Node node, String attributeName) {
        try {
            return Long.parseLong(getAttributeValue(node, attributeName));
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean getBooleanAttributeValue(Node node, String attributeName) {
        return getBooleanAttributeValue(node, attributeName, false);
    }

    public static boolean getBooleanAttributeValue(Node node, String attributeName, boolean defaultValue) {
        try {
            String value = getAttributeValue(node, attributeName);
            return StringUtils.isBlank(value) ? defaultValue : Boolean.parseBoolean(value);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static double getDoubleAttributeValue(Node node, String attributeName) {
        try {
            return Double.parseDouble(getAttributeValue(node, attributeName));
        } catch (Exception e) {
            return 0;
        }
    }

    public static List<String> readParameters(Document document, String param) {
        List<String> result = new ArrayList<String>();
        String params = param + "s";
        Node rootNode = document.getChildNodes().item(0);
        NodeList nodes = rootNode.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (params.equalsIgnoreCase(node.getNodeName())) {
                NodeList subNodes = node.getChildNodes();
                for (int j = 0; j < subNodes.getLength(); j++) {
                    Node subNode = subNodes.item(j);
                    if(param.equalsIgnoreCase(subNode.getNodeName())) {
                        result.add(subNode.getFirstChild().getNodeValue().trim());
                    }
                }
            }
        }
        return result;
    }

    public static String readParameter(Document document, String param) {
        Node rootNode = document.getChildNodes().item(0);
        NodeList nodes = rootNode.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (param.equalsIgnoreCase(node.getNodeName())) {
                return node.getFirstChild().getNodeValue().trim();
            }
        }
        return null;
    }

    public static Node getFirstNamedNode(Node rootNode, String name) {
        List<Node> list = getNamedNodes(rootNode, name);
        return list.size() > 0 ? list.get(0) : null;
    }

    public static List<Node> getNamedNodes(Node rootNode, String name) {
        while(name.startsWith("/")) {
            name = name.substring(1);
        }
        while(name.endsWith("/")) {
            name = name.substring(0, name.length() - 1);
        }
        int slashIdx = name.indexOf('/');
        if(slashIdx < 0) {
            return getNamedNodesInternal(rootNode, name);
        } else {
            String prefix = name.substring(0, slashIdx);
            String suffix = name.substring(slashIdx + 1);
            List<Node> result = new ArrayList<Node>();
            List<Node> nodes = getNamedNodesInternal(rootNode, prefix);
            for(Node node:nodes) {
                result.addAll(getNamedNodes(node, suffix));
            }
            return result;
        }
    }

    public static List<String> getNamedNodesValues(Node rootNode, String name) {
        List<Node> nodes = getNamedNodes(rootNode, name);
        List<String> result = new ArrayList<String>();
        for(Node node:nodes) {
            result.add(getNodeValue(node));
        }
        return result;
    }

    public static String getFirstNamedNodeValue(Node rootNode, String name) {
        Node firstNamedNode = getFirstNamedNode(rootNode, name);
        if (firstNamedNode == null) {
            return "";
        }
        return getNodeValue(firstNamedNode);
    }

    public static Node getRootNode(Document document) {
        return document.getChildNodes().item(0);
    }

    private static List<Node> getNamedNodesInternal(Node rootNode, String name) {
        List<Node> result = new ArrayList<Node>();
        NodeList nodes = rootNode.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if(name.equals(node.getNodeName())) {
                result.add(node);
            }
        }
        return result;
    }
}
