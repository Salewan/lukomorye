package util.format

import util.StringUtil

object BigNumber {

  def apply(x: Long): String = StringUtil.formatBigInteger(x)

}
