package util.format

import util.StringUtil

object HumanFormat {

  def apply(x: Long): String = if (x >= StringUtil.KILO_100) StringUtil.toHumanFormat(x) else BigNumber(x)
}
