package util.javas;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Longs;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.TimeUtil;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class OnlineUtil {

    private static final Logger logger = LoggerFactory.getLogger(OnlineUtil.class);

    private static List<Integer> administration = new ArrayList<>();
    private static List<String> administrationNicks = new ArrayList<>();

    public static int CLAN_REQUIRED_LEVEL = 1;
    public static long ONLINE_TIME = TimeUtil.HOUR_MILLIS;
    public static long AFK_TIME = TimeUtil.MINUTE_MILLIS * 5;
    public static final long ACTIVE_TIME = TimeUtil.WEEK_MILLIS;

    private static long lastAdministrationSortTime;
    private static Map<Integer, UserEntry> times = Maps.newConcurrentMap();
    private static final LoadingCache<Boolean, List<UserEntry>> ONLINE_USERS = CacheBuilder.newBuilder()
            .concurrencyLevel(10)
            .expireAfterWrite(10, TimeUnit.SECONDS)
            .build(new CacheLoader<Boolean, List<UserEntry>>() {
                @Override
                public List<UserEntry> load(@Nullable Boolean key) throws Exception {
                    return getOnlineInternal();
                }
            });
    private static final LoadingCache<Boolean, List<UserEntry>> NO_GUILD_ONLINE_USERS = CacheBuilder.newBuilder()
            .concurrencyLevel(10)
            .expireAfterWrite(10, TimeUnit.SECONDS)
            .build(new CacheLoader<Boolean, List<UserEntry>>() {
                @Override
                public List<UserEntry> load(@Nullable Boolean key) throws Exception {
                    return getNoGuildOnlineInternal();
                }
            });

    private static List<UserEntry> getOnlineInternal() {
        List<UserEntry> values = Lists.newArrayList(Iterables.filter(times.values(), new Predicate<UserEntry>() {
            @Override
            public boolean apply(@Nullable UserEntry input) {
                return input != null && isEntryOnline(input);
            }
        }));
        Collections.sort(values, LEVEL_COMPARATOR);
        return values;
    }

    private static List<UserEntry> getNoGuildOnlineInternal() {
        List<UserEntry> values = Lists.newArrayList(Iterables.filter(times.values(), new Predicate<UserEntry>() {
            @Override
            public boolean apply(@Nullable UserEntry input) {
                return input != null && isEntryOnline(input) && input.level >= CLAN_REQUIRED_LEVEL &&
                        !input.inGuild && input.receiveGuildInvites;
            }
        }));
        Collections.sort(values, LEVEL_COMPARATOR);
        return values;
    }
    public static void iamOnline(UserEntry entry) {
        times.put(entry.uid, entry);
    }

    public static void initAdministration(List<Integer> administrationIds, List<String> adminNicks) {
        administration = administrationIds;
        administrationNicks = ImmutableList.copyOf(Iterables.transform(adminNicks, new Function<String, String>() {
            @Override
            public String apply(String input) {
                return StringUtils.capitalize(input);
            }
        }));
    }

    public static List<String> getAdministrationNicks() {
        return administrationNicks;
    }

    public static List<Integer> getAdministration() {
        long now = System.currentTimeMillis();
        synchronized (OnlineUtil.class) {
            if (now - lastAdministrationSortTime < TimeUtil.MINUTE_MILLIS) {
                return administration;
            } else {
                lastAdministrationSortTime = now;
            }
        }

        Collections.sort(administration, new Comparator<Integer>() {
            @Override
            public int compare(Integer a1, Integer a2) {
                boolean online1 = isOnline(a1);
                boolean online2 = isOnline(a2);
                int compare;
                if (online1 == online2) {
                    compare = a1.compareTo(a2);
                } else {
                    compare = online2 ? 1 : -1;
                }
                return compare;
            }
        });
        return administration;
    }

    private static UserEntry getEntry(int uid) {
        return times.get(uid);
    }

    public static boolean isAfk(int id) {
        UserEntry entry = getEntry(id);
        return isEntryAfk(entry);
    }

    public static boolean isOnline(int id) {
        UserEntry entry = getEntry(id);
        return isEntryOnline(entry);
    }

    private static boolean isEntryOnline(@Nullable UserEntry entry) {
        long time = entry != null ? entry.time : 0;
        return System.currentTimeMillis() - time < ONLINE_TIME;
    }

    private static boolean isEntryAfk(@Nullable UserEntry entry) {
        long time = entry != null ? entry.time : 0;
        long away = System.currentTimeMillis() - time;
        return away < ONLINE_TIME && away > AFK_TIME;
    }

    public static List<UserEntry> getOnlineUsers() {
        try {
            return ONLINE_USERS.get(true);
        } catch (ExecutionException e) {
            logger.warn("Failed getting online from cache", e);
            List<UserEntry> entries = getOnlineInternal();
            ONLINE_USERS.put(true, entries);
            return entries;
        }
    }

    public static List<UserEntry> getNoGuildOnlineUsers() {
        try {
            return NO_GUILD_ONLINE_USERS.get(true);
        } catch (ExecutionException e) {
            logger.warn("Failed getting online from cache", e);
            List<UserEntry> entries = getNoGuildOnlineInternal();
            NO_GUILD_ONLINE_USERS.put(true, entries);
            return entries;
        }
    }

    public static class UserEntry {
        public final int uid;
        public final long time;
        public final int level;
        public final long experience;
        public final boolean inGuild;
        public final boolean receiveGuildInvites;

        public UserEntry(int uid, long time, int level, long experience, boolean inGuild, boolean receiveGuildInvites) {
            this.uid = uid;
            this.time = time;
            this.level = level;
            this.experience = experience;
            this.inGuild = inGuild;
            this.receiveGuildInvites = receiveGuildInvites;
        }
    }

    private static final Comparator<UserEntry> LEVEL_COMPARATOR = new Comparator<UserEntry>() {
        @Override
        public int compare(UserEntry o1, UserEntry o2) {
            int levelDiff = o2.level - o1.level;
            return levelDiff != 0 ? levelDiff : Longs.compare(o2.experience, o1.experience);
        }
    };
}
