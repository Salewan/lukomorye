package views

/**
 * @author Sergey Lebedev (salewan@gmail.com) 15.10.2015.
 */
object Helper {

  def args: Seq[(Symbol, Any)] = Seq('_showConstraints -> false, '_showErrors -> false)

  def args(label: (Symbol, Any)) = Seq(label, '_showConstraints -> false, '_showErrors -> false)
  val x: Seq[(Symbol, Any)] = args :+ '_label -> "Email" :+ 'disabled -> ""
}
