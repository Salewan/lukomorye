name := "hayday"

version := "1.0"

sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false

resolvers += Resolver.url("Typesafe Ivy releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns)

//lazy val root = (project in file(".")).enablePlugins(PlayScala, SbtWeb)
lazy val root = (project in file(".")).enablePlugins(PlayScala, LauncherJarPlugin)

scalaVersion := "2.11.8"

val postgres = "org.postgresql" % "postgresql" % "9.4.1208"
val hibernate = "org.hibernate" % "hibernate-core" % "5.2.11.Final"
val hibernateEhCache = "org.hibernate" % "hibernate-ehcache" % "5.2.11.Final"
val hibC3P0 = "org.hibernate" % "hibernate-c3p0" % "5.2.11.Final"
val enumeratum = "com.beachape" %% "enumeratum-play" % "1.5.10"
val janino = "org.codehaus.janino" % "janino" % "3.0.7"
val javaxMail = "javax.mail" % "mail" % "1.4"

libraryDependencies ++= Seq(
  cache,
  ws,
  specs2 % Test,
  postgres, hibernate, hibernateEhCache, hibC3P0, enumeratum, janino,
  "com.google.code.findbugs" % "jsr305" % "3.0.1", javaxMail
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

scalacOptions += "-feature"
