function getRealDisplay(elem) {
    if (elem.currentStyle) {
        return elem.currentStyle.display;
    } else if (window.getComputedStyle) {
        var computedStyle = window.getComputedStyle(elem, null );
        return computedStyle.getPropertyValue('display');
    }
}
function hide(id) {
    var el = document.getElementById(id);
    if (!el.getAttribute('displayOld')) {
        el.setAttribute("displayOld", el.style.display);
    }
    el.style.display = "none";
    //setCookie(id, false, {expires: 0, path: '/'});
}

displayCache = {};
function isHidden(id) {
    var el = document.getElementById(id);
    var width = el.offsetWidth, height = el.offsetHeight,
        tr = el.nodeName.toLowerCase() === "tr";

    return width === 0 && height === 0 && !tr ?
        true : width > 0 && height > 0 && !tr ? false :	getRealDisplay(el);
}
function toggle(id) {
    isHidden(id) ? show(id) : hide(id);
}
function show(id) {
    var el = document.getElementById(id);
    if (getRealDisplay(el) != 'none') return;
    var old = el.getAttribute("displayOld");
    el.style.display = old || "";
    if ( getRealDisplay(el) === "none" ) {
        var nodeName = el.nodeName, body = document.body, display;
        if ( displayCache[nodeName] ) {
            display = displayCache[nodeName];
        } else {
            var testElem = document.createElement(nodeName);
            body.appendChild(testElem);
            display = getRealDisplay(testElem);
            if (display === "none" ) {
                display = "block";
            }
            body.removeChild(testElem);
            displayCache[nodeName] = display;
        }
        el.setAttribute('displayOld', display);
        el.style.display = display;
    }
    //setCookie(id, true, {expires: 0, path: '/'})
}
function getCookie(name) {
    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, props) {
    props = props || {} ;
    var exp = props.expires;
    if (typeof exp == "number" && exp) {
        var d = new Date();
        d.setTime(d.getTime() + exp*1000);
        exp = props.expires = d;
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString(); }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for(var propName in props){
        updatedCookie += "; " + propName;
        var propValue = props[propName];
        if(propValue !== true){ updatedCookie += "=" + propValue; }
    }
    document.cookie = updatedCookie;
}
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 });
}
function bindReady(handler) {
    var called = false;
    function ready() {
        if (called) return;
        called = true;
        handler();
    }
    if ( document.addEventListener ) {
        document.addEventListener( "DOMContentLoaded", function() {
            ready();
        }, false );
    } else if ( document.attachEvent ) {
        if ( document.documentElement.doScroll && window == window.top ) {
            function tryScroll() {
                if (called) return;
                if (!document.body) return;
                try {
                    document.documentElement.doScroll("left");
                    ready();
                } catch(e) {
                    setTimeout(tryScroll, 0);
                }
            }
            tryScroll();
        }
        document.attachEvent("onreadystatechange", function() {
            if ( document.readyState === "complete" ) {
                ready();
            }
        });
    }
    if (window.addEventListener)
        window.addEventListener('load', ready, false);
    else if (window.attachEvent)
        window.attachEvent('onload', ready);
}

readyList = [];
function onReady(handler) {
    if (!readyList.length) {
        bindReady(function() {
            for(var i=0; i<readyList.length; i++) {
                readyList[i]();
            }
        })
    }
    readyList.push(handler);
}
