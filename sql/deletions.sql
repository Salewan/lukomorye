select count(id) from hd_mission_slots where mission in (select id from hd_mission where closed = true);
select count(id) from hd_mission where closed = true;
delete from hd_mission_slots where id in (select id from hd_mission_slots where mission in (select id from hd_mission where closed = true));
delete from hd_mission where id in (select id from hd_mission where closed = true);

select count(id) from hd_hero h where mission is not null and not exists (select id from hd_mission where id = h.mission);
update hd_hero set mission = null where id in (select id from hd_hero h where mission is not null and not exists (select id from hd_mission where id = h.mission));

select count(id) from hd_task where completiontime is not null;
select count(id) from hd_task_slots where task in (select id from hd_task where completiontime is not null);
delete from hd_task_slots where id in (select id from hd_task_slots where task in (select id from hd_task where completiontime is not null));
delete from hd_task where completiontime is not null;