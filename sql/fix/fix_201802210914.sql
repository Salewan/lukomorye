update hd_hero set mission = null where id in (
  select id from hd_hero h where mission is not null and not exists (select id from hd_mission m where h.mission = m.id)
);