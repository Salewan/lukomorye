CREATE INDEX hd_mission_owner ON hd_mission(owner);
CREATE INDEX hd_task_owner ON hd_task(owner);

CREATE INDEX hd_clan_exp_idx ON hd_clan_user_experience(club);
CREATE INDEX hd_offer_candidate_idx ON hd_offer(candidate);
CREATE INDEX hd_offer_clan_idx ON hd_offer(clan);
CREATE INDEX hd_gameuser_clan_idx ON hd_gameuser(clanid);
CREATE INDEX hd_clan_reputation_idx ON hd_clan_reputation(club);
CREATE INDEX hd_mission_skills_idx ON hd_mission_skills(missionid);
CREATE INDEX hd_mission_slots_idx ON hd_mission_slots(mission);
CREATE INDEX hd_production_owner_idx ON hd_production(owner);