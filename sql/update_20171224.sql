ALTER TABLE hd_reputation ADD COLUMN repforlvl bigint;
ALTER TABLE hd_reputation ADD COLUMN repforrating bigint;
update hd_reputation set repforrating = value;
update hd_reputation set repforlvl = least(value, 1000000);
