CREATE TABLE hd_obtained_events(owner integer,typeid integer NOT NULL) WITH (OIDS=FALSE);
ALTER TABLE hd_obtained_events OWNER TO hayday;
insert into hd_obtained_events select owner, gentype from hd_task where gentype in (6,13,17,122,123,124,125,126,127,128,129,130,131,132,251,252,253,254,256,257,258,259,260,261,262,360,361,362,363,364,365,366,367,368,4401,4402,4403,4404,4405,4406,4407,4408,4409,4410,4411,4412,4413) group by owner, gentype;
CREATE UNIQUE INDEX obtained_events_unique on hd_obtained_events(owner, typeid);